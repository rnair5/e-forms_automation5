package libraries;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;

public class Utilities {
    /*This is a builder class and returns the element back so that we can continue to call other functions on top of it*/
    static Long defaultTimeout = Long.valueOf(30);

    /*Function to refresh the browser*/
    public static void refreshPage(WebDriver driver) {
        Logg.logger.info("");
        driver.navigate().refresh();
        waitForPageLoaded(driver);
    }

    /*Function to wait until element is clickable and then click it*/
    public static WebElement waitAndClick(WebDriver driver, WebElement element) {
        Logg.logger.info("element:" + element);
        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.elementToBeClickable(element));
            scrollToElement(driver, element);
            extent_Pass(driver, "Scrolled on element successfully", element.toString(), "", "");
            element.click();
            extent_Pass(driver, "Clicked on element successfully", element.toString(), "", "");
            return element;
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitAndClick: " + e.toString());
            extent_Warn(driver, "Failed to click on element", element.toString(), "", "");
            throw e;
        }

    }

    /*Function to wait until element located by the By locator is clickable and then click it*/
    public static WebElement waitAndClick(WebDriver driver, By by) {
        Logg.logger.info("By:" + by);
        try {
            WebElement element = waitUntilElementIsVisible(driver, by);
            extent_Pass(driver, "Found the element successfully", by.toString(), "", "");
            return waitAndClick(driver, element);
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitAndClick: " + e.toString());
            extent_Warn(driver, "Failed to click on element", by.toString(), "", "");
            throw e;
        }

    }

    /*Function to wait until all the elements in the list are visible*/
    public static List<WebElement> waitUntilElementIsVisible(WebDriver driver, List<WebElement> element, int timeout) {
        Logg.logger.info("element:" + element);
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.visibilityOfAllElements(element));
            scrollToElement(driver, element.get(0));
            return element;
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitUntilElementIsVisible: " + e.toString());
            extent_Warn(driver, "Could not find the elements", element.toString(), "", e.getMessage());
            throw e;
        }
    }

    /*Function to wait until all elements located by the By locator are present in DOM*/
    public static List<WebElement> waitUntilElementsArePresent(WebDriver driver, By by, int timeout) {
        Logg.logger.info("By:" + by);
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
            List<WebElement> elements = driver.findElements(by);
            scrollToElement(driver, elements.get(0));
            return elements;
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitUntilElementsArePresent: " + e.toString());
            extent_Warn(driver, "Could not find the elements", by.toString(), "", e.getMessage());
            throw e;
        }
    }

    /*Function to wait until the element located by the xpath is clickable and then click it*/
    public static WebElement waitAndClick(WebDriver driver, String xpath) {
        Logg.logger.info("xpath:" + xpath);
        try {
            WebElement element = waitUntilElementIsVisible(driver, xpath);
            return waitAndClick(driver, element);
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitUntilElementIsVisible: ");
            extent_Warn(driver, "Cannot click element with xpath", xpath, "", e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    /*Function to wait until the element located by the xpath is visible*/
    public static WebElement waitUntilElementIsVisible(WebDriver driver, String xpath) {
        Logg.logger.info("xpath:" + xpath + "timeout:" + defaultTimeout);
        try {
            FluentWait<WebDriver> wait = new WebDriverWait(driver, defaultTimeout);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
            return scrollToElement(driver, driver.findElement(By.xpath(xpath)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitUntilElementIsVisible: " + e.toString());
            extent_Warn(driver, "Could not find the element", xpath, "", e.getMessage());
            throw e;
        }
    }

    /*Function to wait until the element located by the By locator is visible*/
    public static WebElement waitUntilElementIsVisible(WebDriver driver, By by) {
        Logg.logger.info("By:" + by);
        try {
            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.visibilityOfElementLocated(by));

            return scrollToElement(driver, driver.findElement(by));
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitUntilElementIsVisible: ");
            extent_Warn(driver, "Could not find the element", by.toString(), "", e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    /*Function to wait until the element is visible*/
    public static WebElement waitUntilElementIsVisible(WebDriver driver, WebElement element) {
        Logg.logger.info("Element:" + element);
        try {
            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.visibilityOf(element));
            return scrollToElement(driver, element);
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitUntilElementIsVisible: ");
            extent_Warn(driver, "Could not find the element", element.toString(), "", e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    /*Function to wait until the element is not visible*/
    public static void waitUntilElementIsNotVisible(WebDriver driver, WebElement element) {
        Logg.logger.info("Element:" + element);
        try {
            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
            wait
                    .ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.invisibilityOf(element));
        } catch (Exception e) {
        }
    }

    /*Function to wait until all the elements in the list are visible*/
    public static List<WebElement> waitUntilElementIsVisible(WebDriver driver, List<WebElement> element) {
        Logg.logger.info("Element list:" + element);
        try {
            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
            wait.ignoring(NoSuchElementException.class)
                    .until(ExpectedConditions.visibilityOfAllElements(element));
            scrollToElement(driver, element.get(0));
            return element;
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitUntilElementIsVisible: ");
            extent_Warn(driver, "Could not find the element", element.toString(), "", e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    /*Function to wait until all the elements located by the By locator are visible*/
    public static List<WebElement> waitUntilElementsAreVisible(WebDriver driver, By by) {
        Logg.logger.info("By:" + by);
        try {
            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
            wait.ignoring(NoSuchElementException.class);
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            List<WebElement> elements = driver.findElements(by);
            scrollToElement(driver, elements.get(0));
            return elements;
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitUntilElementIsVisible: ");
            extent_Warn(driver, "Could not find the element", by.toString(), "", e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    /*Function to scroll to the element. This is usually called so that it can be in the viewport for the screenshot*/
    public static WebElement scrollToElement(WebDriver driver, WebElement element) {
        Logg.logger.info("element:" + element);
        try {
            ((JavascriptExecutor) driver).executeScript(
                    "arguments[0].scrollIntoView(true); ", element);
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("scroll(10, 0)");
            return element;
        } catch (Exception e) {
            Logg.logger.info("Exception occured in ScrollToElement:" + e.toString());
            extent_Warn(driver, "Could not scroll to the element", element.toString(), "", e.getMessage());
            throw e;
        }
    }

    /*Function returns true if element is displayed and false if it is not displayed*/
    public static boolean isElementDisplayed(WebDriver driver, By by) {
        Logg.logger.info("by:" + by);
        return driver.findElement(by).isDisplayed();
    }

    /*Function returns true if element located by the By locator is present in DOM and false if it is not present in DOM*/
    public static boolean isElementPresent(WebDriver driver, By by) {
        Logg.logger.info("by:" + by);
        try {
            driver.findElement(by);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*Function to create a WebDriver and return it based on Browser name*/
    public static WebDriver createDriver(String browserName) {
        Logg.logger.info("browserName:" + browserName);
        WebDriver driver = null;
        try {
            switch (browserName.toLowerCase()) {
                case "ie":
                    System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "/src/test/resources/IEDriverServer.exe");
                    InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
                    internetExplorerOptions.ignoreZoomSettings();
                    //internetExplorerOptions.destructivelyEnsureCleanSession();
                    internetExplorerOptions.enablePersistentHovering();
                    internetExplorerOptions.disableNativeEvents();
                    internetExplorerOptions.setCapability("locationContextEnabled", "true");
                    driver = new InternetExplorerDriver(internetExplorerOptions);
                    break;

                case "chrome":


                    Map<String, Object> prefs = new HashMap<String, Object>();
                    prefs.put("credentials_enable_service", false);
                    prefs.put("password_manager_enabled", false);

                    ChromeOptions options = new ChromeOptions();
                    options.setExperimentalOption("prefs", prefs);
                    options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
//                    options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
//
                    options.addArguments("--no-sandbox");
                    options.addArguments("--headless");

                    options.addArguments("start-maximized");
//                    options.addArguments("--disable-popup-blocking");
                    options.addArguments("window-size=1920,1080");
//                    options.addArguments("--start-maximized");
//                    Dimension d = new Dimension(1680,939);
//                    driver.manage().window().setSize(d);
////                    options.addArguments("--headless", "--disable-gpu", "--window-size=1680,1050");
////                    options.addArguments("window-size=1920,1080");
////                    driver.manage().window().maximize();
////                    options.addArguments("start-maximized");
////                    options.addArguments("--headless", "--disable-gpu", "--window-size=1680,1050");
                    System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");

//                    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/chromedriver.exe");
//                    System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Jenkins\\workspace\\E-Forms_Automation\\src\\test\\resources\\chromedriver.exe");
//                    options.addArguments("--headless", "--disable-gpu", "--window-size=1680,1050");
                    driver = new ChromeDriver(options);
//                    driver.get("chrome://settings/clearBrowserData");
                    break;



                case "firefox":

                    System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/test/resources/geckodriver.exe");
                    driver = new FirefoxDriver();

                    break;
                case "edge":
                    System.setProperty("webdriver.edge.driver", "C:\\Selenium\\msedgedriver.exe");
                    driver = new EdgeDriver();

                    break;
            }
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
            return driver;
        } catch (Exception e) {
            Logg.logger.info("Failed to create webdriver:" + browserName);
            throw e;
        }
    }

    /**
     * verify then page is loaded successfully
     */
    public static void waitForPageLoaded(WebDriver driver) {
        Logg.logger.info("");
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        WebDriverWait wait = new WebDriverWait(driver, 30);
        try {
            wait.until(expectation);
            extent_Pass(driver, "The page is loaded successfully ", driver.getTitle(), "", "");
        } catch (Throwable error) {
            extent_Warn(driver, "Timeout waiting for Page Load Request to complete.", driver.getTitle(), "", "");
        }
    }



    /*Function to wait until the element is visible and then enter the given text to the element*/
    public static WebElement waitAndSendKeys(WebDriver driver, WebElement element, String text) {
        Logg.logger.info(String.format("element:%s, text:%s", element, text));
        try {
            waitUntilElementIsVisible(driver, element);
            element.clear();
            element.sendKeys(text);
            extent_Pass(driver, "Entered the text", text, " to element:" + element, "with value" + text);
            return element;
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitAndSendKeys: " + e.getMessage());
            extent_Warn(driver, "Failed to enter text", text, " on " + element, "");
            e.printStackTrace();
            throw e;
        }
    }

    /*Function to wait until the element located by the xpath is visible and then send the given text to the element*/
    public static WebElement waitAndSendKeys(WebDriver driver, String xpath, String text) {
        Logg.logger.info(String.format("xpath:%s, text:%s", xpath, text));
        try {
            WebElement element = waitUntilElementIsVisible(driver, xpath);
            element.clear();
            element.sendKeys(text);
            extent_Pass(driver, "Entered the text", text, " to xpath:" + xpath, "");
            return element;
        } catch (Exception e) {
            Logg.logger.info("Exception occured in waitAndSendKeys: " + e.getMessage());
            extent_Warn(driver, "Failed to enter text", text, " on " + xpath, "");
            e.printStackTrace();
            throw e;
        }
    }

    /*Function to take screenshot*/
    public static String takeScreenshot(WebDriver driver) {
        Logg.logger.info("");
        try {
            String fileName = new SimpleDateFormat("yyyyMMddmmssSSSZ").format(new Date());
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String path = System.getProperty("user.dir") + "/test-output/Screenshots/" + fileName + ".jpeg";
            FileUtils.copyFile(screenshot, new File(path));
            return "Screenshots/" + fileName + ".jpeg";
        } catch (Exception e) {
            Logg.logger.info("Failed to take screenshot : " + e.getMessage());
            extent_Warn(driver, "Failed to take screenshot", "", "", "");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Function to select a drop down option by Visible Text
     *
     * @param dropDownBox
     * @param value
     * @param driver
     */
    public static void selectDropdown_VisibleText(WebDriver driver, WebElement dropDownBox, String value) {
        Logg.logger.info(String.format("dropDown:%s, value:%s", dropDownBox, value));
        try {
            Select se1ect1 = new Select(dropDownBox);
            //se1ect1.deselectAll();
            se1ect1.selectByVisibleText(value);
            extent_Pass(driver, "The element is found " + dropDownBox.toString() + " and selected the value in the drop down", value, "", "");
        } catch (Exception e) {
            extent_Warn(driver, "The element is not found ", value, "", "");
            throw e;
        }
    }

    /**
     * Function to select drop down option By index
     *
     * @param dropDownBox
     * @param index
     * @param driver
     */
    public static void selectDropdown_Index(WebDriver driver, WebElement dropDownBox, int index) {
        Logg.logger.info(String.format("dropDown:%s, index:%d", dropDownBox, index));
        try {
            Select se1ect1 = new Select(dropDownBox);
            //se1ect1.deselectAll();
            se1ect1.selectByIndex(index);
            extent_Pass(driver, "The element is found " + dropDownBox.toString() + " and selected the value in the drop down", Integer.toString(index), "", "");
        } catch (Exception e) {
            extent_Warn(driver, "The element is not found ", Integer.toString(index), "", "");
            throw e;
        }
    }

    /**
     * Function to load url with parameter
     *
     * @param url
     * @throws InterruptedException
     */
    public static void loadurl(WebDriver driver, String url) throws InterruptedException {
        Logg.logger.info("url:" + url);
//        driver.get(url);
        driver.navigate().to(url);
        waitForPageLoaded(driver);
    }

    public static void switchToFrame(WebDriver driver, WebElement element) {
        Logg.logger.info("element:" + element);
        try {
            waitUntilElementIsVisible(driver, element);
            driver.switchTo().frame(element);
            extent_Pass(driver, "Switched to frame:", element.toString(), "", "");
            waitForPageLoaded(driver);
        } catch (Exception e) {
            extent_Warn(driver, "Unable to switch to frame", element.toString(), "", "");
            throw e;
        }
    }

    /*Function to get the URL of the currently displayed page on the browser*/
    public static String getCurrentUrl(WebDriver driver) {
        Logg.logger.info("");
        try {
            return driver.getCurrentUrl();
        } catch (Exception e) {
            extent_Warn(driver, "Error getting current URL", "", "", "");
            throw e;
        }
    }

    /*Function to get the Title of the currently displayed page on the browser*/
    public static String getCurrentTitle(WebDriver driver) {
        Logg.logger.info("");
        return driver.getTitle();
    }


    /*Function to perform the mouse over action on the element located by the By locator*/
    public static void hover_On_Element(WebDriver driver, By by) {
        Logg.logger.info(String.format("By:%s", by.toString()));
        try {
            WebElement element = waitUntilElementIsVisible(driver, by);
            Actions actions = new Actions(driver);
            actions.moveToElement(element).build().perform();
            extent_Pass(driver, "Hovered the element ", by.toString(), "", "");

        } catch (Exception e) {
            extent_Warn(driver, "The element is not found for hovering ", by.toString(), "", "");
            throw e;
        }
    }

    /*Function to switch to a window that contains the given URL.This function checks all the open window handles and switches to the window with the given url*/
    public static void switchToWindowWithUrl(WebDriver driver, String url) {
        Logg.logger.info("url:" + url);
        try {
            ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
            String currentWindow = driver.getWindowHandle();

            for (String winHandle : tabs) {
                if (driver.switchTo().window(winHandle).getCurrentUrl().contains(url)) {
                    extent_Pass(driver, "Switched to window ", url, url, driver.getCurrentUrl());
                    break;
                } else {
                    driver.switchTo().window(currentWindow);
                }
            }
        } catch (Exception e) {
            extent_Warn(driver, "Unable to switch to window ", url, "", "");
            throw e;
        }
    }

    /*Function returns the total number of windows open the browser*/
    public static int getNoOfWindowsOpen(WebDriver driver) {
        Logg.logger.info("");
        return driver.getWindowHandles().size();
    }

    /*Function to perform the navigate to the previous page on the browser*/
    public static void navigateBack(WebDriver driver) {
        Logg.logger.info("");
        driver.navigate().back();
        waitForPageLoaded(driver);
    }

    /*This is a customized function. It selects the first item in the AJAX listing that contains the given text and has a (PUBLISHED) text written net to it. It is used in the website testing.*/
    public static void selectFirstPublishedMenuItem(WebDriver driver, String text) {
        Logg.logger.info("text:" + text);
        WebElement element = waitUntilElementIsVisible(driver, By.xpath("//li[@class='ui-menu-item']//a[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'" + text.toLowerCase() + "')][contains(text(),'(PUBLISHED)')]"));
        waitAndClick(driver, element);
    }

    /*This is a customized function. It selects the first item in the AJAX listing that contains the given text. It is used in the website testing.*/
    public static void selectMenuItem(WebDriver driver, String text, int index) {
        Logg.logger.info(String.format("Text entered:%s, index:%s", text, index));
        waitAndClick(driver, By.xpath("//li[@class='ui-menu-item']//span[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'" + text.toLowerCase() + "')]"));
    }

    /*Function to tick a checkbox if it is not already checked*/
    public static void checkCheckbox(WebDriver driver, WebElement checkbox) {
        Logg.logger.info(String.format("Checkbox to be checked:%s", checkbox));
        if (!checkbox.isSelected())
            waitAndClick(driver, checkbox);
    }

    /*Function to uncheck a checkbox if it is checked*/
    public static void uncheckCheckbox(WebDriver driver, WebElement checkbox) {
        Logg.logger.info(String.format("Checkbox to be unchecked:%s", checkbox));
        if (checkbox.isSelected())
            waitAndClick(driver, checkbox);
    }

    /*Function to select a date from the date picker*/
    public static void selectDateFromPicker(WebDriver driver, WebElement element, String date, String month, String year) {
        Logg.logger.info(String.format("Date picker:%s\n Date:%s, Month:%s, Year:%s", element, date, month, year));
        waitAndClick(driver, element);
        selectDropdown_VisibleText(driver, driver.findElement(By.className("ui-datepicker-year")), year);
//        driver.findElement(By.className("ui-datepicker-month")).click();
        // displaying three-letter month

        selectDropdown_VisibleText(driver, driver.findElement(By.className("ui-datepicker-month")), month);

        waitAndClick(driver, "//a[@class='ui-state-default'][text()='" + date + "']");
    }

    /*This is a customised function for the Forms testing. It is used to enter the given text in the lookup field and select the first item in the AJAX list*/
    public static void selectFirstItemFromLookup(WebDriver driver, WebElement lookup, String text) {
        Logg.logger.info(String.format("Lookup element:%s, Lookup text:%s", lookup, text));
        waitAndSendKeys(driver, lookup, text);
        waitAndClick(driver, waitUntilElementIsVisible(driver, By.xpath("//ul[contains(@class,'ui-autocomplete')]//li")));
    }

    /*Function to click on the upload button and set the given the file into upload field*/
    public static void uploadFile(WebElement uploadButton, String file) throws AWTException, InterruptedException {
        Logg.logger.info("File:" + file);
        try {
            uploadButton.click();

            Thread.sleep(2000);
            //Alert alert = driver.switchTo().alert();
            StringSelection ss = new StringSelection(file);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(2000);
        } catch (UnhandledAlertException e) {
            throw e;
        }
    }
}
