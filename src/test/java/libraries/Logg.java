package libraries;
import org.apache.log4j.*;

import java.time.LocalDateTime;
import java.util.Date;

public class Logg {
    public static PatternLayout layout;
    public static ConsoleAppender consoleAppender;
    public static FileAppender fileAppender = null;
    public static Logger rootLogger;
    public static Logger logger;
    /*Function to configure the log4j report
    * filename - log.txt
    * threshold - set to info
    * */
    public static void configure() {
        if(fileAppender == null) {
            layout = new PatternLayout();
            //String conversionPattern = "%-7p %d [%t] %c %x - %m%n";
            String conversionPattern = "%d{dd MMM yyyy HH:mm:ss} %C{1}.%M:%L -- %m%n";
            layout.setConversionPattern(conversionPattern);
            // creates console appender
            consoleAppender = new ConsoleAppender();
            consoleAppender.setLayout(layout);
            consoleAppender.activateOptions();
            consoleAppender.setThreshold(Priority.INFO);
            BasicConfigurator.configure(consoleAppender);

            // creates file appender
            fileAppender = new FileAppender();
            fileAppender.setFile("log"+ LocalDateTime.now().toString().replaceAll("(\\s*)\\.(\\s*)","$1-$2").replace(':','-')+".txt");
            fileAppender.setLayout(layout);
            fileAppender.setThreshold(Priority.INFO);
            fileAppender.activateOptions();
            BasicConfigurator.configure(fileAppender);

            // creates a custom logger and log messages
            logger = Logger.getLogger(Logg.class);
            logger.debug("this is a debug log message");
            logger.info("this is a information log message");
            logger.warn("this is a warning log message");
        }
    }
}
