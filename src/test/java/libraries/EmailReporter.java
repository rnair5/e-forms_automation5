package libraries;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import static libraries.Utilities.selectDropdown_Index;
import static libraries.Utilities.selectDropdown_VisibleText;

public class EmailReporter {
    public static WebDriver driver=null;
    /*This function creates a snapshot from the ExtentReports.html in the test_ouput folder to a file called emailreport.html in the test_output folder which is sent out as email once execution is complete from Jenkins*/
    public static void createEmailReport(String reportPath) {
        try {
            Logg.logger.info("reportPath:"+reportPath);
            String destinationPath = reportPath.substring(0,reportPath.lastIndexOf('\\'));
            if (!new File(reportPath).exists()) {
                throw new Exception("Report not found at " + reportPath);
            }
            if (!new File(destinationPath).exists()) {
                throw new Exception("Destination folder not found at " + destinationPath);
            }
            System.out.println("Report Path - " + reportPath);
            System.out.println("Destination Folder - " + destinationPath);
            System.out.println("Creating Headless Chromedriver Instance...");
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("window-size=1200,1100");
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
            driver= new ChromeDriver(chromeOptions);
            driver.get("file:///" + reportPath);                                                                       //append htmlfilelocation
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            System.out.println("Navigating to Dashboard...");
            driver.findElement(By.xpath("//a[@class='dashboard-view']")).click();                                 //clicking on dashboard of html page
            System.out.println("Getting snapshot of Report sections...");
            WebElement elementDashboard = driver.findElement(By.id("dashboard-view"));
            takeSnapShot(driver,destinationPath+"\\dashboard.png",elementDashboard);
            System.out.println("Creating HTML file...");
//            File resultFile = new File(destinationPath + "\\emailreport.html");
//            PrintWriter writer = new PrintWriter(resultFile);
            //Trial From Here - To be Deleted
            System.out.println("Navigating to Failed View...");
            driver.findElement(By.xpath("//a[@class='test-view']")).click();                                 //clicking on dashboard of html page
            System.out.println("Getting snapshot of Failed sections...");
            WebElement statusDropdown= driver.findElement(By.xpath("//*[@id=\"test-view\"]/div[1]/div/div[2]/div[1]/a/i"));
            statusDropdown.click();
//            new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[@class='bta-description' and text()='Our calculators']//following::div[@class='bta-select-table row']//b[@class='button']"))).click();
            new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"tests-toggle\"]/li[2]/a"))).click();
//            WebElement statusDropdownFail= driver.findElement(By.xpath("//*[@id=\"test-view\"]/div[1]/div/div[1]"));
            WebElement statusDropdownFail= driver.findElement(By.xpath("//*[@id=\"test-view\"]/div[1]/div/div[1]"));
            statusDropdownFail.click();
            WebElement statusDropdownWarning= driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div/div/div[1]/span[6]"));
            statusDropdownWarning.click();
//            System.out.println("The payment details are " + statusDropdownFail1+ "");

//            statusDropdown.isSelected();


//            selectDropdown_Index(driver, statusDropdown,2);
            Thread.sleep(5000);
//            WebElement elementDashboard3 = driver.findElement(By.xpath("//*[@id=\"test-view\"]/div[1]/div"));
            WebElement elementDashboard3 = driver.findElement(By.xpath("//*[@id=\"test-view\"]"));
            takeSnapShot(driver,destinationPath+"\\Detailed.png",elementDashboard3);
//            System.out.println("Getting snapshot of Warning sections...");
//            WebElement elementDashboard4 = driver.findElement(By.xpath("//div[@id='test-details-wrapper']/div/div/div/span[6]/i"));
//            elementDashboard1.click();
//            Thread.sleep(5000);
//            WebElement elementDashboard5 = driver.findElement(By.id("test-view"));
//            takeSnapShot(driver,destinationPath+"\\WarningAlert.png",elementDashboard5);
//            System.out.println("Creating HTML file...");
            System.out.println("Creating HTML file...");
            File resultFile = new File(destinationPath + "\\emailreport.html");
            PrintWriter writer = new PrintWriter(resultFile);
//            File resultFile1 = new File(destinationPath + "\\emailreport.html");
//            PrintWriter writer1 = new PrintWriter(resultFile1);
            //My script for Sure fire report screenshot begins here

            System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
            driver= new ChromeDriver(chromeOptions);
            driver.get("C:\\Users\\rnair\\e-forms_endtoend/target/surefire-reports/emailable-report.html");
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(destinationPath+"\\IndexEmail.png"));
            //Ends Before this Line
            writer.write("<html>\n");
            writer.append("<body>\n");
            writer.append("<table ><tr> <td style=\"display:block; border:solid 2px #000000; \"><img src=\"dashboard.png\"/></td></tr></table>\n");
            writer.append("<table ><tr> <td style=\"display:block; border:solid 2px #000000; \"><img src=\"Detailed.png\"/></td></tr></table>\n");
            writer.append("<table ><tr> <td style=\"display:block; border:solid 2px #000000; \"><img src=\"IndexEmail.png\"/></td></tr></table>\n");
            writer.append("</body>\n");
            writer.append("</html>\n");
            writer.close();
            System.out.println("Email Report Generation Complete!");
        }
        catch (NoSuchElementException e)
        {
            System.out.println("Error with XPath. Please check the XPath values");
            e.printStackTrace();
        }
        catch (Exception e) {
            System.out.println("Error Message = " + e.getMessage());
            e.printStackTrace();
        }finally{
            driver.close();
        }
    }
    /*Creates a snapshot to be appended in the emailreport.html file. Snapshot is taken from the ExtentReportResults.html in the test_output folder*/
    public static void takeSnapShot(WebDriver webdriver,String destinationPath,WebElement webElement) {
        //Logg.logger.info("");
        try {
            TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
            File srcFile = scrShot.getScreenshotAs(OutputType.FILE);                                      //storing screenshot in srcfile
            BufferedImage fullImg = ImageIO.read(srcFile);
            Point point = webElement.getLocation();                                                      // getting upper left corner of particular section
            int width = webElement.getSize().getWidth();                                                 //find width from that corner
            int height = webElement.getSize().getHeight();                                              // find height from that corner
            BufferedImage ElementScreenshot = fullImg.getSubimage(point.getX(), point.getY(), width, height);//getting subimage from complete webpage
            ImageIO.write(ElementScreenshot, "png", srcFile);
            File DestFile = new File(destinationPath);                                                       //create the png file in particular folder
            FileUtils.copyFile(srcFile, DestFile);
        }
        catch (WebDriverException e) {
            System.out.println("webdriver Exception");
        }
        catch (IOException e){
            System.out.println("InputOutput Exception");
        }
    }
}
