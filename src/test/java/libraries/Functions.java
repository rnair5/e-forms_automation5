package libraries;

import Tests.BC1.BaseClass;
import com.relevantcodes.extentreports.ExtentReports;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static libraries.ExtentReporting.*;

/**
 * Created by nshagin on 3/02/2017.
 * Functions: This class is a function library, contains functions for all the generic operations on a web application
 */
public class Functions extends BaseClass {

    private static StringBuffer verificationErrors = new StringBuffer();

    /**
     * Function to Close the driver
     *
     * @throws Exception
     */
    public static void tearDownDriver() throws Exception {
        Logg.logger.info("");
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            Assert.fail(verificationErrorString);
        }
    }


    //*************************************//
    //AutoIT Functions
    //************************************//


    /**
     * Function to load url with Auto IT script which has windows security pop up
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public static void loadurl_script() throws IOException, InterruptedException {
        Logg.logger.info("");
        String baseurl = BaseClass.url;
        Logg.logger.info("The Base URL is : " + baseurl);
        driver.get(baseurl);
         if ("ie".equalsIgnoreCase(BaseClass.WebBrowserName)) {
            String path = System.getProperty("user.dir") + "\\src\\test\\AutoIt\\IE_WindowSecurity.exe";
            Runtime.getRuntime().exec(path);
            //Waiting for the AutoIT script to open and execute. It sometimes takes time.
            Functions.wait(30000);
        }
        if ("chrome".equalsIgnoreCase(BaseClass.WebBrowserName)) {
            String path = System.getProperty("user.dir") + "\\src\\test\\AutoIt\\Chrome_Security.exe";
            Runtime.getRuntime().exec(path);
            //Waiting for the AutoIT script to open and execute. It sometimes takes time.
            Functions.wait(30000);
        }
    }


    /**
     * Function to upload a feature image with Auto IT script
     *
     * @throws IOException
     * @throws InterruptedException
     *
     */
    public static void imageFeatureUpload() throws IOException, InterruptedException {
        Logg.logger.info("");
        Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\test\\AutoIt\\FileUpload_Feature.exe");
    }


    /**
     * FFunction to upload a teaser image with Auto IT script
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public static void imageTeaserUpload() throws IOException, InterruptedException {
        Logg.logger.info("");
        Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\test\\AutoIt\\FileUpload_Teaser.exe");
    }


    //*************************************//
    //AutoIT Functions End
    //************************************//


    /**
     * Function to load url_old without windows security pop up
     *
     * @throws InterruptedException
     */
    public static void loadurl() throws InterruptedException {
        Logg.logger.info("");
        //Functions.wait(2000);
        String baseurl = BaseClass.url;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.navigate().to(baseurl);
        Functions.waitForPageLoaded();
    }


    /**
     * Function to load url with parameter
     *
     * @param url
     * @throws InterruptedException
     */
    public static void loadurl(String url) throws InterruptedException {
        Logg.logger.info("");
        driver.navigate().to(url);
    }


    /**
     * Function to capture the desktop browser screenshot and save it under "Screenshots" folder "C:/workspace/BC1/Screenshots"
     */
    public static void screenshot() {
        Logg.logger.info("");
        try {
            java.text.SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMDD'T'HHMMSSsss");
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot, new File(System.getProperty("user.dir") + "/Screenshots/" + sdf.format(new java.util.Date()) + ".jpeg"));
        } catch (Exception e) {
            Logg.logger.info("Failed to take screenshot : " + e);
        }
    }
    /**
     * Function to call wait
     * @param time
     * @throws InterruptedException
     */
    public static void wait(int time) throws InterruptedException {
        Logg.logger.info("time:"+time);
        Thread.sleep(time);
    }


    /**
     * Function to enter the variable string into the 'xpath' input field
     * @param ElementXpath
     * @param variable
     * @param ObjName
     * @throws InterruptedException
     */
    public static void Sendkey_xpath(String ElementXpath, String variable, String ObjName) throws InterruptedException {
        Logg.logger.info("ElementXPath:"+ElementXpath+" variable:"+variable+"ObjName:"+ObjName);
        try {
            //Thread.sleep(2000);
            WebElement element= waitUntilElementIsVisible(ElementXpath);
            element.clear();element.sendKeys(variable);
            extent_Pass(driver,"The Data Entered for the textbox ", ObjName, "", variable);
        } catch (Exception e) {
            Logg.logger.info("Exception occured in Sendkey_xpath:"+e.getMessage());
            e.printStackTrace();
            extent_Failure(driver,"The element is not found ", ObjName, "", variable);
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to enter the variable string into the 'id' input field
     * @param ElementId
     * @param variable
     * @param ObjName
     * @throws AWTException
     */
    public static void Sendkey_id(String ElementId, String variable, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementId: %s, variable:%s, ObjName:%s",ElementId,variable,ObjName));
        try {
            //Thread.sleep(2000);
            driver.findElement(By.id(ElementId)).sendKeys(variable);
            extent_Pass(driver,"The Data Entered for the textbox ", ObjName, "", variable);
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found ", ObjName, "", variable);
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to enter the variable string into the 'name' input field
     * @param ElementName
     * @param variable
     * @param ObjName
     * @throws AWTException
     */
    public static void Sendkey_name(String ElementName, String variable, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementName: %s, variable:%s, ObjName:%s",ElementName,variable,ObjName));
        try {
            //Thread.sleep(2000);
            WebElement element=waitUntilElementIsVisible(By.name(ElementName));
            element.sendKeys(variable);
            extent_Pass(driver,"The Data Entered for the textbox ", ObjName, "", variable);
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found ", ObjName, "", variable);
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to click on the 'xpath' button
     * @param ElementXpath
     * @param ObjName
     * @throws AWTException
     */
    public static void Click_Button_Xpath(String ElementXpath, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementXpath:%s, ObjName:%s",ElementXpath,ObjName));
        try {
            //Thread.sleep(2000);
            WebElement element= waitAndClick(ElementXpath);
            extent_Pass(driver,"The Element is found and clicked ", ObjName, "", "");
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found for clicking ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to click on the 'className' button
     * @param className
     * @param ObjName
     * @throws AWTException
     */
    public static void Click_Button_className(String className, String ObjName) throws AWTException {
        Logg.logger.info(String.format("className:%s, ObjName:%s",className,ObjName));
        try {
            //Thread.sleep(2000);
            waitAndClick(driver.findElement(By.className(className)));
            extent_Pass(driver,"The Element is found and clicked ", ObjName, "", "");

        } catch (Exception e) {
            extent_Failure(driver,"The element is not found for clicking ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to click on the 'Linktext' button
     * @param ElementLinkText
     * @param ObjName
     * @throws AWTException
     */
    public static void Object_Click_LinkText(String ElementLinkText, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementLinkText:%s, ObjName:%s",ElementLinkText,ObjName));
        try {
            //Threaad.sleep(2000);
            waitAndClick(driver.findElement(By.linkText(ElementLinkText)));
            extent_Pass(driver,"The Element is found and clicked ", ObjName, "", "");
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found for clicking ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to click on the 'PartialLinktext' button
     * @param ElementPartialLinkText
     * @param ObjName
     * @throws AWTException
     */
    public static void Object_Click_PartialLinkText(String ElementPartialLinkText, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementPartialLinkText:%s, ObjName:%s",ElementPartialLinkText,ObjName));
        try {
            //Thread.sleep(2000);
            waitAndClick(driver.findElement(By.partialLinkText(ElementPartialLinkText)));
            extent_Pass(driver,"The element is found and clicked ", ObjName, "", "");
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found for clicking ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to click on the 'ID' button
     * @param ElementId
     * @param ObjName
     * @throws AWTException
     */
    public static void Object_Click_ID(String ElementId, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementId:%s, ObjName:%s",ElementId,ObjName));
        try {
            //Functions.wait(2000);
            waitAndClick(driver.findElement(By.id(ElementId)));
            extent_Pass(driver,"The element is found and clicked ", ObjName, "", "");
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found for clicking ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to click on the checkbox using 'xpath'
     * @param ElementXpath
     * @param ObjName
     * @throws AWTException
     */
    public static void CheckBox_Click_Xpath(String ElementXpath, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementXpath:%s, ObjName:%s",ElementXpath,ObjName));
        try {
            //Functions.wait(2000);
            if (!waitUntilElementIsVisible(By.xpath(ElementXpath)).isSelected()) {
                WebElement element= driver.findElement(By.xpath(ElementXpath));
                waitAndClick(element);
                extent_Pass(driver,"The element is found and clicked ", ObjName, "", "");
            }
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found for clicking ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to Click on the field using 'ID' and clear field
     * @param ElementId
     * @param ObjName
     * @throws AWTException
     */
    public static void Object_ID_Clear(String ElementId, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementId:%s, ObjName:%s",ElementId,ObjName));
        try {
            //Thread.sleep(2000);
            waitUntilElementIsVisible(By.id(ElementId)).clear();
            extent_Pass(driver,"The element is found and field value cleared ", ObjName, "", "");
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found for clicking and Clearing field ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to validate the assertions
     * @param message
     */
    public static void Assert(String message) {
        Logg.logger.info("");Assert.assertEquals(status1, 0, message);
    }


    /**
     * Function to verify the link text is same as the desired text
     * @param ElementLinkText
     * @param ObjName
     * @throws AWTException
     */
    public static void verify_link_text(String ElementLinkText, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementLinkText:%s, ObjName:%s",ElementLinkText,ObjName));
        try {
            //Thread.sleep(2000);
            String element1 = waitUntilElementIsVisible((By.linkText(ElementLinkText))).getText();
            //waitUntilElementIsVisible(element1);
            element1 = element1.replaceAll("\\r\\n|\\r|\\n", " ");
            String element2 = ObjName;
            if (element1.equalsIgnoreCase(element2)) {
                extent_Pass(driver,"The element is found and same as the desired text ", ObjName, element2, element1);
            } else {
                extent_Failure(driver,"The element is not found or the desired text is not matching ", ObjName, element2, element1);
                screenshot();
                status1 = 1;
            }
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to verify the id element text is same as the desired text
     * @param ElementId
     * @param ObjName
     * @throws AWTException
     */
    public static void verify_id_text(String ElementId, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementId:%s, ObjName:%s",ElementId,ObjName));
        try {
            //Thread.sleep(2000);
            String element1 = waitUntilElementIsVisible(By.id(ElementId)).getText();

            String element2 = ObjName;
            element1 = element1.replaceAll("\\r\\n|\\r|\\n", " ");
            if (element1.equalsIgnoreCase(element2)) {
                extent_Pass(driver,"The element is found and same as the desired text ", "", element2, element1);
            } else {
                extent_Failure(driver,"The element is not found or the desired text is not matching ", "", element2, element1);
                screenshot();
                status1 = 1;
            }
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to verify the xpath element text is same as the desired text
     * @param ElementXpath
     * @param ObjName
     * @throws AWTException
     */
    public static void verify_xpath_text(String ElementXpath, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementXpath:%s, ObjName:%s",ElementXpath,ObjName));
        try {
            //Thread.sleep(2000);
            WebElement element =waitUntilElementIsVisible(ElementXpath);
            String element1 = element.getText();

            String element2 = ObjName;
            element1 = element1.replaceAll("\\r\\n|\\r|\\n", " ");
            if (element1.equalsIgnoreCase(element2)) {
                extent_Pass(driver,"The element is found and same as the desired text ", "", element2, element1);
            } else {
                extent_Failure(driver,"The element is not found or the desired text is not matching ", "", element2, element1);
                screenshot();
                status1 = 1;
            }
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to verify that the xpath element text contains the desired text
     * @param ElementXpath
     * @param ObjName
     * @throws AWTException
     */
    public static void verify_xpath_contains_text(String ElementXpath, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementXpath:%s, ObjName:%s",ElementXpath,ObjName));
        try {
            //Thread.sleep(2000);
            WebElement element=waitUntilElementIsVisible(ElementXpath);
            String element1 = element.getText();
            String element2 = ObjName;
            if (element1.toLowerCase().contains(element2.toLowerCase())) {
                extent_Pass(driver,"The element is found and contains the desired text:", "", element2, element1);
            } else {
                extent_Failure(driver,"The element not found or the desired text is not matching:", "", element2, element1);
                screenshot();
                status1 = 1;
            }
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to retrieve all elements with the same link text
     * @param ElementLinkText
     * @return
     * @throws AWTException
     * @throws InterruptedException
     */
    public static List<WebElement> retrieve_linktext_elements(String ElementLinkText) throws AWTException, InterruptedException {
        Logg.logger.info("ElementLinkText:"+ElementLinkText);
        //Thread.sleep(2000);
        List<WebElement> element1 = driver.findElements(By.linkText(ElementLinkText));
        ScrollToElement(element1.get(0));
        //extent_Pass(driver,"Retrieved all elements with the same link text ", ElementLinkText, "", "");
        return(element1);
    }


    /**
     * Function to retrieve all elements with the same xpath
     * @param xpath
     * @return
     * @throws AWTException
     * @throws InterruptedException
     */
    public static List<WebElement> retrieve_xpath_elements(String xpath) throws AWTException, InterruptedException {
        Logg.logger.info("xpath:"+xpath);
        //Thread.sleep(2000);
        List<WebElement> element1 = driver.findElements(By.xpath(xpath));
        //extent_Pass(driver,"Retrieved all elements with the same link text ", xpath, "", "");
        if(element1.size()>0) ScrollToElement(element1.get(0));
        return(element1);
    }


    /**
     * Function to verify the attribute value for an xpath element
     * @param ElementXpath
     * @param ObjName
     * @param attribute
     * @throws AWTException
     */
    public static void verify_xpath_Attribute_Value(String ElementXpath, String ObjName, String attribute) throws AWTException {
        Logg.logger.info(String.format("ElementXpath:%s, ObjName:%s",ElementXpath,ObjName));
        try {
            //Thread.sleep(2000);
            String element1 = driver.findElement(By.xpath(ElementXpath)).getAttribute(attribute);
            element1 = element1.replaceAll("\\r\\n|\\r|\\n", " ");
            String element2 = ObjName;
            if (element1.equalsIgnoreCase(element2)) {
                extent_Pass(driver,"The element is found and same as the desired text ",ObjName, element2, element1);
            } else {
                extent_Failure(driver,"The element is not found or the desired text is not matching ", ObjName, element2, element1);
                screenshot();
                status1 = 1;
            }
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to verify the getText value for an xpath element is not null
     * @param ElementXpath
     * @param ObjName
     * @throws AWTException
     */
    public static void verify_xpath_getText_NotNull(String ElementXpath, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementXpath:%s, ObjName:%s",ElementXpath,ObjName));
        try {
            //Thread.sleep(2000);
            String element1 = driver.findElement(By.xpath(ElementXpath)).getText();
            element1 = element1.replaceAll("\\r\\n|\\r|\\n", " ");
            if (!element1.isEmpty()) {
                extent_Pass(driver,"The element is found and is not null ",ObjName, "", element1);
            } else {
                extent_Failure(driver,"The element is not found or the desired text is null ", ObjName, "", element1);
                screenshot();
                status1 = 1;
            }
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to verify the getText value for an xpath element is not null
     * @param ElementXpath
     * @param ObjName
     * @return
     * @throws AWTException
     */
    public static String retrieve_xpath_getText_Value(String ElementXpath, String ObjName) throws AWTException {
        Logg.logger.info(String.format("ElementXpath:%s, ObjName:%s",ElementXpath,ObjName));
        try {
            //Thread.sleep(2000);
            String element1 = ScrollToElement(driver.findElement(By.xpath(ElementXpath))).getText();
            element1 = element1.replaceAll("\\r\\n|\\r|\\n", " ");
            if (!element1.isEmpty()) {
                extent_Pass(driver,"The element is found and is not null ",ObjName, "", element1);
                return element1;
            } else {
                extent_Failure(driver,"The element is not found or the desired text is null ", ObjName, "", element1);
                screenshot();
                status1 = 1;
                return null;
            }
        } catch (Exception e) {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
            return null;
        }
    }


    /**
     * Function to verify if the xpath element is present on the screen
     * @param xpath
     * @return
     */
    public static boolean isElementPresent(String xpath) {
        Logg.logger.info("xpath:"+xpath);
        try {
            //driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            if (!driver.findElements(By.xpath(xpath)).isEmpty()) {
                //driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
                ScrollToElement(driver.findElement(By.xpath(xpath)));
                return true;
            } else {
                //driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
                return false;
            }
        } catch (NoSuchElementException e) {
            //driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
            return false;
        }
    }


    /**
     * Function to verify if the xpath element is present on the screen
     * @param xpath
     * @param ObjName
     */
    public static void isElementExist(String xpath, String ObjName) {
        Logg.logger.info(String.format("xpath:%s, ObjName:%s",xpath,ObjName));
        try {
            if (!driver.findElements(By.xpath(xpath)).isEmpty()) {
                //driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
                ScrollToElement(driver.findElement(By.xpath(xpath)));
                extent_Pass(driver,"The element is found ",ObjName, "", "");
            } else {
                //driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
                extent_Failure(driver,"The element is not found ", ObjName, "", "");
            }
        } catch (NoSuchElementException e) {
            //driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        }
    }


    /**
     * Function to select a drop down option by Visible Text
     * @param ElementXpath
     * @param value
     * @param ObjName
     */
    public static void selectDropdown_VisibleText(String ElementXpath, String value, String ObjName)
    {
        Logg.logger.info(String.format("ElementXpath:%s, value:%s, ObjName:%s",ElementXpath,value,ObjName));
        try
        {
            WebElement DropDownBox = waitUntilElementIsVisible(ElementXpath);
            Select se1ect1 = new Select(DropDownBox);
            //se1ect1.deselectAll();
            se1ect1.selectByVisibleText(value);
            extent_Pass(driver,"The element is found and selected the value in the drop down",ObjName, "", "");
        }
        catch(Exception e)
        {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to select a drop down option by Value
     * @param ElementId
     * @param value
     * @param ObjName
     */
    public static void selectDropdown_Value(String ElementId, String value, String ObjName)
    {
        Logg.logger.info(String.format("ElementId:%s, value:%s, ObjName:%s",ElementId,value,ObjName));
        try
        {
            WebElement DropDownBox = waitUntilElementIsVisible(By.id(ElementId));
            Select se1ect1 = new Select(DropDownBox);
            se1ect1.selectByValue(value);
            extent_Pass(driver,"The element is found and selected the value in the drop down ",ObjName, "", "");
        }
        catch(Exception e)
        {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to select a drop down option by Value xpath
     * @param ElementXpath
     * @param value
     * @param ObjName
     */
    public static void selectDropdown_Value_xpath(String ElementXpath, String value, String ObjName)
    {
        Logg.logger.info(String.format("ElementXpath:%s, value:%s, ObjName:%s",ElementXpath,value,ObjName));
        try
        {
            WebElement DropDownBox = waitUntilElementIsVisible(ElementXpath);
            Select se1ect1 = new Select(DropDownBox);
            se1ect1.deselectAll();
            se1ect1.selectByValue(value);
            extent_Pass(driver,"The element is found and selected the value in the drop down ",ObjName, "", "");
        }
        catch(Exception e)
        {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to select a drop down option by Index
     * @param ElementId
     * @param index
     * @param ObjName
     */
    public static void selectDropdown_index(String ElementId, int index, String ObjName)
    {
        Logg.logger.info(String.format("ElementId:%s, index:%s, ObjName:%s",ElementId,index,ObjName));
        try{
            WebElement DropDownBox = waitUntilElementIsVisible(By.id(ElementId));
            Select select1 = new Select(DropDownBox);
            select1.selectByIndex(index);
            extent_Pass(driver,"The element is found and selected the value in the drop down ",ObjName, "", "");
        }
        catch(Exception e)
        {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to return the element by id
     * @param ElementId
     * @param ObjName
     * @return
     */
    public static WebElement returnElement(String ElementId, String ObjName)
    {
        Logg.logger.info(String.format("ElementId:%s, ObjName:%s",ElementId,ObjName));
        try{
            WebElement element1 = driver.findElement(By.id(ElementId));
            ScrollToElement(element1);
            extent_Pass(driver,"The element is found ",ObjName, "", "");
            return(element1);
        }
        catch(Exception e)
        {
            extent_Failure(driver,"The element is not found ", ObjName, "", "");
            screenshot();
            status1 = 1;
            return null;
        }
    }


    /**
     * Get active element
     * @return
     * @throws AWTException
     */
    public static WebElement Get_activeElement() throws AWTException
    {
        Logg.logger.info("");
        WebElement currentElement = driver.switchTo().activeElement();
        return(currentElement);
    }


    /**
     * Enter value to the current active element
     * @param variable
     * @throws AWTException
     */
    public static void Sendkey_activeElement(String variable) throws AWTException
    {
        Logg.logger.info("variable:"+variable);
        try{
            //Functions.wait(2000);
            WebElement currentElement = driver.switchTo().activeElement();
            currentElement.sendKeys(variable);
            extent_Pass(driver,"The data entered for text box ", "", "", variable);
        }
        catch(Exception e)
        {
            extent_Failure(driver,"The element is not found ", "", "", variable);
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Verify if current active element contains text
     * @param text1
     * @throws AWTException
     */
    public static void activeElement_contains_text(String text1) throws AWTException
    {
        Logg.logger.info("text1:"+text1);
        try{
            WebElement currentElement = driver.switchTo().activeElement();
            String text2=currentElement.getText();

            if(text2.contains(text1))
            {
                extent_Pass(driver,"The element is found and contains the text ", "", text1, text2);
            }
            else {
                extent_Failure(driver,"The text is not found ", "", text1, text2);
                screenshot();
                status1 = 1;
            }
        }
        catch(Exception e)
        {
            extent_Failure(driver,"The element is not found ", "", text1, "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Verify page title is as expected
     * @param title
     */
    public static void checkPageTitle(String title)
    {
        Logg.logger.info("title:"+title);
        try{
            //Functions.wait(2000);
            String text=driver.getTitle();
            if(driver.getTitle().contains(title))
            {
                extent_Pass(driver,"The page title is same as expected ", "", title, text);
            }
            else {
                extent_Failure(driver,"The page title does not found ", "", title, text);
                screenshot();
                status1 = 1;
            }
        }
        catch(Exception e){
            extent_Failure(driver,"The element is not found ", "", title, "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to switch to new tab
     */
    public static void switchToNewTab()
    {
        Logg.logger.info("");
        try{
            ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
            driver.switchTo().window(tabs2.get(1));
            String text=driver.getTitle();
            extent_Pass(driver,"Switched to new tab ", "", "", text);
        }
        catch(Exception e){
            extent_Failure(driver,"Unable to switch to new tab ", "", "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to switch back to previous tab
     */
    public static void switchbackToPerviousTab()
    {
        Logg.logger.info("");
        try{
            driver.close();
            ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
            driver.switchTo().window(tabs2.get(0));
            String text=driver.getTitle();
            extent_Pass(driver,"Switched to previous tab ", "", "", text);
        }
        catch(Exception e){
            extent_Failure(driver,"Unable to switch to previous tab ", "", "", "");
            screenshot();
            status1 = 1;
        }
    }
    public static void switchToWindow(String title)
    {
        Logg.logger.info("title:"+title);
        try{
            ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
            String currentWindow = driver.getWindowHandle();

            for (String winHandle: tabs) {
                if (driver.switchTo().window(winHandle).getTitle().contains(title)) {
                    extent_Pass(driver,"Switched to window ", title, title, driver.getTitle());
                    break;
                }
                else {
                    driver.switchTo().window(currentWindow);
                }
            }
        }
        catch(Exception e){
            extent_Failure(driver,"Unable to switch to window ", title, "", "");
            screenshot();
            status1 = 1;
        }
    }
    public static void closeWindow(String title)
    {
        Logg.logger.info("title:"+title);
        boolean flag=false;
        try{
            ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
            for (String winHandle: tabs) {
                if (driver.switchTo().window(winHandle).getTitle().contains(title)) {
                    flag=true;
                    extent_Pass(driver,"Found the window and Closing it ", title, "", "");
                    driver.close();
                    break;
                }
            }
            switchToWindow("Libraries");
            if(flag!=true)
                extent_Failure(driver,"Window not found:",title,"","");
        }
        catch(Exception e){
            extent_Failure(driver,"Unable to close window ", title, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to Accept Alert Message
     */
    public static void acceptAlert()
    {
        Logg.logger.info("");
        String AlertMessage=null;
        AlertMessage = driver.switchTo().alert().getText();
        driver.switchTo().alert().accept();
        extent_Pass(driver,"Accepted the alert message ", "", AlertMessage, "");
    }


    /**
     * Function to pass value to Alert Message
     * @param value
     */
    public static void Alert_value(String value)
    {
        Logg.logger.info("value:"+value);
        Alert alert=driver.switchTo().alert();
        driver.switchTo().alert().sendKeys(value);
        alert.accept();
    }


    /**
     * Get Config Values from Excel for data driven framework
     * @param key
     * @return
     */
    public static String getConfigValueFromExcel(String key) {
        Logg.logger.info("key:"+key);
        if(!excelValues.isEmpty()) {
            if(excelValues.containsKey(key))
                return excelValues.get(key);
        }
        return "";
    }


    /**
     * Insert data to Cke Editor
     * @param elementName
     * @param editorBodyText
     * @param ObjName
     * @return
     */
    public static boolean addDataToRichText(String elementName, String editorBodyText, String ObjName) {
        Logg.logger.info(String.format("elementName:%s, editorBodyText:%s, ObjName:%s",elementName,editorBodyText,ObjName));
        try {
            WebElement ckeEditorFrame = driver.findElement(By.className(elementName));
            driver.switchTo().frame(ckeEditorFrame);
            //Functions.wait(2000);
            WebElement editor_body = driver.findElement(By.tagName("body"));
            ScrollToElement(editor_body);
            if (driver instanceof JavascriptExecutor) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].innerHTML = '"+editorBodyText+"'", editor_body);
                extent_Pass(driver,"The Data Entered in rich text editor", ObjName, "", editorBodyText);
            } else {
                throw new IllegalStateException("This driver does not support JavaScript!");
            }
            //Functions.wait(2000);
            driver.switchTo().defaultContent();
            //Functions.wait(2000);
            return true;
        }
        catch (Exception ex) {
            extent_Failure(driver,"Unable to switch to iframe ", "", "", "");
            screenshot();
            return false;
        }

    }


    /**
     * verify then page is loaded successfully
     */
    public static void waitForPageLoaded() {
        Logg.logger.info("");
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        WebDriverWait wait = new WebDriverWait(driver,60);
        try {
            wait.until(expectation);
            extent_Pass(driver,"The page is loaded successfully ", driver.getTitle(), "", "");
        } catch(Throwable error) {
            extent_Warn(driver,"Timeout waiting for Page Load Request to complete.", driver.getTitle(), "", "");
        }
    }
    public static void waitForPageToStartLoading() {
        Logg.logger.info("");
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("loading");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver,20);
        try {
            wait.until(expectation);
        } catch(Throwable error) {
        }
    }


    /**
     * verify the format of the date displayed in the UI for eg, "EEE dd MMMMMM yyyy' go through website https://docs.oracle.com/javase/7/docs/api/java/text/DateFormat.html for format in detail
     * @param inDate
     * @param format
     * @return
     */
    public static boolean isValidDate(String inDate,String format) {
        Logg.logger.info(String.format("inDate:%s, format:%s",inDate,format));
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }


    /**
     * Function to hover on the element
     * @param className
     * @param ObjName
     * @throws AWTException
     */
    public static void Hover_On_Element(String className, String ObjName) throws AWTException {
        Logg.logger.info(String.format("className:%s, ObjName:%s",className,ObjName));
        try {

            //Threaad.sleep(3000);
            WebElement element = driver.findElement(By.xpath(className));
            //WebElement element=waitUntilElementIsVisible(className);
            if ("firefox".equalsIgnoreCase(BaseClass.WebBrowserName)) {
                javascriptMouseOver(element);
            }else{
                Actions actions = new Actions(driver);
                actions.moveToElement(element).build().perform();
            }
            extent_Pass(driver,"Hovered the element ", ObjName, "", "");

        } catch (Exception e) {
            extent_Failure(driver,"The element is not found for hovering ", ObjName, "", "");
            screenshot();
            status1 = 1;
        }
    }


    /**
     * Function to verify checkbox checked
     * @param objlocator
     * @param elemName
     * @return
     */
    public static String isCheckBoxChecked(String objlocator, String elemName) {
        Logg.logger.info(String.format("objlocator:%s, elemName:%s",objlocator,elemName));
        try {
            //Assuming the objLocator contains xpath
            //Threaad.sleep(2000);
            if (driver.findElement(By.xpath(objlocator)).isSelected()) {
                extent_Pass(driver,"The checkbox is checked", elemName, "", "");
            }else{
                extent_Failure(driver,"The checkbox is not checked", elemName, "", "");
                return null;
            }
        }
        catch (Exception e) {
            extent_Failure(driver,"The checkbox is not found or checked", elemName, "", "");
            return null;
        }
        return elemName;
    }


    /**
     * Executes the hovering functionality using JavascriptExecutor
     * This is a workaround added as the Gecko driver doesn't support actions currently
     * TODO : change the method to use Actions as soon as the gecko team fixes the bug.
     * @param element
     */
    protected static void javascriptMouseOver(WebElement element) {
        Logg.logger.info(String.format("element:%s",element));

        String script = "var fireOnThis = arguments[0];"
                + "var evObj = document.createEvent('MouseEvents');"
                + "evObj.initEvent( 'mouseover', true, true );"
                + "fireOnThis.dispatchEvent(evObj);";
        ((JavascriptExecutor) driver).executeScript(script, element);
    }
    public static WebElement waitAndClick(WebElement element) {
        Logg.logger.info("element:" + element);
        return Utilities.waitAndClick(driver,element);
    }
    public static WebElement waitAndClick(String xpath) {
        Logg.logger.info("xpath:"+xpath);
        return Utilities.waitAndClick(driver,xpath);
    }
    public static WebElement  waitUntilElementIsVisible(String xpath)
    {
        Logg.logger.info("xpath:"+xpath+"timeout:"+defaultTimeout);
        return Utilities.waitUntilElementIsVisible(driver,xpath);
    }
    public static void waitUntilElementIsNotVisible(WebElement element)
    {
        Logg.logger.info("Element:"+element);
        Utilities.waitUntilElementIsNotVisible(driver,element);
    }

    public static WebElement waitUntilElementIsVisible(By by)
    {
        Logg.logger.info("By:"+by);
        return Utilities.waitUntilElementIsVisible(driver,by);
    }
    public static String takeScreenshot()
    {
        Logg.logger.info("");
        return Utilities.takeScreenshot(driver);
    }
    public static boolean IsElementDisplayed(String xpath, String ObjName) {
        Logg.logger.info("xpath:" + xpath);

        return (driver.findElement(By.xpath(xpath)).isDisplayed());

        }


    public static void Content_ID_Visible(String xpath, String ObjName) {
        Logg.logger.info(String.format("xpath:%s, ObjName:%s",xpath,ObjName));
       if(!IsElementDisplayed(xpath, "Content Button")) {
           Functions.waitAndClick("//div[@class='toolbar-tab']//a[@href='/admin']");
       }
        Functions.waitAndClick(xpath);
    }
    public static void WriteStringToFile(String content, String path) throws  IOException
    {
        Logg.logger.info("Writing given 'content' to file: "+ path);  // Let's do only low level debug logging here.
        FileOutputStream writer = new FileOutputStream(path,true);
        writer.write(Byte.parseByte(content));
    }
    public static WebElement ScrollToElement(WebElement element)
    {
        Logg.logger.info("element:"+element);
        return Utilities.scrollToElement(driver,element);
    }
//    public static void waitUntilWindowsSize(int size)
//    {
//        Logg.logger.info("Size:"+size);
//        try {
//            WebDriverWait wait = new WebDriverWait(driver, defaultTimeout + 20);
//            wait.until(driver -> driver.getWindowHandle().size() == size);
//        } catch (Exception e)
//        {
//            Logg.logger.info("Exception occured in waitUntilWindowsSize:"+e.getMessage());
//            e.printStackTrace();
//        }
//    }
    public static void configureExtentReport(String url) {
        Logg.logger.info("url:"+url);
        extentReport = new ExtentReports(System.getProperty("user.dir")+"\\test-output\\ExtentReportResults.html");
        extentReport.addSystemInfo("Test Environment",url);
        extentReport.loadConfig(new File(System.getProperty("user.dir")+"//src//test//configs//extentReportConfig.xml"));
    }
}