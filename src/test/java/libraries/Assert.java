package libraries;

import org.openqa.selenium.WebDriver;

import java.util.List;

import static libraries.ExtentReporting.extent_Failure;
import static libraries.ExtentReporting.extent_Pass;

public class Assert {
    /* This function is to mark the test as pass to Extent report if Assert is true and mark the test as Fail a with screenshot if Assert is false with the required messages for the report */
    public static void assertTrue(boolean var0, String passMessage, String failMessage, WebDriver driver)
    {
        try{
            org.testng.Assert.assertTrue(var0,failMessage);
            extent_Pass(driver,passMessage,"","","");
        } catch(AssertionError e)
        {
            extent_Failure(driver, failMessage,"","","");
            throw e;
        }
    }
    /* This function is to mark the test as pass to Extent report if Assert is false and mark the test as Fail a with screenshot if Assert is true with the required messages for the report */
    public static void assertFalse(boolean var0, String passMessage, String failMessage, WebDriver driver)
    {
        try{
            org.testng.Assert.assertFalse(var0,failMessage);
            extent_Pass(driver,passMessage,"","","");
        } catch(AssertionError e)
        {
            extent_Failure(driver, failMessage,"","","");
            throw e;
        }
    }

    /* This function is to mark the test as pass to Extent report if Object passed is not null and mark the test as Fail a with screenshot if object is null with the required messages for the report */

    public static void assertNotNull(Object var0, String passMessage, String failMessage, WebDriver driver)
    {
        try{
            org.testng.Assert.assertNotNull(var0,failMessage);
            extent_Pass(driver,passMessage,"","",var0.toString());
        } catch(AssertionError e)
        {
            extent_Failure(driver, failMessage,"","","");
            throw e;
        }
    }
    /* This function is to mark the test as pass to Extent report if both passed Strings are equal and mark the test as Fail a with screenshot if both Strings are not equal with the required messages for the report */
    public static void assertEquals(String var0, String var1, String passMessage, String failMessage, WebDriver driver) {
        try{
            org.testng.Assert.assertEquals(var0,var1,failMessage);
            extent_Pass(driver,passMessage,"",var0,var1);
        } catch(AssertionError e)
        {
            extent_Failure(driver, failMessage,"",var0,var1);
            throw e;
        }
    }
    /* This function is to mark the test as pass to Extent report if both passed integers are equal and mark the test as Fail a with screenshot if both integers are not equal with the required messages for the report */
    public static void assertEquals(int var0, int var1, String passMessage, String failMessage, WebDriver driver) {
        try{
            org.testng.Assert.assertEquals(var0,var1,failMessage);
            extent_Pass(driver,passMessage,"",Integer.toString(var0),Integer.toString(var1));
        } catch(AssertionError e)
        {
            extent_Failure(driver, failMessage,"",Integer.toString(var0),Integer.toString(var1));
            throw e;
        }
    }
    /* This function is to mark the test as pass to Extent report if substring is present in the query string and mark the test as Fail a with screenshot if the substring is not present in the query string with the required messages for the report */
    public static void assertContains(String query, String substring, String passMessage, String failMessage, WebDriver driver) {
        try{
            org.testng.Assert.assertTrue(query.contains(substring),failMessage);
            extent_Pass(driver,passMessage,"","Expected substring:"+substring,"Actual Full string:"+query);
        } catch(AssertionError e)
        {
            extent_Failure(driver, failMessage,"","Expected substring:"+substring,"Actual Full string:"+query);
            throw e;
        }
    }
    /* This function is to mark the test as pass to Extent report if substring is not present in the query string and mark the test as Fail a with screenshot if the substring is present in the query string with the required messages for the report */
    public static void assertNotContains(String query, String substring, String passMessage, String failMessage, WebDriver driver) {
        try{
            org.testng.Assert.assertFalse(query.contains(substring),failMessage);
            extent_Pass(driver,passMessage,"","Expected substring:"+substring,"Actual Full string:"+query);
        } catch(AssertionError e)
        {
            extent_Failure(driver, failMessage,"","Expected substring:"+substring,"Actual Full string:"+query);
            throw e;
        }
    }
    /* This function is to mark the test as pass to Extent report if query is not present in the list and mark the test as Fail a with screenshot if the query is present in the list string with the required messages for the report */
    public static void assertNotContains(String query, List<String> list, String passMessage, String failMessage, WebDriver driver) {
        try{
            boolean found=false;
            for(int i=0;i<list.size();i++)
            {
                if(query.contains(list.get(i))) {
                    found = true;
                    break;
                }
            }
            org.testng.Assert.assertFalse(found,failMessage);
            extent_Pass(driver,passMessage,"","Expected substring:"+list,"Actual Full string:"+query);
        } catch(AssertionError e)
        {
            extent_Failure(driver, failMessage,"","Expected substring:"+list,"Actual Full string:"+query);
            throw e;
        }
    }
}
