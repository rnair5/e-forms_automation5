package libraries;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.javafx.binding.StringFormatter;
import org.openqa.selenium.WebDriver;

import java.io.File;

public class ExtentReporting {
    public static ExtentTest extentTest;
    public static ExtentReports extentReport = null;
    protected static boolean addPassScreenshot = true;
    protected static String reportFilePath=null;
    public static ExtentReports getExtentReport()
    {
        return extentReport;
    }
    public static boolean getAddPassScreenshot() {
        return addPassScreenshot;
    }
    public static String getReportFilePath() {
        return reportFilePath;
    }
    /*Function sets the report file path*/
    public static void setReportFilePath(String reportFilePath) {
        ExtentReporting.reportFilePath = reportFilePath;
    }
    /*Function to set if the screenshots should be taken in case of pass. They are always taken in case of failure. This is set from the testng parameter */
    public static void setAddPassScreenshot(boolean addPassScreenshot) {
        ExtentReporting.addPassScreenshot = addPassScreenshot;
    }
    /*Configure the extent report parameters like Report name, whether to take screenshot in case of Pass and load the configurations*/
    public static ExtentReports configureExtentReport(String reportName, boolean addPassScreenshot1,String url) {
        if(extentReport == null ) {
            Logg.logger.info(StringFormatter.format(" reportName: %s", reportName));
            setReportFilePath(System.getProperty("user.dir") + "\\test-output\\" + reportName);
            extentReport = new ExtentReports(reportFilePath);
            extentReport.addSystemInfo("Test Environment",url);
            extentReport.loadConfig(new File(System.getProperty("user.dir") + "//src//test//configs//extentReportConfig.xml"));
            setAddPassScreenshot(addPassScreenshot1);
        }
        return extentReport;
    }
     /* Function to add a pass to the extent report with all details and screenshot if it is set to true in testng parameter
     * @param step_description
     * @param input_value
     * @param expected_value
     * @param actual_value
     */
    public static void extent_Pass(WebDriver driver,String step_description, String input_value, String expected_value, String actual_value) {
        Logg.logger.info(String.format("step_description:%s , input_value: %s, expected_value:%s, actual_value:%s",step_description,input_value,expected_value,actual_value));
        try {
            //ATUReports.add(step_description, input_value, expected_value, actual_value, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
            input_value=(input_value==""?"":" | Input_value:"+input_value);
            expected_value=(expected_value==""?"":" | Expected_value:"+expected_value);
            actual_value=(actual_value==""?"":" | Actual_value:"+actual_value);
            extentTest.log(LogStatus.PASS,step_description+input_value+ expected_value + actual_value);
            if(addPassScreenshot) extentTest.log(LogStatus.PASS, extentTest.addScreenCapture(Utilities.takeScreenshot(driver)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured: " +e.getMessage());
            extentTest.log(LogStatus.INFO,e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }
    /**
     * Function to add a pass to the extent report with all details without any  screenshot
     * @param step_description
     * @param input_value
     * @param expected_value
     * @param actual_value
     */
    public static void extent_Pass_NoScreenshot(WebDriver driver,String step_description, String input_value, String expected_value, String actual_value) {
        Logg.logger.info(String.format("step_description:%s , input_value: %s, expected_value:%s, actual_value:%s",step_description,input_value,expected_value,actual_value));
        try {
            //ATUReports.add(step_description, input_value, expected_value, actual_value, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
            input_value=(input_value==""?"":" | Input_value:"+input_value);
            expected_value=(expected_value==""?"":" | Expected_value:"+expected_value);
            actual_value=(actual_value==""?"":" | Actual_value:"+actual_value);
            extentTest.log(LogStatus.PASS,step_description+input_value+ expected_value + actual_value);
            //if(addPassScreenshot) extentTest.log(LogStatus.PASS, extentTest.addScreenCapture(Utilities.takeScreenshot(driver)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured: " +e.getMessage());
            extentTest.log(LogStatus.INFO,e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Function to add a warning to the extent report with all given details and a screenshot
     * @param step_description
     * @param input_value
     * @param expected_value
     * @param actual_value
     */
    public static void extent_Warn(WebDriver driver,String step_description, String input_value, String expected_value, String actual_value) {
        Logg.logger.info(String.format("step_description:%s , input_value: %s, expected_value:%s, actual_value:%s",step_description,input_value,expected_value,actual_value));
        try {
            //ATUReports.add(step_description, input_value, expected_value, actual_value, LogAs.WARNING, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
            input_value=(input_value==""?"":" | Input_value:"+input_value);
            expected_value=(expected_value==""?"":" | Expected_value:"+expected_value);
            actual_value=(actual_value==""?"":" | Actual_value:"+actual_value);
            extentTest.log(LogStatus.WARNING,step_description+input_value+expected_value+actual_value);
            extentTest.log(LogStatus.WARNING, extentTest.addScreenCapture(Utilities.takeScreenshot(driver)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured: " +e.getMessage());
            extentTest.log(LogStatus.INFO,e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Function to add a failure to the extent report with all given details and a screenshot
     * @param step_description
     * @param input_value
     * @param expected_value
     * @param actual_value
     */
    public static void extent_Failure(WebDriver driver,String step_description, String input_value, String expected_value, String actual_value) {
        Logg.logger.info(String.format("step_description:%s , input_value: %s, expected_value:%s, actual_value:%s", step_description, input_value, expected_value, actual_value));
        try {
            //screenshot();
            //ATUReports.add(step_description, input_value, expected_value, actual_value, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
            input_value = (input_value == "" ? "" : " | Input_value:" + input_value);
            expected_value = (expected_value == "" ? "" : " | Expected_value:" + expected_value);
            actual_value = (actual_value == "" ? "" : " | Actual_value:" + actual_value);
            extentTest.log(LogStatus.FAIL, step_description + input_value + expected_value + actual_value);
            extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(Utilities.takeScreenshot(driver)));
        } catch (Exception e) {
            Logg.logger.info("Exception occured; " + e.getMessage());
            extentTest.log(LogStatus.INFO, e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

//
//
        /**
         * Function to set Test case description in ATU Report for each script
         * @param testCaseDesc
         */
        public static void extent_SetTestCaseRegCoverageForReports(String testCaseDesc) {
            Logg.logger.info("TestCaseDesc:"+ testCaseDesc);
            //ATUReports.setTestCaseReqCoverage(TestCaseDesc);
            extentTest.log(LogStatus.INFO,testCaseDesc);
        }




        
}

