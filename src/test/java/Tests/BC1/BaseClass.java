package Tests.BC1;
import libraries.ExtentReporting;
import libraries.Utilities;
import com.relevantcodes.extentreports.ExtentReports;
import libraries.Logg;
import libraries.Functions;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class BaseClass {

    public static int status1;
    public static Properties OR = null;
    public static WebDriver driver;
    public static FileInputStream fp = null;
    public static String url;
    public static Map<String, String> excelValues = null;
    public static List<String> urls = null;
    public static long StartTime = new Date().getTime();
    public static String WebBrowserName= null;
    public static long defaultTimeout;
    public static ExtentReports extentReport;
    public static String reportName="ExtentReportResults.html";

    @BeforeSuite
    @Parameters( { "url", "run_desc", "browserName" ,"addPassScreenshot"})
    public static void BeforeSuite(String url,  String run_desc,String browserName, boolean addScreenshotIfStepPass) {
        Logg.configure();
        Logg.logger.info("");
        StartTime = new Date().getTime();
        setUrl(url);
        WebBrowserName=browserName;
        //Functions.configureExtentReport(url);
        extentReport = ExtentReporting.configureExtentReport(reportName,addScreenshotIfStepPass,url);
        //Functions.atu_Report_Customise();
    }
    @BeforeTest
    public void beforeTest() throws IOException
    {
        loadUrlsFromExcel();
        loadConfigsFromExcel();
        Load_Properties_File_DesktopBrowser();
        defaultTimeout = Long.parseLong(OR.getProperty("defaultTimeout"));
        //ATU report customisation
        // Functions.atu_Report_Customise();
        //ATUReports.currentRunDescription = run_desc;
    }
    /**
     * BaseMethod executes before Test suite, initialises web browser and loads property file and config files within the framework
     * @param url
     * @param run_desc
     * @param browserName
     * @throws IOException
     * @throws InterruptedException
     * @throws AWTException
     */
    @BeforeClass
    @Parameters( { "url", "run_desc", "browserName" })
    public static void BaseMethod(String url,  String run_desc,String browserName) throws IOException, InterruptedException, AWTException {
        Logg.logger.info("");
        driver = Utilities.createDriver(browserName);
        Functions.loadurl_script();

    }
    @AfterSuite
    public static void afterSuite() {
        Logg.logger.info("");
        driver.quit();
    }

    /**
     * This executes After test suite
     * @throws Exception
     */
    @AfterClass
    public static void afterClass() throws Exception {
        Logg.logger.info("");
        Functions.tearDownDriver();
        long runtime = new Date().getTime() - StartTime;
        Logg.logger.info("Total Runtime was: " + runtime);
    }

    public static String getUrl() {
        return BaseClass.url;
    }

    public static void setUrl(String url) {
        BaseClass.url = url;
    }


    /**
     * Loading config file for data driven framework
     * @throws IOException
     */
    public static void loadConfigsFromExcel() throws IOException{
        Logg.logger.info("");
        excelValues = new HashMap<String, String>();
        String path=System.getProperty("user.dir")+"\\src\\test\\Configs\\ConfigFile.xlsx";
        FileInputStream file = new FileInputStream(new File(path));
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        while(rowIterator.hasNext()) {
            Row row = (Row) rowIterator.next();
            if (row.getRowNum() == 0) {
                continue;
            }
            Iterator<Cell> cellIterator = row.cellIterator();
            String key = null;
            String value = null;
            while(cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if(cell.getColumnIndex() == 0 ){
                    key=cell.getStringCellValue();
                }
                if(cell.getColumnIndex() == 1 ){
                    value=cell.getStringCellValue();
                }
                if(key != null && value != null){
                    excelValues.put(key, value);
                }
            }
        }

    }


    /**
     * Loading urls form the excel file
     * @throws IOException
     */
    public static void loadUrlsFromExcel() throws IOException{
        Logg.logger.info("");
        urls = new ArrayList<String>();
        String path=System.getProperty("user.dir")+"\\src\\test\\Configs\\URLS.xlsx";
        FileInputStream file = new FileInputStream(new File(path));
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        while(rowIterator.hasNext()) {
            Row row = (Row) rowIterator.next();

            Iterator<Cell> cellIterator = row.cellIterator();

            String value = null;
            while(cellIterator.hasNext()) {
                Cell cell = cellIterator.next();

                value =cell.getStringCellValue();

                if( value != null){
                    urls.add(value);
                }

            }
        }    }


    /**
     * loading the properties file for desktop browser (config.properties and OR.properties)
     * @throws IOException
     */
    public static void Load_Properties_File_DesktopBrowser() throws IOException{
        Logg.logger.info("");
        OR = new Properties();
        String path=System.getProperty("user.dir")+"\\src\\test\\resources\\Properties\\PropertyFile.properties";
        fp = new FileInputStream(path);
        OR.load(fp);

    }


    /**
     * Login function for different users
     * @param Role
     * @throws AWTException
     * @throws InterruptedException
     */
    public static void Login(LoginRole Role) throws AWTException, InterruptedException {
        Logg.logger.info("Role:"+Role);
        //Login page
        String username="";
        String password="";
        switch(Role) {
            case Admin:
                //Functions.wait(1000);
                Functions.waitForPageLoaded();
                username = Functions.getConfigValueFromExcel("BC1_AdminUserName");
                password = Functions.getConfigValueFromExcel("BC1_AdminPassword");
            case ContentAuthor:
                username = Functions.getConfigValueFromExcel("BC1_AdminUserName");
                password = Functions.getConfigValueFromExcel("BC1_AdminPassword");
            case SiteAdmin:
                username = Functions.getConfigValueFromExcel("BC1_SiteAdminUserName");
                password = Functions.getConfigValueFromExcel("BC1_SiteAdminPassword");
            default:
        }

        Functions.Sendkey_xpath(OR.getProperty("UserName"), username, "UserName");//username
        Functions.Sendkey_xpath(OR.getProperty("Password"), password, "Password");//password
        driver.findElement(By.id("edit-pass")).sendKeys(Keys.TAB);
        Functions.Click_Button_Xpath(OR.getProperty("Login"), "Login");
        //Functions.wait(1000);
        Functions.waitForPageLoaded();
        Logg.logger.info("Successfully Logged In to the System");

    }


    /**
     * function for logout
     * @throws AWTException
     */
    public static void Logout() throws AWTException {
        Logg.logger.info("");
        //Logout
        Functions.Click_Button_Xpath(OR.getProperty("User_link"), "User_link");
        Functions.Click_Button_Xpath(OR.getProperty("User_logout"), "User_logout");
        Logg.logger.info("Successfully Logged Out from the System");

    }

    /**
     * declaring Login roles for BC1
     */
    public enum LoginRole{

        Admin,
        ContentAuthor,
        ContentPublisher,
        SiteAdmin

    };

}
