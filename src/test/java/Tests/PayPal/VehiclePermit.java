package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.PaymentPage;
import pageObjects.forms.VehiclePermitPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class VehiclePermit extends BaseClass {
    VehiclePermitPage vcp;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/apply-for-a-vehicle-crossing-permit";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "VCP_DataProvider", description = "Verify that user can submit an vcp form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String firstAddress, String multicheck, String secondAddress, String category, String description, String valueWorks, String startDate, String endDate, String person, String property,
                       String fName, String lName, String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String phoneType, String mobilePhoneNo,  String landPhone,String receiveSMS, String email,String permit,
                       String declarationOne, String declarationTwo, String declarationThree,String expReviewPage, String expPrice,
                       String expCategoryBack, String expWorksBack, String reviewPageBack, String expPriceBack,
                       String CardNo, String expMonth, String expYear, String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expPdfFormat, String expDial, String expCertificate,  String expNotification,
                       String expReceipt, String expSite, String expSupport1, String expSupport2,
                       String expSupport3, String expSupport4, String expSupport5, String expTraffic,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a VCP form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Asset Protection")
                        .setOrigin(origin);
                isUserCso = true;
                vcp = new VehiclePermitPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                vcp = new VehiclePermitPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
            }
            vcp.clickNext("1").
                    setFirstAddress(firstAddress).
//                    setMultiCheck(multicheck).
//                    setSecondAddress(secondAddress).
        setCategory(category).
                    setWorks(description).
                    setPermitChoice(valueWorks).
                    setPermitNumber(startDate).
                    uploadSitePlan().
                    uploadDigReport().
                    uploadSupporting().
                    setComments(endDate);
            vcp.clickNext("2").
                    selectPerson(person);
            if (person.equalsIgnoreCase("Builder")) {
                vcp.selectProperty(property).
                        setPersonalDetails(fName, lName, postalStreet, postalSuburb, postalState, postalPostCode);

                vcp.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    vcp.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    vcp.setlandPhoneNumber(landPhone);

                }
                    vcp.setSMSNotifications("Yes");
                    vcp.setEmail("Reshma.Nair@boroondara.vic.gov.au");
                            vcp.setDeclarations(declarationOne)
                            .clickNext("3");
                    vcp.getReviewPage(expReviewPage, expPrice);
                    vcp.saveForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
//                    Thread.sleep(10000);
                    vcp.getPaymentDetails();
                    Thread.sleep(20000);
                    vcp.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                            expPdfFormat, expDial, expCertificate, expNotification,
                            expReceipt, expSite, expSupport1, expSupport2,
                            expSupport3, expSupport4, expSupport5, expTraffic,
                            expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);
//                    vcp.readPDF(expPdf, expPdfText);


                }
//                vcp.setSMSNotifications("Yes");
//                vcp.setEmail("Reshma.Nair@boroondara.vic.gov.au");
//                vcp.setPermit("Email")
//                        .setDeclarations("The information I have provided is true and correct.")
//                        .setDeclarationsTwo("Building works will not start until I have received the asset protection permit.")
//                        .setDeclarationsThree("I will keep a clean and safe worksite that meets all permit conditions.")
//                        .clickNext("3");
//                vcp.getReviewPage(expReviewPage, expPrice);
////                vcp.clickPrevious("4");
////                vcp.clickPrevious("3");
////                vcp.setCategory("Major impact works");
////                vcp.setDescription("Text test after review page");
////                vcp.clickDrain("No");
////                vcp.clickNext("2");
////                vcp.clickNext("3");
////                vcp.getReviewPageBack(reviewPageBack, expPriceBack);
//                vcp.saveForm();
//                paymentPage.setCardNo(CardNo);
//                paymentPage.setExpiryMonth(expMonth);
//                paymentPage.setExpiryYear(expYear);
//                paymentPage.clickVerify()
//                        .setCVVNo(CVV)
//                        .clickPay()
//                        .isAppPaymentSuccessful();
////                Thread.sleep(30000);
//                vcp.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
//                        expPdfFormat, expCertificate, expDial, expNotification,
//                        expReceipt, expSite, expSupport1, expSupport2,
//                        expSupport3, expSupport4, expSupport5, expTraffic,
//                        expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);



            }
        }


        @DataProvider

        public Object[][] VCP_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "vcp");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "vcp", 64);

            return testObjArray;
        }

    }

