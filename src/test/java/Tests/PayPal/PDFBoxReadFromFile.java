package Tests.PayPal;

import pageObjects.forms.ReportAnIssuePage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reshma
 */
public class PDFBoxReadFromFile {

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        ReportAnIssuePage rai;
        PDFReader pdfManager = new PDFReader();
        pdfManager.setFilePath("C:\\Users\\rnair\\Downloads\\Request - Public Litter Bin Overfilled - 25 Fordham Avenue CAMBERWELL.pdf");

//            String filepath = "Request - Public Litter Bin Overfilled - 25 Fordham Avenue CAMBERWELL";
//        pdfManager.setFilePath("C:\\Users\\rnair\\Downloads\\" +filepath+ ".pdf");
        try {
            String text = pdfManager.toText();
            String[] linesFile = text.split("\n");// this array is initialized with a single element

            String gravityID = linesFile[1]; // not fine, the array has size 1, so no element at second index
            String line2 = linesFile[4]; // not fine, the array has size 1, so no element at second index
//            String line2 = linesFile[2];
            System.out.println(gravityID);
            System.out.println(line2);
//            System.out.println(line1);
//            System.out.println(text);

        } catch (IOException ex) {
            Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}