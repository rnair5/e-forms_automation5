package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.NewAssetProtectionPayPalPage;
import pageObjects.forms.PayPalPage;
import pageObjects.forms.PaymentPage;
import pageObjects.forms.RefundBrainTreePage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

/**
 * Created by Reshma Nair on 3/12/2020.
 * Functions: This class is refers to the functions in NewAssetProtectionPayPalPage
 */

public class AssetProtectionPayPalLoggedInUser extends BaseClass {
    NewAssetProtectionPayPalPage app;
    PayPalPage pal;
    PaymentPage paymentPage;
    String url;
    String cso_url;
    RefundBrainTreePage rapp;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/apply-for-an-asset-protection-permit";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "APPL_DataProvider", description = "Verify that user can submit an APP form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String firstAddress, String multicheck, String secondAddress, String category, String description, String valueWorks, String startDate, String endDate, String person, String property,
                       String fName, String lName, String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String phoneType, String mobilePhoneNo,  String landPhone,String receiveSMS, String email,String permit,
                       String declarationOne, String declarationTwo, String declarationThree,String expReviewPage, String expPrice,
                       String expCategoryBack, String expWorksBack, String reviewPageBack, String expPriceBack,
                       String country, String payPalCard, String paypalExpiry, String payPalCVV,
                       String paypalFN, String payPalLN, String payPalBillingAddress1, String payPalBillingAddress2, String payPalSuburb,
                       String payPalPostCode, String payPalState, String payPalPhone, String payPalEmail,
                       String CardNo, String expMonth, String expYear, String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expAmount, String expTotal, String expTax, String expSurcharge,
                       String expPdfFormat, String expReceipt,  String expSupport,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an APP form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Asset Protection")
                        .setOrigin(origin);
                isUserCso = true;
                app = new NewAssetProtectionPayPalPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                app = new NewAssetProtectionPayPalPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
                pal = new PayPalPage(ChromeDriver);
            }
            app.clickNext("1").
                    setFirstAddress(firstAddress).
        setCategory(category).
                    setDescription(description).
                    setestimateValue(valueWorks).
                    setStartDate(startDate).
                    setEndDate(endDate);
            app.uploadMicrochipFile();

            app.clickNext("2").
                    selectPerson(person);
            if (person.equalsIgnoreCase("Owner")) {
                app.selectProperty(property).
                        setPersonalDetails(fName, lName, postalStreet, postalSuburb, postalState, postalPostCode);

                app.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    app.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    app.setlandPhoneNumber(landPhone);

                }
                app.setSMSNotifications("Yes");
                app.setEmail("Reshma.Nair@boroondara.vic.gov.au");
                app.setPermit("Email")
                        .setDeclarations("The information I have provided is true and correct.")
                        .setDeclarationsTwo("Building works will not start until I have received the asset protection permit.")
                        .setDeclarationsThree("I will keep a clean and safe worksite that meets all permit conditions.")
                        .clickNext("3");
                app.getReviewPage(expReviewPage, expPrice);
                app.clickPrevious("4");
                app.clickPrevious("3");
                app.setCategory(expCategoryBack);
                app.setDescription(expWorksBack);
                app.clickDrain("Yes");
                app.clickNext("2");
                app.clickNext("3");
                app.getReviewPageBack(reviewPageBack, expPriceBack);
                app.payPalPayment();
                Thread.sleep(7000);
//                pal.selectTopLoginOption();
                pal.enterEmail();
                pal.continueLogin();
                pal.enterPassword();
                Thread.sleep(10000);
                pal.Login();
                pal.paymentLoggedIn();
                Thread.sleep(20000);
                app.getPaymentDetails();
//                    pal.selectUserAgreement();
            } else {

                app.selectProperty(property);
                app.setCompanyName("NAIR.LTD -123 & Five's");
                app.setABN("123 456 789");
                app.setABN("123 456 789");
                app.setContactName("Neil 'O BRian");
                app.setCompAddressOne("38 dunnon street");
                app.setCompAddressTwo("Moroolbank");
                app.setCompAddressFour("Victoria");
                app.setCompPostCode("3164");
                app.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    app.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    app.setlandPhoneNumber(landPhone);

                }
                app.setSMSNotifications("Yes");
                app.setEmail("Reshma.Nair@boroondara.vic.gov.au");
                app.selectAppOwnerProperty("No");
                app.setAppOwnerFName("Reshma");
                app.setAppOwnerLName("Nair");
                app.setAppOwnerAddressOne("673");
                app.setAppOwnerAddressTwo("Burke Road");
                app.setAppOwnerAddressFour("Victoria");
                app.setAppOwnerPostCode("3012");
                app.setAppOwnerPhoneType("Mobile");
                app.setAppOwnerMobPhoneNumber("0406361552");
                app.setAppOwnerEmail("Reshma.Nair@boroondara.vic.gov.au");
                app.setPermit("Email")
                        .setDeclarations("The information I have provided is true and correct.")
                        .setDeclarationsTwo("Building works will not start until I have received the asset protection permit.")
                        .setDeclarationsThree("I will keep a clean and safe worksite that meets all permit conditions.")
                        .clickNext("3");
                app.getReviewPage(expReviewPage, expPrice);
                app.clickPrevious("4");
                app.clickPrevious("3");
                app.setCategory(expCategoryBack);
                app.setDescription(expWorksBack);
                app.clickDrain("Yes");
                app.clickNext("2");
                app.clickNext("3");
                app.getReviewPageBack(reviewPageBack, expPriceBack);
                Thread.sleep(7000);
                app.payPalPayment();
                Thread.sleep(7000);
                    pal.enterEmail();
                    pal.continueLogin();
                    pal.enterPassword();
                    Thread.sleep(10000);
                    pal.Login();
                    pal.paymentLoggedIn();
                    Thread.sleep(20000);
                    app.getPaymentDetails();

            }
            app.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                    expAmount, expTotal, expTax, expSurcharge,
                    expPdfFormat, expReceipt, expSupport,
                    expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);
            app.readPDF(expPdf,expPdfText);



            }
        }


        @DataProvider

        public Object[][] APPL_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APPL");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APPL", 72);

            return testObjArray;
        }

    }

