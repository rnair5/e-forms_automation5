package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.ReportABinPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

;

public class ReportABin extends BaseClass {
    ReportABinPage rbp;
    String url;
    String cso_url;
    @Parameters({"form_url","cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url=url+"/bin-request";
        this.cso_url = cso_url;
    }
    @Test(dataProvider = "RBP_DataProvider" , description = "Verify that user can submit an rbp form" )

    public void Submit(String tc_name,String cso, String cso_id, String cso_pass, String origin,
                       String address, String reason, String binType, String issueType, String damageType,
                       String startDate, String locType,String descLoc,
                       String fName, String lName,
                       String email, String phType, String phoneNo,
                       String receiveSMS, String expReviewPage, String expCase, String expSubCase,
                       String expCustomer, String expLocation, String expInstructions, String expType, String expBusinessNo,
                       String expAlternateNo, String expCaseCount) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a RBP form");

        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Bin request")
                        .setOrigin(origin);
                isUserCso = true;
                rbp = new ReportABinPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                rbp = new ReportABinPage(ChromeDriver);

            }
            rbp.clickNext("1")
                    .setAddress(address);
            rbp.clickNext("2")
                    .setReasonType(reason);
            if (reason.equalsIgnoreCase("My bin wasn't emptied")) {
                rbp.setBinType(binType)
                        .setProblemType(issueType)
//                        .enterDate(startDate)
                        .selectStartDate(startDate)
                        .setBinCollection(locType)
                        .setLocationInformation(descLoc);
                rbp.clickNext("3")
                        .setFName(fName)
                        .setLName(lName)
                        .setEmail(email)
                        .setPhoneType(phType);
                if (phType.equalsIgnoreCase("Mobile")) {
                    rbp.setPhoneNumber(phoneNo);
                    rbp.setSMSNotifications(receiveSMS);

                } else if (phType.equalsIgnoreCase("Landline"))
                    rbp.setPhoneNumber(phoneNo);
            } else if (reason.equalsIgnoreCase("My bin is lost")) {
                rbp.setBinType(binType)
                        .setBinCollection(locType)
                        .setLocationInformation(descLoc);
                rbp.clickNext("3")
                        .setFName(fName)
                        .setLName(lName)
                        .setEmail(email)
                        .setPhoneType(phType);
                if (phType.equalsIgnoreCase("Mobile")) {
                    rbp.setPhoneNumber(phoneNo);
                    rbp.setSMSNotifications(receiveSMS);

                } else if (phType.equalsIgnoreCase("Landline"))
                    rbp.setPhoneNumber(phoneNo);
            } else if (reason.equalsIgnoreCase("My bin is damaged")) {
                rbp.setBinType1(binType)
                        .setDamagedPart(damageType)
                        .setBinCollection(locType);

                rbp.clickNext("3")
                        .setFName(fName)
                        .setLName(lName)
                        .setEmail(email)
                        .setPhoneType(phType);
                if (phType.equalsIgnoreCase("Mobile")) {
                    rbp.setPhoneNumber(phoneNo);
                    rbp.setSMSNotifications(receiveSMS);

                } else if (phType.equalsIgnoreCase("Landline"))
                    rbp.setPhoneNumber(phoneNo);
            } else if (reason.equalsIgnoreCase("I need FOGO equipment")) {
                rbp.setBinType1(binType);
                rbp.clickNext("3")
                        .setFName(fName)
                        .setLName(lName)
                        .setEmail(email)
                        .setPhoneType(phType);
                if (phType.equalsIgnoreCase("Mobile")) {
                    rbp.setPhoneNumber(phoneNo);
                    rbp.setSMSNotifications(receiveSMS);

                } else if (phType.equalsIgnoreCase("Landline"))
                    rbp.setPhoneNumber(phoneNo);
            }
            }

                    rbp.clickNext("4");
                    rbp.getReviewPage(expReviewPage);
                    rbp.submitForm();
                    rbp.captureCRM(expCase,expSubCase,expCustomer,expLocation,expInstructions,expType,expBusinessNo,phoneNo,expAlternateNo,
                            fName,lName,email);

}
    @DataProvider
    public Object[][] RBP_DataProvider() throws Exception {
        // Setting up the Test Data Excel file
        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"RBP");
        // Fetching the Test Case row number from the Test Data Sheet
        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"RBP",29);
        return testObjArray;
    }
}

