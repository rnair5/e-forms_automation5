package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.AssetProtectionPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class AssetProtection extends BaseClass {
    AssetProtectionPage app;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/apply-for-an-asset-protection-permit";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "APP_DataProvider", description = "Verify that user can submit an APP form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String firstAddress, String multicheck, String secondAddress, String category, String description, String valueWorks, String startDate, String endDate, String person, String property,
                       String fName, String lName, String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String phoneType, String mobilePhoneNo,  String landPhone,String receiveSMS, String email,String permit,
                       String declarationOne, String declarationTwo, String declarationThree,String expReviewPage, String expPrice,
                       String expCategoryBack, String expWorksBack, String reviewPageBack, String expPriceBack,
                       String CardNo, String expMonth, String expYear, String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expPdfFormat, String expCertificate, String expDial, String expNotification,
                       String expReceipt, String expSite, String expSupport1, String expSupport2,
                       String expSupport3, String expSupport4, String expSupport5, String expTraffic,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an APP form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Asset Protection")
                        .setOrigin(origin);
                isUserCso = true;
                app = new AssetProtectionPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                app = new AssetProtectionPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
            }
            app.clickNext("1").
                    setFirstAddress(firstAddress).
//                    setMultiCheck(multicheck).
//                    setSecondAddress(secondAddress).
        setCategory(category).
                    setDescription(description).
                    setestimateValue(valueWorks).
                    setStartDate(startDate).
                    setEndDate(endDate);
//            app.uploadMicrochipFile(System.getProperty("user.dir") + "\\TestData\\TestImage1.JPG");
            app.uploadMicrochipFile();
            app.clickNext("2").
                    selectPerson(person);
            if (person.equalsIgnoreCase("Owner")) {
                app.selectProperty(property).
                        setPersonalDetails(fName, lName, postalStreet, postalSuburb, postalState, postalPostCode);

                app.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    app.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    app.setlandPhoneNumber(landPhone);

                }
                    app.setSMSNotifications("Yes");
                    app.setEmail("Reshma.Nair@boroondara.vic.gov.au");
                    app.setPermit("Email")
                            .setDeclarations("Building works will not start until I have received the asset protection permit.")
                            .setDeclarationsTwo("I will keep a clean and safe worksite that meets all permit conditions.")
                            .setDeclarationsThree("I declare I am the applicant and that the contents of this application are true and accurate to the best of my knowledge. I understand that providing false information is an offense and penalties may apply.")
                            .clickNext("3");
                    app.getReviewPage(expReviewPage, expPrice);
                    app.clickPrevious("4");
                    app.clickPrevious("3");
                    app.setCategory("Major impact works");
                    app.setDescription("Text test after review page");
                    app.clickDrain("Yes");
                    app.clickNext("2");
                    app.clickNext("3");
                    app.getReviewPageBack(reviewPageBack, expPriceBack);
                    app.saveForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
//                    Thread.sleep(10000);
                    app.getPaymentDetails();
//                    Thread.sleep(20000);
                    app.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                            expPdfFormat, expCertificate, expDial, expNotification,
                            expReceipt, expSite, expSupport1, expSupport2,
                            expSupport3, expSupport4, expSupport5, expTraffic,
                            expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);


                }
//                app.setSMSNotifications("Yes");
//                app.setEmail("Reshma.Nair@boroondara.vic.gov.au");
//                app.setPermit("Email")
//                        .setDeclarations("The information I have provided is true and correct.")
//                        .setDeclarationsTwo("Building works will not start until I have received the asset protection permit.")
//                        .setDeclarationsThree("I will keep a clean and safe worksite that meets all permit conditions.")
//                        .clickNext("3");
//                app.getReviewPage(expReviewPage, expPrice);
////                app.clickPrevious("4");
////                app.clickPrevious("3");
////                app.setCategory("Major impact works");
////                app.setDescription("Text test after review page");
////                app.clickDrain("No");
////                app.clickNext("2");
////                app.clickNext("3");
////                app.getReviewPageBack(reviewPageBack, expPriceBack);
//                app.saveForm();
//                paymentPage.setCardNo(CardNo);
//                paymentPage.setExpiryMonth(expMonth);
//                paymentPage.setExpiryYear(expYear);
//                paymentPage.clickVerify()
//                        .setCVVNo(CVV)
//                        .clickPay()
//                        .isAppPaymentSuccessful();
////                Thread.sleep(30000);
//                app.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
//                        expPdfFormat, expCertificate, expDial, expNotification,
//                        expReceipt, expSite, expSupport1, expSupport2,
//                        expSupport3, expSupport4, expSupport5, expTraffic,
//                        expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);



            }
        }


        @DataProvider

        public Object[][] APP_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APP");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APP", 64);

            return testObjArray;
        }

    }

