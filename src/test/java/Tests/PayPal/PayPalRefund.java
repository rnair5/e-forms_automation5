package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.NewAssetProtectionPayPalPage;
import pageObjects.forms.PayPalPage;
import pageObjects.forms.PaymentPage;
import pageObjects.forms.RefundBrainTreePage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class PayPalRefund extends BaseClass {
    NewAssetProtectionPayPalPage app;
    PayPalPage pal;
    PaymentPage paymentPage;
    RefundBrainTreePage RBT;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/apply-for-an-asset-protection-permit";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "APPL_DataProvider", description = "Verify that user can submit an APP form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String firstAddress, String multicheck, String secondAddress, String category, String description, String valueWorks, String startDate, String endDate, String person, String property,
                       String fName, String lName, String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String phoneType, String mobilePhoneNo,  String landPhone,String receiveSMS, String email,String permit,
                       String declarationOne, String declarationTwo, String declarationThree,String expReviewPage, String expPrice,
                       String expCategoryBack, String expWorksBack, String reviewPageBack, String expPriceBack,
                       String country, String payPalCard, String paypalExpiry, String payPalCVV,
                       String paypalFN, String payPalLN, String payPalBillingAddress1, String payPalBillingAddress2, String payPalSuburb,
                       String payPalPostCode, String payPalState, String payPalPhone, String payPalEmail,
                       String CardNo, String expMonth, String expYear, String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expAmount, String expTotal, String expTax, String expSurcharge,
                       String expPdfFormat, String expReceipt,  String expSupport,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an APP form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Asset Protection")
                        .setOrigin(origin);
                isUserCso = true;
                app = new NewAssetProtectionPayPalPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                app = new NewAssetProtectionPayPalPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
                pal = new PayPalPage(ChromeDriver);
                RBT = new RefundBrainTreePage(ChromeDriver);
            }
            RBT.refundProcess();



        }
    }


    @DataProvider

    public Object[][] APPL_DataProvider () throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APPL");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APPL", 72);

        return testObjArray;
    }

}



