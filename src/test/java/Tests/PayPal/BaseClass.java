package Tests.PayPal;


import libraries.ExtentReporting;
import libraries.Logg;
import libraries.Utilities;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import pageObjects.forms.Cso;

import java.util.Date;

public class BaseClass {
    public WebDriver chromeDriver;
    public static WebDriver ieDriver;
    public static WebDriver ieDriverCso;
    public static WebDriver ChromeDriver;
    public static WebDriver ChromeDriver1;
    public static WebDriver driver;
    public static WebDriver ChromeDriverCso;
    public static WebDriver FireFoxDriver;
    public static WebDriver FireFoxDriverCso;
    public static WebDriver edgeDriver;
    public static WebDriver edgeDriverCso;
    static Cso csoPage;
    public static long StartTime = new Date().getTime();
    public static long defaultTimeout;
    public static String reportName="ExtentReportResults.html";
    public static String formsTestData="";

    @BeforeSuite
    @Parameters ( { "addPassScreenshot" , "form_url" , "forms_sheet" })
    public void beforeSuite(boolean addScreenshotIfStepPass,String url,String fileName) {
        Logg.configure();
        Logg.logger.info("");
        StartTime = new Date().getTime();
        defaultTimeout = 20;
        ExtentReporting.configureExtentReport(reportName,addScreenshotIfStepPass,url);
        formsTestData=fileName;
    }
    //Creates 2 drivers- one for CSO and one for user execution
    @BeforeTest
    public void beforeTest() {
//          ieDriver = Utilities.createDriver("ie");
//          edgeDriver = Utilities.createDriver("edge");
//          ieDriverCso=Utilities.createDriver("ie");
            ChromeDriver = Utilities.createDriver("chrome");
//        ChromeDriver1 = Utilities.createDriver("chrome");
//        ReportABinPage = Utilities.createDriver("chrome");
//            edgeDriverCso=Utilities.createDriver("edge");
//        csoPage = new Cso(ChromeDriverCso);
//        FireFoxDriver = Utilities.createDriver("firefox");
//        FireFoxDriverCso=Utilities.createDriver("firefox");
//        csoPage = new Cso(FireFoxDriverCso);
    }
//    @AfterTest
//    public void afterTest()
//    {
//        Logg.logger.info("");
//        chromeDriver.quit();
//        ieDriver.quit();
//    }
public WebDriver getWebDriver(String cso)
{
    Logg.logger.info("CSO:"+cso);
    if (cso.equalsIgnoreCase("cso"))
        return edgeDriverCso;
    else
        return ChromeDriver;
}

}
