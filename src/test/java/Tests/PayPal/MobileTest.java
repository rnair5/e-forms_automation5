package Tests.PayPal;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.forms.WPAPage;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

class TestClass1 implements Runnable {
    public void run() {
        Hashtable<String, String> capsHashtable = new Hashtable<String, String>();
        capsHashtable.put("device", "Samsung Galaxy S21 Ultra");
        capsHashtable.put("os_version", "11.0");
        capsHashtable.put("browserName", "android");
        capsHashtable.put("realMobile", "true");
        capsHashtable.put("build", "browserstack-build-1");
        capsHashtable.put("name", "Thread 1");
        MobileTest r1 = new MobileTest();
        r1.executeTest(capsHashtable);
    }
}

class TestClass2 implements Runnable {
    public void run() {
        Hashtable<String, String> capsHashtable = new Hashtable<String, String>();
        capsHashtable.put("device", "Samsung Galaxy Tab S7");
        capsHashtable.put("os_version", "10.0");
        capsHashtable.put("browserName", "android");
        capsHashtable.put("realMobile", "true");
        capsHashtable.put("build", "browserstack-build-1");
        capsHashtable.put("name", "Thread 2");
        MobileTest r2 = new MobileTest();
        r2.executeTest(capsHashtable);
    }
}

class TestClass3 implements Runnable {
    public void run() {
        Hashtable<String, String> capsHashtable = new Hashtable<String, String>();
        capsHashtable.put("device", "iPad Pro 11 2018");
        capsHashtable.put("os_version", "12");
        capsHashtable.put("browserName", "ios");
        capsHashtable.put("realMobile", "true");
        capsHashtable.put("build", "browserstack-build-1");
        capsHashtable.put("name", "Thread 3");
        MobileTest r3 = new MobileTest();
        r3.executeTest(capsHashtable);
    }
}

public class MobileTest extends BaseClass {
    public static final String USERNAME = "reshmanair_f05uza";
    public static final String AUTOMATE_KEY = "zfg4BxGxMtY2XnFjS9tB";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
    public static void main(String[] args) throws Exception {
        Thread object1 = new Thread(new TestClass1());
        object1.start();
        Thread object2 = new Thread(new TestClass2());
        object2.start();
        Thread object3 = new Thread(new TestClass3());
        object3.start();
    }
    public void executeTest(Hashtable<String, String> capsHashtable)  {

        String key;
        String url = "https://service-staging.boroondara.vic.gov.au/written-planning-advice";
        WPAPage rop;
        DesiredCapabilities caps = new DesiredCapabilities();
        // Iterate over the hashtable and set the capabilities
        Set<String> keys = capsHashtable.keySet();
        Iterator<String> itr = keys.iterator();
        while (itr.hasNext()) {
            key = itr.next();
            caps.setCapability(key, capsHashtable.get(key));
        }
        WebDriver driver;
        try {

            driver = new RemoteWebDriver(new URL(URL), caps);
            JavascriptExecutor jse = (JavascriptExecutor)driver;

            driver.get("https://service-staging.boroondara.vic.gov.au/written-planning-advice");
            WebDriverWait wait = new WebDriverWait(driver, 20);
            try {
                wait.until(ExpectedConditions.titleContains("Request written planning advice – Boroondara eForms"));
                jse.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": \"passed\", \"reason\": \"Title matched!\"}}");
            }
            catch(Exception e) {
                jse.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\":\"failed\", \"reason\": \"Title not matched\"}}");
            }
            System.out.println(driver.getTitle());
            //Click the next button: Introduction to Page 2
            WebElement firstNext = driver.findElement(By.xpath("//*[@id=\"gform_next_button_242_1\"]"));
            firstNext.click();
            Thread.sleep(5000);

            //Enter the Address
            WebElement serviceAddress = driver.findElement(By.id("input_242_13"));
            serviceAddress.sendKeys("673 Burke Road");
            WebDriverWait wait1 = new WebDriverWait(driver, 10);
            wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[5]")));
            JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
            jsExecutor.executeScript("window.alert = function(){}");
            jsExecutor.executeScript("window.confirm = function(){return true;}");
            driver.findElement(By.xpath("html/body/div[0]")).click();
            Thread.sleep(5000);



            driver.quit();
        } catch (MalformedURLException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}