package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.ResidentialParkingPermitPage;

import java.awt.*;
import java.text.ParseException;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

;

public class ResidentialParkingPermit extends BaseClass {
    ResidentialParkingPermitPage rpp;
    String url;
    String cso_url;
    @Parameters({"form_url","cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url=url+"/residential-parking-permit";
        this.cso_url = cso_url;
    }
    @Test(dataProvider = "RPP_DataProvider" , description = "Verify that user can submit an rpp form" )

    public void Submit(String tc_name,String cso, String cso_id, String cso_pass, String origin,String address, String propertyType, String propertyOwnerTenant, String pr, String specifiedNo, String visitorNo, String s1Fname, String s1Lname, String s1Rego, String s1IsCommercial, String s2Fname, String s2Lname, String s2Rego, String s2IsCommercial, String s3Fname, String s3Lname, String s3Rego, String s3IsCommercial, String fName, String lName, String email, String phoneType, String phoneNo, String sms, String acceptPolicy, String postalAddressDifferent, String postalStreet, String postalSuburb, String postalState, String postalPostCode) throws InterruptedException, AWTException, ParseException {
        Logg.logger.info("");
        boolean isUserCso=false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a DPP form");

        if(tc_name.equalsIgnoreCase("submit")) {
            if(cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id,cso_pass)
                        .openForm("Residential parking permit")
                        .setOrigin(origin);
                isUserCso = true;
                rpp = new ResidentialParkingPermitPage(edgeDriverCso);
            }
            else {
                loadurl(ChromeDriver, url);
                rpp = new ResidentialParkingPermitPage(ChromeDriver);

            }
            rpp.clickNext("1").
                    setPermitAddress(address).
                    setPropertyType(propertyType).
                    setPropertyOwnerTenant(propertyOwnerTenant).
                    setPR(pr);
            if(isUserCso)
                rpp.setResidentialProofSighted();
            else
//                rpp.uploadResidentialStatus(System.getProperty("user.dir")+"\\TestData\\TestImage1.JPG");
            rpp.uploadResidentialStatus();
            rpp.clickNext("2")
               .setPermits(specifiedNo, visitorNo)
               .setSpecified(s1Fname, s1Lname, s1Rego,  s1IsCommercial, s2Fname, s2Lname, s2Rego,  s2IsCommercial, s3Fname, s3Lname, s3Rego, s3IsCommercial,isUserCso)
                    .uploadFile()
               .clickNext("3")
                    .setPersonalDetails(fName, lName, phoneType, phoneNo, sms);
            if(postalAddressDifferent.equalsIgnoreCase("yes"))
                rpp.setPostalAddress(postalStreet,postalSuburb,postalState,postalPostCode);
            if(isUserCso)
                rpp.setCsoEmail(email)
                   .clickNext("4")
                   .submitForm();
            else
            {
                rpp.setPermitHolderEmail(email)
//                   .setPermitHolderConfEmail(email)
                   .clickNext("4")
//                   .setPolicy(acceptPolicy)
                   .submitForm();
            }
        }
    }
    @DataProvider
    public Object[][] RPP_DataProvider() throws Exception {
        // Setting up the Test Data Excel file
        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"RPP");
        // Fetching the Test Case row number from the Test Data Sheet
        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"RPP",35);
        return testObjArray;
    }
}
