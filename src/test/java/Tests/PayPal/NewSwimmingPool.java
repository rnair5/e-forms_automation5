package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.NewSwimmingPoolPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class NewSwimmingPool extends BaseClass {
    NewSwimmingPoolPage nsp;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/spr2";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "NSP_DataProvider", description = "Verify that user can submit an SPR form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String firstAddress, String regcategory, String regSteps, String date,String bPoolNo, String safetyBarrier, String descField, String outCome, String person, String property,
                       String companyName, String companyABN, String contactName, String corpNumber,
                       String fName, String lName, String email,
                       String phoneType, String mobilePhoneNo, String landPhone, String receiveSMS,
                       String permit, String postalStreet, String postalSuburb, String postalState, String postalPostCode, String country,
                       String declarationOne,String expReviewPage,String regAmount, String CardNo, String expMonth, String expYear,String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation,  String expDoc1, String expDoc2, String expDoc3, String expDoc4,String expCount,
                       String expType, String expBusinessNo,String expAlternate,String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an SPR form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("spr2")
                        .setOrigin(origin);
                isUserCso = true;
                nsp = new NewSwimmingPoolPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                nsp = new NewSwimmingPoolPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
            }
            nsp.clickNext("1").
                    setFirstAddress(firstAddress);
            nsp.setCategory(regcategory);
            nsp.setRegistrationStep(regSteps);
            nsp.uploadMicrochipFileSpa();

//            nsp.setDateDesc(date);
            //A Pool -->Register a Pool-->Property Owner/Building Surveyor
            if (regcategory.equalsIgnoreCase("A pool and spa") && (regSteps.equalsIgnoreCase("Register and lodge a certificate of compliance for a "))) {
                Thread.sleep(7000);
                nsp.uploadMicrochipFileSpa();
                nsp.clickNext("2").
                        setPersonDetails(person);

                if (person.equalsIgnoreCase("Property Owner") || (person.equalsIgnoreCase("Building surveyor"))) {
                    nsp.setCorporatePerson(person);
                    nsp.selectProperty(property);
                    if (property.equalsIgnoreCase("The property is managed by an owners corporation")) {
                        nsp.setCorporationName(companyName);
                        nsp.setCorporationNumber(corpNumber);
                        nsp.setCorporationContactName(contactName);
                        nsp.setCorporationEmail(email);
                        nsp.setPhoneType(phoneType);
                        if (phoneType.equalsIgnoreCase("Mobile")) {
                            nsp.setMobPhoneNumber(mobilePhoneNo);
                        } else {
                            nsp.setlandPhoneNumber(landPhone);

                        }
                        nsp.setSMSNotifications(receiveSMS);
                        nsp.setCorpContactAddressOne(postalStreet);
                        nsp.setCorpContactAddressTwo(postalSuburb);
                        nsp.setCorpContactAddressFour(postalState);
                        nsp.setCorpPostCode(postalPostCode);
                        nsp.setDeclarations(declarationOne);
                        nsp.clickNext("3");
                        nsp.getReviewPage(expReviewPage,regAmount);
//                        nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                        nsp.saveFormNormal();
//                    paymentPage.setCardNo(CardNo);
//                    paymentPage.clickVerify()
//                            .setCVVNo(CVV)
//                            .clickPay()
//                            .isAppPaymentSuccessful();
//                    Thread.sleep(30000);
//                    nsp.getPaymentDetails();
                        nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                                expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                    nsp.readPDF(pdfformat);
                    } else
                        nsp.setPersonalDetails(fName, lName).
                                setEmail(email).
                                setPhoneType(phoneType);
                    if (phoneType.equalsIgnoreCase("Mobile")) {
                        nsp.setMobPhoneNumber(mobilePhoneNo);
                    } else {
                        nsp.setlandPhoneNumber(landPhone);

                    }
                    nsp.setSMSNotifications(receiveSMS);
                    nsp.setPermit(permit);
                    nsp.setAddressOne(postalStreet);
                    nsp.setAddressTwo(postalSuburb);
                    nsp.setAddressFour(postalState);
                    nsp.setPostCode(postalPostCode);
//                    nsp.selectInterAddress(country);
                    nsp.setDeclarations(declarationOne);
                    nsp.clickNext("3");
                    nsp.getReviewPage(expReviewPage,regAmount);
//                    nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                    nsp.saveForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
                    Thread.sleep(30000);
                nsp.getPaymentDetails();
                    nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                            expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                nsp.captureCRMProperty(expcaseType, expSubcase, expCustomerName, expLocation, fName, lName,
//                        expBusinessNo, mobilePhoneNo, landPhone, email, pdfformat, expDoc1, expDoc2, expDoc3);
//                nsp.readPDF(pdfformat);
                } else if (person.equalsIgnoreCase("Agent on behalf of the property owner")) {
//                    nsp.uploadMicrochipFileSpa();
                    nsp.uploadMicrochipFileAuthority();
                    nsp.selectProperty(property);
                    if (property.equalsIgnoreCase("The property is owned by a company")) {
                        nsp.setCompanyName(companyName);
                        nsp.setCompanyABN(companyABN);
                        nsp.setCompanyContactName(contactName);
                        nsp.setCompanyEmail(email);
                        nsp.setPhoneType(phoneType);
                        if (phoneType.equalsIgnoreCase("Mobile")) {
                            nsp.setMobPhoneNumber(mobilePhoneNo);
                        } else {
                            nsp.setlandPhoneNumber(landPhone);

                        }
                        nsp.setSMSNotifications(receiveSMS);
                        nsp.setCompContactAddressOne(postalStreet);
                        nsp.setCompContactAddressTwo(postalSuburb);
                        nsp.setCompContactAddressFour(postalState);
                        nsp.setCompPostCode(postalPostCode);
                        nsp.setDeclarations(declarationOne);
                        nsp.clickNext("3");
                        nsp.getReviewPage(expReviewPage,regAmount);
//                        nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                        nsp.saveForm();
                        paymentPage.setCardNo(CardNo);
                        paymentPage.setExpiryMonth(expMonth);
                        paymentPage.setExpiryYear(expYear);
                        paymentPage.clickVerify()
                                .setCVVNo(CVV)
                                .clickPay()
                                .isAppPaymentSuccessful();
                        Thread.sleep(30000);
                    nsp.getPaymentDetails();
//
                    nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                            expBusinessNo, mobilePhoneNo, expAlternate, fName,companyName ,email,expCaseCount);
//                    nsp.readPDF(pdfformat);
                    } else {
                        nsp.setPersonalDetails(fName, lName).
                                setEmail(email).
                                setPhoneType(phoneType);
                        if (phoneType.equalsIgnoreCase("Mobile")) {
                            nsp.setMobPhoneNumber(mobilePhoneNo);
                        } else {
                            nsp.setlandPhoneNumber(landPhone);

                        }
                        nsp.setSMSNotifications(receiveSMS);
                        nsp.setPermit(permit);
                        nsp.setAddressOne(postalStreet);
                        nsp.setAddressTwo(postalSuburb);
                        nsp.setAddressFour(postalState);
                        nsp.setPostCode(postalPostCode);
                        nsp.setDeclarations(declarationOne);
                        nsp.clickNext("3");
                        nsp.getReviewPage(expReviewPage,regAmount);
//                        nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                        nsp.saveForm();
                        paymentPage.setCardNo(CardNo);
                        paymentPage.setExpiryMonth(expMonth);
                        paymentPage.setExpiryYear(expYear);
                        paymentPage.clickVerify()
                                .setCVVNo(CVV)
                                .clickPay()
                                .isAppPaymentSuccessful();
                        Thread.sleep(30000);
                    nsp.getPaymentDetails();
                    }
                    nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                            expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                    nsp.captureCRMProperty(expcaseType, expSubcase, expCustomerName, expLocation, fName, lName,
//                            expBusinessNo, mobilePhoneNo, landPhone, email, pdfformat, expDoc1, expDoc2, expDoc3);
//                    nsp.readPDF(pdfformat);
                } else if (person.equalsIgnoreCase("Owners corporation (previously called body corporate)")) {
                    nsp.uploadMicrochipFileAuthority();
                    nsp.setCorporationName(companyName);
                    nsp.setCorporationNumber(corpNumber);
                    nsp.setCorporationContactName(contactName);
                    nsp.setCorporationEmail(email);
                    nsp.setPhoneType(phoneType);
                    if (phoneType.equalsIgnoreCase("Mobile")) {
                        nsp.setMobPhoneNumber(mobilePhoneNo);
                    } else {
                        nsp.setlandPhoneNumber(landPhone);

                    }
                    nsp.setSMSNotifications(receiveSMS);
                    nsp.setCorpContactAddressOne(postalStreet);
                    nsp.setCorpContactAddressTwo(postalSuburb);
                    nsp.setCorpContactAddressFour(postalState);
                    nsp.setCorpPostCode(postalPostCode);
                    nsp.setDeclarations(declarationOne);
                    nsp.clickNext("3");
                    nsp.getReviewPage(expReviewPage,regAmount);
//                    nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                    nsp.saveFormNormal();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
                    Thread.sleep(30000);
                nsp.getPaymentDetails();
                    nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                            expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                nsp.readPDF(pdfformat);


                }
            }

            }
            if (regcategory.equalsIgnoreCase("A Pool")) {
                nsp.setDateDesc(date);
                nsp.uploadMicrochipFile();
                nsp.setBarrierDetails(safetyBarrier);
                nsp.setDescField(descField);
                nsp.uploadMicrochipFileSecond();
            } else {
                nsp.setBPoolNo(bPoolNo);
                nsp.setBarrierDetails(safetyBarrier);
                nsp.setDescField(descField);
                nsp.uploadMicrochipFileSecond();
                nsp.setInspection(outCome);
                if (outCome.equalsIgnoreCase("Compliant")) {
                    nsp.uploadMicrochipFileSpa();
                } else
                    nsp.uploadMicrochipFileNonCompliance();
            }
            nsp.clickNext("2").
                    setPersonDetails(person);

            if (person.equalsIgnoreCase("Property Owner") || (person.equalsIgnoreCase("Building surveyor"))) {
                nsp.setCorporatePerson(person);
                nsp.selectProperty(property);
                if(property.equalsIgnoreCase("The property is managed by an owners corporation")){
                    nsp.setCorporationName(companyName);
                    nsp.setCorporationNumber(corpNumber);
                    nsp.setCorporationContactName(contactName);
                    nsp.setCorporationEmail(email);
                    nsp.setPhoneType(phoneType);
                    if (phoneType.equalsIgnoreCase("Mobile")) {
                        nsp.setMobPhoneNumber(mobilePhoneNo);
                    } else {
                        nsp.setlandPhoneNumber(landPhone);

                    }
                    nsp.setSMSNotifications(receiveSMS);
                    nsp.setCorpContactAddressOne(postalStreet);
                    nsp.setCorpContactAddressTwo(postalSuburb);
                    nsp.setCorpContactAddressFour(postalState);
                    nsp.setCorpPostCode(postalPostCode);
                    nsp.setDeclarations(declarationOne);
                    nsp.clickNext("3");
                    nsp.getReviewPage(expReviewPage,regAmount);
//                    nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                    nsp.saveForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
                    Thread.sleep(30000);
                    nsp.getPaymentDetails();
                    nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                            expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                    nsp.captureCRMOwnersCorp(expcaseType, expSubcase, expCustomerName, expLocation, fName, lName,
//                            expBusinessNo, mobilePhoneNo, landPhone, email, pdfformat, expDoc1, expDoc2, expDoc3);
//                    nsp.readPDF(pdfformat);
                }else
                nsp.setPersonalDetails(fName, lName).
                        setEmail(email).
                        setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    nsp.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    nsp.setlandPhoneNumber(landPhone);

                }
                nsp.setSMSNotifications(receiveSMS);
                nsp.setPermit(permit);
                nsp.setAddressOne(postalStreet);
                nsp.setAddressTwo(postalSuburb);
                nsp.setAddressFour(postalState);
                nsp.setPostCode(postalPostCode);
                nsp.setDeclarations(declarationOne);
                nsp.clickNext("3");
                nsp.getReviewPage(expReviewPage,regAmount);
//                nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                nsp.saveForm();
                paymentPage.setCardNo(CardNo);
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear(expYear);
                paymentPage.clickVerify()
                        .setCVVNo(CVV)
                        .clickPay()
                        .isAppPaymentSuccessful();
                Thread.sleep(30000);
                nsp.getPaymentDetails();
                nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                        expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                nsp.captureCRMProperty(expcaseType, expSubcase, expCustomerName, expLocation, fName, lName,
//                        expBusinessNo, mobilePhoneNo, landPhone, email, pdfformat, expDoc1, expDoc2, expDoc3);
//                nsp.readPDF(pdfformat);
            } else if (person.equalsIgnoreCase("Agent on behalf of the property owner")) {
                nsp.uploadMicrochipFileAuthority();
                nsp.selectProperty(property);
                if (property.equalsIgnoreCase("The property is owned by a company")) {
                    nsp.setCompanyName(companyName);
                    nsp.setCompanyABN(companyABN);
                    nsp.setCompanyContactName(contactName);
                    nsp.setCompanyEmail(email);
                    nsp.setPhoneType(phoneType);
                    if (phoneType.equalsIgnoreCase("Mobile")) {
                        nsp.setMobPhoneNumber(mobilePhoneNo);
                    } else {
                        nsp.setlandPhoneNumber(landPhone);

                    }
                    nsp.setSMSNotifications(receiveSMS);
                    nsp.setCompContactAddressOne(postalStreet);
                    nsp.setCompContactAddressTwo(postalSuburb);
                    nsp.setCompContactAddressFour(postalState);
                    nsp.setCompPostCode(postalPostCode);
                    nsp.setDeclarations(declarationOne);
                    nsp.clickNext("3");
                    nsp.getReviewPage(expReviewPage,regAmount);
//                    nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                    nsp.saveForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
                    Thread.sleep(30000);
                    nsp.getPaymentDetails();
                    nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                            expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
////
//                    nsp.captureCRMProperty(expcaseType, expSubcase, expCustomerName, expLocation, fName, lName,
//                            expBusinessNo, mobilePhoneNo, landPhone, email, pdfformat, expDoc1, expDoc2, expDoc3);
//                    nsp.readPDF(pdfformat);
                } else {
                    nsp.setPersonalDetails(fName, lName).
                            setEmail(email).
                            setPhoneType(phoneType);
                    if (phoneType.equalsIgnoreCase("Mobile")) {
                        nsp.setMobPhoneNumber(mobilePhoneNo);
                    } else {
                        nsp.setlandPhoneNumber(landPhone);

                    }
                    nsp.setSMSNotifications(receiveSMS);
                    nsp.setPermit(permit);
                    nsp.setAddressOne(postalStreet);
                    nsp.setAddressTwo(postalSuburb);
                    nsp.setAddressFour(postalState);
                    nsp.setPostCode(postalPostCode);
                    nsp.setDeclarations(declarationOne);
                    nsp.clickNext("3");
                    nsp.getReviewPage(expReviewPage,regAmount);
//                    nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                    nsp.saveForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
                    Thread.sleep(30000);
                    nsp.getPaymentDetails();
                    nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                            expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
                }
//                nsp.captureCRMProperty(expcaseType, expSubcase, expCustomerName, expLocation, fName, lName,
//                        expBusinessNo, mobilePhoneNo, landPhone, email, pdfformat, expDoc1, expDoc2, expDoc3);
//                nsp.readPDF(pdfformat);
            } else if (person.equalsIgnoreCase("Owners corporation (previously called body corporate)")) {
                nsp.uploadMicrochipFileAuthority();
                nsp.setCorporationName(companyName);
                nsp.setCorporationNumber(corpNumber);
                nsp.setCorporationContactName(contactName);
                nsp.setCorporationEmail(email);
                nsp.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    nsp.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    nsp.setlandPhoneNumber(landPhone);

                }
                nsp.setSMSNotifications(receiveSMS);
                nsp.setCorpContactAddressOne(postalStreet);
                nsp.setCorpContactAddressTwo(postalSuburb);
                nsp.setCorpContactAddressFour(postalState);
                nsp.setCorpPostCode(postalPostCode);
                nsp.setDeclarations(declarationOne);
                nsp.clickNext("3");
                nsp.getReviewPage(expReviewPage,regAmount);
//                nsp.compareReviewPage(regcategory, regSteps, firstAddress);
                nsp.saveForm();
                paymentPage.setCardNo(CardNo);
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear(expYear);
                paymentPage.clickVerify()
                        .setCVVNo(CVV)
                        .clickPay()
                        .isAppPaymentSuccessful();
                Thread.sleep(30000);
                nsp.getPaymentDetails()
//                nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
//                        expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
                .captureCRMCorp(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                        expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                nsp.readPDF(pdfformat);


            }
        }


    


    @DataProvider

    public Object[][] NSP_DataProvider () throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "NSP");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "NSP", 54);

        return testObjArray;
    }

}


