package Tests.PayPal;

import libraries.Assert;
import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.WasteCollectionPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class WasteCollection extends BaseClass {
    WasteCollectionPage wcp;
    String url;
    String cso_url;


    @Parameters({"form_url","cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url=url+"/book-a-waste-collection";
        this.cso_url = cso_url;
    }
    @Test(dataProvider = "waste_DataProvider" , description = "Verify that user can submit a waste collection form" )
    public void Submit(String tc_name,String cso, String cso_id, String cso_pass, String origin,
                       String address, String wasteType, String extraWaste, String reason,String otherReason, String urgentRoute, String collectionDate, String cornerBlock, String pickupInstructions,
                       String fname, String lname, String email, String phType, String phoneNo, String receivSms,String expReviewPage, String acceptPolicy,
                       String expCase, String expSubCase, String expCustomer, String expLocation, String expType, String expBusiness, String expAlternate, String expCaseCount) throws Exception {
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a waste collection form");
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a Waste Collection form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Book a waste collection")
                        .setOrigin(origin);
                isUserCso = true;
                wcp = new WasteCollectionPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                wcp = new WasteCollectionPage(ChromeDriver);
            }

            wcp.clickNext("1")
                    .setCollectionAddress(address)
                    .selectWasteType(wasteType);
            if (wasteType.equalsIgnoreCase("Green waste")) {
                wcp.setGreenWasteLabel();
            }
            if (wasteType.equalsIgnoreCase("Christmas Tree")) {
                wcp.setChristmasWasteLabel();
            }

            if (wasteType.equalsIgnoreCase("Hard waste")) {
                if (extraWaste.equalsIgnoreCase("yes")) {
                    if (isUserCso)
                        Assert.assertEquals("Each Boroondara property is eligible for two hard waste collections per financial year. The property you have selected exceeds this amount. If you believe you are eligible for further collections, please complete the reason below.", wcp.getErrorMsg(), "Error message received:" + wcp.getErrorMsg(), "Error message for extra hard waste collection not shown", getWebDriver(cso));
                    else
                        Assert.assertEquals("Each Boroondara property is eligible for two hard waste collections per financial year. The property you have selected exceeds this amount. If you believe you are eligible for further collections, please complete the reason below.", wcp.getErrorMsg(), "Error message received:" + wcp.getErrorMsg(), "Error message for extra hard waste collection not shown", getWebDriver(cso));
                    wcp.setReason(reason, otherReason);
                }
                if (urgentRoute.equalsIgnoreCase("yes") && isUserCso)
                    wcp.setUrgentRoute();
                wcp.selectDate(Integer.parseInt(collectionDate))
                        .setCornerBlock(cornerBlock);
                if (cornerBlock.equalsIgnoreCase("yes"))
                    wcp.setCornerBlockInst(pickupInstructions);
                else
                    wcp.setInstructions(pickupInstructions);

                wcp.clickNext("2").setPersonalDetails(fname, lname, phType, phoneNo, receivSms);
                if (isUserCso)
                    wcp.setCsoEmail(email)
                            .clickNext("3")
//                            .setWasteLabel(acceptPolicy)

                            .submitForm();
                else
                    wcp.setEmail(email)
                            .clickNext("3")
                            .setWasteLabel(acceptPolicy)
                            .setPolicy(acceptPolicy)
                    .getReviewPage(expReviewPage)
                            .submitForm()
                    .captureCRMIndividual(expCase,expSubCase,expCustomer,expLocation,expType,expBusiness,phoneNo,expAlternate,fname,lname,email,expCaseCount);

            }
        }


    }
    @DataProvider

    public Object[][] waste_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"Waste");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"Waste",30);

        return testObjArray;
    }

}
