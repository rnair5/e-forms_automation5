package Tests.PayPal;

import libraries.Assert;
import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.DisabledParkingPermitPage;

import java.text.ParseException;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class DisabledParkingPermit extends BaseClass {
    DisabledParkingPermitPage dpp;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/renew-a-disabled-parking-permit";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "DPP_DataProvider", description = "Verify that user can submit a DPP form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String permitNo, String permitValid, String permitAddress,
                       String isCurrentAddress, String currentAddress,
                       String isPostalAddress, String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String isAgent, String agentFname, String agentLname, String agentRelationship,
                       String permitHolderFname, String permitHolderLname, String permitHolderDOB,
                       String permitHolderEmail, String permitHolderConfEmail, String permitHolderPhType, String permitHolderPhone,
                       String receiveSMS, String acceptPolicy) throws InterruptedException, ParseException {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a DPP form");

        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Renew a Disability Parking Permit")
                        .setOrigin(origin);
                isUserCso = true;
                dpp = new DisabledParkingPermitPage(ieDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                dpp = new DisabledParkingPermitPage(ChromeDriver);

            }
            dpp.clickNext("1");
            if (!permitValid.equalsIgnoreCase("yes")) {
                Assert.assertFalse(dpp.isDppValid(permitNo), "Permit number is not valid because " + dpp.getErrorMsg(), "Permit number is valid", getWebDriver(cso));
                Assert.assertTrue(dpp.getErrorMsg().contains(permitValid), "Permit number is not valid because " + dpp.getErrorMsg(), "Permit number is valid", getWebDriver(cso));
            } else {
                Assert.assertTrue(dpp.isDppValid(permitNo), "Permit number is valid", "Permit number is not valid because " + dpp.getErrorMsg(), getWebDriver(cso));


                dpp.clickNext("2")
                        .setPermitAddress(permitAddress)
                        .setIsCurrentAddress(isCurrentAddress)
                        .setCurrentAddress(currentAddress)
                        .setIsPostalAddress(isPostalAddress)
                        .setPostalAddress(postalStreet, postalSuburb, postalState, postalPostCode)
                        .clickNext("3")
                        .setIsCustomerAgent(isAgent)
                        .setAgentDetails(agentFname, agentLname, agentRelationship)
                        .setPermitHolderDetails(permitHolderFname, permitHolderLname, permitHolderDOB, permitHolderPhType, permitHolderPhone, receiveSMS);
                if (cso.equalsIgnoreCase("cso")) {
                    dpp.setCsoEmail(permitHolderEmail)
                            .clickNext("4");
                } else {  if(tc_name.equalsIgnoreCase("submit")) {
                    dpp.setPermitHolderEmail(permitHolderEmail)
//                            .setPermitHolderConfEmail(permitHolderConfEmail)
                            .clickNext("4");
                            dpp.submitForm();
                }

                }
//                dpp.submitForm();
            }
        }
    }

    @DataProvider
    public Object[][] DPP_DataProvider() throws Exception {
        // Setting up the Test Data Excel file
        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//"+formsTestData, "DPP");
        // Fetching the Test Case row number from the Test Data Sheet
        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "DPP", 28);
        return testObjArray;
    }
}
