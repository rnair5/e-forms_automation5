package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.PaymentPage;
import pageObjects.forms.SWIPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class SWI extends BaseClass {
    SWIPage rop;
    PaymentPage paymentPage;
    String url;
    String cso_url;


    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/inspection-for-an-osd-system";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "SWI_DataProvider", description = "Verify that user can submit an SWI Form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String firstAddress, String refNumber, String osdDetection,String worksDesc, String user, String otherDesc,
                       String applicant,String companyName,String companyABN,
                       String postalStreet, String postalSuburb, String postalState, String postalPostCode,String country,
                       String fname, String lname, String email, String phoneType, String mobilePhoneNo, String landPhoneNo,
                       String receivSms, String declaration, String expReviewPage,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expDetection, String expRequest, String expSupport,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a waste collection form");
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an SPR form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("road opening permit")
                        .setOrigin(origin);
                isUserCso = true;
                rop = new SWIPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                rop = new SWIPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);

            }

            rop.clickNext("1")
                    .setAddress(firstAddress);
            rop.setReference(refNumber);
            rop.setDetection(osdDetection);
            rop.setWorkInformation(worksDesc);
            rop.uploadDetentionSystem();
            rop.uploadSupporting();
            rop.clickNext("2");
            Thread.sleep(5000);
            rop.setUserType(user);
            rop.setOtherInformation(otherDesc);
            rop.setPersonType(applicant);
            if (applicant.equalsIgnoreCase("Yes")) {
                rop.setCompanyName(companyName);
                rop.setCompanyABN(companyABN);
                rop.setCorpContactAddressOne(postalStreet);
                rop.setCorpContactAddressTwo(postalSuburb);
//                rop.setCorpContactAddressFour(postalState);
                rop.setCorpPostCode(postalPostCode);
                rop.setPersonalDetails(fname, lname, phoneType);
                rop.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    rop.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    rop.setlandPhoneNumber(landPhoneNo);

                }
                rop.setSMSNotifications(receivSms);
                rop.setEmail(email);
                rop.setDeclarations(declaration);
                Thread.sleep(3000);
                rop.clickNext("3");
                rop.getReviewPage(expReviewPage);
                rop.submitForm();
                rop.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                        expDetection, expRequest, expSupport,
                        expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fname, expCustomerName, email, expCaseCount);
//
//                    rop.getPrinterVersionDetails();
//
//                } else {
//                    rop.setPersonalDetails(fname, lname, phoneType);
//                    rop.setPhoneType(phoneType);
//                    if (phoneType.equalsIgnoreCase("Mobile")) {
//                        rop.setMobPhoneNumber(mobilePhoneNo);
//                    } else {
//                        rop.setlandPhoneNumber(landPhoneNo);
//
//                    }
//
//                    rop.setSMSNotifications(receivSms);
//                    rop.setPersonAddressOne(postalStreet);
//                    rop.setPersonAddressTwo(postalSuburb);
//                    rop.setPersonPostCode(postalPostCode);
//                    rop.setEmail(email);
//                    rop.setDeclarations(declaration);
//                    rop.clickNext("3");
//                    rop.getReviewPage(expReviewPage, price);
//                    rop.submitForm();
//                    paymentPage.setCardNo(CardNo);
//                    paymentPage.setExpiryMonth(expMonth);
//                    paymentPage.setExpiryYear(expYear);
//                    paymentPage.clickVerify()
//                            .setCVVNo(CVV)
//                            .clickPay()
//                            .isAppPaymentSuccessful();
//                    Thread.sleep(10000);
//                    rop.getPaymentDetails();
//
//                    rop.captureCRMIndividual(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
//                            expPdfFormat, expCertificate, expDial, expNotification,
//                            expReceipt, expSite, expSupport1, expSupport2,
//                            expSupport3, expSupport4, expSupport5, expTraffic,
//                            expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fname, lname, email, expCaseCount);
//                    rop.getFeedback();
////                rop.readPDF(expPdf,expPdfText);
//
//
//                    Thread.sleep(10000);
//
//            }
            }
        }
    }
    @DataProvider

    public Object[][] SWI_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"SWI");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"SWI",43);

        return testObjArray;
    }

}
