package Tests.PayPal;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.PaymentPage;
import pageObjects.forms.TNQPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class TNQ extends BaseClass {
    TNQPage nsp;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/tree-enquiry";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "TNQ_DataProvider", description = "Verify that user can submit a Tree Enquiry form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String address, String enquiry, String detailsEnquiry, String userType, String propertyType,
                       String companyName,String companyABN,String contactName,

                       String fname, String lname, String email, String phoneType, String mobilePhoneNo, String landPhoneNo,
                       String receiveSMS, String mailingType, String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String declaration, String expReviewPage,String price,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expPdfFormat, String expCertificate, String expDial, String expNotification,
                       String expReceipt, String expSite, String expSupport1, String expSupport2,
                       String expSupport3, String expSupport4, String expSupport5, String expTraffic,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an SPR form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("TNQ")
                        .setOrigin(origin);
                isUserCso = true;
                nsp = new TNQPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                nsp = new TNQPage(ChromeDriver);

            }

            nsp.clickNext("1").
                    setFirstAddress(address);
            nsp.setEnquiry(enquiry);
            nsp.setEnquiryDetail(detailsEnquiry);
            nsp.uploadSupporting();
            nsp.clickNext("2").


            setPersonDetails(userType);
            if (userType.equalsIgnoreCase("Property owner") || (userType.equalsIgnoreCase("Agent on behalf of the owner"))) {
                nsp.selectAgentDetails(propertyType);

            }
            if (propertyType.equalsIgnoreCase("The Property is owned by a company") || (propertyType.equalsIgnoreCase("I work for a company that is acting for the owner"))) {

                nsp.setCompanyName(companyName);
                nsp.setCompanyABN(companyABN);
                nsp.setCompanyContactName(contactName);
                nsp.setCompanyEmail(email);
                nsp.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    nsp.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    nsp.setlandPhoneNumber(landPhoneNo);

                }
                nsp.setSMSNotifications(receiveSMS);
                nsp.setMailingType(mailingType);
                nsp.setCompContactAddressOne(postalStreet);
                nsp.setCompContactAddressTwo(postalSuburb);
                nsp.setCompContactAddressFour(postalState);
                nsp.setCompPostCode(postalPostCode);
                nsp.setDeclarations(declaration);
                nsp.clickNext("3");
                nsp.getReviewPage(expReviewPage, price);
                nsp.saveForm();

                Thread.sleep(30000);

//
//                nsp.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1, expDoc2, expDoc3, expDoc4, expCount, expType,
//                        expBusinessNo, mobilePhoneNo, expAlternate, fName, companyName, email, expCaseCount);
//                    nsp.readPDF(pdfformat);
            }
        }


    }

    @DataProvider

    public Object[][] TNQ_DataProvider () throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "TNQ");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "TNQ", 52);

        return testObjArray;
    }

}


