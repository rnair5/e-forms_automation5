package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.OSDPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class OSD extends BaseClass {
    OSDPage sec;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/get-approval-for-an-on-site-detention-osd-system";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "OSD_DataProvider", description = "Verify that user can submit an OSD form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String address, String multiCheck, String legalApproval, String propertyType, String multiType,
                       String permitType, String permitNumber, String additional,
                       String person,  String company, String ABN,
                       String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String ownerName, String ownerFamily, String ownerEmail,
                       String phoneType, String mobilePhoneNo,  String landPhone,String receiveSMS,
                       String declarationOne,String expReviewPage, String expPrice,
                       String CardNo, String expMonth, String expYear, String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expPdfFormat, String expCertificate, String expDrainComputation, String expPlans,String expSupporting,
                     String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an LPD form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("PAW")
                        .setOrigin(origin);
                isUserCso = true;
                sec = new OSDPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                sec = new OSDPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
            }
            Thread.sleep(7000);
            sec.clickNextOne().

                    setFirstAddress(address).
                    setLegalType(legalApproval).
                    setPropertyType(propertyType).
                    setMultiUnitType(multiType).
                    setPermit(permitType).
                    setPermitNumber(permitNumber).
                    setCAdditionalComments(additional).
                    uploadDrainage().
                    uploadComputation().
                    uploadCertificate().
                    uploadSupporting();
            Thread.sleep(7000);
            sec.clickNext("2").
                    //RegEx- 0
//                    selectPerson(person).
//                    setCompanyName("0").
//                    setABN("0").
//                    setAddressOne(postalStreet).
//                    setAddressTwo(postalSuburb).
////                    setAddressFour(postalState).
//                    setPostCode(postalPostCode).
//                    setOwnerFName("0").
//                    setOwnerLName("0").
//                    setEmail("0").
//                    setPhoneType(phoneType);
//            sec.setMobPhoneNumber("0");
//            sec.setSMSNotifications(receiveSMS);
//            sec.setDeclarations(declarationOne).
                    //Actual
                    selectPerson(person);
            if(person.equalsIgnoreCase("Yes")) {
                sec.setCompanyName(company).
                        setABN(ABN).
                        setAddressOne(postalStreet).
                        setAddressTwo(postalSuburb).
//                    setAddressFour(postalState).
        setPostCode(postalPostCode).
                        setOwnerFName(ownerName).
                        setOwnerLName(ownerFamily).
                        setEmail(ownerEmail).
                        setPhoneType(phoneType);
                sec.setMobPhoneNumber(mobilePhoneNo);
                sec.setSMSNotifications(receiveSMS);
                sec.setDeclarations(declarationOne);
            }else{
                sec.setOwnerFName(ownerName).
                        setOwnerLName(ownerFamily).
                        setEmail(ownerEmail).
                        setPhoneType(phoneType);
                sec.setMobPhoneNumber(mobilePhoneNo);
                sec.setSMSNotifications(receiveSMS);
                sec.setPersonAddressOne(postalStreet);
                        sec.setPersonAddressTwo(postalSuburb);
                sec.setPersonPostCode(postalPostCode);
                sec.setDeclarations(declarationOne);
            }
            sec.clickNext("3");
                    sec.getReviewPage(expReviewPage, expPrice);
                    sec.saveForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
                    Thread.sleep(10000);
                    sec.getPaymentDetails();
                    Thread.sleep(20000);
                    sec.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                            expPdfFormat, expCertificate, expDrainComputation, expPlans,expSupporting,
                    expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, company,ownerEmail, expCaseCount);



//                sec.setSMSNotifications("Yes");
//                sec.setEmail("Reshma.Nair@boroondara.vic.gov.au");
//                sec.setPermit("Email")
//                        .setDeclarations("The information I have provided is true and correct.")
//                        .setDeclarationsTwo("Building works will not start until I have received the asset protection permit.")
//                        .setDeclarationsThree("I will keep a clean and safe worksite that meets all permit conditions.")
//                        .clickNext("3");
//                sec.getReviewPage(expReviewPage, expPrice);
////                sec.clickPrevious("4");
////                sec.clickPrevious("3");
////                sec.setCategory("Major impact works");
////                sec.setDescription("Text test after review page");
////                sec.clickDrain("No");
////                sec.clickNext("2");
////                sec.clickNext("3");
////                sec.getReviewPageBack(reviewPageBack, expPriceBack);
//                sec.saveForm();
//                paymentPage.setCardNo(CardNo);
//                paymentPage.setExpiryMonth(expMonth);
//                paymentPage.setExpiryYear(expYear);
//                paymentPage.clickVerify()
//                        .setCVVNo(CVV)
//                        .clickPay()
//                        .issecPaymentSuccessful();
////                Thread.sleep(30000);
//                sec.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
//                        expPdfFormat, expCertificate, expDial, expNotification,
//                        expReceipt, expSite, expSupport1, expSupport2,
//                        expSupport3, expSupport4, expSupport5, expTraffic,
//                        expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);



            }
        }


        @DataProvider

        public Object[][] OSD_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "OSD");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "OSD", 51);

            return testObjArray;
        }

    }

