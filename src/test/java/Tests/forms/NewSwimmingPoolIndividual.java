package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.NewSwimmingPoolPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class NewSwimmingPoolIndividual extends BaseClass {
    NewSwimmingPoolPage nsp;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/spr2";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "NSPINDI_DataProvider", description = "Verify that user can submit an SPR form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String firstAddress, String regcategory, String regSteps, String date,String bPoolNo, String safetyBarrier, String descField, String outCome, String person, String property,
                       String companyName, String companyABN, String contactName, String corpNumber,
                       String fName, String lName, String email,
                       String phoneType, String mobilePhoneNo, String landPhone, String receiveSMS,
                       String permit, String postalStreet, String postalSuburb, String postalState, String postalPostCode, String country,
                       String declarationOne,String expReviewPage,String regAmount, String CardNo, String expMonth, String expYear,String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation,  String expDoc1, String expDoc2, String expDoc3, String expDoc4,String expCount,
                       String expType, String expBusinessNo,String expAlternate,String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an SPR form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("spr2")
                        .setOrigin(origin);
                isUserCso = true;
                nsp = new NewSwimmingPoolPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                nsp = new NewSwimmingPoolPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
            }
            nsp.clickNext("1").
                    setFirstAddress(firstAddress);
            nsp.setCategory(regcategory);
            nsp.setRegistrationStep(regSteps);
                nsp.setDateDesc(date);
                nsp.uploadMicrochipFile();
                nsp.setBarrierDetails(safetyBarrier);
                nsp.setDescField(descField);
                nsp.uploadMicrochipFileSecond();
                Thread.sleep(10000);
            nsp.clickNext("2").
                    setPersonDetails(person);
                nsp.setCorporatePerson(person);
                nsp.selectProperty(property);
                nsp.setPersonalDetails(fName, lName).
                        setEmail(email).
                        setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    nsp.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    nsp.setlandPhoneNumber(landPhone);

                }
                nsp.setSMSNotifications(receiveSMS);
                nsp.setPermit(permit);
                nsp.setAddressOne(postalStreet);
                nsp.setAddressTwo(postalSuburb);
                nsp.setAddressFour(postalState);
                nsp.setPostCode(postalPostCode);
                nsp.setDeclarations(declarationOne);
                nsp.clickNext("3");
                nsp.getReviewPage(expReviewPage,regAmount);
                nsp.saveForm();
                paymentPage.setCardNo(CardNo);
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear(expYear);
                paymentPage.clickVerify()
                        .setCVVNo(CVV)
                        .clickPay()
                        .isAppPaymentSuccessful();
                Thread.sleep(30000);
                nsp.getPaymentDetails();
                nsp.captureCRMIndividual(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                        expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                nsp.readPDF(pdfformat);


            }
        }



    @DataProvider

    public Object[][] NSPINDI_DataProvider () throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "NSPINDI");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "NSPINDI", 54);

        return testObjArray;
    }

}


