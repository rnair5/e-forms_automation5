package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.ReportAnIssuePage;

import static libraries.ExtentReporting.*;
import static libraries.Utilities.loadurl;


public class ReportAnIssueWaste extends BaseClass {
    ReportAnIssuePage rai;
    PDFBoxReadFromFile Pdf;
    String url;
    String cso_url;



    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/report-an-issue";
        this.cso_url = cso_url;

    }

    @Test(dataProvider = "rai_DataProvider", description = "Verify that user can submit an RAI form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String issueType, String personType, String subType, String subQuestion, String binType, String describeIssue,
                       String tPark, String addressName, String enterAddress, String outsideClick, String describeLoc,
                       String fName, String lName, String orgName,
                       String email, String phoneType, String mobilePhoneNo, String landPhoneNo,
                       String receiveSMS, String pdfformat, String expReviewPageOne, String expcaseType,String expSubcase,
                       String expCustomerName,String expInstructions, String expSupporting, String expPdfFormat,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;

            extent_SetTestCaseRegCoverageForReports("Verify that user can submit an rai form");
            if(tc_name.equalsIgnoreCase("submit")) {
                if(cso.equalsIgnoreCase("cso")) {
                    loadurl(edgeDriverCso, cso_url);
                    csoPage.clickFormsLogin()
                            .csoLogin(cso_id,cso_pass)
                            .openForm("Report an Issue")
                            .setOrigin(origin);
                    isUserCso = true;
                    rai = new ReportAnIssuePage(edgeDriverCso);
                }
                else {
                    loadurl(ChromeDriver, url);
                    rai = new ReportAnIssuePage(ChromeDriver);

                }
            rai.clickNext("1").
                    setIssueType(issueType).
                    setUserType(personType).
                    setTreeType(subType).
                    setLocType(subQuestion).
                    setBinType(binType);
                rai.uploadHeadlessDocument();
//            rai.uploadMicrochipFile(System.getProperty("user.dir") + "\\TestData\\TestImage1.JPG");
//                    rai.uploadMicrochipFile(System.getProperty("user.dir") + "\\TestData\\TestImage1.JPG");
//                    rai.uploadMicrochipFile(System.getProperty("user.dir") + "\\TestData\\TestImage1.JPG");
//                    rai.uploadMicrochipFile(System.getProperty("user.dir") + "\\TestData\\TestImage1.JPG");
//                    rai.uploadMicrochipFile(System.getProperty("user.dir") + "\\TestData\\TestImage1.JPG");
                    rai.getMaxLabel();
                    rai.setIssueDesc(describeIssue).
                    clickNext("2");
            if (issueType.equalsIgnoreCase("Trees")) {
                rai.setPark(tPark).
                        setLocDesc(describeLoc);
            } else if (issueType.equalsIgnoreCase("Council buildings, sportsgrounds and facilities")) {
                rai.setBuildingName(addressName).
                        setCouncilLocDesc(describeLoc);
            } else if(issueType.equalsIgnoreCase("Street cleaning and toilets")){
                rai.setLocDesc(describeLoc);
            }else if(issueType.equalsIgnoreCase("Parks and playgrounds")){
                rai.setParkLocDesc(describeLoc);
            }

//                rai.setCurrentLocation();
//                rai.setMapLocation();
//                rai.setInvalidAddress(enterAddress);
                rai.setAddress("45 Suffolk Road Surrey");

                        rai.clickNext("3").
                        setPersonalDetails(fName, lName, phoneType);
                if (issueType.equalsIgnoreCase("Council buildings, sportsgrounds and facilities")) {
                    rai.setOrgName(orgName);
                }

                rai.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    rai.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    rai.setlandPhoneNumber(landPhoneNo);

                }
                rai.setSMSNotifications(receiveSMS);
                if (isUserCso) {
                    rai.setCsoEmail(email)

                            .clickNext("4")

                            .submitForm();
                } else {
                    rai.setEmail(email)
//                        .setConfEmail(confEmail)
                            .clickNext("4")
                            .getReviewPage(pdfformat)
                            .clickPrevious("5")
                            .clickPrevious("4")
                            .clickPrevious("3")
                            .setIssueType("Drains and pits")
                            .clickDrainIssue("No")
                            .clickNext("2")
                            .clickDrainLocation("On Council land, such as a naturestrip or park")
                            .clickNext("3")
                            .setDrainageLocDesc(describeLoc);
                            Thread.sleep(10000);
                            rai.clickNext("4");
                            Thread.sleep(10000);
                            rai.compareReviewPageOne(expReviewPageOne);
                    //Repeat the same Steps
                    rai.clickPrevious("5");
                    rai.clickPrevious("4");
                    rai.clickPrevious("3")
                            .setIssueType(issueType)
                            .setUserType(personType)
                            .setTreeType(subType)
                            .setLocType(subQuestion)
                            .setBinType(binType);
                    rai.setIssueDesc(describeIssue).
                            clickNext("2");

                        rai.setBuildingName(addressName).
                                setCouncilLocDesc(describeLoc);


                    rai.clickNext("3");
                    Thread.sleep(10000);
                            rai.setPersonalDetails(fName, lName, phoneType);
                    if (issueType.equalsIgnoreCase("Council buildings, sportsgrounds and facilities")) {
                        rai.setOrgName(orgName);
                    }

                    rai.setPhoneType(phoneType);
                    if (phoneType.equalsIgnoreCase("Mobile")) {
                        rai.setMobPhoneNumber(mobilePhoneNo);
                    } else {
                        rai.setlandPhoneNumber(landPhoneNo);

                    }
                    rai.setSMSNotifications(receiveSMS);
                    if (isUserCso) {
                        rai.setCsoEmail(email)

                                .clickNext("4")

                                .submitForm();
                    } else {
                        rai.setEmail(email)
//                        .setConfEmail(confEmail)
                                .clickNext("4")
                                .getReviewPage(pdfformat);

//                            .compareReviewPage();
                        rai.submitForm();
                        if (issueType.equalsIgnoreCase("Council buildings, sportsgrounds and facilities")) {
                            rai.captureCRM(expcaseType, expSubcase, expCustomerName, expInstructions, expSupporting,expPdfFormat,
                                    expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, expCustomerName, email, expCaseCount);
//                        rai.readPDF(pdfformat, issueType, personType, subType, subQuestion, binType, describeIssue, tPark, addressName, enterAddress, describeLoc,
//                                fName, lName, orgName, email, mobilePhoneNo, landPhoneNo);
                        }

//

//

                    }

                }
            }

        }


        @DataProvider

        public Object[][] rai_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "rai");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "rai", 39);

            return testObjArray;
        }

    }
