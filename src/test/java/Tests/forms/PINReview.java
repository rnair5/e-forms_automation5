package Tests.forms;

import libraries.Assert;
import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.*;

import java.awt.*;
import java.text.ParseException;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class PINReview extends BaseClass {
    PINReviewPage parkingReview;
    String url;
    String cso_url;


    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/review-my-parking-fine";

        this.cso_url = cso_url;
    }

    @Test(dataProvider = "PIN_DataProvider", description = "Verify that user can submit a PIN collection form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String vehicleNo, String infringementNo, String groundsDropdown, String specialDropdown, String additionalInfo,
                       String fname, String lname, String email, String phType, String phoneNo, String manual,
                       String postalStreet, String postalSuburb, String postalState, String postalPostCode, String country, String receivSms,
                       String expReviewPage, String expCase, String expSubCase, String expCustomer,String expInstructions,
                       String expDoc1, String expDoc2, String expType, String expBusinessNo, String expAlternate,
                       String expCaseCount) throws Exception {
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a waste collection form");
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an SPR form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Review my parking fine")
                        .setOrigin(origin);
                isUserCso = true;
                parkingReview = new PINReviewPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                parkingReview = new PINReviewPage(ChromeDriver);

            }

            parkingReview.clickNext("1")
                    .setVehicleNo(vehicleNo)
                    .setInfringementNo(infringementNo)
                    .selectGroundsDropdown(groundsDropdown);
            if (groundsDropdown.equalsIgnoreCase("Special Circumstances")) {
                parkingReview.selectSpecialDropdown(specialDropdown);
            } else
                parkingReview.setInst(additionalInfo);
//            parkingReview.uploadMicrochipFileSpecial(System.getProperty("user.dir") + "\\TestData\\TestImage1.JPG");
            parkingReview.uploadMicrochipFileSpecial();
            parkingReview.clickNext("2")
                    .setPersonalDetails(fname, lname, phType, phoneNo, receivSms);
            parkingReview.setEmail(email)
                    .setManualAddressCheck(manual)
                    .setCorpContactAddressOne(postalStreet)
                    .setCorpContactAddressTwo(postalSuburb)
                    .setCorpContactAddressFour(postalState)
                    .setCorpPostCode(postalPostCode)
                    .selectInterAddress(country)
                    .clickNext("3")
                    .getReviewPage(expReviewPage)
                    .submitForm()
                    .captureCRM(expCase,expSubCase,expCustomer,infringementNo,expInstructions,expDoc1,expDoc2,expType,expBusinessNo,phoneNo,expAlternate,fname,lname,email);

        }
    }

    @DataProvider

    public Object[][] PIN_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"PIN");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"PIN",33);

        return testObjArray;
    }

}
