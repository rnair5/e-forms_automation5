package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.ManualPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class ManualCase extends BaseClass {
    ManualPage man;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/public-authority-works-notice";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "MAN_DataProvider", description = "Verify that user can create a manual CRM CASE")
    public void Submit(
                       String expSubCase, String expOrigin, String expCustomerName, String expLocation, String expInstructions, String expCase,
                       String expAddress, String SLATimer, String firstResponse, String slaStatus
                   ) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a PAW form");

                man = new ManualPage(ChromeDriver);
                man.captureCRM(expSubCase, expOrigin, expCustomerName, expLocation, expInstructions, expCase, expAddress,
                        SLATimer, firstResponse, slaStatus);
            }




        @DataProvider

        public Object[][] MAN_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "MAN");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "MAN", 10);

            return testObjArray;
        }

    }

