package Tests.forms;

import libraries.Assert;
import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.*;

import java.awt.*;
import java.text.ParseException;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class RoadOpeningPermit extends BaseClass {
    RoadOpeningPermitPage rop;
    PaymentPage paymentPage;
    String url;
    String cso_url;


    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/road-opening-permit";

        this.cso_url = cso_url;
    }

    @Test(dataProvider = "ROP_DataProvider", description = "Verify that user can submit an ROP form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin, String expIntro,
                       String firstAddress, String worksInformation, String sizeHole, String locationWorks, String speedLimit,
                       String startDate, String endDate,String reinstatement, String applicant,String companyName,String companyABN,
                       String postalStreet, String postalSuburb, String postalState, String postalPostCode,String country,
                       String fname, String lname, String email, String phoneType, String mobilePhoneNo, String landPhoneNo,
                       String receivSms, String declaration, String expReviewPage,String price,
                       String sizeHoleback, String locationWorksBack, String expReviewback, String expPriceBack,
                       String CardNo,String expMonth, String expYear, String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expPdfFormat, String expCertificate, String expDial, String expNotification,
                       String expReceipt, String expSite, String expSupport1, String expSupport2,
                       String expSupport3, String expSupport4, String expSupport5, String expTraffic,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a Road Opening Permit Form");
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an ROP form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("road opening permit")
                        .setOrigin(origin);
                isUserCso = true;
                rop = new RoadOpeningPermitPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                rop = new RoadOpeningPermitPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);

            }
            rop.getIntroPage(expIntro);
            rop.clickNext("1")
                    .setInvalidAddress("United States Of America")
                    .setAddress(firstAddress)
                    .setWorkInformation(worksInformation)
                    .setHoleSize(sizeHole)
                    .setHoleLocation(locationWorks)
                    .setSpeedLimit(speedLimit)
                    .selectStartDate(startDate);
            Thread.sleep(5000);
            rop.selectEndDate(endDate);
            Thread.sleep(5000);
            rop.setReinstatement(reinstatement);
            rop.uploadHeadlessDocument();
            rop.clickNext("2");
            Thread.sleep(10000);
            rop.setPersonType(applicant);
            if (applicant.equalsIgnoreCase("Yes")) {
                rop.setCompanyName(companyName);
                rop.setCompanyABN("1234567890");
                rop.setCorpContactAddressOne(postalStreet);
                rop.setCorpContactAddressTwo(postalSuburb);
                rop.setCorpContactAddressFour(postalState);
                rop.setCorpPostCode(postalPostCode);
                rop.setPersonalDetails(fname, lname, phoneType);
                rop.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    rop.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    rop.setlandPhoneNumber(landPhoneNo);

                }
                rop.setSMSNotifications(receivSms);
                rop.setEmail(email);
                rop.setDeclarations(declaration);
                rop.clickNext("3");
                rop.setInvalidABN();
                rop.setCompanyABN(companyABN);
                rop.setMobPhoneNumber("0061406361552");
                rop.clickNext("3");
                rop.setInvalidPhone();
                rop.setMobPhoneNumber(mobilePhoneNo);
                rop.setSMSNotifications(receivSms);
                rop.clickNext("3");
                rop.getReviewPage(expReviewPage, price);
                rop.clickPrevious("4");
                Thread.sleep(9000);
                rop.clickPrevious("3");
                rop.setWorkInformation(sizeHoleback);
                rop.setHoleLocation(locationWorksBack);
                Thread.sleep(5000);
                rop.clickNext("2");
                Thread.sleep(3000);
                rop.clickNext("3");
                Thread.sleep(3000);
                rop.getReviewPageBack(expReviewback, expPriceBack);
                rop.submitForm();
                paymentPage.setCardNo(CardNo);
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear(expYear);
                paymentPage.clickVerify()
                        .setCVVNo(CVV)
                        .clickDifferentPayment();
                paymentPage.setCardNo("371449635398431");
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear(expYear);
                paymentPage.clickVerify()
                        .setCVVNo(CVV)
                        .clickPay();
                rop.isPaymentDeclined();
                rop.submitPaymentAgain();
                rop.submitForm();
                paymentPage.setCardNo("37144963539843");
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear(expYear);
                paymentPage.clickVerify();
                rop.setInvalidCard();
                paymentPage.setCardNo(CardNo);
                paymentPage.setExpiryMonth("21");
                paymentPage.setExpiryYear(expYear);
                Thread.sleep(7000);
                paymentPage.clickVerify();
                rop.setInvalidExpiryMonth();
                paymentPage.setCardNo(CardNo);
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear("12");
                paymentPage.clickVerify();
                rop.setInvalidExpiryYear();
                paymentPage.setCardNo(CardNo);
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear(expYear);

                paymentPage.clickVerify();
                paymentPage.setCVVNo(CVV);
                paymentPage.clickPay();
                paymentPage.isAppPaymentSuccessful();
                Thread.sleep(10000);
//                rop.getPaymentDetails();
//                Thread.sleep(30000);
//                rop.browserBack();
//                Thread.sleep(10000);
//                paymentPage.setCardNo(CardNo);
//                paymentPage.setExpiryMonth(expMonth);
//                paymentPage.setExpiryYear(expYear);
//
//                paymentPage.clickVerify();
//                paymentPage.setCVVNo(CVV);
//                paymentPage.clickPay();
////                rop.sessionEnd();
//                paymentPage.isAppPaymentSuccessful();
//                rop.submitForm();
//                rop.sessionEnd();

//
//                //TRIAL FROM HERE
//
//                rop.getIntroPage(expIntro);
//                rop.clickNext("1")
//                        .setInvalidAddress("United States Of America")
//                        .setAddress(firstAddress)
//                        .setWorkInformation(worksInformation)
//                        .setHoleSize(sizeHole)
//                        .setHoleLocation(locationWorks)
//                        .setSpeedLimit(speedLimit)
//                        .selectStartDate(startDate);
//                Thread.sleep(5000);
//                rop.selectEndDate(endDate);
//                Thread.sleep(5000);
//                rop.setReinstatement(reinstatement);
//                rop.uploadHeadlessDocument();
//
////                rop.uploadSitePlan(System.getProperty("user.dir") + "\\TestData\\TestPNG.PNG");
////                rop.uploadCurrency(System.getProperty("user.dir") + "\\TestData\\TestPNG.PNG");
////                rop.uploadDial(System.getProperty("user.dir") + "\\TestData\\TestDOC.doc");
////                rop.uploadTraffic(System.getProperty("user.dir") + "\\TestData\\TestPNG.PNG");
////                rop.uploadLetter(System.getProperty("user.dir") + "\\TestData\\TestPNG.PNG");
////                Thread.sleep(5000);
////                rop.uploadSupportingDocument(System.getProperty("user.dir") + "\\TestData\\TestImage1.JPG");
////                Thread.sleep(5000);
////                rop.uploadSupportingDocument(System.getProperty("user.dir") + "\\TestData\\TestPNG.PNG");
////                Thread.sleep(5000);
////                rop.uploadSupportingDocument(System.getProperty("user.dir") + "\\TestData\\TestDOC.doc");
////                Thread.sleep(5000);
////                rop.uploadSupportingDocument(System.getProperty("user.dir") + "\\TestData\\TestDOCX.docx");
////                Thread.sleep(5000);
////                rop.uploadSupportingDocument(System.getProperty("user.dir") + "\\TestData\\TestJPEG.jpeg");
////                Thread.sleep(10000);
//
//
//                rop.clickNext("2");
//                rop.setPersonType(applicant);
//                    rop.setCompanyName(companyName);
//                    rop.setCompanyABN("1234567890");
//                    rop.setCorpContactAddressOne(postalStreet);
//                    rop.setCorpContactAddressTwo(postalSuburb);
//                    rop.setCorpContactAddressFour(postalState);
//                    rop.setCorpPostCode(postalPostCode);
//                    rop.setPersonalDetails(fname, lname, phoneType);
//                    rop.setPhoneType(phoneType);
//                    if (phoneType.equalsIgnoreCase("Mobile")) {
//                        rop.setMobPhoneNumber(mobilePhoneNo);
//                    } else {
//                        rop.setlandPhoneNumber(landPhoneNo);
//
//                    }
//                    rop.setSMSNotifications(receivSms);
//                    rop.setEmail(email);
//                    rop.setDeclarations(declaration);
//                    rop.clickNext("3");
//                    rop.setInvalidABN();
//                    rop.setCompanyABN(companyABN);
//                    rop.setMobPhoneNumber("0061406361552");
//                    rop.clickNext("3");
//                    rop.setInvalidPhone();
//                    rop.setMobPhoneNumber(mobilePhoneNo);
//                    rop.setSMSNotifications(receivSms);
//                    rop.clickNext("3");
//                    rop.getReviewPage(expReviewPage, price);
//                    rop.clickPrevious("4");
//                    rop.clickPrevious("3");
//                    rop.setWorkInformation(sizeHoleback);
//                    rop.setHoleLocation(locationWorksBack);
//                    rop.clickNext("2");
//                    Thread.sleep(3000);
//                    rop.clickNext("3");
//                    Thread.sleep(3000);
//                    rop.getReviewPageBack(expReviewback, expPriceBack);
//                    rop.submitForm();
//                    paymentPage.setCardNo(CardNo);
//                    paymentPage.setExpiryMonth(expMonth);
//                    paymentPage.setExpiryYear(expYear);
//                    paymentPage.clickVerify()
//                            .setCVVNo(CVV)
//                            .clickPay();
//                    paymentPage.isAppPaymentSuccessful();
//                Thread.sleep(30000);
//                    rop.getPaymentDetails();
                    rop.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                            expPdfFormat, expCertificate, expDial, expNotification,
                            expReceipt, expSite, expSupport1, expSupport2,
                            expSupport3, expSupport4, expSupport5, expTraffic,
                            expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fname, expCustomerName, email, expCaseCount);
//
//                    rop.getPrinterVersionDetails();

                } else {
                    rop.setPersonalDetails(fname, lname, phoneType);
                    rop.setPhoneType(phoneType);
                    if (phoneType.equalsIgnoreCase("Mobile")) {
                        rop.setMobPhoneNumber(mobilePhoneNo);
                    } else {
                        rop.setlandPhoneNumber(landPhoneNo);

                    }

                    rop.setSMSNotifications(receivSms);
                    rop.setPersonAddressOne(postalStreet);
                    rop.setPersonAddressTwo(postalSuburb);
                    rop.setPersonPostCode(postalPostCode);
                    rop.setEmail(email);
                    rop.setDeclarations(declaration);
                    rop.clickNext("3");
                    rop.getReviewPage(expReviewPage, price);
                    rop.submitForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
                    Thread.sleep(10000);
                    rop.getPaymentDetails();

                    rop.captureCRMIndividual(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                            expPdfFormat, expCertificate, expDial, expNotification,
                            expReceipt, expSite, expSupport1, expSupport2,
                            expSupport3, expSupport4, expSupport5, expTraffic,
                            expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fname, lname, email, expCaseCount);
//                    rop.getFeedback();
//                    rop.getPrinterVersionDetails();
//                rop.readPDF(expPdf,expPdfText);


                    Thread.sleep(10000);

            }
        }
    }

    @DataProvider

    public Object[][] ROP_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"ROP");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"ROP",64);

        return testObjArray;
    }

}
