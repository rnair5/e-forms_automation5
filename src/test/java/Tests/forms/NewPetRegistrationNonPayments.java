package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.NewPetRegistrationPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class NewPetRegistrationNonPayments extends BaseClass {
    NewPetRegistrationPage npr;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url","cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url=url+"/pet-registration";
        this.cso_url = cso_url;
    }
    @Test(dataProvider = "npr_DataProvider" , description = "Verify that user can submit a npr form" )
    public void Submit(String tc_name,String cso, String cso_id, String cso_pass, String origin,
                       String petAddress, String petName,String petType, String primaryBreed,String petColor,String petAge,String petGender, String deRegister,String microNumber,String desexed, String anotherCouncil,String councilName,String menacing, String dangerous,String obedient, String club,String specialServies, String pensioner, String pensionCard,
                       String fName, String lName,
                       String email, String phType, String phoneNo,
                       String receiveSMS,String postalAddressDifferent, String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String expReviewPage, String expPrice,
                       String CardNo,String expMonth, String expYear, String CVV, String expcaseType, String expSubcase,String expCustomerName, String expLocation,
                       String expCertificate,String expReceipt,String expPdfFormat,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText ) throws Exception {
        Logg.logger.info("");
        boolean isUserCso=false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a npr form");
        if(tc_name.equalsIgnoreCase("submit")) {
            if(cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id,cso_pass)
                        .openForm("Pet registration")
                        .setOrigin(origin);
                isUserCso = true;
                npr = new NewPetRegistrationPage(edgeDriverCso);
            }
            else {
                loadurl(ChromeDriver, url);
                npr = new NewPetRegistrationPage(ChromeDriver);
                paymentPage= new PaymentPage(ChromeDriver);
            }

            npr.clickNext1().
//            npr.clickNext("1").
                    setPetAddress(petAddress).
                    setPetName(petName);
            npr.clickNext2().
//                    clickNext("2").
                    setPetType(petType).
                    setPrimaryBreed(primaryBreed).
                    setPetColor(petColor).
                    setPetAge(petAge).
                    setPetGender(petGender).
                    setDeregisterAnimals(deRegister);
            npr.clickNext3().
//                    clickNext("3").
                    setMicrochipNo(microNumber).
                    setDesexed(desexed).
                    setRegisteredOtherCouncil(anotherCouncil);
            if(anotherCouncil.equalsIgnoreCase("yes"))  npr.setCouncil(councilName);
            if(petType.equalsIgnoreCase("dog"))
            {
                npr.setMenacing(menacing)
                        .setDangerous(dangerous)
                        .setObedienceTrained(obedient)
                        .setSpecialServices(specialServies);
            }
            npr.setOtherClub(club)
                    .setPension(pensioner);
            if(pensioner.equalsIgnoreCase("yes"))
                npr.setPensionCard(pensionCard);
            if(isUserCso)
                npr.checkUploadBox();
            else
                npr.uploadMicrochipFile(System.getProperty("user.dir")+"\\TestData\\TestImage1.JPG");
            npr.clickNext4();
//            npr.clickNext("4")
                   npr.setPersonalDetails(fName,lName,phType,phoneNo,receiveSMS);
            if(postalAddressDifferent.equalsIgnoreCase("yes"))
                npr.setPostalAddress(postalStreet,postalSuburb,postalState,postalPostCode);

            if(isUserCso) {
                npr.setCsoEmail(email)
                        .clickNext("5")
                        .submitForm();
            } else {
                npr.setEmail(email);
                npr.clickNext5()
                        .getReviewPage(expReviewPage,expPrice);
                        npr.submitForm();

//                Thread.sleep(30000);
//                npr.captureCRM(expcaseType,expSubcase,expCustomerName,expLocation,expCertificate,expReceipt,expPdfFormat,expCount,expType,
//                        expBusinessNo,phoneNo, expAlternateNo, fName,lName,email,expCaseCount);


            }
        }
    }
    @DataProvider

    public Object[][] npr_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"npr");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"npr",55);

        return testObjArray;
    }

}
