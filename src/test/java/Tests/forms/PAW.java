package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.PAWPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class PAW extends BaseClass {
    PAWPage app;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/public-authority-works-notice";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "PAW_DataProvider", description = "Verify that user can submit a PAW form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String address, String intersection, String compass, String holeSize, String dimensions, String speedLimit,
                       String descWorks, String startDate, String endDate, String approx,
                       String reinstate, String descReinstate, String category, String whatWorks, String who, String person, String reference, String company, String ABN,
                       String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String ownerName, String ownerFamily, String ownerEmail,
                       String phoneType, String mobilePhoneNo,  String landPhone,String receiveSMS,
                       String manager, String managerCompany, String managerName, String managerFamily,String managerEmail, String managerNumber,
                       String declarationOne,String expReviewPage, String expPrice,
                       String CardNo, String expMonth, String expYear, String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expPdfFormat, String expCertificate,
                       String expReceipt, String expSite, String expSupport1, String expTraffic,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a PAW form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("PAW")
                        .setOrigin(origin);
                isUserCso = true;
                app = new PAWPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                app = new PAWPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
            }

            app.clickNext("1").
//               movePointer().
                    setAddress(address).
//                    movePointer().
                    setIntersection(intersection).
                    setCompass(compass).
                    setHoleSize(holeSize).
                    setDimensions(dimensions).
                    setSpeedLimit(speedLimit).
                    setWorksDescription(descWorks).
                    setStartDate(startDate).
                    setEndDate(endDate).
                    setApproxDate(approx).
                    setReinstatement(reinstate).
                    enterReinstatement(descReinstate).
                    uploadSitePlan().
                    uploadCurrency().
                    uploadTraffic().
                    uploadSupporting();
            app.clickNext("2").
                    selectPerson(category).
                    setOtherWorks(whatWorks).
                    selectProperty(who);
            if (who.equalsIgnoreCase("Applicant on behalf of the public authority"))
            {
                app.setWorking(person);
            }
                    app.setReferenceNumber(reference).
                    setCompanyName(company).
                    setABN(ABN).
                    setAddressOne(postalStreet).
                    setAddressTwo(postalSuburb).
                    setAddressFour(postalState).
                    setPostCode(postalPostCode).
                    setOwnerFName(ownerName).
                    setOwnerLName(ownerFamily).
                    setEmail(ownerEmail).
                    setPhoneType(phoneType);
            app.setMobPhoneNumber(mobilePhoneNo);
                    app.setManager(manager).
                    setManagerCompany(managerCompany).
                    setManagerName(managerName).
                    setManagerFamily(managerFamily).
                    setManagerEmail(managerEmail).
                    setManagerNumber(managerNumber).
                    setDeclarations(declarationOne);
            app.clickNext("3");
                    app.getReviewPage(expReviewPage, expPrice);
                    app.saveForm();
                    paymentPage.setCardNo(CardNo);
                    paymentPage.setExpiryMonth(expMonth);
                    paymentPage.setExpiryYear(expYear);
                    paymentPage.clickVerify()
                            .setCVVNo(CVV)
                            .clickPay()
                            .isAppPaymentSuccessful();
                    Thread.sleep(10000);
                    app.getPaymentDetails();
//                    Thread.sleep(20000);
                    app.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                            expPdfFormat, expCertificate,
                            expReceipt, expSite, expSupport1, expTraffic,
                            expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, company,ownerEmail, expCaseCount);



//                app.setSMSNotifications("Yes");
//                app.setEmail("Reshma.Nair@boroondara.vic.gov.au");
//                app.setPermit("Email")
//                        .setDeclarations("The information I have provided is true and correct.")
//                        .setDeclarationsTwo("Building works will not start until I have received the asset protection permit.")
//                        .setDeclarationsThree("I will keep a clean and safe worksite that meets all permit conditions.")
//                        .clickNext("3");
//                app.getReviewPage(expReviewPage, expPrice);
////                app.clickPrevious("4");
////                app.clickPrevious("3");
////                app.setCategory("Major impact works");
////                app.setDescription("Text test after review page");
////                app.clickDrain("No");
////                app.clickNext("2");
////                app.clickNext("3");
////                app.getReviewPageBack(reviewPageBack, expPriceBack);
//                app.saveForm();
//                paymentPage.setCardNo(CardNo);
//                paymentPage.setExpiryMonth(expMonth);
//                paymentPage.setExpiryYear(expYear);
//                paymentPage.clickVerify()
//                        .setCVVNo(CVV)
//                        .clickPay()
//                        .isAppPaymentSuccessful();
////                Thread.sleep(30000);
//                app.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
//                        expPdfFormat, expCertificate, expDial, expNotification,
//                        expReceipt, expSite, expSupport1, expSupport2,
//                        expSupport3, expSupport4, expSupport5, expTraffic,
//                        expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);



            }
        }


        @DataProvider

        public Object[][] PAW_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "PAW");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "PAW", 66);

            return testObjArray;
        }

    }

