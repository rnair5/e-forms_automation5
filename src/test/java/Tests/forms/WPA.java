package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.PaymentPage;
import pageObjects.forms.WPAPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class WPA extends BaseClass {
    WPAPage rop;
    PaymentPage paymentPage;
    String url;
    String cso_url;


    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/written-planning-advice";

        this.cso_url = cso_url;
    }

    @Test(dataProvider = "WPA_DataProvider", description = "Verify that user can submit a WPA form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin,
                       String firstAddress, String multiCheck, String addressInformation, String proposalInfo,
                       String fname, String lname, String email, String phoneType, String mobilePhoneNo, String landPhoneNo,
                       String receivSms, String companyName, String expReviewPage,String price,
                       String CardNo,String expMonth, String expYear, String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expPdfFormat, String expCertificate,
                       String expReceipt, String expSite, String expSupport1, String expTraffic,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf) throws Exception {
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a WPA form");
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a WPA form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("WPA")
                        .setOrigin(origin);
                isUserCso = true;
                rop = new WPAPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                rop = new WPAPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);

            }
            rop.clickNext("1")
                    .setAddress(firstAddress)
                    .setMultiCheck(multiCheck)
                    .setAddressInformation(addressInformation)
                    .setProposalInformation(proposalInfo)
                    .uploadSupporting()
                    .uploadCertificate();
            rop.clickNext("2");
            Thread.sleep(7000);
                rop.setPersonalDetails(fname, lname, phoneType);
                rop.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    rop.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    rop.setlandPhoneNumber(landPhoneNo);

                }
                rop.setSMSNotifications(receivSms);
                rop.setEmail(email);
                rop.setPostalType();
                rop.setCompanyName(companyName);
                rop.clickNext("3");
                Thread.sleep(3000);
                rop.getReviewPage(expReviewPage, price);
                Thread.sleep(3000);
                rop.submitForm();
                paymentPage.setCardNo(CardNo);
                paymentPage.setExpiryMonth(expMonth);
                paymentPage.setExpiryYear(expYear);
                paymentPage.clickVerify()
                        .setCVVNo(CVV)
                        .clickPay();
                paymentPage.isAppPaymentSuccessful();
                Thread.sleep(20000);
                    rop.captureCRMOrg(expcaseType, expSubcase, expCustomerName, expLocation,
                            expPdfFormat, expCertificate,
                            expReceipt, expSite, expSupport1,
                            expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fname, lname, email, expCaseCount);
//
//                    rop.getPrinterVersionDetails();




        }
    }

    @DataProvider

    public Object[][] WPA_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"WPA");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"WPA",40);

        return testObjArray;
    }

}
