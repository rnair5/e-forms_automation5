package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.OOHPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class OOHPermits extends BaseClass {
    OOHPage ooh;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url","cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url=url+"/out-of-hours-works-permit";
        this.cso_url = cso_url;
    }
    @Test(dataProvider = "OOHP_DataProvider" , description = "Verify that user can submit an OOH form" )
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin, String workOutside, String permitNo, String condition,
                       String firstAddress, String checkMulti, String additionalAddress, String descWorks,String outOfHours, String dateDesc, String descField, String person,
                       String companyName, String companyABN, String postalStreet, String postalSuburb, String postalState, String postalPostCode, String country,
                       String fName, String lName, String email,
                       String phoneType, String mobilePhoneNo, String landPhone, String receiveSMS,
                       String FirstName, String LastName, String Number,
                       String declaration,String expReviewPage,String regAmount, String CardNo, String expMonth, String expYear,String CVV,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation,  String expDoc1, String expDoc2, String expDoc3, String expDoc4,String expCount,
                       String expType, String expBusinessNo,String expAlternate,String expCaseCount,
                       String expPdf, String expPdfText ) throws Exception {
        Logg.logger.info("");
        boolean isUserCso=false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit a npr form");
        if(tc_name.equalsIgnoreCase("submit")) {
            if(cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id,cso_pass)
                        .openForm("Pet registration")
                        .setOrigin(origin);
                isUserCso = true;
                ooh = new OOHPage(edgeDriverCso);
            }
            else {
                loadurl(ChromeDriver, url);
                ooh = new OOHPage(ChromeDriver);
                paymentPage= new PaymentPage(ChromeDriver);
            }

            ooh.clickNext("1").
                    setWorkOutside(workOutside)
                    .setAddress(firstAddress)
                    .checkMultipleAddress(checkMulti)
                    .setMultipleAddress(additionalAddress)
                    .setWorksEntry(descWorks)
                    .setOOHEntry(outOfHours)
                    .setDateEntry(dateDesc)
                    .uploadLetters()
                    .uploadActivities()
                    .setScaleEntry(descField)
                    .uploadTraffic()
                    .uploadSupporting();
            ooh.clickNext("2").
                    setPersonType(person);
            ooh.setFName(fName);
            ooh.setLName(lName);
            ooh.setEmail(email);
            ooh.setPhoneType(phoneType);
            if (phoneType.equalsIgnoreCase("Mobile")) {
                ooh.setMobPhoneNumber(mobilePhoneNo);
            } else {
                ooh.setlandPhoneNumber(landPhone);
            }
            ooh.setSMSNotifications(receiveSMS);
            ooh.setSiteFName(FirstName);
            ooh.setSiteLName(LastName);
            ooh.setSiteNumber(Number);
            ooh.setDeclarations(declaration);
            ooh.clickNext("3");
            ooh.getReviewPage(expReviewPage,regAmount);
            ooh.submitForm();
            paymentPage.setCardNo(CardNo);
            paymentPage.setExpiryMonth(expMonth);
            paymentPage.setExpiryYear(expYear);
            paymentPage.clickVerify()
                    .setCVVNo(CVV)
                    .clickPay()
                    .isAppPaymentSuccessful();
            Thread.sleep(30000);
            ooh.getPaymentDetails();
//
            ooh.captureCRMIndividual(expcaseType, expSubcase, expCustomerName, expLocation, expDoc1,expDoc2,expDoc3,expDoc4,expCount,expType,
                    expBusinessNo, mobilePhoneNo, expAlternate, fName,lName ,email,expCaseCount);
//                    ooh.readPDF(pdfformat);
        }
    }




    @DataProvider

    public Object[][] OOHP_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"OOHP");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"OOHP",55);

        return testObjArray;
    }

}
