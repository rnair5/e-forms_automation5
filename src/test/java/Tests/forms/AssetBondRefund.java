package Tests.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.forms.AssetBondRefundPage;
import pageObjects.forms.PaymentPage;

import static libraries.ExtentReporting.extent_SetTestCaseRegCoverageForReports;
import static libraries.Utilities.loadurl;

public class AssetBondRefund extends BaseClass {
    AssetBondRefundPage apr;
    PaymentPage paymentPage;
    String url;
    String cso_url;

    @Parameters({"form_url", "cso_url"})
    @BeforeClass
    //Called before running all tests in this class
    public void BeforeTest(String url, String cso_url) throws Exception {
        Logg.logger.info("");
        this.url = url + "/apply-for-an-asset-protection-bond-refund";
        this.cso_url = cso_url;
    }

    @Test(dataProvider = "APR_DataProvider", description = "Verify that user can submit an APR form")
    public void Submit(String tc_name, String cso, String cso_id, String cso_pass, String origin, String category,
                       String firstAddress, String multicheck, String secondAddress, String permitNumber, String choice1, String choice2, String choice3,
                       String choice4,String choice5,String choice6,String choice7,String choice8,String choice9,String declaration1,String declaration2,
                       String person, String property,
                       String fName, String lName, String postalStreet, String postalSuburb, String postalState, String postalPostCode,
                       String phoneType, String mobilePhoneNo,  String landPhone,String receiveSMS, String email,String bankName,
                       String accountName, String BSB, String accountNumber,String expReviewPage,
                       String expcaseType, String expSubcase,String expCustomerName, String expLocation, String expInstructions,
                       String expPdfFormat, String expCertificate, String expDial, String expNotification,
                       String expReceipt, String expSite, String expSupport1, String expSupport2,
                       String expSupport3, String expSupport4, String expSupport5, String expTraffic,
                       String expCount, String expType,String expBusinessNo, String expAlternateNo, String expCaseCount,
                       String expPdf, String expPdfText) throws Exception {
        Logg.logger.info("");
        boolean isUserCso = false;
        extent_SetTestCaseRegCoverageForReports("Verify that user can submit an APR form");
        if (tc_name.equalsIgnoreCase("submit")) {
            if (cso.equalsIgnoreCase("cso")) {
                loadurl(edgeDriverCso, cso_url);
                csoPage.clickFormsLogin()
                        .csoLogin(cso_id, cso_pass)
                        .openForm("Asset Protection")
                        .setOrigin(origin);
                isUserCso = true;
                apr = new AssetBondRefundPage(edgeDriverCso);
            } else {
                loadurl(ChromeDriver, url);
                apr = new AssetBondRefundPage(ChromeDriver);
                paymentPage = new PaymentPage(ChromeDriver);
            }
            Thread.sleep(3000);
            apr.clickFirstPage();
//            apr.clickNext("1");
                    apr.setChoice(category).
                    setFirstAddress(firstAddress).
                    setPermitNumber(permitNumber);
//                    setMultiCheck(multicheck).
//                    setSecondAddress(secondAddress)
            apr.clickNext("2");
                    Thread.sleep(5000);
                    apr.setChoice1(choice1).
                    setChoice2(choice2).
                    setChoice3(choice3).
                    setChoice4(choice4).
                    setChoice5(choice5).
                    setChoice6(choice6).
                    setChoice7(choice7).
                    setChoice8(choice8).
                    setChoice9(choice9).
                            setAdditionalComments("Testing").
                    setDeclarations(declaration1).
                    setDeclarationsTwo(declaration2).
                     setDeclarationsThree("I declare I am the applicant and that the contents of this application are true and accurate to the best of my knowledge. I understand that providing false information is an offense and penalties may apply.")  ;
            apr.clickNext("3");
            Thread.sleep(5000);
                    apr.selectPerson(person);
            if (person.equalsIgnoreCase("Owner")) {
                apr.selectProperty(property).
                        setPersonalDetails(fName, lName, postalStreet, postalSuburb, postalState, postalPostCode);

                apr.setPhoneType(phoneType);
                if (phoneType.equalsIgnoreCase("Mobile")) {
                    apr.setMobPhoneNumber(mobilePhoneNo);
                } else {
                    apr.setlandPhoneNumber(landPhone);

                }
                    apr.setSMSNotifications("Yes");
                    apr.setEmail("Reshma.Nair@boroondara.vic.gov.au");
                            apr.setBankName(bankName)
                            .setAccountName(accountName)
                            .setBSB(BSB)
                            .setAccountNumber(accountNumber);
                Thread.sleep(5000);
                apr.clickNext("4");
                    apr.getReviewPage(expReviewPage);
                    apr.saveForm();
                    apr.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
                            expPdfFormat, expCertificate, expDial, expNotification,
                            expReceipt, expSite, expSupport1, expSupport2,
                            expSupport3, expSupport4, expSupport5, expTraffic,
                            expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);


                }
//                apr.setSMSNotifications("Yes");
//                apr.setEmail("Reshma.Nair@boroondara.vic.gov.au");
//                apr.setPermit("Email")
//                        .setDeclarations("The information I have provided is true and correct.")
//                        .setDeclarationsTwo("Building works will not start until I have received the asset protection permit.")
//                        .setDeclarationsThree("I will keep a clean and safe worksite that meets all permit conditions.")
//                        .clickNext("3");
//                apr.getReviewPage(expReviewPage, expPrice);
////                apr.clickPrevious("4");
////                apr.clickPrevious("3");
////                apr.setCategory("Major impact works");
////                apr.setDescription("Text test after review page");
////                apr.clickDrain("No");
////                apr.clickNext("2");
////                apr.clickNext("3");
////                apr.getReviewPageBack(reviewPageBack, expPriceBack);
//                apr.saveForm();
//                paymentPage.setCardNo(CardNo);
//                paymentPage.setExpiryMonth(expMonth);
//                paymentPage.setExpiryYear(expYear);
//                paymentPage.clickVerify()
//                        .setCVVNo(CVV)
//                        .clickPay()
//                        .isAppPaymentSuccessful();
////                Thread.sleep(30000);
//                apr.captureCRM(expcaseType, expSubcase, expCustomerName, expLocation, expInstructions,
//                        expPdfFormat, expCertificate, expDial, expNotification,
//                        expReceipt, expSite, expSupport1, expSupport2,
//                        expSupport3, expSupport4, expSupport5, expTraffic,
//                        expCount, expType, expBusinessNo, mobilePhoneNo, expAlternateNo, fName, lName, email, expCaseCount);



            }
        }


        @DataProvider

        public Object[][] APR_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APR");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APR", 63);

            return testObjArray;
        }

    }

