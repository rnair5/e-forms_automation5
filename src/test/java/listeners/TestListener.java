package listeners;

import libraries.EmailReporter;
import com.relevantcodes.extentreports.LogStatus;
import libraries.Logg;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import static libraries.ExtentReporting.*;

public class TestListener implements ITestListener {
    /*This function overrides the onTestStart of ITestListener interface.It is to log the current test executed into the log4j and the Extent Report*/
    @Override
    public void onTestStart(ITestResult iTestResult) {
        Logg.logger.info("**** Test: " + iTestResult.getMethod().getMethodName() + " started ****");
        extentTest = extentReport.startTest(iTestResult.getTestClass().getName() + "." + iTestResult.getName());
//        extentTest = extentReport.startTest(iTestResult.getTestClass().getName());
        extentReport.endTest(extentTest);
        extentReport.flush();
    }

    /*This function overrides the onTestSuccess of ITestListener interface.It is to log a pass after successful test execution into the log4j and the Extent Report and flush the report so that it has the current test execution result*/
    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        Logg.logger.info("**** Test: " + iTestResult.getMethod().getMethodName() + " PASSED ****");
        extentTest.log(LogStatus.PASS, "Test " + iTestResult.getMethod().getMethodName() + " passed");
        extentReport.endTest(extentTest);
        extentReport.flush();
    }

    /*This function overrides the onTestSFailure of ITestListener interface.It is to log a fail after failed test execution into the log4j and the Extent Report and flush the report so that it has the current test execution result*/
    @Override
    public void onTestFailure(ITestResult iTestResult) {
        Logg.logger.info("**** Test: " + iTestResult.getMethod().getMethodName() + " FAILED ****");
        extentTest.log(LogStatus.FAIL, "Test " + iTestResult.getMethod().getMethodName() + " failed");
        extentReport.endTest(extentTest);
        extentReport.flush();
    }

    /*This function overrides the onTestSkipped of ITestListener interface.It is to log a skip after test execution is skipped into the log4j and the Extent Report and flush the report so that it has the current test execution result*/
    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Logg.logger.info("**** Test: " + iTestResult.getMethod().getMethodName() + " SKIPPED ****");
        extentTest.log(LogStatus.SKIP, "Test " + iTestResult.getMethod().getMethodName() + " failed");
        extentReport.endTest(extentTest);
        extentReport.flush();
    }



    /*This function is the implementation of the onTestFailedButWithinSuccessPercentage of ITestListener interface.*/
    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    /*This function overrides the onStart of ITestListener interface.It is to start logging the Extent Report*/
    @Override
    public void onStart(ITestContext iTestContext) {
        Logg.logger.info("REGRESSION SUITE");
//        extentTest = extentReport.startTest("Test: " + iTestContext.getName());
//        extentTest = extentReport.startTest("Regression Suite");

    }

    /*This function overrides the onFinish of ITestListener interface.It is to create the Emailable report to sent out as email*/
    @Override
    public void onFinish(ITestContext iTestContext) {
        Logg.logger.info("");
        EmailReporter.createEmailReport(getReportFilePath());
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("BaseClass's After Test method");
    }
}
