package pageObjects.forms;

import libraries.ExcelUtils;
import libraries.ExtentReporting;
import libraries.Logg;
import libraries.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class PINReviewPage extends BaseFormsClass {
    String formNo="228";
    static String GUID ="";
    public PINReviewPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    //Submit Form
    @FindBy(id="submitbtn")
    private WebElement finalSubmit;

    String form_ID="#gform_page_"+formNo;
    @FindBy(xpath="//*[@id=\"gform_next_button_228_13\"]")
    private WebElement nextButton;

    //Vehicle Registration
    @FindBy(id="input_228_8")
    private WebElement vehicleRegNo;

    //Infringement Notice Number
    @FindBy(id="input_228_9")
    private WebElement infringementNoticeNumber;

    //Grounds for Reviewing
    @FindBy(id="input_228_2")
    private WebElement groundsDropdown;

    //Special Circumstances Dropdown
    @FindBy(name="input_26")
    private WebElement specialDropdown;

    //Additional Information
    @FindBy(id="input_228_33")
    private WebElement additionalInfo;

    //Upload Supporting Information
    @FindBy(id="gform_browse_button_228_25")
    private WebElement uploadSupportingSpecial;

    //Contact Page
    @FindBy(id="input_228_38")
    private  WebElement fName;
    @FindBy(id="input_228_39")
    private  WebElement lName;
    @FindBy(id="input_228_40")
    private  WebElement phoneType;
    @FindBy(id="input_228_52")
    private  WebElement phoneNo;
    @FindBy(id="input_228_41")
    private  WebElement landLineNo;
    @FindBy(id="input_228_7")
    private  WebElement email;
    @FindBy(id="input_193_14_2")
    private  WebElement confEmail;
    @FindBy(id="choice_228_42_1")
    private WebElement smsNotification;
    @FindBy(id="label_228_42_1")
    private WebElement smsNotificationLabel;

    //Manual Address Entry
    @FindBy(id="input_228_49_2")
    private WebElement manualAddress;

    //Address Entry
    @FindBy(id="input_228_5_1")
    private WebElement corpAddressLineOne;
    @FindBy(id="input_228_5_3")
    private WebElement corpAddressLineThree;
    @FindBy(id="input_228_5_4")
    private WebElement corpAddressLineFour;
    @FindBy(id="input_228_5_5")
    private WebElement corpPostCode;
    @FindBy(id="input_228_5_6")
    private WebElement countryDropdown;

    //Submit Button
    @FindBy(id="gform_submit_button_228")
    private WebElement submitButton;



    public PINReviewPage clickNext(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickNext(formNo,pageNo);
        return this;
    }
    public PINReviewPage clickPrevious(String pageNo)
    {
        Logg.logger.info("Page no:"+pageNo);
        super.clickPrevious(formNo,pageNo);
        return this;
    }

    public PINReviewPage setVehicleNo(String number)
    {
        Logg.logger.info("Vehicle Number:"+number);
        waitAndSendKeys(driver,vehicleRegNo,number);
        return this;
    }

    public PINReviewPage setInfringementNo(String number)
    {
        Logg.logger.info("Infringement Number:"+number);
        waitAndSendKeys(driver,infringementNoticeNumber,number);
        return this;
    }

    public PINReviewPage selectGroundsDropdown(String grounds)
    {
        Logg.logger.info("Ground:"+grounds);
        groundsDropdown.click();
        if(grounds.equalsIgnoreCase("Contrary to Law"))
            selectDropdown_Index(driver, groundsDropdown,2);

        if(grounds.equalsIgnoreCase("Special Circumstances"))
            selectDropdown_Index(driver, groundsDropdown,1);
        return this;
    }

    public PINReviewPage selectSpecialDropdown(String grounds) throws InterruptedException {
        Logg.logger.info("Ground:"+grounds);
        Thread.sleep(15000);
        specialDropdown.click();
        if(grounds.equalsIgnoreCase("homelessness"))
            selectDropdown_Index(driver, specialDropdown,2);

        return this;
    }


    public PINReviewPage setInst(String instructions)
    {
        Logg.logger.info("Corner block instructions:"+instructions);
        waitAndSendKeys(driver,additionalInfo,instructions);
        return this;
    }


    public PINReviewPage setFName(String name)
    {
        Logg.logger.info("First Name:"+name);
        waitAndSendKeys(driver,fName,name);
        return this;
    }
    public PINReviewPage setLName(String name)
    {
        Logg.logger.info("Last Name:"+name);
        waitAndSendKeys(driver,lName,name);
        return this;
    }
    public PINReviewPage setEmail(String email)
    {
        Logg.logger.info("Email:"+email);
        waitAndSendKeys(driver,this.email,email);
        return this;
    }
    public PINReviewPage setConfEmail(String email)
    {
        Logg.logger.info("Conf Email:"+email);
        waitAndSendKeys(driver,confEmail,email);
        return this;
    }

    public PINReviewPage setPhoneType(String phoneType)
    {
        Logg.logger.info(" phone type:"+phoneType);
        selectDropdown_VisibleText(driver,this.phoneType,phoneType);
        return this;
    }
    public PINReviewPage setPhoneNumber(String phoneNumber)
    {
        Logg.logger.info(" Phone number:"+phoneNumber);
        waitAndSendKeys(driver,phoneNo,phoneNumber);
        return this;
    }
    public PINReviewPage setSMSNotifications(String option)
    {
        Logg.logger.info("Set SMS Notifications to:"+option);
        if(option.equalsIgnoreCase("yes"))
            if(!smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        if(option.equalsIgnoreCase("no"))
            if(smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        return this;
    }

    public PINReviewPage setManualAddressCheck(String option)
    {
        Logg.logger.info("Set SMS Notifications to:"+option);
//        if(option.equalsIgnoreCase("Enter address manually"))
                waitAndClick(driver,manualAddress);

        return this;
    }

    public PINReviewPage uploadMicrochipFileSpecial() throws AWTException, InterruptedException {
//        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
//        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[11]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        Thread.sleep(5000);
//        Logg.logger.info("File:"+fileName);
//        Utilities.uploadFile(uploadSupportingSpecial,fileName);
        return this;
    }

    public PINReviewPage setCorpContactAddressOne(String addressOne) {
        Logg.logger.info(" addressOne:" + addressOne);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, corpAddressLineOne, addressOne);
        return this;
    }

    public PINReviewPage setCorpContactAddressTwo(String addressTwo) {
        Logg.logger.info(" addressTwo:" + addressTwo);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, corpAddressLineThree, addressTwo);
        return this;
    }

    public PINReviewPage setCorpContactAddressFour(String addressFour) {
        Logg.logger.info(" addressFour:" + addressFour);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, corpAddressLineFour, addressFour);
        return this;
    }

    public PINReviewPage setCorpPostCode(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, corpPostCode, setPostCode);
        return this;
    }

    public PINReviewPage selectInterAddress(String category)
    {
        Logg.logger.info("category:"+category);
//        WebElement addressDropdown= driver.findElement(By.id("input_236_60_6"));
        countryDropdown.click();
        if(category.equalsIgnoreCase("India"))
            selectDropdown_Index(driver, countryDropdown,103);
        return this;
    }



    public PINReviewPage setPersonalDetails(String fname,String lname,  String phonetype, String phoneNumber,String sms) throws ParseException {
        Logg.logger.info(String.format("Personal details:  Fname:%s,  Lname:%s,  Email:%s, Conf Email:%s, Phone type:%s, Phone:%s, receive SMS notification:%s", fname,lname,email,confEmail,phonetype,phoneNumber,sms));
        this.setFName(fname)
                .setLName(lname)
                .setPhoneType(phonetype)
                .setPhoneNumber(phoneNumber);
        if(phonetype.equalsIgnoreCase("mobile"))
            setSMSNotifications(sms);
        return this;
    }


    public PINReviewPage submitForm() {
        Logg.logger.info("");
        waitAndClick(driver,finalSubmit);
//        super.submitForm(formNo);
        return this;
    }

    public PINReviewPage getReviewPage(String expReviewPage) throws Exception {

        //Get the Hidden Values
        //GUID
        GUID = driver.findElement(By.id("input_228_46")).getAttribute("value");
        System.out.println("The GUID is " + GUID + "");

        Thread.sleep(3000);


        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_228_16\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
        if (reviewPage.equalsIgnoreCase(expReviewPage)) {
            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
        } else {
            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");

        }


        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_228_48\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }
        return this;

    }

    @Test(dataProvider = "PIN_DataProvider", description = "Verify that the CRM values are verified")
    public PINReviewPage captureCRM(String expCaseType, String expSubType, String expCustomerName, String infringementNo,
                                             String instructions,
                                             String expPdfFormat, String expCertificate,
                                              String expType,String expBusinessNo, String expMobileNo, String expAlternateNo,
                                             String expFName, String expLName, String expEmail) throws Exception {


        Thread.sleep(15000);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.addArguments("window-size=1920,1080");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
        System.out.println(driver.manage().window().getSize());
        driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
        System.out.println(driver.manage().window().getSize());

        Thread.sleep(20000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);


        //Switch to the frame that contains the grid
        driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
        Thread.sleep(10000);


        Actions action = new Actions(driver);
        String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
        System.out.println("The GUID text in CRM is " + identifierText + "");
        WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
        Logg.logger.info("Is the GUID correct:" + identifier);



        action.doubleClick(identifier).perform();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        if (caseID.isDisplayed()) {
            extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
            throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
        }
        caseID.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseName.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }
        Thread.sleep(5000);
        //Select the Sub-case Type
        String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
        System.out.println("The SubcaseType is " + subCaseName + "");
        if (subCaseName.equalsIgnoreCase(expSubType)) {
            extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
        }
        //Select the origin
        String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
        Logg.logger.info("Origin name:" + originName);
        if (originName.equalsIgnoreCase("Web")) {
            extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
        } else {
            extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
        }
        //Select the customer name
        String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
        Logg.logger.info("Customer name:" + customerName);

        if (customerName.equalsIgnoreCase(expCustomerName)) {
            extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
        } else {
            extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
        }

        //Verify the Location Field
        String locationField = driver.findElement(By.xpath("//*[@id=\"cob_infringementnumber\"]/div[1]/span")).getText();
        if (locationField.contains(infringementNo)) {
            extent_Pass(driver, "IN Verification Passed", locationField.toString(), infringementNo, "");
        } else {
            extent_Warn(driver, "IN verification Failed", locationField.toString(), infringementNo, "");
            throw new Exception("TEST CASE FAILED AT THE INFRINGEMENT NUMBER LEVEL ");
        }

        //Verify the Instructions Field
        String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();

        if (instructionsField.equalsIgnoreCase(instructions)) {
            extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
        } else {
            extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
        }

        Thread.sleep(10000);



        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to the Case ESCALATION TAB
        WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
        Thread.sleep(10000);
        caseTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String firstResponse = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
        Logg.logger.info("First Response is:" + firstResponse);

        //Check if the Timer is not ticking and First response is Succeeded
        if (firstResponse.equalsIgnoreCase("Succeeded")) {
            extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
        }


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to Notes
        WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
        Thread.sleep(10000);
        notesTabSelected.click();
//        Thread.sleep(20000);

        //Navigate to Activities
        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
        actTabSelected.click();
        Thread.sleep(10000);


        String emailTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div")).getAttribute("objecttypecode");
        Thread.sleep(20000);
        Logg.logger.info("Email:" + emailTrigger);

        Thread.sleep(20000);


        //Check if SMS is triggered
        Thread.sleep(20000);
        String smsTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[3]/div[1]/div/div")).getAttribute("objecttypecode");
        Logg.logger.info("SMS:" + smsTrigger);




        //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseIDRefresh.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);

        //DOCUMENT DETAILS
        //Navigate to the Document Tabs
        Thread.sleep(20000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
        Thread.sleep(10000);
        if (documentTabSelected.isDisplayed()) {
            extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
        } else {
            extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
            throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
        }
        documentTabSelected.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Thread.sleep(10000);
        driver.switchTo().parentFrame();
        Thread.sleep(5000);


        //Get the Name of the First Document - First Document
        String oneDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (oneDocName.equalsIgnoreCase(expPdfFormat)) {
            extent_Pass(driver, "NAME FORMAT - DOC1- Verification Passed", oneDocName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - DOC1 - verification Failed", oneDocName.toString(), "", "");
            throw new Exception("DOC1 NAMING CONVENTION FAILED ");
        }
//
        //Get the Name of the Second Document - PDF
        String certName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
        if (certName.equalsIgnoreCase(expCertificate)) {
            extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", certName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", certName.toString(), "", "");
            throw new Exception("PDF NAMING CONVENTION FAILED ");
        }



//        //Get the Name of the Fourth Document - Notification
//        String notificationName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
//        if (notificationName.contains(expReceipt)) {
//            extent_Pass(driver, "NAME FORMAT - NOTIFICATION - Verification Passed", notificationName.toString(), "", "");
//        } else {
//            extent_Warn(driver, "NAME FORMAT - NOTIFICATION - verification Failed", notificationName.toString(), "", "");
//            throw new Exception("NOTIFICATION NAMING CONVENTION FAILED ");
//        }







//
//        //Get the Total Number of records
//        String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
//        if (totalDocuments.equalsIgnoreCase(expCount)) {
//            extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
//        } else {
//            extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
//            throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
//        }



//        //Click to download PDF Document
//        String parenthandle = driver.getWindowHandle();
//        WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[3]/nobr/a"));
//        pdfDocOne.click();
//        Thread.sleep(10000);
//        driver.switchTo().window(parenthandle);
//        driver.navigate().refresh();
//        Thread.sleep(10000);
//        driver.switchTo().defaultContent();
//        WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
//        caseIDone.click();
//
//        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
//        driver.switchTo().frame(0);
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }


        //Select the customer name
        Thread.sleep(20000);
        WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
        custName.click();
        Thread.sleep(10000);

        //Customer Matching
        //Contact Type
        String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
        if (contactType.equalsIgnoreCase(expType)) {
            extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
            throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
        }
//        //Get the Business Phone Number
//        String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//        if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//            throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//        }
        //Get the Mobile Number
//        String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//        if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//        }
//        //Get the Landline Number
//        String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//        if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//            throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//        }

//        //Get the First Name
//        String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
//        if (firstName.equalsIgnoreCase(expFName)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
//            throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
//        }

        //Get the Last Name OR Business Name
        String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
        if (lastName.equalsIgnoreCase(expLName)) {
            extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLName, lastName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLName, lastName);
            throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
        }
        //Get the Email
        String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
        if (emailName.equalsIgnoreCase(expEmail)) {
            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
        }

//        //Get the ACN
//        String actACN = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[17]/td[2]/div/div[1]/span")).getText();
//        if (actACN.equalsIgnoreCase("123456789")) {
//            extent_Pass(driver, "CUSTOMER MATCHING - ACN- Verification Passed", actACN.toString(),"" , "");
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - ACN- verification Failed", actACN.toString(), "", "");
//            throw new Exception(" VERIFICATION OF ACN FAILED ");
//        }
//        //Get the ABN
//        String actABN = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[16]/td[2]/div/div[1]/span")).getText();
//        if (actABN.equalsIgnoreCase("12345678901")) {
//            extent_Pass(driver, "CUSTOMER MATCHING - ABN- Verification Passed", actABN.toString(),"" , "");
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - ABN- verification Failed", actABN.toString(), "", "");
//            throw new Exception(" VERIFICATION OF ABN FAILED ");
//        }

        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
        Thread.sleep(10000);


        //Navigate to the Customer Case Records Tab
        WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
        Thread.sleep(10000);
        customerCaseRecords.click();

        Thread.sleep(5000);

        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
        Logg.logger.info("Total Number of Cases:" + caseRecords);
//        if (caseRecords) {
//            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
//            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
//        }


        Thread.sleep(10000);
        navigateBack(driver);
        Thread.sleep(10000);

        driver.quit();

        return this;
    }
    @DataProvider

    public Object[][] PIN_DataProvider () throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "PIN");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "PIN", 32);

        return testObjArray;
    }

}
