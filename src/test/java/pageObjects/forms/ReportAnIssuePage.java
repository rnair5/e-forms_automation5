package pageObjects.forms;

import com.relevantcodes.extentreports.LogStatus;
import libraries.ExcelUtils;
import libraries.Logg;
import libraries.Utilities;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

/**
 * Created by Reshma Nair on 5/06/2020.
 * Functions: This class is a Page Class, contains functions for all the operations on the Report an Issue form
 */

public class ReportAnIssuePage extends BaseFormsClass {
    ReportAnIssuePage rai;
    String formNo = "229";
    static String GUID ="";
    public ReportAnIssuePage(WebDriver driver) {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver, this);
    }

    String form_ID = "#gform_page_" + formNo;


    //Submit Form
    @FindBy(id="submitbtn")
    private WebElement finalSubmit;
    //Issue Type - Form Type
    //Trees
    @FindBy(id = "label_229_83_0")
    private WebElement treeIssue;
    @FindBy(id = "label_229_84_1")
    private WebElement pruningIssue;
    @FindBy(id = "label_229_54_1")
    private WebElement locIssue;
    @FindBy(id = "input_229_44")
    private WebElement descIssue;
    @FindBy(id = "label_229_69_0")
    private WebElement parkYes;
    @FindBy(id = "label_229_69_1")
    private WebElement parkNo;

    //Issue Type - Drainage
    @FindBy(id = "label_229_83_1")
    private WebElement drainAndPitsIssue;
    //Call Out Box
    @FindBy(xpath= "//*[@id=\"field_229_87\"]")
    private WebElement drainsInfoBox;
    //Does the issue relate to the cover of the storm water
    @FindBy(id = "label_229_88_1")
    private WebElement stormWaterNo;
    //Where is the Issue?
    @FindBy(id = "label_229_111_1")
    private WebElement councilLand;
    //Describe the Location
    @FindBy(id = "input_229_137")
    private WebElement drainageLocation;




    //Issue Type - Form Type
    //Council Buildings and Facilities - Tenancy
    @FindBy(id = "label_229_83_2")
    private WebElement councilIssue;
    @FindBy(id = "label_229_95_0")
    private WebElement tenantIssue;
    @FindBy(id = "label_229_95_1")
    private WebElement NonTenantIssue;
    @FindBy(id = "label_229_98_4")
    private WebElement publicLitterBin;
    @FindBy(id = "label_229_101_1")
    private WebElement overFlowing;
    @FindBy(id = "label_229_102_1")
    private WebElement recycleBin;
    @FindBy(id = "input_229_135")
    private WebElement buildingName;
    @FindBy(id = "input_229_136")
    private WebElement orgName;
    @FindBy(id = "input_229_110")
    private WebElement cblocDesc;

    //Issue Type - Form Type
    //Street Cleaning and Toilets
    @FindBy(id = "label_229_83_3")
    private WebElement cleaningIssue;
    @FindBy(id = "label_229_130_1")
    private WebElement toiletIssue;
    @FindBy(id = "label_229_100_0")
    private WebElement neededIssue;
    @FindBy(id = "label_229_79_1")
    private WebElement anonymousContact;

    //Issue Type - Form Type
    //Parks And Playgrounds - Other
    @FindBy(id = "label_229_83_4")
    private WebElement parksIssue;
    @FindBy(id = "label_229_104_7")
    private WebElement otherIssue;
    @FindBy(id = "input_229_115")
    private WebElement ppDescLoc;

    //Address Selection
    @FindBy(id = "input_229_15")
    private WebElement addressEnter;
    @FindBy(id = "input_229_14")
    private WebElement outsideClick;
    @FindBy(id = "input_229_18")
    private WebElement descLoc;

    //Contact Page
    @FindBy(id = "input_229_21")
    private WebElement fName;
    @FindBy(id = "input_229_22")
    private WebElement lName;
    //    @FindBy(id = "label_229_79_1")
//    private WebElement Anonymous;
    @FindBy(id = "label_229_81_0")
    private WebElement mobType;
    @FindBy(id = "label_229_81_1")
    private WebElement landType;
    @FindBy(id = "input_229_124")
    private WebElement mobNo;
    @FindBy(id = "input_229_24")
    private WebElement landNo;
    @FindBy(id = "input_229_26")
    private WebElement email;
    @FindBy(id = "choice_229_25_1")
    private WebElement smsNotification;
    @FindBy(id = "label_229_25_1")
    private WebElement smsNotificationLabel;
    @FindBy(id = "gform_submit_button_229")
    private WebElement submitButton;
    @FindBy(id = "gform_browse_button_229_43")
    private WebElement uploadButton;
    @FindBy(id = "label_203_184_1")
    private WebElement checkUpload;
    @FindBy(id = "input_203_229")
    private WebElement csoEmail;


    public ReportAnIssuePage uploadHeadlessDocument() throws AWTException, InterruptedException {
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        return this;
    }







    //Current Location
    @FindBy(xpath = "//*[@id=\"gfgeo-locator-button-229_74\"]")
    private WebElement currentLocation;
//   /html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[3]/div[1]/ul/li[12]/div/div/div

    //map
    @FindBy(css = "//*[@id=\"gfgeo-map-229_14\"]")
    private WebElement mapLocation1;
    //map view
    @FindBy(xpath = "//*[@id=\"gfgeo-map-229_14\"]/div/div/div[1]/div[3]/div/div[3]/div/img")
    private WebElement mapLocation;

    //Maximum Number of Uploads
    @FindBy(xpath = "//*[@id=\"gform_multifile_messages_229_43\"]/li")
    private WebElement maxUploads;

    //Verify the Max uploads label on the form
    public ReportAnIssuePage getMaxLabel() {
        Logg.logger.info("");
        String maxUploads = driver.findElement(By.xpath("//*[@id=\"gform_multifile_messages_229_43\"]/li")).getText();
        System.out.println("The Label is " + maxUploads + "");
        if(maxUploads.equalsIgnoreCase("Maximum number of files reached")){
            Logg.logger.info(LogStatus.PASS);
        } else {
            Logg.logger.info(LogStatus.FAIL);
        }

        return this;
    }





    public ReportAnIssuePage clickNext(String pageNo) {
        Logg.logger.info("PageNo:" + pageNo);
        super.clickNext(formNo, pageNo);
        return this;
    }

//    public ReportAnIssueWastePage setCurrentLocation() throws InterruptedException {
//        Logg.logger.info("");
//        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
//        jsExecutor.executeScript("window.alert = function(){}");
//        jsExecutor.executeScript("window.confirm = function(){return true;}");
//        waitAndClick(driver, currentLocation);
//
////        Thread.sleep(5000);
////        // Switch the driver context to the alert
////        Alert alertDialog = driver.switchTo().alert();
////        // Get the alert text
////        String alertText = alertDialog.getText();
////        System.out.println("The alert text message is " + alertText + "");
////        // Click the OK button on the alert.
////        Thread.sleep(15000);
////        if (alertText.equalsIgnoreCase("Location Found."))
////        alertDialog.accept();
////        Thread.sleep(5000);
////        // Switch the driver context to the alert
////        Alert alertDialog1 = driver.switchTo().alert();
////        // Get the alert text
////        String alertText1 = alertDialog.getText();
////        System.out.println("The alert text message is " + alertText1 + "");
////        // Click the OK button on the alert.
////        Thread.sleep(15000);
////        if (alertText.equalsIgnoreCase("Make sure the address you enter is within Boroondara."))
////        alertDialog1.accept();
////        alertDialog1.dismiss();
//
//        Thread.sleep(10000);
//        return this;
//
//
//    }


    public ReportAnIssuePage setMapLocation() throws InterruptedException, AWTException {
//        Logg.logger.info("");
//        Point coordinates = driver.findElement(By.id("ctl00_portalmaster_txtUserName")).getLocation();
//        Robot robot = new Robot();
//        robot.mouseMove(coordinates.getX(),coordinates.getY()+120);
//        return this;


//        Actions builder = new Actions(driver);
//        waitAndClick(driver, mapLocation);
//        builder.keyDown(Keys.CONTROL);
//
//                builder.moveByOffset( 10, 25 );
//                builder.keyUp(Keys.CONTROL).build().perform();
//
//        Robot robot = new Robot ();
//
//        robot.mouseMove(350, 226);
//        robot.keyPress(InputEvent.BUTTON1_MASK);
//        robot.mouseMove(250, 350);
//        robot.keyRelease(InputEvent.BUTTON1_MASK);
        WebElement mapView = driver.findElement(By.xpath("//div[@id='gfgeo-map-wrapper-229_14']"));
        mapView.click();
        Thread.sleep(10000);
        WebElement mapMarker = driver.findElement(By.xpath("(//img[contains(@src,'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png')])[2]"));
        Thread.sleep(10000);
        mapMarker.click();
        Thread.sleep(10000);
        Actions builder = new Actions(driver);
        builder.clickAndHold(mapMarker)
                .moveByOffset(10, 25)
                .click()
                .release(mapMarker)
                .build();
        builder.perform();



                return this;

    }



        public ReportAnIssuePage clickPrevious (String pageNo){
            Logg.logger.info("PageNo:" + pageNo);
            super.clickPrevious(formNo, pageNo);
            return this;
        }

    public ReportAnIssuePage clickDrainIssue (String issueType){
        Logg.logger.info("Issue Type for Drains:" + issueType);
        if (issueType.equalsIgnoreCase("No"))
            waitAndClick(driver, stormWaterNo);
        return this;
    }

    public ReportAnIssuePage clickDrainLocation (String issueType){
        Logg.logger.info("Location Type for Drains:" + issueType);
        if (issueType.equalsIgnoreCase("On Council land, such as a naturestrip or park"))
            waitAndClick(driver, councilLand);
        return this;
    }


        public ReportAnIssuePage setIssueType (String issueType){
            Logg.logger.info("Issue:" + issueType);
            if (issueType.equalsIgnoreCase("Trees"))
                waitAndClick(driver, treeIssue);
            if (issueType.equalsIgnoreCase("Drains and pits"))
                waitAndClick(driver, drainAndPitsIssue);
            if (issueType.equalsIgnoreCase("Street cleaning and toilets"))
                waitAndClick(driver, cleaningIssue);
            if (issueType.equalsIgnoreCase("Council buildings, sportsgrounds and facilities"))
                waitAndClick(driver, councilIssue);
            if (issueType.equalsIgnoreCase("Parks and playgrounds"))
                waitAndClick(driver, parksIssue);
            return this;
        }

        public ReportAnIssuePage setTreeType (String treeType){
            Logg.logger.info("Tree Issue:" + treeType);
            if (treeType.equalsIgnoreCase("Pruning"))
                waitAndClick(driver, pruningIssue);
            if (treeType.equalsIgnoreCase("Public litter bins"))
                waitAndClick(driver, publicLitterBin);
            if (treeType.equalsIgnoreCase("Toilets"))
                waitAndClick(driver, toiletIssue);
            if (treeType.equalsIgnoreCase("Other"))
                waitAndClick(driver, otherIssue);
            return this;
        }

        public ReportAnIssuePage setUserType (String userType){
            Logg.logger.info("User Type:" + userType);
            if (userType.equalsIgnoreCase("Yes"))
                waitAndClick(driver, tenantIssue);
            if (userType.equalsIgnoreCase("No"))
                waitAndClick(driver, NonTenantIssue);
            return this;
        }

        public ReportAnIssuePage setLocType (String locType){
            Logg.logger.info("Location Issue:" + locType);
            if (locType.equalsIgnoreCase("On Council land, such as a naturestrip or park"))
                waitAndClick(driver, locIssue);
            if (locType.equalsIgnoreCase("Overflowing"))
                waitAndClick(driver, overFlowing);
            if (locType.equalsIgnoreCase("Cleaning"))
                waitAndClick(driver, neededIssue);
            return this;
        }

        public ReportAnIssuePage setBinType (String binType){
            Logg.logger.info("Bin :" + binType);
            if (binType.equalsIgnoreCase("Recycling"))
                waitAndClick(driver, recycleBin);
            return this;
        }


        public ReportAnIssuePage setIssueDesc (String issueDesc){
            Logg.logger.info("Issue Description:" + issueDesc);
            waitAndSendKeys(driver, descIssue, issueDesc);
            return this;
        }

        public ReportAnIssuePage setBuildingName (String buildName){
            Logg.logger.info("Building Name:" + buildName);
            waitAndSendKeys(driver, buildingName, buildName);
            return this;
        }


        public ReportAnIssuePage setPark (String option){
            Logg.logger.info("In a Park:" + option);
            if (option.equalsIgnoreCase("yes"))
                waitAndClick(driver, parkYes);
            if (option.equalsIgnoreCase("no"))
                waitAndClick(driver, parkNo);
            return this;
        }

        public ReportAnIssuePage setAddress (String address) throws InterruptedException {
            Logg.logger.info("Address Entered:" + address);
            addressEnter.clear();
            addressEnter.sendKeys(address);
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[5]")));
            driver.findElement(By.xpath("html/body/div[5]")).click();
            Thread.sleep(4000);
            return this;
        }

    public ReportAnIssuePage setInvalidAddress (String address) throws InterruptedException {
        Logg.logger.info("Address Entered:" + address);
        addressEnter.clear();
        addressEnter.sendKeys(address);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("html/body/div[5]")));
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("window.alert = function(){}");
        jsExecutor.executeScript("window.confirm = function(){return true;}");
        driver.findElement(By.xpath("html/body/div[5]")).click();
        Thread.sleep(5000);
//        Alert alertDialog = driver.switchTo().alert();
//        // Get the alert text
//        String alertText = alertDialog.getText();
//        System.out.println("The alert text message is " + alertText + "");
//        // Click the OK button on the alert.
//        Thread.sleep(15000);
//        if (alertText.equalsIgnoreCase("Make sure the address you enter is within Boroondara."))
//            alertDialog.accept();
//        Thread.sleep(5000);

        return this;
    }



        public ReportAnIssuePage setClick (String click){
            Logg.logger.info("In a Park:" + click);
            if (click.equalsIgnoreCase("Drag the pin on the map to the exact location of the issue."))
                waitAndClick(driver, outsideClick);
            return this;
        }

        public ReportAnIssuePage setLocDesc (String locDesc){
            Logg.logger.info("Issue Description:" + locDesc);
            waitAndSendKeys(driver, descLoc, locDesc);
            return this;
        }

        public ReportAnIssuePage setCouncilLocDesc (String councilLocDesc){
            Logg.logger.info("Issue Description:" + councilLocDesc);
            waitAndSendKeys(driver, cblocDesc, councilLocDesc);
            return this;
        }

        public ReportAnIssuePage setParkLocDesc (String parkLocDesc){
            Logg.logger.info("Address Description:" + parkLocDesc);
            waitAndSendKeys(driver, ppDescLoc, parkLocDesc);
            return this;
        }

    public ReportAnIssuePage setDrainageLocDesc (String parkLocDesc){
        Logg.logger.info("Address Description:" + parkLocDesc);
        waitAndSendKeys(driver, drainageLocation, parkLocDesc);
        return this;
    }



//    public ReportAnIssuePage setAnonymous(String anonymous) {
//        Logg.logger.info("Anonymous User:" + anonymous);
//        if (anonymous.equalsIgnoreCase("Yes"))
//            waitAndClick(driver, mobType);
//        return this;
//    }

        public ReportAnIssuePage setFName (String name){
            Logg.logger.info("First Name:" + name);
            waitAndSendKeys(driver, fName, name);


            return this;
        }

        public ReportAnIssuePage setLName (String name){
            Logg.logger.info("Last Name:" + name);
            waitAndSendKeys(driver, lName, name);
            return this;
        }

        public ReportAnIssuePage setOrgName (String orgname){
            Logg.logger.info("Org Name:" + orgname);
            waitAndSendKeys(driver, orgName, orgname);
            return this;
        }

        public ReportAnIssuePage setEmail (String email){
            Logg.logger.info("Email:" + email);
            waitAndSendKeys(driver, this.email, email);

            return this;
        }

        public ReportAnIssuePage setPhoneType (String phoneType){
            Logg.logger.info("Phone Type:" + phoneType);
            if (phoneType.equalsIgnoreCase("Mobile"))
                waitAndClick(driver, mobType);
            if (phoneType.equalsIgnoreCase("Landline"))
                waitAndClick(driver, landType);
            return this;
        }


        public ReportAnIssuePage setPhoneNumber (String phoneNumber){
            Logg.logger.info(" Phone number:" + phoneNumber);
            if (mobType.isSelected())
                waitAndSendKeys(driver, mobNo, phoneNumber);
            if (landType.isSelected())
                waitAndSendKeys(driver, landNo, phoneNumber);
            return this;
        }

        public ReportAnIssuePage setMobPhoneNumber (String mobphoneNum){
            Logg.logger.info(" Phone number:" + mobphoneNum);
            waitAndSendKeys(driver, mobNo, mobphoneNum);

            return this;
        }

        public ReportAnIssuePage setlandPhoneNumber (String lanphoneNum){
            Logg.logger.info(" Phone number:" + lanphoneNum);
            waitAndSendKeys(driver, landNo, lanphoneNum);
            return this;
        }


        public ReportAnIssuePage setSMSNotifications (String option){
            Logg.logger.info("Set SMS Notifications to:" + option);
            if (option.equalsIgnoreCase("yes"))
                if (!smsNotification.isSelected())
                    waitAndClick(driver, smsNotificationLabel);
            if (option.equalsIgnoreCase("no"))
                if (smsNotification.isSelected())
                    waitAndClick(driver, smsNotificationLabel);
            return this;
        }

        public ReportAnIssuePage setPersonalDetails (String fname, String lname, String phonetype) throws
        ParseException {
            Logg.logger.info(String.format(" details:  Fname:%s, Lname:%s, Phone type:%s", fname, lname, phonetype));
            this.setFName(fname)
                    .setLName(lname)
                    .setPhoneType(phonetype);
//        if(phonetype.equalsIgnoreCase("mobile"))
//            setSMSNotifications(sms);
            return this;
        }

        public ReportAnIssuePage setCsoEmail (String email){
            Logg.logger.info("Email:" + email);
            waitAndSendKeys(driver, csoEmail, email);
            return this;
        }


        public ReportAnIssuePage getReviewPage (String expReviewPage) throws Exception {

            // Grab the table
            WebElement table = driver.findElement(By.xpath("//li[@id='field_229_12']/table/tbody/tr/td"));
            String reviewPage = driver.findElement(By.xpath("//li[@id='field_229_12']/table/tbody/tr/td")).getText();
            if (reviewPage.equalsIgnoreCase(expReviewPage.trim())) {
                extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
            } else {
                extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LABEL ");

            }

            //Capture the Hidden Elements
            Thread.sleep(5000);
            //Capture GUID
            GUID = driver.findElement(By.xpath("//*[@id=\"input_229_28\"]")).getAttribute("value");
            System.out.println("The GUID is " + GUID + "");

            //Check the Privacy Policy
            String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_229_76\"]")).getText();
            if (privacyPolicy.contains("Privacy policy")) {
                extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
            } else {
                extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

            }


            return this;
        }

        @Test(dataProvider = "rai_DataProvider", description = "Verify that the review page values can be compared")
        public ReportAnIssuePage compareReviewPageOne (String expReviewPage) throws Exception {

            // Grab the table
            WebElement table = driver.findElement(By.xpath("//li[@id='field_229_12']/table/tbody/tr/td"));
            String reviewPage = driver.findElement(By.xpath("//li[@id='field_229_12']/table/tbody/tr/td")).getText();
            if (reviewPage.equalsIgnoreCase(expReviewPage.trim())) {
                extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
            } else {
                extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LABEL ");

            }

            //Capture the Hidden Elements
            Thread.sleep(5000);
            //Capture GUID
            GUID = driver.findElement(By.xpath("//*[@id=\"input_229_28\"]")).getAttribute("value");
            System.out.println("The GUID is " + GUID + "");

            //Check the Privacy Policy
            String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_229_76\"]")).getText();
            if (privacyPolicy.contains("Privacy policy")) {
                extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
            } else {
                extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

            }


            return this;
        }

        public ReportAnIssuePage submitForm () {
            Logg.logger.info("");
            waitAndClick(driver, finalSubmit);
//        super.submitForm(formNo);
            return this;
        }

        public ReportAnIssuePage checkUploadBox () {
            Logg.logger.info("");
            waitAndClick(driver, checkUpload);
            return this;
        }

        public ReportAnIssuePage uploadMicrochipFile (String fileName) throws AWTException, InterruptedException {
            Logg.logger.info("File:" + fileName);
            Utilities.uploadFile(uploadButton, fileName);
            return this;
        }


        @Test(dataProvider = "rai_DataProvider", description = "Verify that the CRM values are verified")
        public ReportAnIssuePage captureCRM (String expCaseType, String expSubType, String expCustomerName, String Instructions,
                                             String expSupporting, String expPdfFormat, String expCount, String expType,
                                             String expBusinessNo, String expMobileNo, String expAlternateNo,
                                             String expFName, String expLName, String expEmail, String expCaseCount) throws Exception {

            Thread.sleep(15000);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--no-sandbox");
            options.addArguments("--headless");
            options.addArguments("start-maximized");
            options.addArguments("window-size=1920,1080");
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
            System.out.println(driver.manage().window().getSize());
            driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
            driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
            System.out.println(driver.manage().window().getSize());

            Thread.sleep(20000);
            WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
            sitemapHomeTab.click();
            Thread.sleep(4000);
            WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
            serviceAreaElement.click();
            Thread.sleep(10000);
            WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
            serviceRightLink.click();
            Thread.sleep(10000);


            //Switch to the frame that contains the grid
            driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
            Thread.sleep(10000);


            Actions action = new Actions(driver);
            String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
            System.out.println("The GUID text in CRM is " + identifierText + "");
            WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
            Logg.logger.info("Is the GUID correct:" + identifier);



            action.doubleClick(identifier).perform();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(2000);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            if (caseID.isDisplayed()) {
                extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
                throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
            }
            caseID.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);


            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseName.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }
            Thread.sleep(5000);
            //Select the Sub-case Type
            String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
            System.out.println("The SubcaseType is " + subCaseName + "");
            if (subCaseName.equalsIgnoreCase(expSubType)) {
                extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
            }
            //Select the origin
            String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
            Logg.logger.info("Origin name:" + originName);
            if (originName.equalsIgnoreCase("Web")) {
                extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            } else {
                extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
            }
            //Select the customer name
            String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
            Logg.logger.info("Customer name:" + customerName);

            if (customerName.equalsIgnoreCase(expCustomerName)) {
                extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
            } else {
                extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
            }


            //Verify the Instructions Field
            String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();

            if (instructionsField.contains(Instructions)) {
                extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
            } else {
                extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
            }

            Thread.sleep(5000);



            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to the Case ESCALATION TAB
            WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
            Thread.sleep(10000);
            caseTabSelected.click();

            driver.switchTo().parentFrame();
            //Get the first response value
            String firstResponse = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[6]/table/tbody/tr[3]/td[2]/div/div[1]/span")).getText();
            Logg.logger.info("First Response is:" + firstResponse);

            //Check if the Timer is not ticking and First response is Succeeded
            if (firstResponse.equalsIgnoreCase("No")) {
                extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "No", "");
            } else {
                extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "No", "");
                throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
            }


            Thread.sleep(5000);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);


            //Navigate to the WASTE TAB
            WebElement wasteTabSelected = driver.findElement(By.id("tab_waste"));
            Thread.sleep(10000);
            wasteTabSelected.click();

            driver.switchTo().parentFrame();
            //Get the first response value
            String relatedWaste = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[2]/td[2]/div[1]/div[1]/span[1]")).getText();
            if (relatedWaste.contains("Public Litter Bin")) {
                extent_Pass(driver, "RELATED WASTE- Verification Passed", relatedWaste.toString(), "Public Litter Bin", relatedWaste);
            } else {
                extent_Warn(driver, "RELATED WASTE - verification Failed", relatedWaste.toString(), "Public Litter Bin", relatedWaste);
                throw new Exception(" VERIFICATION OF RELATED WASTE FAILED ");
            }

            String binType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[3]/td[1]/div/div/div[2]/div/div/div/div/div/div/div/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
            if (binType.equalsIgnoreCase("Recycle")) {
                extent_Pass(driver, "Bin Type- Verification Passed", binType.toString(), "Recycle", binType);
            } else {
                extent_Warn(driver, "Bin Type - verification Failed", binType.toString(), "Recycle", binType);
                throw new Exception(" VERIFICATION OF Bin Type FAILED ");
            }

            String exactLocation = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[3]/td[1]/div/div/div[2]/div/div/div/div/div/div/div/table/tbody/tr[10]/td[2]/div")).getText();
            if (exactLocation.contains("45 Suffolk Rd, Surrey Hills VIC 3127")) {
                extent_Pass(driver, "Exact Location- Verification Passed", exactLocation.toString(), "45 Suffolk Rd, Surrey Hills VIC 3127 - (-37.83022690000001, 145.0935567)", "");
            } else {
                extent_Warn(driver, "Exact Location - verification Failed", exactLocation.toString(), "45 Suffolk Rd, Surrey Hills VIC 3127 - (-37.83022690000001, 145.0935567)", "");
                throw new Exception(" VERIFICATION OF Exact Location FAILED ");
            }



            Thread.sleep(10000);

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to Notes
            WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
            Thread.sleep(10000);
            notesTabSelected.click();
//        Thread.sleep(20000);

            //Navigate to Activities
            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
            actTabSelected.click();
            Thread.sleep(10000);


            String emailTrigger = driver.findElement(By.xpath("(//img[@alt='Email'])[3]")).getAttribute("src");
            Thread.sleep(20000);
            Logg.logger.info("Email:" + emailTrigger);



            //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(2000);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            caseIDRefresh.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);

            //DOCUMENT DETAILS
            //Navigate to the Document Tabs
            Thread.sleep(20000);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
            Thread.sleep(10000);
            if (documentTabSelected.isDisplayed()) {
                extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
            } else {
                extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
                throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
            }
            documentTabSelected.click();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            Thread.sleep(10000);
            driver.switchTo().parentFrame();
            Thread.sleep(5000);


            //Get the Name of the First Document
            String supportDocName1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDocName1.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC1 - Verification Passed", supportDocName1.toString(), expSupporting, supportDocName1);
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC1 - verification Failed", supportDocName1.toString(), expSupporting, supportDocName1);
                throw new Exception("DOC1 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Second Document
            String supportDoc2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (supportDoc2.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC2 - Verification Passed", supportDoc2.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC2 - verification Failed", supportDoc2.toString(), expSupporting, "");
                throw new Exception("DOC2 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Third Document
            String supportDoc3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
            if (supportDoc3.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC3 - Verification Passed", supportDoc3.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC3 - verification Failed", supportDoc3.toString(), expSupporting, "");
                throw new Exception("DOC3 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Fourth Document
            String supportDoc4 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
            if (supportDoc4.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC4 - Verification Passed", supportDoc4.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC4 - verification Failed", supportDoc4.toString(), expSupporting, "");
                throw new Exception("DOC4 NAMING CONVENTION FAILED ");
            }


            //Click the Next page to view the Documents

            WebElement docPage2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
            docPage2.click();
            Thread.sleep(10000);


            //Get the Name of the 5th Document
            String supportDoc5 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDoc5.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC5 - Verification Passed", supportDoc5.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC5 - verification Failed", supportDoc5.toString(), expSupporting, "");
                throw new Exception("DOC5 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the PDF
            String pdfDoc = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (pdfDoc.equalsIgnoreCase(expPdfFormat)) {
                extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", pdfDoc.toString(), expPdfFormat, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", pdfDoc.toString(), expPdfFormat, "");
                throw new Exception("PDF NAMING CONVENTION FAILED ");
            }


            //Get the Total Number of records
            String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
            if (totalDocuments.equalsIgnoreCase(expCount)) {
                extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
            } else {
                extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
                throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
            }


            //Click to download PDF Document
//            String parenthandle = driver.getWindowHandle();
//            WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[3]/nobr/a"));
//            pdfDocOne.click();
//            Thread.sleep(10000);
//            driver.switchTo().window(parenthandle);
//            driver.navigate().refresh();
//            Thread.sleep(2000);
//            Thread.sleep(10000);
//            driver.switchTo().defaultContent();
//            WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
//            caseIDone.click();
//
//            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
//            driver.switchTo().frame(0);
//            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);



            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }


            //Select the customer name
            Thread.sleep(20000);
            WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
            custName.click();
            Thread.sleep(10000);

            //Customer Matching
            //Contact Type
            String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
            if (contactType.equalsIgnoreCase(expType)) {
                extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
                throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
            }
//            //Get the Business Phone Number
//            String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//            if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//                throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//            }
//            //Get the Mobile Number
//            String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//            if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//                throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//            }
//            //Get the Landline Number
//            String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//            if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//                throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//            }

//            //Get the First Name
//            String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
//            if (firstName.equalsIgnoreCase(expFName)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
//                throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
//            }

            //Get the Last Name OR Business Name
            String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
            if (lastName.equalsIgnoreCase(expLName)) {
                extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLName, lastName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLName, lastName);
                throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
            }
            //Get the Email
            String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
            if (emailName.equalsIgnoreCase(expEmail)) {
                extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
                throw new Exception(" VERIFICATION OF EMAIL FAILED ");
            }


            Thread.sleep(5000);


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
            Thread.sleep(10000);


            //Navigate to the Customer Case Records Tab
            WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
            Thread.sleep(10000);
            customerCaseRecords.click();

            Thread.sleep(5000);

            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
            Logg.logger.info("Total Number of Cases:" + caseRecords);


            Thread.sleep(10000);
            navigateBack(driver);
            Thread.sleep(10000);

            driver.quit();

            return this;
        }

        @Test(dataProvider = "raim_DataProvider", description = "Verify that the CRM values are verified")
        public ReportAnIssuePage captureCRMMaintenance (String expCaseType, String expSubType, String expCustomerName, String expLocation, String Instructions,
                                                        String expSupporting, String expPdfFormat, String expCount, String expType,
                                                        String expBusinessNo, String expMobileNo, String expAlternateNo,
                                                        String expFName, String expLastName, String expEmail, String expCaseCount) throws Exception {



            Thread.sleep(15000);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--no-sandbox");
            options.addArguments("--headless");
            options.addArguments("start-maximized");
            options.addArguments("window-size=1920,1080");
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
            System.out.println(driver.manage().window().getSize());
            driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
            driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
            System.out.println(driver.manage().window().getSize());
            Thread.sleep(20000);
            WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
            sitemapHomeTab.click();
            Thread.sleep(4000);
            WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
            serviceAreaElement.click();
            Thread.sleep(10000);
            WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
            serviceRightLink.click();
            Thread.sleep(10000);


            //Switch to the frame that contains the grid
            driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
            Thread.sleep(10000);


            Actions action = new Actions(driver);
            String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
            System.out.println("The GUID text in CRM is " + identifierText + "");
            WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
            Logg.logger.info("Is the GUID correct:" + identifier);


            action.doubleClick(identifier).perform();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(2000);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            if (caseID.isDisplayed()) {
                extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
                throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
            }
            caseID.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);


            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseName.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }
            Thread.sleep(5000);
            //Select the Sub-case Type
            String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
            System.out.println("The SubcaseType is " + subCaseName + "");
            if (subCaseName.equalsIgnoreCase(expSubType)) {
                extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
            }
            //Select the origin
            String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
            Logg.logger.info("Origin name:" + originName);
            if (originName.equalsIgnoreCase("Web")) {
                extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            } else {
                extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
            }
            //Select the customer name
            String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
            Logg.logger.info("Customer name:" + customerName);

            if (customerName.equalsIgnoreCase(expCustomerName)) {
                extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
            } else {
                extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
            }


            //Verify the Instructions Field
            String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();

            if (instructionsField.contains(Instructions)) {
                extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
            } else {
                extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
            }

            Thread.sleep(5000);



            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to the Case ESCALATION TAB
            WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
            Thread.sleep(10000);
            caseTabSelected.click();

            driver.switchTo().parentFrame();
            //Get the Timer value
            String firstResponseTimer = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
            Logg.logger.info("First Response is:" + firstResponseTimer);

            //Check if the Timer is not ticking and First response is Succeeded
            if (!firstResponseTimer.equalsIgnoreCase("No") && (!firstResponseTimer.equalsIgnoreCase("Yes"))) {
                extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponseTimer.toString(), "No", "");
            } else {
                extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponseTimer.toString(), "No", "");
                throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
            }
            //Get the first response value
            String firstResponse = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[6]/table/tbody/tr[3]/td[2]/div/div[1]/span")).getText();
            Logg.logger.info("First Response is:" + firstResponse);

            //Check if the Timer is not ticking and First response is Succeeded
            if (firstResponse.equalsIgnoreCase("No")) {
                extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "No", "");
            } else {
                extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "No", "");
                throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
            }


            Thread.sleep(5000);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);


            //Navigate to the Maintenance TAB
            WebElement wasteTabSelected = driver.findElement(By.id("tab_maintenance"));
            Thread.sleep(10000);
            wasteTabSelected.click();

            driver.switchTo().parentFrame();
            //Get the first response value
            String relatedWaste = driver.findElement(By.xpath("//*[@id=\"gridBodyTable\"]/tbody/tr/td[4]/div")).getText();
            if (relatedWaste.contains("Yes")) {
                extent_Pass(driver, "CONQUEST- Verification Passed", relatedWaste.toString(), "Public Litter Bin", relatedWaste);
            } else {
                extent_Warn(driver, "CONQUEST - verification Failed", relatedWaste.toString(), "Public Litter Bin", relatedWaste);
                throw new Exception(" VERIFICATION OF CONQUEST FAILED ");
            }


            Thread.sleep(10000);

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to Notes
            WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
            Thread.sleep(10000);
            notesTabSelected.click();
//        Thread.sleep(20000);

            //Navigate to Activities
            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
            actTabSelected.click();
            Thread.sleep(10000);


            String emailTrigger = driver.findElement(By.xpath("(//img[@alt='Email'])[3]")).getAttribute("src");
            Thread.sleep(20000);
            Logg.logger.info("Email:" + emailTrigger);



            //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(2000);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            caseIDRefresh.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);

            //DOCUMENT DETAILS
            //Navigate to the Document Tabs
            Thread.sleep(20000);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
            Thread.sleep(10000);
            if (documentTabSelected.isDisplayed()) {
                extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
            } else {
                extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
                throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
            }
            documentTabSelected.click();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            Thread.sleep(10000);
            driver.switchTo().parentFrame();
            Thread.sleep(5000);


            //Get the Name of the First Document
            String supportDocName1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDocName1.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC1 - Verification Passed", supportDocName1.toString(), expSupporting, supportDocName1);
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC1 - verification Failed", supportDocName1.toString(), expSupporting, supportDocName1);
                throw new Exception("DOC1 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Second Document
            String supportDoc2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (supportDoc2.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC2 - Verification Passed", supportDoc2.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC2 - verification Failed", supportDoc2.toString(), expSupporting, "");
                throw new Exception("DOC2 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Third Document
            String supportDoc3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
            if (supportDoc3.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC3 - Verification Passed", supportDoc3.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC3 - verification Failed", supportDoc3.toString(), expSupporting, "");
                throw new Exception("DOC3 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Fourth Document
            String supportDoc4 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
            if (supportDoc4.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC4 - Verification Passed", supportDoc4.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC4 - verification Failed", supportDoc4.toString(), expSupporting, "");
                throw new Exception("DOC4 NAMING CONVENTION FAILED ");
            }


            //Click the Next page to view the Documents

            WebElement docPage2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
            docPage2.click();
            Thread.sleep(10000);


            //Get the Name of the 5th Document
            String supportDoc5 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDoc5.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC5 - Verification Passed", supportDoc5.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC5 - verification Failed", supportDoc5.toString(), expSupporting, "");
                throw new Exception("DOC5 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the PDF
            String pdfDoc = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (pdfDoc.equalsIgnoreCase(expPdfFormat)) {
                extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", pdfDoc.toString(), expPdfFormat, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", pdfDoc.toString(), expPdfFormat, "");
                throw new Exception("PDF NAMING CONVENTION FAILED ");
            }


            //Get the Total Number of records
            String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
            if (totalDocuments.equalsIgnoreCase(expCount)) {
                extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
            } else {
                extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
                throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
            }


            //Click to download PDF Document
//            String parenthandle = driver.getWindowHandle();
//            WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[3]/nobr/a"));
//            pdfDocOne.click();
//            Thread.sleep(10000);
//            driver.switchTo().window(parenthandle);
//            driver.navigate().refresh();
//            Thread.sleep(2000);
//            Thread.sleep(10000);
//            driver.switchTo().defaultContent();
//            WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
//            caseIDone.click();
//
//            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
//            driver.switchTo().frame(0);
//            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);



            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }


            //Select the customer name
            Thread.sleep(20000);
            WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
            custName.click();
            Thread.sleep(10000);

            //Customer Matching
            //Contact Type
            String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
            if (contactType.equalsIgnoreCase(expType)) {
                extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
                throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
            }
//            //Get the Business Phone Number
//            String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//            if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//                throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//            }
//            //Get the Mobile Number
//            String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//            if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//                throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//            }
//            //Get the Landline Number
//            String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//            if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//                throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//            }

            //Get the First Name
            String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
            if (firstName.equalsIgnoreCase(expFName)) {
                extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
                throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
            }

            //Get the Last Name OR Business Name
            String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
            if (lastName.equalsIgnoreCase(expLastName)) {
                extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLastName, lastName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLastName, lastName);
                throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
            }
            //Get the Email
            String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
            if (emailName.equalsIgnoreCase(expEmail)) {
                extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
                throw new Exception(" VERIFICATION OF EMAIL FAILED ");
            }


            Thread.sleep(5000);


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
            Thread.sleep(10000);


            //Navigate to the Customer Case Records Tab
            WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
            Thread.sleep(10000);
            customerCaseRecords.click();

            Thread.sleep(5000);

            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
            Logg.logger.info("Total Number of Cases:" + caseRecords);


            Thread.sleep(10000);
            navigateBack(driver);
            Thread.sleep(10000);

            driver.quit();

            return this;
        }


        @Test(dataProvider = "raii_DataProvider", description = "Verify that the CRM values are verified")
        public ReportAnIssuePage captureCRMInfrastructure (String expCaseType, String expSubType, String expCustomerName, String expLocation, String Instructions,
                                                           String expSupporting, String expPdfFormat, String expCount, String expType,
                                                           String expBusinessNo, String expMobileNo, String expAlternateNo,
                                                           String expFName, String expLastName, String expEmail, String expCaseCount) throws Exception {
            Thread.sleep(15000);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--no-sandbox");
            options.addArguments("--headless");
            options.addArguments("start-maximized");
            options.addArguments("window-size=1920,1080");
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
            System.out.println(driver.manage().window().getSize());
            driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
            driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
            System.out.println(driver.manage().window().getSize());

            Thread.sleep(20000);
            WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
            sitemapHomeTab.click();
            Thread.sleep(4000);
            WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
            serviceAreaElement.click();
            Thread.sleep(10000);
            WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
            serviceRightLink.click();
            Thread.sleep(10000);


            //Switch to the frame that contains the grid
            driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
            Thread.sleep(10000);


            Actions action = new Actions(driver);
            String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
            System.out.println("The GUID text in CRM is " + identifierText + "");
            WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
            Logg.logger.info("Is the GUID correct:" + identifier);



            action.doubleClick(identifier).perform();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(2000);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            if (caseID.isDisplayed()) {
                extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
                throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
            }
            caseID.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);


            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseName.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }
            Thread.sleep(5000);
            //Select the Sub-case Type
            String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
            System.out.println("The SubcaseType is " + subCaseName + "");
            if (subCaseName.equalsIgnoreCase(expSubType)) {
                extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
            }
            //Select the origin
            String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
            Logg.logger.info("Origin name:" + originName);
            if (originName.equalsIgnoreCase("Web")) {
                extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            } else {
                extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
            }
            //Select the customer name
            String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
            Logg.logger.info("Customer name:" + customerName);

            if (customerName.equalsIgnoreCase(expCustomerName)) {
                extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
            } else {
                extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
            }


            //Verify the Instructions Field
            String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();

            if (instructionsField.contains(Instructions)) {
                extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
            } else {
                extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), Instructions, "");
                throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
            }

            Thread.sleep(5000);


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to the Case ESCALATION TAB
            WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
            Thread.sleep(10000);
            caseTabSelected.click();

            driver.switchTo().parentFrame();
            //Get the Timer value
            String firstResponseTimer = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
            Logg.logger.info("First Response is:" + firstResponseTimer);

            //Check if the Timer is not ticking and First response is Succeeded
            if (!firstResponseTimer.equalsIgnoreCase("No") && (!firstResponseTimer.equalsIgnoreCase("Yes"))) {
                extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponseTimer.toString(), "No", "");
            } else {
                extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponseTimer.toString(), "No", "");
                throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
            }
            //Get the first response value
            String firstResponse = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[6]/table/tbody/tr[3]/td[2]/div/div[1]/span")).getText();
            Logg.logger.info("First Response is:" + firstResponse);

            //Check if the Timer is not ticking and First response is Succeeded
            if (firstResponse.equalsIgnoreCase("No")) {
                extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "No", "");
            } else {
                extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "No", "");
                throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
            }




            Thread.sleep(10000);

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to Notes
            WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
            Thread.sleep(10000);
            notesTabSelected.click();
//        Thread.sleep(20000);

            //Navigate to Activities
            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
            actTabSelected.click();
            Thread.sleep(10000);


            String emailTrigger = driver.findElement(By.xpath("(//img[@alt='Email'])[3]")).getAttribute("src");
            Thread.sleep(20000);
            Logg.logger.info("Email:" + emailTrigger);


            //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(2000);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            caseIDRefresh.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);

            //DOCUMENT DETAILS
            //Navigate to the Document Tabs
            Thread.sleep(20000);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
            Thread.sleep(10000);
            if (documentTabSelected.isDisplayed()) {
                extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
            } else {
                extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
                throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
            }
            documentTabSelected.click();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            Thread.sleep(10000);
            driver.switchTo().parentFrame();
            Thread.sleep(5000);


            //Get the Name of the First Document
            String supportDocName1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDocName1.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC1 - Verification Passed", supportDocName1.toString(), expSupporting, supportDocName1);
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC1 - verification Failed", supportDocName1.toString(), expSupporting, supportDocName1);
                throw new Exception("DOC1 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Second Document
            String supportDoc2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (supportDoc2.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC2 - Verification Passed", supportDoc2.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC2 - verification Failed", supportDoc2.toString(), expSupporting, "");
                throw new Exception("DOC2 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Third Document
            String supportDoc3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
            if (supportDoc3.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC3 - Verification Passed", supportDoc3.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC3 - verification Failed", supportDoc3.toString(), expSupporting, "");
                throw new Exception("DOC3 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Fourth Document
            String supportDoc4 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
            if (supportDoc4.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC4 - Verification Passed", supportDoc4.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC4 - verification Failed", supportDoc4.toString(), expSupporting, "");
                throw new Exception("DOC4 NAMING CONVENTION FAILED ");
            }


            //Click the Next page to view the Documents

            WebElement docPage2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
            docPage2.click();
            Thread.sleep(10000);


            //Get the Name of the 5th Document
            String supportDoc5 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDoc5.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC5 - Verification Passed", supportDoc5.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC5 - verification Failed", supportDoc5.toString(), expSupporting, "");
                throw new Exception("DOC5 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the PDF
            String pdfDoc = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (pdfDoc.equalsIgnoreCase(expPdfFormat)) {
                extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", pdfDoc.toString(), expPdfFormat, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", pdfDoc.toString(), expPdfFormat, "");
                throw new Exception("PDF NAMING CONVENTION FAILED ");
            }


            //Get the Total Number of records
            String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
            if (totalDocuments.equalsIgnoreCase(expCount)) {
                extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
            } else {
                extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
                throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
            }


            //Click to download PDF Document
//            String parenthandle = driver.getWindowHandle();
//            WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[3]/nobr/a"));
//            pdfDocOne.click();
//            Thread.sleep(10000);
//            driver.switchTo().window(parenthandle);
//            driver.navigate().refresh();
//            Thread.sleep(2000);
//            Thread.sleep(10000);
//            driver.switchTo().defaultContent();
//            WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
//            caseIDone.click();
//
//            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
//            driver.switchTo().frame(0);
//            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }


            //Select the customer name
            Thread.sleep(20000);
            WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
            custName.click();
            Thread.sleep(10000);

            //Customer Matching
            //Contact Type
            String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
            if (contactType.equalsIgnoreCase(expType)) {
                extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
                throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
            }
//            //Get the Business Phone Number
//            String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//            if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//                throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//            }
//            //Get the Mobile Number
//            String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//            if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//                throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//            }
//            //Get the Landline Number
//            String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//            if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//                throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//            }

            //Get the First Name
            String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
            if (firstName.equalsIgnoreCase(expFName)) {
                extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
                throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
            }

            //Get the Last Name OR Business Name
            String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
            if (lastName.equalsIgnoreCase(expLastName)) {
                extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLastName, lastName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLastName, lastName);
                throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
            }
            //Get the Email
            String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
            if (emailName.equalsIgnoreCase(expEmail)) {
                extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
                throw new Exception(" VERIFICATION OF EMAIL FAILED ");
            }


            Thread.sleep(5000);


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
            Thread.sleep(10000);


            //Navigate to the Customer Case Records Tab
            WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
            Thread.sleep(10000);
            customerCaseRecords.click();

            Thread.sleep(5000);

            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
            Logg.logger.info("Total Number of Cases:" + caseRecords);


            Thread.sleep(10000);
            navigateBack(driver);
            Thread.sleep(10000);

            driver.quit();

            return this;
        }

        @Test(dataProvider = "rai_DataProvider", description = "Verify that the CRM values are verified")
        public ReportAnIssuePage captureCRMParksInfra (String expCaseType, String expSubType, String expCustomerName, String expLocation, String Instructions,
                                                       String expSupporting, String expPdfFormat, String expCount, String expType,
                                                       String expBusinessNo, String expMobileNo, String expAlternateNo,
                                                       String expFName, String expLastName, String expEmail, String expCaseCount) throws Exception {

            Thread.sleep(15000);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--no-sandbox");
            options.addArguments("--headless");
            options.addArguments("start-maximized");
            options.addArguments("window-size=1920,1080");
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
            System.out.println(driver.manage().window().getSize());
            driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
            driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
            System.out.println(driver.manage().window().getSize());

            Thread.sleep(20000);
            WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
            sitemapHomeTab.click();
            Thread.sleep(4000);
            WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
            serviceAreaElement.click();
            Thread.sleep(10000);
            WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
            serviceRightLink.click();
            Thread.sleep(10000);


            //Switch to the frame that contains the grid
            driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
            Thread.sleep(10000);


            Actions action = new Actions(driver);
            String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
            System.out.println("The GUID text in CRM is " + identifierText + "");
            WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
            Logg.logger.info("Is the GUID correct:" + identifier);



            action.doubleClick(identifier).perform();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(10000);
            driver.navigate().refresh();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            if (caseID.isDisplayed()) {
                extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
                throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
            }
            caseID.click();


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);


            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseName.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }
            Thread.sleep(5000);
            //Select the Sub-case Type
            String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
            System.out.println("The SubcaseType is " + subCaseName + "");
            if (subCaseName.equalsIgnoreCase(expSubType)) {
                extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
            }
            //Select the origin
            String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
            Logg.logger.info("Origin name:" + originName);
            if (originName.equalsIgnoreCase("Web")) {
                extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            } else {
                extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
            }
            //Select the customer name
            String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
            Logg.logger.info("Customer name:" + customerName);

            if (customerName.equalsIgnoreCase(expCustomerName)) {
                extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
            } else {
                extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
            }


            //Verify the Instructions Field
            String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();

            if (instructionsField.contains(Instructions)) {
                extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
            } else {
                extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), Instructions, "");
                throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
            }

            Thread.sleep(5000);



            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to the Case ESCALATION TAB
            WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
            Thread.sleep(10000);
            caseTabSelected.click();

            driver.switchTo().parentFrame();
            //Get the Timer value
            String firstResponseTimer = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
            Logg.logger.info("First Response is:" + firstResponseTimer);

            //Check if the Timer is not ticking and First response is Succeeded
            if (!firstResponseTimer.equalsIgnoreCase("No") && (!firstResponseTimer.equalsIgnoreCase("Yes"))) {
                extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponseTimer.toString(), "No", "");
            } else {
                extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponseTimer.toString(), "No", "");
                throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
            }
            //Get the first response value
            String firstResponse = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[6]/table/tbody/tr[3]/td[2]/div/div[1]/span")).getText();
            Logg.logger.info("First Response is:" + firstResponse);

            //Check if the Timer is not ticking and First response is Succeeded
            if (firstResponse.equalsIgnoreCase("No")) {
                extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "No", "");
            } else {
                extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "No", "");
                throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
            }




            Thread.sleep(10000);

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to Notes
            WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
            Thread.sleep(10000);
            notesTabSelected.click();
//        Thread.sleep(20000);

            //Navigate to Activities
            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
            actTabSelected.click();
            Thread.sleep(10000);


            String emailTrigger = driver.findElement(By.xpath("(//img[@alt='Email'])[3]")).getAttribute("src");
            Thread.sleep(20000);
            Logg.logger.info("Email:" + emailTrigger);


            //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(2000);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            caseIDRefresh.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);

            //DOCUMENT DETAILS
            //Navigate to the Document Tabs
            Thread.sleep(20000);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
            Thread.sleep(10000);
            if (documentTabSelected.isDisplayed()) {
                extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
            } else {
                extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
                throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
            }
            documentTabSelected.click();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            Thread.sleep(10000);
            driver.switchTo().parentFrame();
            Thread.sleep(5000);


            //Get the Name of the First Document
            String supportDocName1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDocName1.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC1 - Verification Passed", supportDocName1.toString(), expSupporting, supportDocName1);
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC1 - verification Failed", supportDocName1.toString(), expSupporting, supportDocName1);
                throw new Exception("DOC1 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Second Document
            String supportDoc2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (supportDoc2.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC2 - Verification Passed", supportDoc2.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC2 - verification Failed", supportDoc2.toString(), expSupporting, "");
                throw new Exception("DOC2 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Third Document
            String supportDoc3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
            if (supportDoc3.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC3 - Verification Passed", supportDoc3.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC3 - verification Failed", supportDoc3.toString(), expSupporting, "");
                throw new Exception("DOC3 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Fourth Document
            String supportDoc4 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
            if (supportDoc4.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC4 - Verification Passed", supportDoc4.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC4 - verification Failed", supportDoc4.toString(), expSupporting, "");
                throw new Exception("DOC4 NAMING CONVENTION FAILED ");
            }


            //Click the Next page to view the Documents

            WebElement docPage2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
            docPage2.click();
            Thread.sleep(10000);


            //Get the Name of the 5th Document
            String supportDoc5 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDoc5.equalsIgnoreCase(expSupporting)) {
                extent_Pass(driver, "NAME FORMAT - DOC5 - Verification Passed", supportDoc5.toString(), expSupporting, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DOC5 - verification Failed", supportDoc5.toString(), expSupporting, "");
                throw new Exception("DOC5 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the PDF
            String pdfDoc = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (pdfDoc.equalsIgnoreCase(expPdfFormat)) {
                extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", pdfDoc.toString(), expPdfFormat, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", pdfDoc.toString(), expPdfFormat, "");
                throw new Exception("PDF NAMING CONVENTION FAILED ");
            }


            //Get the Total Number of records
            String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
            if (totalDocuments.equalsIgnoreCase(expCount)) {
                extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
            } else {
                extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
                throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
            }


            //Click to download PDF Document
//            String parenthandle = driver.getWindowHandle();
//            WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[3]/nobr/a"));
//            pdfDocOne.click();
//            Thread.sleep(10000);
//            driver.switchTo().window(parenthandle);
//            driver.navigate().refresh();
//            Thread.sleep(2000);
//            Thread.sleep(10000);
//            driver.switchTo().defaultContent();
//            WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
//            caseIDone.click();
//
//            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
//            driver.switchTo().frame(0);
//            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }


            //Select the customer name
            Thread.sleep(20000);
            WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
            custName.click();
            Thread.sleep(10000);

            //Customer Matching
            //Contact Type
            String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
            if (contactType.equalsIgnoreCase(expType)) {
                extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
                throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
            }
//            //Get the Business Phone Number
//            String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//            if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//                throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//            }
//            //Get the Mobile Number
//            String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//            if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//                throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//            }
//            //Get the Landline Number
//            String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//            if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//                extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//            } else {
//                extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//                throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//            }

            //Get the First Name
            String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
            if (firstName.equalsIgnoreCase(expFName)) {
                extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
                throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
            }

            //Get the Last Name OR Business Name
            String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
            if (lastName.equalsIgnoreCase(expLastName)) {
                extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLastName, lastName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLastName, lastName);
                throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
            }
            //Get the Email
            String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
            if (emailName.equalsIgnoreCase(expEmail)) {
                extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
                throw new Exception(" VERIFICATION OF EMAIL FAILED ");
            }


            Thread.sleep(5000);


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
            Thread.sleep(10000);


            //Navigate to the Customer Case Records Tab
            WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
            Thread.sleep(10000);
            customerCaseRecords.click();

            Thread.sleep(5000);

            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
            Logg.logger.info("Total Number of Cases:" + caseRecords);


            Thread.sleep(10000);
            navigateBack(driver);
            Thread.sleep(10000);

            driver.quit();

            return this;
        }


//        @Test(dataProvider = "rai_DataProvider", description = "Verify that the CRM values are verified")
//
//        public ReportAnIssueWastePage readPDF (String pdf, String pdfIssueType, String pdfUser, String
//        pdfSubType, String pdfSubQuestion, String pdfBinType, String pdfDescribeIssue,
//                String pdfpark, String pdfAddressName, String pdfAddress, String pdfDescribeloc,
//                String pdfFname, String pdfLname, String pdfOrgName, String pdfEmail,
//                String pdfMob, String pdflanNum) throws IOException {
//            Logg.logger.info("");
//
//            PDFReader pdfManager = new PDFReader();
//
//            pdfManager.setFilePath("C:\\Users\\rnair\\Downloads\\" + pdf + ".pdf");
//
//            try {
//
//                String text = pdfManager.toText();
//                String[] linesFile = text.split("\n");// this array is initialized with a single element
//                String issueTypePDF = linesFile[4].replaceFirst("^\\s* ", "");
//                System.out.println(issueTypePDF);
//                System.out.println(pdfIssueType);
//                if (reviewPage.contains(text)) {
//                    Logg.logger.info(LogStatus.PASS);
//                    System.out.println("The element present in the review page are also present in the CRM Form PDF");
//
//
//                } else {
//                    Logg.logger.info(LogStatus.PASS);
//                    System.out.println("The element present in the review page are also present in the CRM Form PDF");
//                }
//                System.out.println(text);
//
//            } catch (IOException ex) {
//                Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//
//            return this;
//        }


    @DataProvider

    public Object[][] rai_DataProvider () throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "rai");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "rai", 39);

        return testObjArray;
    }

    @DataProvider
    public Object[][] raim_DataProvider () throws Exception {

        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "raim");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "raim", 39);

        return testObjArray;
    }

    @DataProvider
    public Object[][] raii_DataProvider () throws Exception {

        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "raii");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "raii", 39);

        return testObjArray;
    }

    @DataProvider
    public Object[][] raip_DataProvider () throws Exception {

        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "raip");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "raip", 39);

        return testObjArray;
    }
    }










