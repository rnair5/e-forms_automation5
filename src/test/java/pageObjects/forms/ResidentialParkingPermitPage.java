package pageObjects.forms;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.awt.*;
import java.text.ParseException;

import static libraries.Utilities.*;

public class ResidentialParkingPermitPage extends BaseFormsClass {
    String formNo="208";
    String propertyType;
    int specifiedNo=0;
    int visitorNo=0;
    public ResidentialParkingPermitPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }


    //Submit Form
    @FindBy(id="submitbtn")
    private WebElement finalSubmit;

    String form_ID="#gform_page_"+formNo;
    @FindBy(id = "input_208_4")
    private WebElement address;
    @FindBy(id = "label_208_6_0")
    private WebElement singleDwelling;
    @FindBy(id = "label_208_6_1")
    private WebElement multiDwelling;
    @FindBy(id = "label_208_6_2")
    private WebElement shopping;
    @FindBy(id = "input_208_100")
    private WebElement residentialFile;
    @FindBy(id="gform_browse_button_208_100")
    private WebElement fileBrowseButton;
    @FindBy(id="label_208_99_0")
    private WebElement propertyOwner;
    @FindBy(id="label_208_99_1")
    private WebElement tenant;
    @FindBy(id="label_208_63_0")
    private WebElement permanentRes;
    @FindBy(id="label_208_63_1")
    private WebElement notPermanentRes;
    @FindBy(id="label_208_162_1")
    private WebElement residentialProofSighted;
    //House
    @FindBy(id="label_208_106_0")
    private WebElement noneSpecified;
    @FindBy(id="label_208_154_0")
    private WebElement noneS1Visitor;
    @FindBy(id="label_208_154_1")
    private WebElement noneS2Visitor;

    @FindBy(id="label_208_106_1")
    private WebElement oneSpecified;
    @FindBy(id="label_208_108_0")
    private WebElement oneS0Visitor;
    @FindBy(id="label_208_108_1")
    private WebElement oneS1Visitor;
    @FindBy(id="label_208_108_2")
    private WebElement oneS2Visitor;

    @FindBy(id="label_208_106_2")
    private WebElement twoSpecified;
    @FindBy(id="label_208_106_3")
    private WebElement threeSpecified;
    @FindBy(id="label_208_109_0")
    private WebElement twoS1Visitor;
    @FindBy(id="label_208_109_1")
    private WebElement twoS0Visitor;

    //Multi
    @FindBy(id="label_208_110_0")
    private WebElement specifiedPermit;
    @FindBy(id="label_208_110_1")
    private WebElement visitorPermit;


    @FindBy(id="input_208_146")
    private WebElement specified1Fname;
    @FindBy(id="input_208_147")
    private WebElement specified1Lname;
    @FindBy(id="input_208_116")
    private WebElement specified1Rego;
    @FindBy(id="label_208_117_0")
    private WebElement specified1CommercialYes;
    @FindBy(id="label_208_117_1")
    private WebElement specified1CommercialNo;
    @FindBy(id="input_208_118")
    private WebElement specified1RegoFile;
    @FindBy(id="gform_browse_button_208_118")
    private WebElement specified1BrowseButton;
    @FindBy(id="label_208_163_1")
    private WebElement specified1Sighted;

    @FindBy(id="input_208_150")
    private WebElement specified2Fname;
    @FindBy(id="input_208_151")
    private WebElement specified2Lname;
    @FindBy(id="input_208_123")
    private WebElement specified2Rego;
    @FindBy(id="label_208_124_0")
    private WebElement specified2CommercialYes;
    @FindBy(id="label_208_124_1")
    private WebElement specified2CommercialNo;
    @FindBy(id="input_208_125")
    private WebElement specified2RegoFile;
    @FindBy(id="gform_browse_button_208_125")
    private WebElement specified2BrowseButton;
    @FindBy(id="label_208_165_1")
    private WebElement specified2Sighted;

    @FindBy(id="input_208_152")
    private WebElement specified3Fname;
    @FindBy(id="input_208_153")
    private WebElement specified3Lname;
    @FindBy(id="input_208_129")
    private WebElement specified3Rego;
    @FindBy(id="label_208_130_0")
    private WebElement specified3CommercialYes;
    @FindBy(id="label_208_130_1")
    private WebElement specified3CommercialNo;
    @FindBy(id="input_208_131")
    private WebElement specified3RegoFile;
    @FindBy(id="gform_browse_button_208_131")
    private WebElement specified3BrowseButton;
    @FindBy(id="label_208_166_1")
    private WebElement specified3Sighted;

    @FindBy(id = "input_208_85")
    private WebElement permitHolderFname;
    @FindBy(id = "input_208_86")
    private WebElement permitHolderLname;
    @FindBy(id = "input_208_14")
    private WebElement permitHolderEmail;
    @FindBy(id = "input_208_14_2")
    private WebElement permitHolderConfEmail;
    @FindBy(id = "input_208_35")
    private WebElement permitHolderPhoneType;
    @FindBy(id = "input_208_144")
    private WebElement permitHolderPhoneNo;
    @FindBy(id = "label_208_139_1")
    private WebElement smsNotificationLabel;
    @FindBy(id = "choice_208_139_1")
    private WebElement smsNotification;
    @FindBy(id = "label_208_149_1")
    private WebElement privacyCheckLabel;
    @FindBy(id = "choice_208_149_1")
    private WebElement privacyCheck;
    @FindBy(id = "gform_submit_button_208")
    private WebElement submitButton;
    @FindBy(id="input_208_167")
    private WebElement csoEmail;

    @FindBy(id="label_208_102_1")
    private WebElement postalAddressDifferent;
    @FindBy(id="input_208_103_1")
    private WebElement streetAddress;
    @FindBy(id="input_208_103_3")
    private WebElement suburb;
    @FindBy(id="input_208_103_4")
    private WebElement stateDropdown;
    @FindBy(id="input_208_103_5")
    private WebElement postCode;

//    public ResidentialParkingPermitPage clickNext(String pageNo)
//    {
//        Logg.logger.info("PageNo:"+pageNo);
//        super.clickNext(formNo,pageNo);
//        return this;
//    }

    public ResidentialParkingPermitPage clickNext(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickNext(formNo,pageNo);
        return this;
    }
    public ResidentialParkingPermitPage clickPrevious(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickPrevious(formNo,pageNo);
        return this;
    }
    public ResidentialParkingPermitPage setPermitAddress(String address) throws InterruptedException {
        Logg.logger.info("address:"+address);
        selectFirstItemFromLookup(driver,this.address,address);
        Thread.sleep(5000);
        return this;
    }
    public ResidentialParkingPermitPage setPropertyType(String propertyType)
    {
        Logg.logger.info("Property Type:"+propertyType);
        this.propertyType = propertyType;
        if (propertyType.equalsIgnoreCase("House"))
            waitAndClick(driver,singleDwelling);
        if(propertyType.equalsIgnoreCase("Multi"))
            waitAndClick(driver,multiDwelling);
        if(propertyType.equalsIgnoreCase("Shopping"))
            waitAndClick(driver,shopping);
        return this;
    }
    public ResidentialParkingPermitPage setPropertyOwnerTenant(String type)
    {
        Logg.logger.info("Owner or Tenant:"+type);
        if (type.equalsIgnoreCase("Owner"))
            waitAndClick(driver,propertyOwner);
        if(type.equalsIgnoreCase("Tenant"))
            waitAndClick(driver,tenant);
        return this;
    }
    public ResidentialParkingPermitPage setPR(String type)
    {
        Logg.logger.info("Permanent Resident:"+type);
        if (type.equalsIgnoreCase("Yes"))
            waitAndClick(driver,permanentRes);
        if(type.equalsIgnoreCase("No"))
            waitAndClick(driver,notPermanentRes);
        return this;
    }
    public ResidentialParkingPermitPage uploadResidentialStatus() throws InterruptedException, AWTException {
//        Logg.logger.info("Filename:"+fileName);
//        try {
//            uploadFile(fileBrowseButton, fileName);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[13]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        //waitAndSendKeys(driver,residentialFile,fileName);
        return this;
    }

    public ResidentialParkingPermitPage uploadFile() throws InterruptedException, AWTException {
//        Logg.logger.info("Filename:"+fileName);
//        try {
//            uploadFile(fileBrowseButton, fileName);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[3]/div[1]/ul/li[19]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        //waitAndSendKeys(driver,residentialFile,fileName);
        return this;
    }
    public ResidentialParkingPermitPage setPermits(String specifiedPermit, String visitorPermit)
    {
        Logg.logger.info("Specified permit:"+specifiedPermit +", Visitor permit:"+visitorPermit);
        if(propertyType.equalsIgnoreCase("multi"))
        {
            if (specifiedPermit.equalsIgnoreCase("0"))
            {
                waitAndClick(driver, noneSpecified);
                specifiedNo=0;
                if (visitorPermit.equalsIgnoreCase("1")) {
                    waitAndClick(driver, noneS1Visitor);
                    visitorNo=1;
                }
                else if (visitorPermit.equalsIgnoreCase("2")) {
                    waitAndClick(driver, noneS2Visitor);
                    visitorNo=2;
                }

            }
            else if (specifiedPermit.equalsIgnoreCase("1"))
            {
                waitAndClick(driver, oneSpecified);
                specifiedNo=1;
                if(visitorPermit.equalsIgnoreCase("0")) {
                    waitAndClick(driver, oneS0Visitor);
                    visitorNo=0;
                }
                else if(visitorPermit.equalsIgnoreCase("1")) {
                    waitAndClick(driver, oneS1Visitor);
                    visitorNo=1;
                }
                else if(visitorPermit.equalsIgnoreCase("2")) {
                    waitAndClick(driver, oneS2Visitor);
                    visitorNo=2;
                }
            }
            else if (specifiedPermit.equalsIgnoreCase("2"))
            {
                waitAndClick(driver, twoSpecified);
                specifiedNo=2;
                if(visitorPermit.equalsIgnoreCase("0")) {
                    waitAndClick(driver, twoS0Visitor);
                    visitorNo=0;
                }
                else if(visitorPermit.equalsIgnoreCase("1")) {
                    waitAndClick(driver, twoS1Visitor);
                    visitorNo=1;
                }
            }
            else if (specifiedPermit.equalsIgnoreCase("3"))
            {
                waitAndClick(driver, threeSpecified);
                specifiedNo=3;
                visitorNo=0;
            }
        }
        else if(propertyType.equalsIgnoreCase("house"))
        {
//            if (specifiedPermit.equalsIgnoreCase("1")) {
                waitAndClick(driver, this.specifiedPermit);
                specifiedNo=1;

            if(visitorPermit.equalsIgnoreCase("1")) {
                waitAndClick(driver, this.visitorPermit);
                visitorNo=1;
            }
        }
        else
        {
            if(specifiedPermit.equalsIgnoreCase("1"))
                specifiedNo=1;
        }
        return this;
    }
    public ResidentialParkingPermitPage setSpecified1ProofSighted()
    {
        Logg.logger.info("");
        waitAndClick(driver,specified1Sighted);
        return this;
    }
    public ResidentialParkingPermitPage setSpecified2ProofSighted()
    {
        Logg.logger.info("");
        waitAndClick(driver,specified2Sighted);
        return this;
    }
    public ResidentialParkingPermitPage setSpecified3ProofSighted()
    {
        Logg.logger.info("");
        waitAndClick(driver,specified3Sighted);
        return this;
    }
    public ResidentialParkingPermitPage setResidentialProofSighted()
    {
        Logg.logger.info("");
        waitAndClick(driver,residentialProofSighted);
        return this;
    }

    public ResidentialParkingPermitPage setSpecifiedDetails(String specifiedNo, String Fname, String Lname, String rego, String isCommercial,boolean isCso) throws InterruptedException, AWTException {
        Logg.logger.info(String.format("Fname:%s, Lname:%s, Rego:%s, IsCommercial:%s", Fname, Lname, rego,  isCommercial));
        if (specifiedNo.equalsIgnoreCase("1")) {
            waitAndSendKeys(driver, specified1Fname, Fname);
            waitAndSendKeys(driver, specified1Lname, Lname);
            waitAndSendKeys(driver, specified1Rego, rego);
            //waitAndSendKeys(driver,specified1RegoFile, regoFile);
//            if(isCso)
//                setSpecified1ProofSighted();
//            else
//
//                uploadFile(specified1BrowseButton,regoFile);
            if (isCommercial.equalsIgnoreCase("Yes"))
                waitAndClick(driver, specified1CommercialYes);
            else if (isCommercial.equalsIgnoreCase("No"))
                waitAndClick(driver, specified1CommercialNo);
        } else if (specifiedNo.equalsIgnoreCase("2")) {
            waitAndSendKeys(driver, specified2Fname, Fname);
            waitAndSendKeys(driver, specified2Lname, Lname);
            waitAndSendKeys(driver, specified2Rego, rego);
            //waitAndSendKeys(driver,specified2RegoFile, regoFile);
//            if(isCso)
//                setSpecified2ProofSighted();
//            else
//                driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[13]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
////                uploadFile(specified2BrowseButton,regoFile);

            if (isCommercial.equalsIgnoreCase("Yes"))
                waitAndClick(driver, specified2CommercialYes);
            else if (isCommercial.equalsIgnoreCase("No"))
                waitAndClick(driver, specified2CommercialNo);
        } else if (specifiedNo.equalsIgnoreCase("3")) {
            waitAndSendKeys(driver, specified3Fname, Fname);
            waitAndSendKeys(driver, specified3Lname, Lname);
            waitAndSendKeys(driver, specified3Rego, rego);
            //waitAndSendKeys(driver,specified3RegoFile, regoFile);
//            if(isCso)
//                setSpecified3ProofSighted();
//            else
//                driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[13]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
////                uploadFile(specified3BrowseButton,regoFile);
            if (isCommercial.equalsIgnoreCase("Yes"))
                waitAndClick(driver, specified3CommercialYes);
            else if (isCommercial.equalsIgnoreCase("No"))
                waitAndClick(driver, specified3CommercialNo);
        }
        return this;
    }


//    public ResidentialParkingPermitPage setSpecified (String s1Fname, String s1Lname, String s1rego, String s1regoFile, String s1isCommercial,String s2Fname, String s2Lname, String s2rego, String s2regoFile, String s2isCommercial, String s3Fname, String s3Lname, String s3rego, String s3regoFile, String s3isCommercial,boolean isCso) throws AWTException, InterruptedException {
//        Logg.logger.info(String.format("s1Fname:%s, s1Lname:%s, s1Rego:%s, s1regofile:%s, s1IsCommercial:%s,s2Fname:%s, s2Lname:%s, s2Rego:%s, s2regofile:%s, s2IsCommercial:%s,s3Fname:%s, s3Lname:%s, s3Rego:%s, s3regofile:%s, s3IsCommercial:%s, isCso:", s1Fname, s1Lname, s1rego, s1regoFile, s1isCommercial,s2Fname, s2Lname, s2rego, s2regoFile, s2isCommercial,s3Fname, s3Lname, s3rego, s3regoFile, s3isCommercial,isCso));
//        if(specifiedNo==1)
//            setSpecifiedDetails("1",s1Fname,s1Lname,s1rego,s1regoFile,s1isCommercial,isCso);
//        else if(specifiedNo==2) {
//            setSpecifiedDetails("1", s1Fname, s1Lname, s1rego, s1regoFile, s1isCommercial,isCso);
//            setSpecifiedDetails("2", s2Fname, s2Lname, s2rego, s2regoFile, s2isCommercial,isCso);
//        }
//        else if(specifiedNo==3) {
//            setSpecifiedDetails("1", s1Fname, s1Lname, s1rego, s1regoFile, s1isCommercial,isCso);
//            setSpecifiedDetails("2", s2Fname, s2Lname, s2rego, s2regoFile, s2isCommercial,isCso);
//            setSpecifiedDetails("3",s3Fname,s3Lname,s3rego,s3regoFile,s3isCommercial,isCso);
//        }
//        return this;
//    }

    public ResidentialParkingPermitPage setSpecified (String s1Fname, String s1Lname, String s1rego, String s1isCommercial,String s2Fname, String s2Lname, String s2rego,  String s2isCommercial, String s3Fname, String s3Lname, String s3rego,  String s3isCommercial,boolean isCso) throws AWTException, InterruptedException {
        Logg.logger.info(String.format("s1Fname:%s, s1Lname:%s, s1Rego:%s, s1IsCommercial:%s,s2Fname:%s, s2Lname:%s, s2Rego:%s,  s2IsCommercial:%s,s3Fname:%s, s3Lname:%s, s3Rego:%s,  s3IsCommercial:%s, isCso:", s1Fname, s1Lname, s1rego, s1isCommercial,s2Fname, s2Lname, s2rego, s2isCommercial,s3Fname, s3Lname, s3rego,  s3isCommercial,isCso));
        if(specifiedNo==1)
            setSpecifiedDetails("1",s1Fname,s1Lname,s1rego,s1isCommercial,isCso);
        else if(specifiedNo==2) {
            setSpecifiedDetails("1", s1Fname, s1Lname, s1rego, s1isCommercial,isCso);
            setSpecifiedDetails("2", s2Fname, s2Lname, s2rego, s2isCommercial,isCso);
        }
        else if(specifiedNo==3) {
            setSpecifiedDetails("1", s1Fname, s1Lname, s1rego,  s1isCommercial,isCso);
            setSpecifiedDetails("2", s2Fname, s2Lname, s2rego,  s2isCommercial,isCso);
            setSpecifiedDetails("3",s3Fname,s3Lname,s3rego,s3isCommercial,isCso);
        }
        return this;
    }

    public ResidentialParkingPermitPage setPersonalDetails(String fname,String lname, String phonetype, String phoneNumber,   String sms) throws ParseException {
        Logg.logger.info(String.format("Permit Holder details: Permit Holder Fname:%s, Permit Holder Lname:%s, Permit holder Phone type:%s,Permit holder Phone:%s,Permit holder receive SMS notification:%s", fname,lname,phonetype,phoneNumber,sms));
        setPermitHolderFName(fname)
                .setPermitHolderLName(lname)
                .setPermitHolderPhoneType(phonetype)
                .setPermitHolderPhoneNumber(phoneNumber);
        if(phonetype.equalsIgnoreCase("mobile"))
            setSMSNotifications(sms);
        return this;
    }
    public ResidentialParkingPermitPage setCsoEmail(String email)
    {
        Logg.logger.info("Email:"+email);
        waitAndSendKeys(driver,csoEmail,email);
        return this;
    }
    public ResidentialParkingPermitPage setPermitHolderFName(String fname)
    {
        Logg.logger.info("Permit Holder First Name:"+fname);
        waitAndSendKeys(driver,permitHolderFname,fname);
        return this;
    }
    public ResidentialParkingPermitPage setPermitHolderLName(String lname)
    {
        Logg.logger.info("Permit Holder last name:"+lname);
        waitAndSendKeys(driver,permitHolderLname,lname);
        return this;
    }

    public ResidentialParkingPermitPage setPermitHolderEmail(String email)
    {
        Logg.logger.info("Permit Holder email:"+email);
        waitAndSendKeys(driver,permitHolderEmail,email);
        return this;
    }
    public ResidentialParkingPermitPage setPermitHolderConfEmail(String email)
    {
        Logg.logger.info("Permit Holder Confirmation Email:"+email);
        waitAndSendKeys(driver,permitHolderConfEmail,email);
        return this;
    }
    public ResidentialParkingPermitPage setPermitHolderPhoneType(String phoneType)
    {
        Logg.logger.info("Permit Holder phone type:"+phoneType);
        selectDropdown_VisibleText(driver,permitHolderPhoneType,phoneType);
        return this;
    }
    public ResidentialParkingPermitPage setPermitHolderPhoneNumber(String phoneNumber)
    {
        Logg.logger.info("Permit Holder Phone number:"+phoneNumber);
        waitAndSendKeys(driver,permitHolderPhoneNo,phoneNumber);
        return this;
    }
    public ResidentialParkingPermitPage setSMSNotifications(String option)
    {
        Logg.logger.info("Set SMS Notifications to:"+option);
        if(option.equalsIgnoreCase("yes"))
            if(!smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        if(option.equalsIgnoreCase("no"))
            if(smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        return this;
    }
    public ResidentialParkingPermitPage setPolicy(String option)
    {
        Logg.logger.info("Set read &accept privacy policy to:"+option);
        if(option.equalsIgnoreCase("yes")) {
            if(!privacyCheck.isSelected())
                waitAndClick(driver,privacyCheckLabel);
        }
        if(option.equalsIgnoreCase("no"))
            if(privacyCheck.isSelected())
                waitAndClick(driver,privacyCheckLabel);
        return this;
    }

    public ResidentialParkingPermitPage submitForm() {
        Logg.logger.info("");
        waitAndClick(driver,finalSubmit);
//        super.submitForm(formNo);
        return this;
    }
    public ResidentialParkingPermitPage setPostalAddress(String streetname, String suburb, String state, String postCode)
    {
        Logg.logger.info(String.format("Street Name:%s, Suburb:%s, State:%s, PostCode:%s",streetname,suburb,state,postCode));
        waitAndClick(driver,postalAddressDifferent);
        waitAndSendKeys(driver,this.streetAddress,streetname);
        waitAndSendKeys(driver,this.suburb,suburb);
        selectDropdown_VisibleText(driver,this.stateDropdown,state);
        waitAndSendKeys(driver,this.postCode,postCode);
        return this;
    }
}
