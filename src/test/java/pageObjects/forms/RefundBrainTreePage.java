package pageObjects.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class RefundBrainTreePage extends BaseFormsClass {
    String formNo = "232";
    static String GUID = "";
    static String compReceiptNumber = "";
    static String formDate = "";


    public RefundBrainTreePage(WebDriver driver) {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver, this);
    }

    String form_ID = "#gform_page_" + formNo;

    //Submit Form
    @FindBy(id = "saveandcontinue")
    private WebElement finalSubmit;

    @FindBy(id = "input_232_7")
    private WebElement address;
    @FindBy(id = "input_232_229")
    private WebElement csoEmail;

    @FindBy(id = "label_232_14_1")
    private WebElement multicheck;
    @FindBy(id = "input_232_15")
    private WebElement secondAddress;

    //Categories
    @FindBy(id = "label_232_21_0")
    private WebElement minorcategory;
    @FindBy(id = "label_232_21_1")
    private WebElement mediumcategory;
    @FindBy(id = "label_232_21_2")
    private WebElement majorcategory;

    //Description of works
    @FindBy(id = "input_232_22")
    private WebElement desc;
    @FindBy(id = "input_232_60")
    private WebElement valueWorks;
    @FindBy(id = "input_232_116")
    private WebElement startDateEstimate;
    @FindBy(id = "input_232_117")
    private WebElement endDateEstimate;
    @FindBy(id = "gform_browse_button_232_31")
    private WebElement uploadButton;

    //Drain Inspection Video
    @FindBy(id = "label_232_59_0")
    private WebElement yesVideo;


    //Who is Applying for the Permit
    @FindBy(id = "label_232_34_0")
    private WebElement owner;
    @FindBy(id = "label_232_34_1")
    private WebElement applicant;

    //Property Owned By
    @FindBy(id = "label_232_126_0")
    private WebElement companyYes;
    @FindBy(id = "label_232_35_1")
    private WebElement companyNo;

    //Company
    //Owner Details
    @FindBy(id = "input_232_122")
    private WebElement companyName;
    @FindBy(id = "input_232_124")
    private WebElement companyABN;
    @FindBy(id = "input_232_123")
    private WebElement compContactName;
    @FindBy(id = "input_232_125_1")
    private WebElement compAddressLineOne;
    @FindBy(id = "input_232_125_3")
    private WebElement compAddressLineThree;
    @FindBy(id = "input_232_125_4")
    private WebElement compAddressLineFour;
    @FindBy(id = "input_232_125_5")
    private WebElement compPostCode;


    //Owner Details
    @FindBy(id = "input_232_119")
    private WebElement ownerFirstName;
    @FindBy(id = "input_232_120")
    private WebElement ownerLastName;
    @FindBy(id = "input_232_121_1")
    private WebElement ownerAddressLineOne;
    @FindBy(id = "input_232_121_3")
    private WebElement ownerAddressLineThree;
    @FindBy(id = "input_232_121_4")
    private WebElement ownerAddressLineFour;
    @FindBy(id = "input_232_121_5")
    private WebElement ownerPostCode;

    @FindBy(id = "label_232_127_0")
    private WebElement mobType;
    @FindBy(id = "label_232_127_1")
    private WebElement landType;
    @FindBy(id = "input_232_130")
    private WebElement mobNo;
    @FindBy(id = "input_232_129")
    private WebElement landNo;

    @FindBy(id = "input_232_132")
    private WebElement email;
    @FindBy(id = "label_232_131_1")
    private WebElement smsNotificationLabel;
    @FindBy(id = "choice_232_131_1")
    private WebElement smsNotification;
    @FindBy(id = "label_232_53_0")
    private WebElement emailYes;

    //Applicant Owner Details
    //Is the Property owned by a company
    @FindBy(id = "input_232_134_0")
    private WebElement applicantCompanyYes;
    @FindBy(id = "label_232_134_1")
    private WebElement applicantCompanyNo;

    //owners contact Details
    @FindBy(id = "input_232_135")
    private WebElement appOwnerFirstName;
    @FindBy(id = "input_232_136")
    private WebElement appOwnerLastName;
    @FindBy(id = "input_232_137_1")
    private WebElement appOwnerAddressLineOne;
    @FindBy(id = "input_232_137_3")
    private WebElement appOwnerAddressLineThree;
    @FindBy(id = "input_232_137_4")
    private WebElement appOwnerAddressLineFour;
    @FindBy(id = "input_232_137_5")
    private WebElement appOwnerPostCode;

    @FindBy(id = "label_232_144_0")
    private WebElement appOwnermobType;
    @FindBy(id = "label_232_127_1")
    private WebElement appOwnerlandType;
    @FindBy(id = "input_232_145")
    private WebElement appOwnermobNo;
    @FindBy(id = "input_232_129")
    private WebElement appOwnerlandNo;

    @FindBy(id = "input_232_147")
    private WebElement appOwneremail;

    //Declarations

    @FindBy(id = "label_232_55_1")
    private WebElement declarationOne;
    @FindBy(id = "label_232_55_2")
    private WebElement declarationTwo;
    @FindBy(id = "label_232_55_3")
    private WebElement declarationThree;


    public RefundBrainTreePage clickDrain(String choice) {
        if (choice.equalsIgnoreCase("Yes"))
            waitAndClick(driver, yesVideo);
        return this;
    }


    public RefundBrainTreePage clickNext(String pageNo) {
        Logg.logger.info("PageNo:" + pageNo);
        super.clickNext(formNo, pageNo);
        return this;
    }

    public RefundBrainTreePage clickPrevious(String pageNo) {
        Logg.logger.info("PageNo:" + pageNo);
        super.clickPrevious(formNo, pageNo);
        return this;
    }

    public RefundBrainTreePage setFirstAddress(String address) {
        Logg.logger.info("address:" + address);
        super.selectAddress(this.address, address);
        return this;
    }


    public RefundBrainTreePage setMultiCheck(String multiCheck) {
        Logg.logger.info("Select Multi Address:" + multiCheck);
        if (multiCheck.equalsIgnoreCase("My building site spans across multiple addresses"))
            waitAndClick(driver, multicheck);
        return this;
    }

    public RefundBrainTreePage setSecondAddress(String seoondAddress) {
        Logg.logger.info("seoondAddress:" + seoondAddress);
        super.selectAddress(this.secondAddress, seoondAddress);
        return this;
    }

    public RefundBrainTreePage setCategory(String category) {
        Logg.logger.info("category:" + category);
        if (category.equalsIgnoreCase("Minor impact works"))
            waitAndClick(driver, minorcategory);
        if (category.equalsIgnoreCase("Medium impact works"))
            waitAndClick(driver, mediumcategory);
        if (category.equalsIgnoreCase("Major impact works"))
            waitAndClick(driver, majorcategory);
        return this;
    }

    public RefundBrainTreePage setDescription(String description) {
        Logg.logger.info("description:" + description);
        waitAndSendKeys(driver, desc, description);
        return this;
    }

    public RefundBrainTreePage setestimateValue(String value) {
        Logg.logger.info("value:" + value);
        //selectFirstItemFromLookup(driver,this.primaryBreed,primaryBreed);
        waitAndSendKeys(driver, valueWorks, value);
        return this;
    }

    public RefundBrainTreePage setStartDate(String startDate) {
        Logg.logger.info("startDate:" + startDate);
        //selectFirstItemFromLookup(driver,this.secondaryBreed,secondaryBreed);
        waitAndSendKeys(driver, startDateEstimate, startDate);
        return this;
    }

    public RefundBrainTreePage setEndDate(String endDate) {
        Logg.logger.info("endDate:" + endDate);
        //selectFirstItemFromLookup(driver,this.secondaryBreed,secondaryBreed);
        waitAndSendKeys(driver, endDateEstimate, endDate);
        return this;
    }

    public RefundBrainTreePage selectPerson(String person) {
        Logg.logger.info("person:" + person);
        if (person.equalsIgnoreCase("Owner"))
            waitAndClick(driver, owner);
        if (person.equalsIgnoreCase("Company"))
            waitAndClick(driver, applicant);
        return this;
    }

    public RefundBrainTreePage selectProperty(String property) {
        Logg.logger.info("property:" + property);
        if (property.equalsIgnoreCase("No"))
            waitAndClick(driver, companyNo);
        if (property.equalsIgnoreCase("Yes"))
            waitAndClick(driver, companyYes);
        return this;
    }

    public RefundBrainTreePage setCompanyName(String name) {
        Logg.logger.info("First Name:" + name);
        waitAndSendKeys(driver, companyName, name);
        return this;
    }

    public RefundBrainTreePage setABN(String name) throws InterruptedException {
        Logg.logger.info("Last Name:" + name);
        Thread.sleep(3000);
        waitAndSendKeys(driver, companyABN, name);
        return this;
    }

    public RefundBrainTreePage setContactName(String name) {
        Logg.logger.info("Last Name:" + name);
        waitAndSendKeys(driver, compContactName, name);
        return this;
    }

    public RefundBrainTreePage setCompAddressOne(String addressOne) {
        Logg.logger.info(" addressOne:" + addressOne);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, compAddressLineOne, addressOne);
        return this;
    }

    public RefundBrainTreePage setCompAddressTwo(String addressTwo) {
        Logg.logger.info(" addressTwo:" + addressTwo);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, compAddressLineThree, addressTwo);
        return this;
    }

    public RefundBrainTreePage setCompAddressFour(String addressFour) {
        Logg.logger.info(" addressFour:" + addressFour);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, compAddressLineFour, addressFour);
        return this;
    }

    public RefundBrainTreePage setCompPostCode(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, compPostCode, setPostCode);
        return this;
    }


    public RefundBrainTreePage setFName(String name) {
        Logg.logger.info("First Name:" + name);
        waitAndSendKeys(driver, ownerFirstName, name);
        return this;
    }

    public RefundBrainTreePage setLName(String name) {
        Logg.logger.info("Last Name:" + name);
        waitAndSendKeys(driver, ownerLastName, name);
        return this;
    }

    public RefundBrainTreePage setEmail(String email) {
        Logg.logger.info("Email:" + email);
        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$";
        waitAndSendKeys(driver, this.email, email);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            Logg.logger.info("Email Validation Passed:" + email);
        } else {
            Logg.logger.info("Email Validation Failed:" + email);
        }
        return this;
    }

    public RefundBrainTreePage setPhoneType(String phoneType) {
        Logg.logger.info("Phone Type:" + phoneType);
        if (phoneType.equalsIgnoreCase("Mobile"))
            waitAndClick(driver, mobType);
        if (phoneType.equalsIgnoreCase("Landline"))
            waitAndClick(driver, landType);
        return this;
    }


    public RefundBrainTreePage setPhoneNumber(String phoneNumber) {
        Logg.logger.info(" Phone number:" + phoneNumber);
        if (mobType.isSelected())
            waitAndSendKeys(driver, mobNo, phoneNumber);
        if (landType.isSelected())
            waitAndSendKeys(driver, landNo, phoneNumber);
        return this;
    }

    public RefundBrainTreePage setMobPhoneNumber(String mobphoneNum) {
        Logg.logger.info(" Phone number:" + mobphoneNum);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, mobNo, mobphoneNum);
//        Pattern pattern = Pattern.compile(regex);
//        Matcher matcher = pattern.matcher(mobphoneNum);
//        if (matcher.matches()) {
//            Logg.logger.info("Mobile Validation Passed:" + mobphoneNum);
//        } else {
//            Logg.logger.info("Mobile Validation Failed:" + mobphoneNum);
//        }
        return this;
    }

    public RefundBrainTreePage setlandPhoneNumber(String lanphoneNum) {
        Logg.logger.info(" Phone number:" + lanphoneNum);
        waitAndSendKeys(driver, landNo, lanphoneNum);
        return this;
    }


    public RefundBrainTreePage setSMSNotifications(String option) {
        Logg.logger.info("Set SMS Notifications to:" + option);
        if (option.equalsIgnoreCase("yes"))
            if (!smsNotification.isSelected())
                waitAndClick(driver, smsNotificationLabel);
        if (option.equalsIgnoreCase("no"))
            if (smsNotification.isSelected())
                waitAndClick(driver, smsNotificationLabel);
        return this;
    }

    public RefundBrainTreePage setAddressOne(String addressOne) {
        Logg.logger.info(" addressOne:" + addressOne);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, ownerAddressLineOne, addressOne);
        return this;
    }

    public RefundBrainTreePage setAddressTwo(String addressTwo) {
        Logg.logger.info(" addressTwo:" + addressTwo);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, ownerAddressLineThree, addressTwo);
        return this;
    }

    public RefundBrainTreePage setAddressFour(String addressFour) {
        Logg.logger.info(" addressFour:" + addressFour);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, ownerAddressLineFour, addressFour);
        return this;
    }

    public RefundBrainTreePage setPostCode(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, ownerPostCode, setPostCode);
        return this;
    }

    public RefundBrainTreePage setPersonalDetails(String fname, String lname, String addressOne, String addressThree, String addressFour, String postCode) throws ParseException {
        Logg.logger.info(String.format(" details:  Fname:%s, Lname:%s, AddressOne type:%s, AddressThree type:%s, AddressFour type:%s, PostCode type:%s", fname, lname, addressOne, addressThree, addressFour, postCode));
        this.setFName(fname)
                .setLName(lname)
                .setAddressOne(addressOne)
                .setAddressTwo(addressThree)
                .setAddressFour(addressFour)
                .setPostCode(postCode);
//        if(phonetype.equalsIgnoreCase("mobile"))
//            setSMSNotifications(sms);
        return this;
    }

    public RefundBrainTreePage setCsoEmail(String email) {
        Logg.logger.info("Email:" + email);
        waitAndSendKeys(driver, csoEmail, email);
        return this;
    }

    //******** Applicant On behalf owner Details*******


    public RefundBrainTreePage selectAppOwnerProperty(String property) {
        Logg.logger.info("property:" + property);
        waitAndClick(driver, applicantCompanyNo);

        return this;
    }

    public RefundBrainTreePage setAppOwnerFName(String name) {
        Logg.logger.info("First Name:" + name);
        waitAndSendKeys(driver, appOwnerFirstName, name);
        return this;
    }

    public RefundBrainTreePage setAppOwnerLName(String name) {
        Logg.logger.info("Last Name:" + name);
        waitAndSendKeys(driver, appOwnerLastName, name);
        return this;
    }

    public RefundBrainTreePage setAppOwnerEmail(String email) {
        Logg.logger.info("Email:" + email);
        waitAndSendKeys(driver, this.appOwneremail, email);
        return this;
    }

    public RefundBrainTreePage setAppOwnerPhoneType(String phoneType) {
        Logg.logger.info("Phone Type:" + phoneType);
        if (phoneType.equalsIgnoreCase("Mobile"))
            waitAndClick(driver, appOwnermobType);
        if (phoneType.equalsIgnoreCase("Landline"))
            waitAndClick(driver, appOwnerlandType);
        return this;
    }


    public RefundBrainTreePage setAppOwnerPhoneNumber(String phoneNumber) {
        Logg.logger.info(" Phone number:" + phoneNumber);
        if (mobType.isSelected())
            waitAndSendKeys(driver, appOwnermobNo, phoneNumber);
        if (landType.isSelected())
            waitAndSendKeys(driver, appOwnerlandNo, phoneNumber);
        return this;
    }

    public RefundBrainTreePage setAppOwnerMobPhoneNumber(String mobphoneNum) {
        Logg.logger.info(" Phone number:" + mobphoneNum);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, appOwnermobNo, mobphoneNum);
//        Pattern pattern = Pattern.compile(regex);
//        Matcher matcher = pattern.matcher(mobphoneNum);
//        if (matcher.matches()) {
//            Logg.logger.info("Mobile Validation Passed:" + mobphoneNum);
//        } else {
//            Logg.logger.info("Mobile Validation Failed:" + mobphoneNum);
//        }
        return this;
    }

    public RefundBrainTreePage setAppOwnerlandPhoneNumber(String lanphoneNum) {
        Logg.logger.info(" Phone number:" + lanphoneNum);
        waitAndSendKeys(driver, landNo, lanphoneNum);
        return this;
    }


    public RefundBrainTreePage setAppOwnerAddressOne(String addressOne) {
        Logg.logger.info(" addressOne:" + addressOne);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, appOwnerAddressLineOne, addressOne);
        return this;
    }

    public RefundBrainTreePage setAppOwnerAddressTwo(String addressTwo) {
        Logg.logger.info(" addressTwo:" + addressTwo);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, appOwnerAddressLineThree, addressTwo);
        return this;
    }

    public RefundBrainTreePage setAppOwnerAddressFour(String addressFour) {
        Logg.logger.info(" addressFour:" + addressFour);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, appOwnerAddressLineFour, addressFour);
        return this;
    }

    public RefundBrainTreePage setAppOwnerPostCode(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, appOwnerPostCode, setPostCode);
        return this;
    }

    public RefundBrainTreePage setAppOwnerPersonalDetails(String fname, String lname, String addressOne, String addressThree, String addressFour, String postCode) throws ParseException {
        Logg.logger.info(String.format(" details:  Fname:%s, Lname:%s, AddressOne type:%s, AddressThree type:%s, AddressFour type:%s, PostCode type:%s", fname, lname, addressOne, addressThree, addressFour, postCode));
        this.setFName(fname)
                .setLName(lname)
                .setAddressOne(addressOne)
                .setAddressTwo(addressThree)
                .setAddressFour(addressFour)
                .setPostCode(postCode);
//        if(phonetype.equalsIgnoreCase("mobile"))
//            setSMSNotifications(sms);
        return this;
    }


    //***END****

    public RefundBrainTreePage setPermit(String permit) {
        Logg.logger.info("permit:" + permit);
        if (permit.equalsIgnoreCase("Email"))
            waitAndClick(driver, emailYes);
        return this;
    }

    public RefundBrainTreePage setDeclarations(String declarations) {
        Logg.logger.info("declarations:" + declarations);
        if (declarations.equalsIgnoreCase("The information I have provided is true and correct."))
            waitAndClick(driver, declarationOne);
        return this;
    }

    public RefundBrainTreePage setDeclarationsTwo(String declarationsTwo) {
        if (declarationsTwo.equalsIgnoreCase("Building works will not start until I have received the asset protection permit."))
            waitAndClick(driver, declarationTwo);
        return this;
    }

    public RefundBrainTreePage setDeclarationsThree(String declarationsThree) {
        if (declarationsThree.equalsIgnoreCase("I will keep a clean and safe worksite that meets all permit conditions."))
            waitAndClick(driver, declarationThree);
        return this;
    }

    public RefundBrainTreePage saveForm() throws InterruptedException {
        Logg.logger.info("");

        String master = driver.getWindowHandle();
        WebElement paypalButton = driver.findElement(By.id("saveandcontinue"));
        waitAndClick(driver, paypalButton);
        Thread.sleep(7000);
        int timeCount = 1;
        do {
            driver.getWindowHandles();
            Thread.sleep(200);
            timeCount++;
            if (timeCount > 50) {
                break;
            }
        }
        while (driver.getWindowHandles().size() == 1);
        //Assigning the handles to a set
        Set<String> handles = driver.getWindowHandles();
        //Switching to the popup window.
        for (String handle : handles) {
            if (!handle.equals(master)) {
                driver.switchTo().window(handle);
                driver.manage().window().maximize();
            }
        }


//        super.submitForm(formNo);
        return this;
    }

    public RefundBrainTreePage payPalPayment() throws InterruptedException {
        Logg.logger.info("");
        String master = driver.getWindowHandle();
        WebElement paypalButton = driver.findElement(By.xpath("//*[@id=\"paypal-button\"]"));
        waitAndClick(driver, paypalButton);
        Thread.sleep(7000);
        int timeCount = 1;
        do {
            driver.getWindowHandles();
            Thread.sleep(200);
            timeCount++;
            if (timeCount > 50) {
                break;
            }
        }
        while (driver.getWindowHandles().size() == 1);
        //Assigning the handles to a set
        Set<String> handles = driver.getWindowHandles();
        //Switching to the popup window.
        for (String handle : handles) {
            if (!handle.equals(master)) {
                driver.switchTo().window(handle);
                driver.manage().window().maximize();
            }
        }

        return this;
    }

    //    public pageObjects.forms.AssetProtectionPage checkUploadBox()
//    {
//        Logg.logger.info("");
//        waitAndClick(driver,checkUpload);
//        return this;
//    }
    public RefundBrainTreePage uploadMicrochipFile() throws AWTException, InterruptedException {
//        Logg.logger.info("File:"+fileName);
//        Utilities.uploadFile(uploadButton,fileName);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        return this;
    }

    public RefundBrainTreePage getReviewPage(String expReviewPage, String price) throws Exception {

        GUID = driver.findElement(By.id("input_232_5")).getAttribute("value");
        System.out.println("The GUID is " + GUID + "");
        String productCode = driver.findElement(By.id("input_232_109")).getAttribute("data-product-id");
        System.out.println("***************The Product Code is*************** \n" + productCode + "");
        Thread.sleep(3000);

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_232_54\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
//        if (reviewPage.equalsIgnoreCase(expReviewPage)) {
//            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
//        } else {
//            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
//            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");
//
//        }
        String lastPrice = reviewPage.substring(reviewPage.lastIndexOf("\n"));
        String actualPrice = lastPrice.replace("\n", "");
        System.out.println("***************The PRICE IS************* " + actualPrice + "");

//        if (actualPrice.equalsIgnoreCase(price.trim())) {
//            extent_Pass(driver, "The Price verification is correct", price.toString(), "", "");
//        } else {
//            extent_Warn(driver, "The Price verification Failed", price.toString(), "", "");
//            throw new Exception("TEST CASE FAILED AT THE PRICE VERIFICATION LEVEL ");
//
//        }

        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_232_56\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }

        return this;
    }

    @Test(dataProvider = "APPL_DataProvider", description = "Verify that the review page values can be compared")
    public RefundBrainTreePage getReviewPageBack(String expReviewBack, String expPriceBack) throws Exception {


        GUID = driver.findElement(By.id("input_232_5")).getAttribute("value");
        System.out.println("The GUID is " + GUID + "");
        String productCode = driver.findElement(By.id("input_232_5")).getAttribute("data-product-id");
        System.out.println("***************The Product Code is*************** \n" + productCode + "");
        Thread.sleep(3000);

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_232_54\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
//        if (reviewPage.equalsIgnoreCase(expReviewBack)) {
//            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
//        } else {
//            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
//            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");
//
//        }
        String lastPrice = reviewPage.substring(reviewPage.lastIndexOf("\n"));
        String actualPrice = lastPrice.replace("\n", "");
        System.out.println("***************The PRICE IS************* " + actualPrice + "");

//        if (actualPrice.equalsIgnoreCase(expPriceBack.trim())) {
//            extent_Pass(driver, "The Price verification is correct", expPriceBack.toString(), "", "");
//        } else {
//            extent_Warn(driver, "The Price verification Failed", expPriceBack.toString(), "", "");
//            throw new Exception("TEST CASE FAILED AT THE PRICE VERIFICATION LEVEL ");
//
//        }

        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_232_56\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }


        return this;
    }


    public RefundBrainTreePage getPaymentDetails() throws Exception {


        //Get the Issue type from the review page
        Thread.sleep(15000);
        String paymentDetails = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]")).getText();
        System.out.println("The payment details are " + paymentDetails + "");
        //String Receipt Number
        String expReceiptNumber = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]/p[1]")).getText();
        System.out.println("The Receipt Number " + expReceiptNumber + "");
        compReceiptNumber = expReceiptNumber.substring(16);
        System.out.println("The Trimmed RN is " + compReceiptNumber + "");
        String receiptDate = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]/p[2]")).getText();
        System.out.println("The Receipt Number " + receiptDate + "");
        //Remove text from Date
        formDate = receiptDate.substring(14);

        //Current Date comparison
        String pattern = "dd-MM-yyyy";

// Create an instance of SimpleDateFormat used for formatting
// the string representation of date according to the chosen pattern
        DateFormat df = new SimpleDateFormat(pattern);

// Get Current date using Calendar object.
        Date today = Calendar.getInstance().getTime();
// Using DateFormat format method we can create a string
// representation of a date with the defined format.
        String todayAsString = df.format(today);

// Print the result!
        System.out.println("Today is: " + todayAsString);


//        if (formDate.equalsIgnoreCase(todayAsString)) {
//            extent_Pass(driver, "The date is current", todayAsString.toString(),formDate , "");
//        } else {
//            extent_Warn(driver, "The date is current", todayAsString.toString(), formDate, "");
//            throw new Exception("TEST CASE FAILED AT THE CURRENT DATE ");
//
//        }

        return this;
    }

    public RefundBrainTreePage getLoggedPaymentDetails() throws Exception {


        //Get the Issue type from the review page
        Thread.sleep(15000);
        String paymentDetails = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]")).getText();
        System.out.println("The payment details are " + paymentDetails + "");
        //String Receipt Number
        String expReceiptNumber = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]/p[1]")).getText();
        System.out.println("The Receipt Number " + expReceiptNumber + "");
        compReceiptNumber = expReceiptNumber.substring(16);
        System.out.println("The Trimmed RN is " + compReceiptNumber + "");
        String receiptDate = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]/p[2]")).getText();
        System.out.println("The Receipt Number " + receiptDate + "");
        //Remove text from Date
        formDate = receiptDate.substring(14);

        //Current Date comparison
        String pattern = "dd-MM-yyyy";

// Create an instance of SimpleDateFormat used for formatting
// the string representation of date according to the chosen pattern
        DateFormat df = new SimpleDateFormat(pattern);

// Get the today date using Calendar object.
        Date today = Calendar.getInstance().getTime();
// Using DateFormat format method we can create a string
// representation of a date with the defined format.
        String todayAsString = df.format(today);

// Print the result!
        System.out.println("Today is: " + todayAsString);


//        if (formDate.equalsIgnoreCase(todayAsString)) {
//            extent_Pass(driver, "The date is current", formDate.toString(),todayAsString , "");
//        } else {
//            extent_Warn(driver, "The date is current", formDate.toString(),todayAsString, "");
//            throw new Exception("TEST CASE FAILED AT THE CURRENT DATE ");
//
//        }

        return this;
    }

    @Test(dataProvider = "APPL_DataProvider", description = "Verify that the CRM values are verified")
    public RefundBrainTreePage refundProcess() throws Exception {

        Thread.sleep(15000);
        String winHandleBefore = driver.getWindowHandle();
        ((JavascriptExecutor)driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));

        loadurl(driver,"https://sandbox.braintreegateway.com/");

        driver.manage().window().setSize(new Dimension(1920, 1080));

        System.out.println(driver.manage().window().getSize());

        Thread.sleep(5000);
        //Enter the login details
        WebElement userName = driver.findElement(By.name("login"));
        waitAndSendKeys(driver, userName, "tst_boroondara");
        Thread.sleep(7000);
        //Enter the Password Details
        WebElement passwordBT = driver.findElement(By.name("password"));
        waitAndSendKeys(driver, passwordBT, "Subwoofer11");
        Thread.sleep(7000);
        //Login
        WebElement loginBT = driver.findElement(By.name("commit"));
        waitAndClick(driver, loginBT);
        Thread.sleep(7000);

        //Click Transactions on Main Screen

        WebElement transactions = driver.findElement(By.xpath("/html/body/header/div[2]/div[2]/div[1]/a[2]"));
        waitAndClick(driver, transactions);

        //Click Search
        WebElement searchBT = driver.findElement(By.name("commit"));
        waitAndClick(driver, searchBT);

        //Select the latest Transaction
        WebElement selectTransaction = driver.findElement(By.xpath("/html/body/div[1]/div/div/table/tbody/tr[1]/td[4]/a"));
        waitAndClick(driver, selectTransaction);


        //Capture CRM Refund details

        //Select Refund
        WebElement selectRefund = driver.findElement(By.xpath("/html/body/div[1]/div/div/header/div/a[2]"));
        waitAndClick(driver, selectRefund);

        //Select Refund after amount
        WebElement amountRefund = driver.findElement(By.xpath("//*[@id=\"refund_form\"]/div[3]/input"));
        waitAndClick(driver, amountRefund);

        //To be moved later
        String getAmount = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/dl/dd[5]")).getText();
        System.out.println("Amount: " + getAmount);

        String getOrderID = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/dl/dd[8]")).getText();
        System.out.println("Order ID: " + getOrderID);
        driver.close();
        driver.switchTo().window(winHandleBefore);
        return this;
    }




//    @Test(dataProvider = "APP_DataProvider", description = "Verify that the CRM values are verified")
//
//    public AssetProtectionPage readPDF(String pdf) throws IOException {
//        Logg.logger.info("");
//
//        PDFReader pdfManager = new PDFReader();
//
//        pdfManager.setFilePath("C:\\Users\\rnair\\Downloads\\" +pdf+ ".pdf");
//
//        try {
//
//            String text = pdfManager.toText();
//            String[] linesFile = text.split("\n");// this array is initialized with a single element
//            String issueTypePDF = linesFile[4].replaceFirst("^\\s* ","");
//
//            System.out.println(text);
//
//        } catch (IOException ex) {
//            Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return this;
//    }




        @DataProvider

        public Object[][] APPL_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APPL");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "APPL", 72);

            return testObjArray;
        }


    }





