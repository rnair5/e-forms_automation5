package pageObjects.forms;

import libraries.Logg;
import libraries.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.Set;

import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class PayPalPage extends BasePageClass {
    public PayPalPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath="//*[@id=\"country\"]")
    private WebElement countryDropDown;
    @FindBy(name="cardnumber")
    private WebElement cardNumber;
    @FindBy(name="exp-date")
    private WebElement cardExpiryNumber;
    @FindBy(name="cvc")
    private WebElement cardCVV;

    //Billing Address
    @FindBy(name="fname")
    private WebElement firstName;
    @FindBy(name="lname")
    private WebElement lastName;
    @FindBy(name="billingLine1")
    private WebElement billingLine1;
    @FindBy(name="billingLine2")
    private WebElement billingLine2;
    @FindBy(name="billingCity")
    private WebElement billingCity;
    @FindBy(name="billingPostalCode")
    private WebElement billingPostCode;
    @FindBy(id="phone")
    private WebElement mobileNumber;
    @FindBy(name="email")
    private WebElement email;
    @FindBy(className = "receiptHeaderText")
    private WebElement paymentSuccess;
    @FindBy(xpath = "/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]")
    private WebElement apppaymentSuccess;

    public String getUrl()
    {
        Logg.logger.info("");
        return Utilities.getCurrentUrl(driver);
    }
//    public PayPalPage clickVerify()
//    {
//        Logg.logger.info("");
//        waitAndClick(driver,verifyButton);
//        return this;
//    }

    public PayPalPage setCountry() throws InterruptedException {

        WebElement addressDropdown= driver.findElement(By.xpath("//*[@id=\"country\"]"));
        addressDropdown.click();
            selectDropdown_Index(driver, addressDropdown,9);
        return this;
    }

    public PayPalPage setCardNo(String num) throws InterruptedException {
        Logg.logger.info("Card NO:"+num);
//        driver.switchTo().defaultContent();
        Thread.sleep(12000);
//        driver.switchTo().frame(2);
        Thread.sleep(9000);
//        cardNumber.clear();
        waitAndSendKeys(driver,cardNumber,num);

        return this;
    }

    public PayPalPage setExpiry(String num) throws InterruptedException {
        Logg.logger.info("Expiry Number:"+num);
        Thread.sleep(3000);
        waitAndSendKeys(driver,cardExpiryNumber,num);

        return this;
    }

    public PayPalPage setCVV(String num) throws InterruptedException {
        Logg.logger.info("CVV Number:"+num);
        Thread.sleep(3000);
        waitAndSendKeys(driver,cardCVV,num);
        return this;
    }

    public PayPalPage setUserDetails(String firstName1, String lastName1, String addressLine11, String addressLine22, String suburb1, String postCode1) throws InterruptedException {
        Thread.sleep(3000);
        waitAndSendKeys(driver,firstName,firstName1);
        waitAndSendKeys(driver,lastName,lastName1);
        waitAndSendKeys(driver,billingLine1,addressLine11);
        waitAndSendKeys(driver,billingLine2,addressLine22);
        waitAndSendKeys(driver,billingCity,suburb1);
        waitAndSendKeys(driver,billingPostCode,postCode1);
        return this;
    }

    public PayPalPage setState(String category) throws InterruptedException {

        WebElement addressDropdown= driver.findElement(By.name("billingState"));
        addressDropdown.click();
        if(category.equalsIgnoreCase("Tasmania")) {
            selectDropdown_Index(driver, addressDropdown, 6);
        } else {
            selectDropdown_Index(driver, addressDropdown, 7);
        }
        return this;
    }

    public PayPalPage setTelephone(String num) throws InterruptedException {
        Logg.logger.info("CVV Number:"+num);
        Thread.sleep(3000);
        waitAndSendKeys(driver,mobileNumber,num);
        return this;
    }
    public PayPalPage setEmail(String num) throws InterruptedException {
        Logg.logger.info("CVV Number:"+num);
        Thread.sleep(3000);
        waitAndSendKeys(driver,email,num);
        return this;
    }

    public PayPalPage selectCreateAccount() throws InterruptedException {
        Thread.sleep(3000);
        WebElement createAccount = driver.findElement(By.xpath("//*[@id=\"signupContainer\"]/fieldset/div[2]/div/label"));
        waitAndClick(driver,createAccount);
        return this;
    }

    public PayPalPage setPassword(String info) throws InterruptedException {
        Thread.sleep(3000);
        WebElement password = driver.findElement(By.name("password"));
        waitAndSendKeys(driver,password,info);
        return this;
    }

    public PayPalPage setDateOfBirth(String info) throws InterruptedException {
        Thread.sleep(3000);
        WebElement dob = driver.findElement(By.name("dobText"));
        waitAndSendKeys(driver,dob,info);
        return this;
    }

    public PayPalPage selectUserAgreement() throws InterruptedException {
        Thread.sleep(3000);
        WebElement userAgreement = driver.findElement(By.xpath("//*[@id=\"signupContainer\"]/fieldset/div[2]/xo-signup-options/div/div/div/div[3]/span/label/span[1]"));
        waitAndClick(driver,userAgreement);
        return this;
    }

    public PayPalPage scrollToBottom() throws InterruptedException {
        Thread.sleep(3000);
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        return this;
    }

    public PayPalPage acceptCookies() throws InterruptedException {
        Thread.sleep(3000);

        if(driver.findElements(By.xpath("//*[@id=\"acceptAllButton\"]")).size() < 1){
            System.out.println("Cookies already Accepted");
        } else {
            WebElement userAgreement = driver.findElement(By.xpath("//*[@id=\"acceptAllButton\"]"));
            waitAndClick(driver,userAgreement);
        }


        return this;
    }



    public PayPalPage selectGuestOption() throws InterruptedException {
        Thread.sleep(9000);
        WebElement userAgreement = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/main/div/form/section[3]/div[2]/div/label"));
        waitAndClick(driver,userAgreement);
        return this;
    }

    public PayPalPage selectPrivacyStatement() throws InterruptedException {
        Thread.sleep(3000);
        WebElement userAgreement = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/main/div/form/section[3]/div[2]/fieldset/div/div/label"));
        waitAndClick(driver,userAgreement);
        return this;
    }

    public PayPalPage selectPayNow() throws InterruptedException {
        Thread.sleep(3000);
        WebElement userAgreement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/main/div/form/input"));
        waitAndClick(driver,userAgreement);
        return this;
    }


    public PayPalPage addressMatch() throws InterruptedException {
        Thread.sleep(3000);
        String master = driver.getWindowHandle();
        WebElement addressContinue = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/main/div/form/input"));
        waitAndClick(driver,addressContinue);
        Thread.sleep(7000);
        int timeCount = 1;
        do
        {
            driver.getWindowHandles();
            Thread.sleep(200);
            timeCount++;
            if ( timeCount > 50 )
            {
                break;
            }
        }
        while ( driver.getWindowHandles().size() == 1 );
        //Assigning the handles to a set
        Set<String> handles = driver.getWindowHandles();
        //Switching to the popup window.
        for ( String handle : handles )
        {
            if(!handle.equals(master))
            {
                driver.switchTo().window(handle);
                driver.manage().window().maximize();
            }
        }
        return this;
    }

    public PayPalPage selectTopLoginOption() throws InterruptedException {
        Thread.sleep(9000);
        WebElement alreadyUser = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/main/div[2]/div[1]/button"));
        waitAndClick(driver,alreadyUser);
        return this;
    }
    public PayPalPage selectPayWithCard() throws InterruptedException {
        Thread.sleep(9000);
        WebElement cardUser = driver.findElement(By.xpath("//*[@id=\"createAccount\"]"));
        waitAndClick(driver,cardUser);
        return this;
    }
    public PayPalPage selectLoginOption() throws InterruptedException {
        Thread.sleep(9000);
        WebElement alreadyUser = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/main/p/a"));
        waitAndClick(driver,alreadyUser);
        return this;
    }

    public PayPalPage enterEmail() throws InterruptedException {
        Thread.sleep(7000);
        WebElement emailEnter = driver.findElement(By.name("login_email"));
        waitAndSendKeys(driver,emailEnter,"buyer_tst@boroondara.vic.gov.au");
        return this;
    }

    public PayPalPage continueLogin() throws InterruptedException {
        Thread.sleep(7000);
        WebElement clickNext = driver.findElement(By.name("btnNext"));
        waitAndClick(driver,clickNext);
        return this;
    }

    public PayPalPage enterPassword() throws InterruptedException {
        Thread.sleep(7000);
        WebElement pswdEnter = driver.findElement(By.name("login_password"));
        waitAndSendKeys(driver,pswdEnter,"Subwoofer11");
        return this;
    }

    public PayPalPage Login() throws InterruptedException {
        Thread.sleep(7000);
        WebElement clickNext = driver.findElement(By.name("btnLogin"));
        waitAndClick(driver,clickNext);
        Thread.sleep(10000);
        return this;
    }

    public PayPalPage paymentLoggedIn() throws InterruptedException {


        Thread.sleep(7000);
        String master = driver.getWindowHandle();
        WebElement clickNext = driver.findElement(By.xpath("//*[@id=\"payment-submit-btn\"]"));
        waitAndClick(driver,clickNext);
        Thread.sleep(7000);
        int timeCount = 1;
        do
        {
            driver.getWindowHandles();
            Thread.sleep(200);
            timeCount++;
            if ( timeCount > 50 )
            {
                break;
            }
        }
        while ( driver.getWindowHandles().size() == 1 );
        //Assigning the handles to a set
        Set<String> handles = driver.getWindowHandles();
        //Switching to the popup window.
        for ( String handle : handles )
        {
            if(!handle.equals(master))
            {
                driver.switchTo().window(handle);
                driver.manage().window().maximize();
            }
        }
        return this;
    }

    public PayPalPage switchPayment() throws InterruptedException {
        Thread.sleep(7000);
        String master = driver.getWindowHandle();
        WebElement paypalButton = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div[2]/footer/a"));
        waitAndClick(driver, paypalButton);
        Thread.sleep(7000);
        int timeCount = 1;
        do
        {
            driver.getWindowHandles();
            Thread.sleep(200);
            timeCount++;
            if ( timeCount > 50 )
            {
                break;
            }
        }
        while ( driver.getWindowHandles().size() == 1 );
        //Assigning the handles to a set
        Set<String> handles = driver.getWindowHandles();
        //Switching to the popup window.
        for ( String handle : handles )
        {
            if(!handle.equals(master))
            {
                driver.switchTo().window(handle);
                driver.manage().window().maximize();
            }
        }
        return this;
    }



//    public PayPalPage setExpiryMonth(String num) throws InterruptedException {
//        Logg.logger.info("ExpiryMonth:"+num);
//        waitAndSendKeys(driver,expMonth,num);
//        return this;
//    }
//
//    public PayPalPage setExpiryYear(String num) throws InterruptedException {
//        Logg.logger.info("ExpiryYear:"+num);
//        waitAndSendKeys(driver,expYear,num);
//        return this;
//    }

//    public PayPalPage clickDifferentPayment() throws InterruptedException {
//        Logg.logger.info("");
//        waitAndClick(driver,differentButton);
//        return this;
//    }
//
//    public PayPalPage setCVVNo(String num) throws InterruptedException {
//        Logg.logger.info("CVV NO:"+num);
//        driver.switchTo().defaultContent();
////        driver.switchTo().frame(UDSframe);
//        Thread.sleep(5000);
////        cardNumber.clear();
//        waitAndSendKeys(driver,cvvNumber,num);
//        return this;
//    }
//
//    public PayPalPage clickPay()
//    {
//        Logg.logger.info("");
//        waitAndClick(driver,payButton);
//        return this;
//    }
    public boolean isPaymentSuccessful()
    {
        Logg.logger.info("");
        try {
            if (waitUntilElementIsVisible(driver,paymentSuccess).isDisplayed())
                return true;
            else
                return false;
        }catch (Exception e)
        {
            return false;
        }
    }

    public boolean isAppPaymentSuccessful()
    {
        Logg.logger.info("");
        try {
            if (waitUntilElementIsVisible(driver,apppaymentSuccess).isDisplayed())

//
                return true;
            else
                return false;
        }catch (Exception e)
        {
            return false;
        }
    }

}
