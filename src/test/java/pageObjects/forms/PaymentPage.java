package pageObjects.forms;

import libraries.ExtentReporting;
import libraries.Logg;
import libraries.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import static libraries.Utilities.*;

public class PaymentPage extends BasePageClass {
    public PaymentPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    @FindBy(id="UDS_PAYMENT_FRAME")
    private WebElement UDSframe;
    @FindBy(id="UDS_CARDNUMBER")
    private WebElement cardNumber;
    @FindBy(id="UDS_EXPIRYMONTH")
    private WebElement expMonth;
    @FindBy(id="UDS_EXPIRYYEAR")
    private WebElement expYear;
    @FindBy(id="UDS_REGISTERCARD")
    private WebElement verifyButton;
    @FindBy(id="UDS_CHANGE")
    private WebElement differentButton;
    @FindBy(id="uds_ccv")
    private WebElement cvvNumber;
    @FindBy(id="UDS_CONFIRM")
    private WebElement payButton;
    @FindBy(className = "receiptHeaderText")
    private WebElement paymentSuccess;
    @FindBy(xpath = "/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]")
    private WebElement apppaymentSuccess;

    public String getUrl()
    {
        Logg.logger.info("");
        return Utilities.getCurrentUrl(driver);
    }
    public PaymentPage clickVerify()
    {
        Logg.logger.info("");
        waitAndClick(driver,verifyButton);
        return this;
    }


    public PaymentPage setCardNo(String num) throws InterruptedException {
        Logg.logger.info("Card NO:"+num);
        driver.switchTo().defaultContent();
        Thread.sleep(5000);
        driver.switchTo().frame(UDSframe);
        Thread.sleep(5000);
        cardNumber.clear();
        waitAndSendKeys(driver,cardNumber,num);

        return this;
    }

    public PaymentPage setExpiryMonth(String num) throws InterruptedException {
        Logg.logger.info("ExpiryMonth:"+num);
        waitAndSendKeys(driver,expMonth,num);
        return this;
    }

    public PaymentPage setExpiryYear(String num) throws InterruptedException {
        Logg.logger.info("ExpiryYear:"+num);
        waitAndSendKeys(driver,expYear,num);
        return this;
    }

    public PaymentPage clickDifferentPayment() throws InterruptedException {
        Logg.logger.info("");
        waitAndClick(driver,differentButton);
        return this;
    }

    public PaymentPage setCVVNo(String num) throws InterruptedException {
        Logg.logger.info("CVV NO:"+num);
        driver.switchTo().defaultContent();
//        driver.switchTo().frame(UDSframe);
        Thread.sleep(5000);
//        cardNumber.clear();
        waitAndSendKeys(driver,cvvNumber,num);
        return this;
    }

    public PaymentPage clickPay()
    {
        Logg.logger.info("");
        waitAndClick(driver,payButton);
        return this;
    }
    public boolean isPaymentSuccessful()
    {
        Logg.logger.info("");
        try {
            if (waitUntilElementIsVisible(driver,paymentSuccess).isDisplayed())
                return true;
            else
                return false;
        }catch (Exception e)
        {
            return false;
        }
    }

    public boolean isAppPaymentSuccessful()
    {
        Logg.logger.info("");
        try {
            if (waitUntilElementIsVisible(driver,apppaymentSuccess).isDisplayed())

//
                return true;
            else
                return false;
        }catch (Exception e)
        {
            return false;
        }
    }

}
