package pageObjects.forms;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import static libraries.Utilities.*;

public class Cso extends BasePageClass {
    public Cso(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    final String formsLinkCss=".members-access-error a";
    @FindBy(css = formsLinkCss)
    private WebElement formsLink;
    final String username="user_login";
    @FindBy(id = username)
    private WebElement csoUserName;
    @FindBy(id = "user_pass")
    private WebElement csoPassword;

    @FindBy(id = "wp-submit")
    private WebElement csoLoginButton;
    @FindBy(className = "recaptcha-checkbox")
    private WebElement captchaCheckbox;
    String form_xpath = "//a[contains(@title,'";
    @FindBy(id="input_68")
    private  WebElement userLoggedIn;
    private String servicingType="#gform_page_FORM_1 select";

    public Cso clickFormsLogin()
    {
        Logg.logger.info("");
        if(isElementPresent(driver, By.cssSelector(formsLinkCss)))
            waitAndClick(driver,formsLink);
        return this;
    }
    public Cso csoLogin(String user, String pass) throws InterruptedException {
        Logg.logger.info("");
        if(isElementPresent(driver,By.id(username))) {
            waitAndSendKeys(driver, csoUserName, user);
            waitAndSendKeys(driver, csoPassword, pass);
            //waitAndClick(driver,captchaCheckbox);
            Thread.sleep(10000);
            waitAndClick(driver, csoLoginButton);
        }
        return this;
    }

    public Cso openForm(String formName)
    {
        Logg.logger.info("Formname:"+formName);
        String form_xpath1=form_xpath+formName+"')]";
        waitAndClick(driver,form_xpath1);
        waitForPageLoaded(driver);
        refreshPage(driver);
        return this;
    }
    public Cso setOrigin(String type)
    {
        Logg.logger.info("Servicing customer by:"+type);
        //String formNo=servicingType.replace("FORM",formN);
        selectDropdown_VisibleText(driver,waitUntilElementIsVisible(driver,"//div[@class='gform_page']//select"), type);
        return this;
    }
}
