package pageObjects.forms;

import Tests.forms.PDFBoxReadFromFile;
import Tests.forms.PDFReader;
import libraries.ExcelUtils;
import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class VehiclePermitPage extends BaseFormsClass {
    String formNo = "234";
    static String GUID = "";
    static String compReceiptNumber = "";
    static String actPDFText = "";

    public VehiclePermitPage(WebDriver driver) {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver, this);
    }

    String form_ID = "#gform_page_" + formNo;

    //Submit Form
    @FindBy(id = "saveandcontinue")
    private WebElement finalSubmit;

    @FindBy(id = "input_234_6")
    private WebElement address;
    @FindBy(id = "input_203_229")
    private WebElement csoEmail;

    @FindBy(id = "label_234_11_1")
    private WebElement multicheck;
    @FindBy(id = "input_234_12")
    private WebElement secondAddress;

    //Categories
    @FindBy(id = "label_234_17_3")
    private WebElement propertyType;
    @FindBy(id = "label_234_18_4")
    private WebElement works;
    @FindBy(id = "label_234_21_0")
    private WebElement permitChoice;

    //Description of works
    @FindBy(id = "input_234_22")
    private WebElement permitNumber;
    @FindBy(id = "input_234_26")
    private WebElement desc;


    //Who is Applying for the Permit
    @FindBy(id = "label_234_29_1")
    private WebElement builder;


    //Property Owned By
    @FindBy(id = "label_234_35_0")
    private WebElement companyYes;
    @FindBy(id = "label_234_32_1")
    private WebElement companyNo;

    //Owner Details
    @FindBy(id = "input_234_38")
    private WebElement ownerFirstName;
    @FindBy(id = "input_234_39")
    private WebElement ownerLastName;
    @FindBy(id = "input_234_46_1")
    private WebElement ownerAddressLineOne;
    @FindBy(id = "input_234_46_3")
    private WebElement ownerAddressLineThree;
    @FindBy(id = "input_234_46_4")
    private WebElement ownerAddressLineFour;
    @FindBy(id = "input_234_46_5")
    private WebElement ownerPostCode;

    @FindBy(id = "label_234_41_0")
    private WebElement mobType;
    @FindBy(id = "label_234_41_1")
    private WebElement landType;
    @FindBy(id = "input_234_43")
    private WebElement mobNo;
    @FindBy(id = "input_234_42")
    private WebElement landNo;

    @FindBy(id = "input_234_40")
    private WebElement email;
    @FindBy(id = "label_234_44_1")
    private WebElement smsNotificationLabel;
    @FindBy(id = "choice_234_44_1")
    private WebElement smsNotification;


    //Declarations

    @FindBy(id = "label_234_48_1")
    private WebElement declarationOne;


    public VehiclePermitPage clickNext(String pageNo) {
        Logg.logger.info("PageNo:" + pageNo);
        super.clickNext(formNo, pageNo);
        return this;
    }

    public VehiclePermitPage clickPrevious(String pageNo) {
        Logg.logger.info("PageNo:" + pageNo);
        super.clickPrevious(formNo, pageNo);
        return this;
    }

    public VehiclePermitPage setFirstAddress(String address) {
        Logg.logger.info("address:" + address);
        super.selectAddress(this.address, address);
        return this;
    }


    public VehiclePermitPage setMultiCheck(String multiCheck) {
        Logg.logger.info("Select Multi Address:" + multiCheck);
        if (multiCheck.equalsIgnoreCase("My building site spans across multiple addresses"))
            waitAndClick(driver, multicheck);
        return this;
    }

    public VehiclePermitPage setInvalidABN() throws Exception {
        String invalidPhoneLabel = driver.findElement(By.xpath("//*[@id=\"error\"]/ol/li/a")).getText();
        if (invalidPhoneLabel.contains("Company ABN or ACN - Please check your ABN or ACN. A valid ABN is 11 digits. A valid ACN is 9 digits.")) {
            extent_Pass(driver, "Invalid ABN- Verification Passed", invalidPhoneLabel.toString(), "", "");
        } else {
            extent_Warn(driver, "Invalid ABN - verification Failed", invalidPhoneLabel.toString(), "", "");
            throw new Exception(" TEST FOR INVALID ABN NUMBER CHECK FAILED ");
        }
        return this;
    }

//    public VehiclePermitPage setSecondAddress(String seoondAddress) {
//        Logg.logger.info("seoondAddress:" + seoondAddress);
//        super.selectAddress2(this.secondAddress, seoondAddress);
//        return this;
//    }

    public VehiclePermitPage setCategory(String category) {
        Logg.logger.info("category:" + category);
        if (category.equalsIgnoreCase("Commercial or industrial site"))
            waitAndClick(driver, propertyType);
        return this;
    }

    public VehiclePermitPage setDescription(String description) {
        Logg.logger.info("description:" + description);
        waitAndSendKeys(driver, desc, description);
        return this;
    }

    public VehiclePermitPage setWorks(String value) {
        Logg.logger.info("value:" + value);
        if (value.equalsIgnoreCase("Repair an existing footpath or kerb"))
            waitAndClick(driver, works);
        return this;
    }

    public VehiclePermitPage setPermitChoice(String value) {
        Logg.logger.info("value:" + value);
        if (value.equalsIgnoreCase("Yes"))
            waitAndClick(driver, permitChoice);
        return this;
    }

    public VehiclePermitPage setPermitNumber(String value) {
        Logg.logger.info("value:" + value);
        waitAndSendKeys(driver, permitNumber, value);
        return this;
    }

    public VehiclePermitPage setComments(String value) {
        Logg.logger.info("value:" + value);
        waitAndSendKeys(driver, desc, value);
        return this;
    }

    public VehiclePermitPage selectPerson(String person) {
        Logg.logger.info("person:" + person);
        if (person.equalsIgnoreCase("Builder"))
            waitAndClick(driver, builder);
        return this;
    }

    public VehiclePermitPage selectProperty(String property) {
        Logg.logger.info("property:" + property);
        if (property.equalsIgnoreCase("No"))
            waitAndClick(driver, companyNo);
        return this;
    }

    public VehiclePermitPage setFName(String name) {
        Logg.logger.info("First Name:" + name);
        waitAndSendKeys(driver, ownerFirstName, name);
        return this;
    }

    public VehiclePermitPage setLName(String name) {
        Logg.logger.info("Last Name:" + name);
        waitAndSendKeys(driver, ownerLastName, name);
        return this;
    }

    public VehiclePermitPage setEmail(String email) {
        Logg.logger.info("Email:" + email);
        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$";
        waitAndSendKeys(driver, this.email, email);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            Logg.logger.info("Email Validation Passed:" + email);
        } else {
            Logg.logger.info("Email Validation Failed:" + email);
        }
        return this;
    }

    public VehiclePermitPage setPhoneType(String phoneType) {
        Logg.logger.info("Phone Type:" + phoneType);
        if (phoneType.equalsIgnoreCase("Mobile"))
            waitAndClick(driver, mobType);
        if (phoneType.equalsIgnoreCase("Landline"))
            waitAndClick(driver, landType);
        return this;
    }


    public VehiclePermitPage setPhoneNumber(String phoneNumber) {
        Logg.logger.info(" Phone number:" + phoneNumber);
        if (mobType.isSelected())
            waitAndSendKeys(driver, mobNo, phoneNumber);
        if (landType.isSelected())
            waitAndSendKeys(driver, landNo, phoneNumber);
        return this;
    }

    public VehiclePermitPage setMobPhoneNumber(String mobphoneNum) {
        Logg.logger.info(" Phone number:" + mobphoneNum);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, mobNo, mobphoneNum);
//        Pattern pattern = Pattern.compile(regex);
//        Matcher matcher = pattern.matcher(mobphoneNum);
//        if (matcher.matches()) {
//            Logg.logger.info("Mobile Validation Passed:" + mobphoneNum);
//        } else {
//            Logg.logger.info("Mobile Validation Failed:" + mobphoneNum);
//        }
        return this;
    }

    public VehiclePermitPage setlandPhoneNumber(String lanphoneNum) {
        Logg.logger.info(" Phone number:" + lanphoneNum);
        waitAndSendKeys(driver, landNo, lanphoneNum);
        return this;
    }


    public VehiclePermitPage setSMSNotifications(String option) {
        Logg.logger.info("Set SMS Notifications to:" + option);
        if (option.equalsIgnoreCase("yes"))
            if (!smsNotification.isSelected())
                waitAndClick(driver, smsNotificationLabel);
        if (option.equalsIgnoreCase("no"))
            if (smsNotification.isSelected())
                waitAndClick(driver, smsNotificationLabel);
        return this;
    }

    public VehiclePermitPage setAddressOne(String addressOne) {
        Logg.logger.info(" addressOne:" + addressOne);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, ownerAddressLineOne, addressOne);
        return this;
    }

    public VehiclePermitPage setAddressTwo(String addressTwo) {
        Logg.logger.info(" addressTwo:" + addressTwo);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, ownerAddressLineThree, addressTwo);
        return this;
    }

    public VehiclePermitPage setAddressFour(String addressFour) {
        Logg.logger.info(" addressFour:" + addressFour);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, ownerAddressLineFour, addressFour);
        return this;
    }

    public VehiclePermitPage setPostCode(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
//        String regex = "/^(?:\\+?61|0)4(?:[01]\\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\\d|(?:200[0-3]|201[01]|8984))\\d{4}$/";
        waitAndSendKeys(driver, ownerPostCode, setPostCode);
        return this;
    }

    public VehiclePermitPage setPersonalDetails(String fname, String lname, String addressOne, String addressThree, String addressFour, String postCode) throws ParseException {
        Logg.logger.info(String.format(" details:  Fname:%s, Lname:%s, AddressOne type:%s, AddressThree type:%s, AddressFour type:%s, PostCode type:%s", fname, lname, addressOne, addressThree, addressFour, postCode));
        this.setFName(fname)
                .setLName(lname)
                .setAddressOne(addressOne)
                .setAddressTwo(addressThree)
                .setAddressFour(addressFour)
                .setPostCode(postCode);
//        if(phonetype.equalsIgnoreCase("mobile"))
//            setSMSNotifications(sms);
        return this;
    }

    public VehiclePermitPage setCsoEmail(String email) {
        Logg.logger.info("Email:" + email);
        waitAndSendKeys(driver, csoEmail, email);
        return this;
    }


    public VehiclePermitPage setDeclarations(String declarations) {
        Logg.logger.info("declarations:" + declarations);
        if (declarations.equalsIgnoreCase("The information I have provided is true and correct."))
            waitAndClick(driver, declarationOne);
        return this;
    }


    public VehiclePermitPage saveForm() {
        Logg.logger.info("");
//        WebElement paySubmit = driver.findElement(By.id("saveandcontinue"));
//        paySubmit.click();
        waitAndClick(driver, finalSubmit);
//        super.submitForm(formNo);
        return this;
    }


    public VehiclePermitPage uploadSitePlan() throws AWTException, InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[19]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        return this;
    }

    public VehiclePermitPage uploadDigReport() throws AWTException, InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[20]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        return this;
    }

    public VehiclePermitPage uploadSupporting() throws AWTException, InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[21]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[21]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[21]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[21]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[21]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
        return this;
    }


    public VehiclePermitPage getReviewPage(String expReviewPage, String price) throws Exception {

        GUID = driver.findElement(By.id("input_234_4")).getAttribute("value");
        System.out.println("The GUID is " + GUID + "");
        String productCode = driver.findElement(By.id("input_234_54")).getAttribute("data-product-id");
        System.out.println("***************The Product Code is*************** \n" + productCode + "");
        Thread.sleep(3000);

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_234_51\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
        if (reviewPage.equalsIgnoreCase(expReviewPage)) {
            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), expReviewPage, "");
        } else {
            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), expReviewPage, "");
            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");

        }
        String lastPrice = reviewPage.substring(reviewPage.lastIndexOf("\n"));
        String actualPrice = lastPrice.replace("\n", "");
        System.out.println("***************The PRICE IS************* " + actualPrice + "");

        if (actualPrice.equalsIgnoreCase(price.trim())) {
            extent_Pass(driver, "The Price verification is correct", price.toString(), "", "");
        } else {
            extent_Warn(driver, "The Price verification Failed", price.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE PRICE VERIFICATION LEVEL ");

        }

        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_234_52\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }

        return this;
    }

    @Test(dataProvider = "APP_DataProvider", description = "Verify that the review page values can be compared")
    public VehiclePermitPage getReviewPageBack(String expReviewBack, String expPriceBack) throws Exception {


        GUID = driver.findElement(By.id("input_232_5")).getAttribute("value");
        System.out.println("The GUID is " + GUID + "");
        String productCode = driver.findElement(By.id("input_232_5")).getAttribute("data-product-id");
        System.out.println("***************The Product Code is*************** \n" + productCode + "");
        Thread.sleep(3000);

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_232_54\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
        if (reviewPage.equalsIgnoreCase(expReviewBack)) {
            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
        } else {
            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");

        }
        String lastPrice = reviewPage.substring(reviewPage.lastIndexOf("\n"));
        String actualPrice = lastPrice.replace("\n", "");
        System.out.println("***************The PRICE IS************* " + actualPrice + "");

        if (actualPrice.equalsIgnoreCase(expPriceBack.trim())) {
            extent_Pass(driver, "The Price verification is correct", expPriceBack.toString(), "", "");
        } else {
            extent_Warn(driver, "The Price verification Failed", expPriceBack.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE PRICE VERIFICATION LEVEL ");

        }

        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_232_56\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }


        return this;
    }


    public VehiclePermitPage getPaymentDetails() throws InterruptedException, ParseException, AWTException {


        //Get the Issue type from the review page
        Thread.sleep(15000);
        String paymentDetails = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]")).getText();
        System.out.println("The payment details are " + paymentDetails + "");
        //String Receipt Number
        String expReceiptNumber = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]/p[1]")).getText();
        System.out.println("The Receipt Number " + expReceiptNumber + "");
        compReceiptNumber = expReceiptNumber.substring(16);
        System.out.println("The Trimmed RN is " + compReceiptNumber + "");

        return this;
    }


    @Test(dataProvider = "VCP_DataProvider", description = "Verify that the CRM values are verified")
    public VehiclePermitPage captureCRM(String expCaseType, String expSubType, String expCustomerName, String expLocation, String Instructions,
                                        String expPdfFormat, String expDial, String expCertificate, String expNotification,
                                        String expReceipt, String expSite, String expSupport1, String expSupport2,
                                        String expSupport3, String expSupport4, String expSupport5, String expTraffic,
                                        String expCount, String expType, String expBusinessNo, String expMobileNo, String expAlternateNo, String expFName, String expLName, String expEmail, String expCaseCount) throws Exception {


        Thread.sleep(15000);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.addArguments("window-size=1920,1080");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
        System.out.println(driver.manage().window().getSize());
        driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
        System.out.println(driver.manage().window().getSize());

        Thread.sleep(20000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);


        //Switch to the frame that contains the grid
        driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
        Thread.sleep(10000);


        Actions action = new Actions(driver);
        String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
        System.out.println("The GUID text in CRM is " + identifierText + "");
        WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
        Logg.logger.info("Is the GUID correct:" + identifier);



        action.doubleClick(identifier).perform();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(5000);
//        driver.switchTo().frame(driver.findElement(By.id("InlineDialog_Iframe")));
//        Thread.sleep(5000);
//        WebElement emailPopup1 = driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/button"));
//        emailPopup1.click();
//        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        if (caseID.isDisplayed()) {
            extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
            throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
        }
            caseID.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);


            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseName.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }
            Thread.sleep(5000);
            //Select the Sub-case Type
            String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
            System.out.println("The SubcaseType is " + subCaseName + "");
            if (subCaseName.equalsIgnoreCase(expSubType)) {
                extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
            }
            //Select the origin
            String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
            Logg.logger.info("Origin name:" + originName);
            if (originName.equalsIgnoreCase("Web")) {
                extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            } else {
                extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
            }
            //Select the customer name
            String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
            Logg.logger.info("Customer name:" + customerName);

            if (customerName.equalsIgnoreCase(expCustomerName)) {
                extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
            } else {
                extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
            }

            //Verify the Location Field
            String locationField = driver.findElement(By.xpath("//*[@id=\"PropertyQuickView_PropertyQuickView_account_address1_composite\"]/div[1]/span")).getText();
            if (locationField.contains(expLocation)) {
                extent_Pass(driver, "Location Verification Passed", locationField.toString(), "", "");
            } else {
                extent_Warn(driver, "Location verification Failed", locationField.toString(), "", "");
                throw new Exception("TEST CASE FAILED AT THE ADDRESS LEVEL ");
            }

            //Verify the Instructions Field
            String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();

            if (instructionsField.equalsIgnoreCase(Instructions)) {
                extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
            } else {
                extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), Instructions, "");
                throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
            }

            Thread.sleep(10000);


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to the Case ESCALATION TAB
            WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
            Thread.sleep(10000);
            caseTabSelected.click();

            driver.switchTo().parentFrame();
            //Get the first response value
            String firstResponse = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
            Logg.logger.info("First Response is:" + firstResponse);

            //Check if the Timer is not ticking and First response is Succeeded
            if (firstResponse.equalsIgnoreCase("Succeeded")) {
                extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "", "");
            } else {
                extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "", "");
                throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
            }


            Thread.sleep(5000);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);


            //Navigate to the PAYMENTS TAB
            WebElement payTabSelected = driver.findElement(By.id("tab_paymentrecords"));
            Thread.sleep(10000);
            payTabSelected.click();

            driver.switchTo().parentFrame();
            //Get the first response value
            String receiptNumber = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[3]/div")).getText();
//        String compReceiptNumber = expReceiptNumber.substring(16);
//        System.out.println("The Trimmed RN is " + compReceiptNumber + "");
            if (receiptNumber.contains(compReceiptNumber)) {
                extent_Pass(driver, "Payment receipt- Verification Passed", receiptNumber.toString(), compReceiptNumber, receiptNumber);
            } else {
                extent_Warn(driver, "Payment receipt - verification Failed", receiptNumber.toString(), compReceiptNumber, receiptNumber);
                throw new Exception(" VERIFICATION OF RECEIPT FAILED ");
            }

            String amount = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[5]/div")).getText();
            if (amount.equalsIgnoreCase("$188.00")) {
                extent_Pass(driver, "Amount- Verification Passed", amount.toString(), "", "");
            } else {
                extent_Warn(driver, "Amount - verification Failed", amount.toString(), "", "");
                throw new Exception(" VERIFICATION OF AMOUNT FAILED ");
            }

            String paidStatus = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[6]/nobr")).getText();
            if (paidStatus.equalsIgnoreCase("Paid")) {
                extent_Pass(driver, "Payment Status- Verification Passed", paidStatus.toString(), "", "");
            } else {
                extent_Warn(driver, "Payment Status - verification Failed", paidStatus.toString(), "", "");
                throw new Exception(" VERIFICATION OF Payment Status FAILED ");
            }


            Thread.sleep(5000);


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            Thread.sleep(10000);

            //Navigate to Notes
            WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
            Thread.sleep(10000);
            notesTabSelected.click();
//        Thread.sleep(20000);

            //Navigate to Activities
            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
            actTabSelected.click();
            Thread.sleep(10000);


            String emailTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div")).getAttribute("objecttypecode");
            Thread.sleep(20000);
            Logg.logger.info("Email:" + emailTrigger);

            Thread.sleep(20000);


            //Check if SMS is triggered
            Thread.sleep(20000);
            String smsTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[3]/div[1]/div/div")).getAttribute("objecttypecode");
            Logg.logger.info("SMS:" + smsTrigger);


//        //Select the customer name
//        Thread.sleep(20000);
//        WebElement custName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[1]/div/table/tbody/tr[9]/td[2]/div[1]/div[1]/span[1]"));
//        custName.click();
//        Thread.sleep(10000);

            //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.navigate().refresh();
            Thread.sleep(2000);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.switchTo().defaultContent();
            WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
            caseIDRefresh.click();

            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(0);
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            Thread.sleep(10000);

            //DOCUMENT DETAILS
            //Navigate to the Document Tabs
            Thread.sleep(20000);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
            Thread.sleep(10000);
            if (documentTabSelected.isDisplayed()) {
                extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
            } else {
                extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
                throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
            }
            documentTabSelected.click();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

            Thread.sleep(10000);
            driver.switchTo().parentFrame();
            Thread.sleep(5000);


            //Get the Name of the First Document - PDF Document
            String pdfDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (pdfDocName.equalsIgnoreCase(expPdfFormat)) {
                extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", pdfDocName.toString(), "", "");
            } else {
                extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", pdfDocName.toString(), "", "");
                throw new Exception("PDF NAMING CONVENTION FAILED ");
            }
//


//        if (certName.equalsIgnoreCase(ossReceiptName)) {
//            extent_Pass(driver, "NAME FORMAT - CERTIFICATE - Verification Passed", certName.toString(), "", "");
//        } else {
//            extent_Warn(driver, "NAME FORMAT - CERTIFICATE - verification Failed", certName.toString(), "", "");
//            throw new Exception("CERTIFICATE NAMING CONVENTION FAILED ");
//        }

            //Get the Name of the Third Document - Dial before you Dig
            String dialName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (dialName.equalsIgnoreCase("Dial Before You Dig - Application - Vehicular Crossing Permit - Tower 1/1A Stanley Terrace Canterbury")) {
                extent_Pass(driver, "NAME FORMAT - DIAL - Verification Passed", dialName.toString(), expDial, "");
            } else {
                extent_Warn(driver, "NAME FORMAT - DIAL - verification Failed", dialName.toString(), expDial, "");
                throw new Exception("DIAL NAMING CONVENTION FAILED ");
            }

            String ossReceiptName = expReceipt + " + " + compReceiptNumber;
            Logg.logger.info("Receipt Format:" + ossReceiptName);
            //Get the Name of the Second Document - Certificate
            String certName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
            //Get the Name of the Fourth Document - Notification
            String notificationName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
            if (notificationName.equalsIgnoreCase(expNotification)) {
                extent_Pass(driver, "NAME FORMAT - NOTIFICATION - Verification Passed", notificationName.toString(), "", "");
            } else {
                extent_Warn(driver, "NAME FORMAT - NOTIFICATION - verification Failed", notificationName.toString(), "", "");
                throw new Exception("NOTIFICATION NAMING CONVENTION FAILED ");
            }


            //Click the Next page to view the Documents

            WebElement docPage2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
            docPage2.click();
            Thread.sleep(10000);

//        driver.switchTo().parentFrame();


            //Get the Document details from the Second Page
            //Get the Name of the First Document - Receipt
//        String ossReceiptName = expReceipt + " + "+ compReceiptNumber;
//        Logg.logger.info("Receipt Format:" + ossReceiptName);

            String receiptDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (receiptDocName.contains(expSupport1)) {
                extent_Pass(driver, "NAME FORMAT - RECEIPT - Verification Passed", receiptDocName.toString(), "", "");
            } else {
                extent_Warn(driver, "NAME FORMAT - RECEIPT - verification Failed", receiptDocName.toString(), "", "");
                throw new Exception("RECEIPT NAMING CONVENTION FAILED ");
            }
            //Get the Name of the Second Document - Site
            String siteName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
            if (siteName.equalsIgnoreCase(expSupport1)) {
                extent_Pass(driver, "NAME FORMAT - SITE - Verification Passed", siteName.toString(), "", "");
            } else {
                extent_Warn(driver, "NAME FORMAT - SITE - verification Failed", siteName.toString(), "", "");
                throw new Exception("SITE NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Third Document - Supporting Document 1
            String supportDoc1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
            if (supportDoc1.equalsIgnoreCase(expSupport1)) {
                extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 1 - Verification Passed", supportDoc1.toString(), "", "");
            } else {
                extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 1 - verification Failed", supportDoc1.toString(), "", "");
                throw new Exception("SUPPORTING DOCUMENT 1 NAMING CONVENTION FAILED ");
            }

            //Get the Name of the Fourth Document - Supporting Document 2
            String supportDoc2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
            if (supportDoc2.equalsIgnoreCase(expSupport1)) {
                extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 2 - Verification Passed", supportDoc2.toString(), "", "");
            } else {
                extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 2 - verification Failed", supportDoc2.toString(), "", "");
                throw new Exception("SUPPORTING DOCUMENT 2 NAMING CONVENTION FAILED ");
            }

            //Click the Next page to view the Documents
            WebElement docPage3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
            docPage3.click();
            Thread.sleep(10000);

            //Get the Document details from the Third Page
            //Get the Name of the First Document - Supporting Document 3
            String supportDoc3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
            if (supportDoc3.contains(expSupport1)) {
                extent_Pass(driver, "NAME FORMAT - SUPPORT 3 - Verification Passed", supportDoc3.toString(), "", "");
            } else {
                extent_Warn(driver, "NAME FORMAT - SUPPORT 3 - verification Failed", supportDoc3.toString(), "", "");
                throw new Exception("SUPPORT 3 NAMING CONVENTION FAILED ");
            }

            //Get the Total Number of records
            String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
            if (totalDocuments.equalsIgnoreCase(expCount)) {
                extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
            } else {
                extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
                throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
            }


            //Click the First Page to view the Documents

//            WebElement docPageFirst = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[1]/img"));
//            docPageFirst.click();
//            Thread.sleep(10000);
//
//            //Click to download PDF Document
//            String parenthandle = driver.getWindowHandle();
//            WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[3]/nobr/a"));
//            pdfDocOne.click();
//            Thread.sleep(10000);
//            driver.switchTo().window(parenthandle);
//            driver.navigate().refresh();
//            Thread.sleep(2000);
//            Thread.sleep(10000);
//            driver.switchTo().defaultContent();
//            WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
//            caseIDone.click();
//
//            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
//            driver.switchTo().frame(0);
//            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


            //CASE AND CUSTOMER DETAILS
            //Select the CaseType name
            String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
            if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
                extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
            } else {
                extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
                System.out.println("The Actual Case Name in CRM is " + caseName + "");
                System.out.println("The expected Case Name is " + expCaseType + "");
                throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

            }


            //Select the customer name
            Thread.sleep(20000);
            WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
            custName.click();
            Thread.sleep(10000);

            //Customer Matching
            //Contact Type
            String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
            if (contactType.equalsIgnoreCase(expType)) {
                extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
                throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
            }
////        //Get the Business Phone Number
////        String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
////        if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
////            extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
////        } else {
////            extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
////            throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
////        }
////        //Get the Mobile Number
////        String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
////        if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
////            extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
////        } else {
////            extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
////            throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
////        }
////        //Get the Landline Number
////        String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
////        if (landPhone.equalsIgnoreCase(expAlternateNo)) {
////            extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
////        } else {
////            extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
////            throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
////        }
////
//        //Get the First Name
//        String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
//        if (firstName.equalsIgnoreCase(expFName)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
//            throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
//        }
//
//        //Get the Last Name OR Business Name
//        String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
//        if (lastName.equalsIgnoreCase(expLName)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLName, lastName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLName, lastName);
//            throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
//        }
            //Get the Email
            String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
            if (emailName.equalsIgnoreCase("Reshma.Nair@boroondara.vic.gov.au")) {
                extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
            } else {
                extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
                throw new Exception(" VERIFICATION OF EMAIL FAILED ");
            }
//
//
            Thread.sleep(5000);


            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
            Thread.sleep(10000);


            //Navigate to the Customer Case Records Tab
            WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
            Thread.sleep(10000);
            customerCaseRecords.click();

            Thread.sleep(5000);

            driver.switchTo().parentFrame();
            Thread.sleep(10000);
            String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
            Logg.logger.info("Total Number of Cases:" + caseRecords);
//        if (caseRecords) {
//            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
//            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
//        }


            Thread.sleep(10000);
            navigateBack(driver);
            Thread.sleep(10000);
//
        driver.quit();


//
            return this;

        }

    @Test(dataProvider = "VCP_DataProvider", description = "Verify that the CRM values are verified")

    public VehiclePermitPage readPDF(String pdf, String expPdfText) throws Exception {
        Logg.logger.info("");

        PDFReader pdfManager = new PDFReader();

        pdfManager.setFilePath("C:\\Users\\rnair\\Downloads\\" +pdf+ ".pdf");

        try {

            String text = pdfManager.toText();
////            String[] text1 = text.split("\n");// this array is initialized with a single element
//            for (int i = 0; i< text.length - 1; i++) {
////               String actPDFText = linesFile[i].substring(linesFile[i].indexOf(".")+1);
////                String actPDFText = text.replaceAll("(?m)^• TestImage.*", "TestImage.JPG");
            String chopLastLine = text.replaceAll("Submission date:.*", "Dynamic Submission Date");
            String emptyLine = chopLastLine.replaceAll("\\\\n", "");
            actPDFText = emptyLine.replaceAll("TestImage.*", "TestImage.JPG");
            System.out.println("PDF Display " + actPDFText + "");
                Thread.sleep(7000);
            if (actPDFText.contains(expPdfText)) {
                extent_Pass(driver, "PDF CONTENT - PDF- Verification Passed", actPDFText.toString(), "", "");
            } else{
                extent_Warn(driver, "PDF CONTENT - PDF- Verification Failed", actPDFText.toString(), expPdfText, "");
                throw new Exception(" VERIFICATION OF PDF CONTENT FAILED ");

            }
        } catch (IOException ex) {
            Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
        }

        return this;
    }


        @DataProvider

        public Object[][] VCP_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "VCP");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "VCP", 64);

            return testObjArray;
        }


    }



