package pageObjects.forms;

import Tests.forms.PDFBoxReadFromFile;
import Tests.forms.PDFReader;
import com.relevantcodes.extentreports.LogStatus;
import libraries.ExcelUtils;
import libraries.Logg;
import libraries.Utilities;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class NewPetRegistrationPage extends BaseFormsClass {
    String formNo="203";
    static String GUID ="";
    static String compReceiptNumber ="";
    public NewPetRegistrationPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    String form_ID="#gform_page_"+formNo;

    //Submit Form
    @FindBy(id="saveandcontinue")
    private WebElement finalSubmit;

    @FindBy(id = "input_239_81")
    private WebElement address;
    @FindBy(id = "input_239_4")
    private WebElement petName;

    @FindBy(id = "label_239_153_0")
    private WebElement petCat;
    @FindBy(id = "label_239_153_1")
    private WebElement petDog;

    @FindBy(id="input_239_107")
    private WebElement primaryBreed;
    @FindBy(id="input_239_157")
    private WebElement secondaryBreed;

    @FindBy(id="input_239_108")
    private WebElement petColor;
    @FindBy(id="input_239_201")
    private WebElement petAge;
    @FindBy(id="label_239_114_0")
    private WebElement petMale;
    @FindBy(id="label_239_114_1")
    private WebElement petFemale;

    @FindBy(id="label_239_45_0")
    private WebElement deregisterYes;
    @FindBy(id="label_239_45_1")
    private WebElement deregisterNo;

    @FindBy(id="input_239_171")
    private WebElement petMicrochipNo;
    @FindBy(id="label_239_63_0")
    private  WebElement desexedYes;
    @FindBy(id="label_239_63_1")
    private  WebElement desexedNo;
    @FindBy(id="label_239_63_2")
    private  WebElement desexedUnsure;
    @FindBy(id="label_239_115_0")
    private WebElement otherCouncilYes;
    @FindBy(id="label_239_115_1")
    private WebElement otherCouncilNo;
    @FindBy(id="input_239_187")
    private WebElement otherCouncilName;
    @FindBy(id="label_239_174_1")
    private WebElement clubNo;
    @FindBy(id="label_239_174_0")
    private WebElement clubYes;
    @FindBy(id="label_239_80_0")
    private WebElement pensionYes;
    @FindBy(id="label_239_80_1")
    private WebElement pensionNo;
    @FindBy(id="input_239_127")
    private WebElement pensionCard;
    @FindBy(id="label_239_161_0")
    private WebElement menacingYes;
    @FindBy(id="label_239_161_1")
    private WebElement menacingNo;
    @FindBy(id="label_239_162_0")
    private WebElement dangerousYes;
    @FindBy(id="label_239_162_1")
    private WebElement dangerousNo;
    @FindBy(id="label_239_173_0")
    private  WebElement obedienceYes;
    @FindBy(id="label_239_173_1")
    private  WebElement obedienceNo;
    @FindBy(id="input_239_89")
    private  WebElement fName;
    @FindBy(id="input_239_90")
    private  WebElement lName;
    @FindBy(id="input_239_97")
    private  WebElement phoneType;
    @FindBy(id="input_239_98")
    private  WebElement phoneNo;
    @FindBy(id="input_239_32")
    private  WebElement email;
    @FindBy(id="input_239_32_2")
    private  WebElement confEmail;
    @FindBy(id="choice_239_186_1")
    private WebElement smsNotification;
    @FindBy(id="label_239_186_1")
    private WebElement smsNotificationLabel;
    @FindBy(id = "label_239_17_1")
    private WebElement privacyCheckLabel;
    @FindBy(id = "choice_239_17_1")
    private WebElement privacyCheck;
    @FindBy(id="label_239_65_0")
    private WebElement notSpecialDog;
    @FindBy(id = "label_239_65_1")
    private WebElement guideDog;
    @FindBy(id = "label_239_65_2")
    private WebElement customsDog;
    @FindBy(id = "label_239_65_3")
    private WebElement policeDog;
    @FindBy(id="gform_submit_button_239")
    private WebElement submitButton;
    @FindBy(id="gform_browse_button_239_172")
    private WebElement uploadButton;
    @FindBy(id="label_239_184_1")
    private WebElement checkUpload;
    @FindBy(id="input_239_229")
    private WebElement csoEmail;
    @FindBy(id="label_239_233_1")
    private WebElement restrictedCheck;
    @FindBy(id="label_239_86_1")
    private WebElement postalAddressDifferent;
    @FindBy(id="input_239_87_1")
    private WebElement streetAddress;
    @FindBy(id="input_239_87_3")
    private WebElement suburb;
    @FindBy(id="input_239_87_4")
    private WebElement stateDropdown;
    @FindBy(id="input_239_87_5")
    private WebElement postCode;




    //Customer Matching
    String expbusinessname = "Carole Dummy";
    String expFirstName = "Carole";
    String expLastName = "Dummy";
    String expbusinessPhone = "0433210000";
    String expmobileNumber = "0406361552";
    String expLandline = "0392784273";
    String expEmail = "Reshma.Nair@boroondara.vic.gov.au";

    //Case Details
    String expCaseType = "Asset Protection";
    String expSubType = "Application - Asset Protection";
    String expOrigin = "Web";
    String expLocation ="45 Suffolk Road\n" +
            "SURREY HILLS VIC 3127\n" +
            "Australia";
    //Document Tabs
    String expPdfname = "Request - Public Litter Bin Overfilled - 25 Fordham Avenue CAMBERWELL";
    String expImage = "Image of Issue - Public Litter Bin Overfilled - 25 Fordham Avenue CAMBERWELL";

    //Next Page 1
    @FindBy(id="gform_next_button_239_151")
    private WebElement nextOne;

    //Next Page 1
    @FindBy(id="gform_next_button_239_152")
    private WebElement nextTwo;

    public NewPetRegistrationPage clickNext1()
    {

        nextOne.click();
        return this;
    }

    public NewPetRegistrationPage clickNext2() throws InterruptedException {

        Thread.sleep(7000);
//        WebElement nextTwo = driver.findElement(By.id("gform_next_button_239_152"));
        nextTwo.click();
        return this;
    }

    public NewPetRegistrationPage clickNext3()
    {

        WebElement nextThree = driver.findElement(By.id("gform_next_button_239_30"));
        nextThree.click();

        return this;
    }

    public NewPetRegistrationPage clickNext4()
    {

        WebElement nextFour = driver.findElement(By.id("gform_next_button_239_66"));
        nextFour.click();
        return this;
    }

    public NewPetRegistrationPage clickNext5()
    {

        WebElement nextFive = driver.findElement(By.id("gform_next_button_239_88"));
        nextFive.click();
        return this;
    }


    public NewPetRegistrationPage clickNext(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickNext(formNo,pageNo);
        return this;
    }
    public NewPetRegistrationPage clickPrevious(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickPrevious(formNo,pageNo);
        return this;
    }
    public NewPetRegistrationPage setPetAddress(String address) throws InterruptedException {
        Logg.logger.info("address:"+address);
        super.selectAddress(this.address,address);
        Thread.sleep(5000);
        return this;
    }
    public NewPetRegistrationPage setSpecialServices(String specialServices)
    {
        Logg.logger.info("Special services dog:"+specialServices);
        if(specialServices.equalsIgnoreCase("Police"))
            waitAndClick(driver,policeDog);
        if(specialServices.equalsIgnoreCase("Guide"))
            waitAndClick(driver,guideDog);
        if(specialServices.equalsIgnoreCase("Customs"))
            waitAndClick(driver,customsDog);
        return this;
    }
    public NewPetRegistrationPage setPetName(String name)
    {
        Logg.logger.info("Pet name:"+name);
        waitAndSendKeys(driver,petName,name);
        return this;
    }
    public NewPetRegistrationPage setPetType(String pet)
    {
        Logg.logger.info("pet:"+pet);
        if(pet.equalsIgnoreCase("cat"))
            waitAndClick(driver,petCat);
        if(pet.equalsIgnoreCase("dog")) {
            waitAndClick(driver, petDog);
            waitAndClick(driver, restrictedCheck);
        }
        return this;
    }
    public NewPetRegistrationPage setPetGender(String gender)
    {
        Logg.logger.info("Pet Gender:"+gender);
        if(gender.equalsIgnoreCase("male"))
            waitAndClick(driver,petMale);
        if(gender.equalsIgnoreCase("female"))
            waitAndClick(driver,petFemale);
        return this;
    }
    public NewPetRegistrationPage setPrimaryBreed(String primaryBreed)
    {
        Logg.logger.info("Primary breed:"+primaryBreed);
        //selectFirstItemFromLookup(driver,this.primaryBreed,primaryBreed);
        waitAndSendKeys(driver,this.primaryBreed,primaryBreed);
        return this;
    }
    public NewPetRegistrationPage setSecondaryBreed(String secondaryBreed)
    {
        Logg.logger.info("Primary breed:"+secondaryBreed);
        //selectFirstItemFromLookup(driver,this.secondaryBreed,secondaryBreed);
        waitAndSendKeys(driver,this.secondaryBreed,secondaryBreed);
        return this;
    }
    public NewPetRegistrationPage setPetColor(String color)
    {
        Logg.logger.info("Color:"+color);
        waitAndSendKeys(driver,petColor,color);
        return this;
    }
    public NewPetRegistrationPage setPetAge(String age)
    {
        Logg.logger.info("Pet age:"+age);
        waitAndSendKeys(driver,petAge,age);
        return this;
    }
    public NewPetRegistrationPage setDeregisterAnimals(String option)
    {
        Logg.logger.info("Are there deregister animals:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,deregisterYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,deregisterNo);
        return this;
    }
    public NewPetRegistrationPage setMicrochipNo(String no)
    {
        Logg.logger.info("Microchip NO:"+no);
        waitAndSendKeys(driver,petMicrochipNo,no);
        return this;
    }
    public NewPetRegistrationPage setDesexed(String option)
    {
        Logg.logger.info("Desexed:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,desexedYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,desexedNo);
        if(option.equalsIgnoreCase("unsure"))
            waitAndClick(driver,desexedUnsure);
        return this;
    }
    public NewPetRegistrationPage setRegisteredOtherCouncil(String option)
    {
        Logg.logger.info("Registered with other council:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,otherCouncilYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,otherCouncilNo);
        return this;
    }
    public NewPetRegistrationPage setCouncil(String council) {
        Logg.logger.info("Registered with other council:" + council);
        waitAndSendKeys(driver, otherCouncilName,council);
        return this;
    }
    public NewPetRegistrationPage setOtherClub(String option)
    {
        Logg.logger.info("is pet registered with a club:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,clubYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,clubNo);
        return this;
    }
    public NewPetRegistrationPage setPension(String option)
    {
        Logg.logger.info("Is pensioner:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,pensionYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,pensionNo);
        return this;
    }
    public NewPetRegistrationPage setPensionCard(String card)
    {
        Logg.logger.info("Pension Card number:"+card);
        waitAndSendKeys(driver,pensionCard,card);
        return this;
    }
    public NewPetRegistrationPage setDangerous(String option)
    {
        Logg.logger.info("Is dog dangerous:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,dangerousYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,dangerousNo);
        return this;
    }
    public NewPetRegistrationPage setMenacing(String option)
    {
        Logg.logger.info("Is dog Menacing:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,menacingYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,menacingNo);
        return this;
    }
    public NewPetRegistrationPage setObedienceTrained(String option)
    {
        Logg.logger.info("Is dog Obedience trained:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,obedienceYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,obedienceNo);
        return this;
    }
    public NewPetRegistrationPage setFName(String name)
    {
        Logg.logger.info("First Name:"+name);
        waitAndSendKeys(driver,fName,name);
        return this;
    }
    public NewPetRegistrationPage setLName(String name)
    {
        Logg.logger.info("Last Name:"+name);
        waitAndSendKeys(driver,lName,name);
        return this;
    }
    public NewPetRegistrationPage setEmail(String email)
    {
        Logg.logger.info("Email:"+email);
        waitAndSendKeys(driver,this.email,email);
        return this;
    }
    public NewPetRegistrationPage setConfEmail(String email)
    {
        Logg.logger.info("Conf Email:"+email);
        waitAndSendKeys(driver,confEmail,email);
        return this;
    }

    public NewPetRegistrationPage setPhoneType(String phoneType)
    {
        Logg.logger.info(" phone type:"+phoneType);
        selectDropdown_VisibleText(driver,this.phoneType,phoneType);
        return this;
    }
    public NewPetRegistrationPage setPhoneNumber(String phoneNumber)
    {
        Logg.logger.info(" Phone number:"+phoneNumber);
        waitAndSendKeys(driver,phoneNo,phoneNumber);
        return this;
    }
    public NewPetRegistrationPage setSMSNotifications(String option)
    {
        Logg.logger.info("Set SMS Notifications to:"+option);
        if(option.equalsIgnoreCase("yes"))
            if(!smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        if(option.equalsIgnoreCase("no"))
            if(smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        return this;
    }
    public NewPetRegistrationPage setPersonalDetails(String fname,String lname,  String phonetype, String phoneNumber,String sms) throws ParseException {
        Logg.logger.info(String.format(" details:  Fname:%s,  Lname:%s,  Email:%s, Conf Email:%s, Phone type:%s, Phone:%s, receive SMS notification:%s", fname,lname,email,confEmail,phonetype,phoneNumber,sms));
        this.setFName(fname)
                .setLName(lname)
                .setPhoneType(phonetype)
                .setPhoneNumber(phoneNumber);
        if(phonetype.equalsIgnoreCase("mobile"))
            setSMSNotifications(sms);
        return this;
    }
    public NewPetRegistrationPage setCsoEmail(String email)
    {
        Logg.logger.info("Email:"+email);
        waitAndSendKeys(driver,csoEmail,email);
        return this;
    }
    public NewPetRegistrationPage setPolicy(String option)
    {
        Logg.logger.info("Set read &accept privacy policy to:"+option);
        if(option.equalsIgnoreCase("yes")) {
            if(!privacyCheck.isSelected())
                waitAndClick(driver,privacyCheckLabel);
        }
        if(option.equalsIgnoreCase("no"))
            if(privacyCheck.isSelected())
                waitAndClick(driver,privacyCheckLabel);
        return this;
    }
    public NewPetRegistrationPage submitForm() throws InterruptedException {
        Logg.logger.info("");
//        String GUID = driver.findElement(By.id("input_203_85")).getAttribute("value");
//        System.out.println("The GUID is " + GUID + "");
        Thread.sleep(10000);
        waitAndClick(driver,finalSubmit);
//        super.submitForm(formNo);
        return this;
    }
    public NewPetRegistrationPage checkUploadBox()
    {
        Logg.logger.info("");
        waitAndClick(driver,checkUpload);
        return this;
    }
    public NewPetRegistrationPage uploadMicrochipFile(String fileName) throws AWTException, InterruptedException {
        Logg.logger.info("File:"+fileName);
        Utilities.uploadFile(uploadButton,fileName);
        return this;
    }
    public NewPetRegistrationPage setPostalAddress(String streetname, String suburb, String state, String postCode)
    {
        Logg.logger.info(String.format("Street Name:%s, Suburb:%s, State:%s, PostCode:%s",streetname,suburb,state,postCode));
        waitAndClick(driver,postalAddressDifferent);
        waitAndSendKeys(driver,this.streetAddress,streetname);
        waitAndSendKeys(driver,this.suburb,suburb);
        selectDropdown_VisibleText(driver,this.stateDropdown,state);
        waitAndSendKeys(driver,this.postCode,postCode);
        return this;
    }


    public NewPetRegistrationPage uploadHeadlessDocument() throws AWTException, InterruptedException {
        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestImage1.JPG ");
        Thread.sleep(5000);
//

        return this;
    }

    public NewPetRegistrationPage getReviewPage(String expReviewPage, String price) throws Exception {

        GUID = driver.findElement(By.id("input_239_85")).getAttribute("value");
        System.out.println("The GUID is " + GUID + "");
        String productCode = driver.findElement(By.id("input_239_194")).getAttribute("data-product-id");
        System.out.println("***************The Product Code is*************** \n" + productCode + "");
        Thread.sleep(3000);

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_239_35\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
        if (reviewPage.equalsIgnoreCase(expReviewPage)) {
            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
        } else {
            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");

        }
        String lastPrice = reviewPage.substring(reviewPage.lastIndexOf("\n"));
        String actualPrice = lastPrice.replace("\n","");
        System.out.println("***************The PRICE IS************* " + actualPrice + "");

        if (actualPrice.equalsIgnoreCase(price.trim())) {
            extent_Pass(driver, "The Price verification is correct", price.toString(), "", "");
        } else {
            extent_Warn(driver, "The Price verification Failed", price.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE PRICE VERIFICATION LEVEL ");

        }

        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_239_237\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }

        return this;
    }
    @Test(dataProvider = "NPR_DataProvider", description = "Verify that the review page values can be compared")
    public NewPetRegistrationPage getReviewPageBack(String expReviewBack, String expPriceBack) throws Exception {


        GUID = driver.findElement(By.id("input_232_5")).getAttribute("value");
        System.out.println("The GUID is " + GUID + "");
        String productCode = driver.findElement(By.id("input_232_109")).getAttribute("data-product-id");
        System.out.println("***************The Product Code is*************** \n" + productCode + "");
        Thread.sleep(3000);

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_232_54\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
        if (reviewPage.equalsIgnoreCase(expReviewBack)) {
            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
        } else {
            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");

        }
        String lastPrice = reviewPage.substring(reviewPage.lastIndexOf("\n"));
        String actualPrice = lastPrice.replace("\n","");
        System.out.println("***************The PRICE IS************* " + actualPrice + "");

        if (actualPrice.equalsIgnoreCase(expPriceBack.trim())) {
            extent_Pass(driver, "The Price verification is correct", expPriceBack.toString(), "", "");
        } else {
            extent_Warn(driver, "The Price verification Failed", expPriceBack.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE PRICE VERIFICATION LEVEL ");

        }

        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_232_56\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }


        return this;
    }


    public NewPetRegistrationPage getPaymentDetails() throws InterruptedException, ParseException, AWTException {


        //Get the Issue type from the review page
        Thread.sleep(15000);
        String paymentDetails = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]")).getText();
        System.out.println("The payment details are " + paymentDetails + "");
        //String Receipt Number
        String expReceiptNumber = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]/p[1]")).getText();
        System.out.println("The Receipt Number " + expReceiptNumber + "");
        compReceiptNumber = expReceiptNumber.substring(16);
        System.out.println("The Trimmed RN is " + compReceiptNumber + "");

        return this;
    }



    @Test(dataProvider = "NPR_DataProvider", description = "Verify that the CRM values are verified")
    public NewPetRegistrationPage captureCRM(String expCaseType, String expSubType, String expCustomerName, String expLocation,
                                          String expCertificate, String expReceipt,String expPdfFormat,
                                          String expCount, String expType,String expBusinessNo,
                                             String expMobileNo, String expAlternateNo, String expFName, String expLName,
                                             String expEmail, String expCaseCount) throws Exception {
        Thread.sleep(15000);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.addArguments("window-size=1920,1080");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
        System.out.println(driver.manage().window().getSize());
        driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@internalcrmuat.boroondara.vic.gov.au/CoBTrain2/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
        System.out.println(driver.manage().window().getSize());
        Thread.sleep(30000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);


        //Switch to the frame that contains the grid
        driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
        Thread.sleep(30000);


        Actions action = new Actions(driver);
        String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
        System.out.println("The GUID text in CRM is " + identifierText + "");
        WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
        Logg.logger.info("Is the GUID correct:" + identifier);


        action.doubleClick(identifier).perform();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        if (caseID.isDisplayed()) {
            extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
            throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
        }
        caseID.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseName.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }
        Thread.sleep(5000);
        //Select the Sub-case Type
        String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
        System.out.println("The SubcaseType is " + subCaseName + "");
        if (subCaseName.equalsIgnoreCase(expSubType)) {
            extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
        }
        //Select the origin
        String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
        Logg.logger.info("Origin name:" + originName);
        if (originName.equalsIgnoreCase("Web")) {
            extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
        } else {
            extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
        }
        //Select the customer name
        String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
        Logg.logger.info("Customer name:" + customerName);

        if (customerName.equalsIgnoreCase(expCustomerName)) {
            extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
        } else {
            extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
        }

        //Verify the Location Field
        String locationField = driver.findElement(By.xpath("//*[@id=\"PropertyQuickView_PropertyQuickView_account_address1_composite\"]/div[1]/span")).getText();
        if (locationField.contains(expLocation)) {
            extent_Pass(driver, "Location Verification Passed", locationField.toString(), "", "");
        } else {
            extent_Warn(driver, "Location verification Failed", locationField.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ADDRESS LEVEL ");
        }




        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to the Case ESCALATION TAB
        WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
        Thread.sleep(10000);
        caseTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String firstResponse = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
        Logg.logger.info("First Response is:" + firstResponse);

        //Check if the Timer is not ticking and First response is Succeeded
        if (firstResponse.equalsIgnoreCase("Succeeded")) {
            extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
        }


        Thread.sleep(5000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);


        //Navigate to the PAYMENTS TAB
        WebElement payTabSelected = driver.findElement(By.id("tab_paymentrecords"));
        Thread.sleep(10000);
        payTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String receiptNumber = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[3]/div")).getText();
//        String compReceiptNumber = expReceiptNumber.substring(16);
//        System.out.println("The Trimmed RN is " + compReceiptNumber + "");
        if (receiptNumber.contains(compReceiptNumber)) {
            extent_Pass(driver, "Payment receipt- Verification Passed", receiptNumber.toString(), compReceiptNumber, receiptNumber);
        } else {
            extent_Warn(driver, "Payment receipt - verification Failed", receiptNumber.toString(), compReceiptNumber, receiptNumber);
            throw new Exception(" VERIFICATION OF RECEIPT FAILED ");
        }

        String amount = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[5]/div")).getText();
        if (amount.equalsIgnoreCase("$188.00")) {
            extent_Pass(driver, "Amount- Verification Passed", amount.toString(), "", "");
        } else {
            extent_Warn(driver, "Amount - verification Failed", amount.toString(), "", "");
            throw new Exception(" VERIFICATION OF AMOUNT FAILED ");
        }

        String paidStatus = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[6]/nobr")).getText();
        if (paidStatus.equalsIgnoreCase("Paid")) {
            extent_Pass(driver, "Payment Status- Verification Passed", paidStatus.toString(), "", "");
        } else {
            extent_Warn(driver, "Payment Status - verification Failed", paidStatus.toString(), "", "");
            throw new Exception(" VERIFICATION OF Payment Status FAILED ");
        }





        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to Notes
        WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
        Thread.sleep(10000);
        notesTabSelected.click();
//        Thread.sleep(20000);

        //Navigate to Activities
        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
        actTabSelected.click();
        Thread.sleep(10000);


        String emailTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div")).getAttribute("objecttypecode");
        Thread.sleep(20000);
        Logg.logger.info("Email:" + emailTrigger);

        Thread.sleep(20000);


//        //Check if SMS is triggered
//        Thread.sleep(20000);
//        String smsTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[3]/div[1]/div/div")).getAttribute("objecttypecode");
//        Logg.logger.info("SMS:" + smsTrigger);


//        //Select the customer name
//        Thread.sleep(20000);
//        WebElement custName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[1]/div/table/tbody/tr[9]/td[2]/div[1]/div[1]/span[1]"));
//        custName.click();
//        Thread.sleep(10000);

        //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseIDRefresh.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);

        //DOCUMENT DETAILS
        //Navigate to the Document Tabs
        Thread.sleep(20000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
        Thread.sleep(10000);
        if (documentTabSelected.isDisplayed()) {
            extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
        } else {
            extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
            throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
        }
        documentTabSelected.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Thread.sleep(10000);
        driver.switchTo().parentFrame();
        Thread.sleep(5000);


        //Get the Name of the First Document - PDF Document
        String certDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (certDocName.equalsIgnoreCase(expCertificate)) {
            extent_Pass(driver, "NAME FORMAT - CERTIFICATE - Verification Passed", certDocName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - CERTIFICATE - verification Failed", certDocName.toString(), "", "");
            throw new Exception("CERTIFICATE NAMING CONVENTION FAILED ");
        }
//

        String ossReceiptName = expReceipt + " + "+ compReceiptNumber;
        Logg.logger.info("Receipt Format:" + ossReceiptName);
        //Get the Name of the Second Document - Certificate
        String certName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
//        if (certName.equalsIgnoreCase(ossReceiptName)) {
//            extent_Pass(driver, "NAME FORMAT - CERTIFICATE - Verification Passed", certName.toString(), "", "");
//        } else {
//            extent_Warn(driver, "NAME FORMAT - CERTIFICATE - verification Failed", certName.toString(), "", "");
//            throw new Exception("CERTIFICATE NAMING CONVENTION FAILED ");
//        }

        //Get the Name of the Third Document - Dial before you Dig
        String pdfDoc = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
        if (pdfDoc.equalsIgnoreCase(expPdfFormat)) {
            extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", pdfDoc.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", pdfDoc.toString(), "", "");
            throw new Exception("PDF NAMING CONVENTION FAILED ");
        }



//
//        //Click to download PDF Document
//        String parenthandle = driver.getWindowHandle();
//        WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[3]/nobr/a"));
//        pdfDocOne.click();
//        Thread.sleep(10000);
//        driver.switchTo().window(parenthandle);
//        driver.navigate().refresh();
//        Thread.sleep(5000);
//        driver.switchTo().defaultContent();
//        WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
//        caseIDone.click();
//
//        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
//        driver.switchTo().frame(0);
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }


        //Select the customer name
        Thread.sleep(20000);
        WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
        custName.click();
        Thread.sleep(10000);

        //Customer Matching
        //Contact Type
        String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
        if (contactType.equalsIgnoreCase(expType)) {
            extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
            throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
        }
//        //Get the Business Phone Number
//        String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//        if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//            throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//        }
        //Get the Mobile Number
//        String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//        if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//        }
//        //Get the Landline Number
//        String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//        if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//            throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//        }

        //Get the First Name
        String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
        if (firstName.equalsIgnoreCase(expFName)) {
            extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
            throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
        }

        //Get the Last Name OR Business Name
        String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
        if (lastName.equalsIgnoreCase(expLName)) {
            extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLName, lastName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLName, lastName);
            throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
        }
        //Get the Email
        String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
        if (emailName.equalsIgnoreCase("Reshma.Nair@boroondara.vic.gov.au")) {
            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
        }


        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
        Thread.sleep(10000);


        //Navigate to the Customer Case Records Tab
        WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
        Thread.sleep(10000);
        customerCaseRecords.click();

        Thread.sleep(5000);

        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
        Logg.logger.info("Total Number of Cases:" + caseRecords);
//        if (caseRecords) {
//            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
//            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
//        }


        Thread.sleep(10000);
        navigateBack(driver);
        Thread.sleep(5000);

        driver.quit();


        return this;
    }


    @DataProvider

    public Object[][] NPR_DataProvider () throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "NPR");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "NPR", 55);

        return testObjArray;
    }


}
