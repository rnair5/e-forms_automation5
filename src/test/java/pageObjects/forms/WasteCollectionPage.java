package pageObjects.forms;

import libraries.ExcelUtils;
import libraries.ExtentReporting;
import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class WasteCollectionPage extends BaseFormsClass {
    String formNo = "193";
    static String GUID ="";

    public WasteCollectionPage(WebDriver driver) {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver, this);
    }

    //Submit Form
    @FindBy(id = "submitbtn")
    private WebElement finalSubmit;

    String form_ID = "#gform_page_" + formNo;
    @FindBy(id = "gform_next_button_193_17")
    private WebElement nextButton;
    @FindBy(id = "label_193_6_0")
    private WebElement hardWasteType;
    @FindBy(id = "label_193_6_1")
    private WebElement greenWasteType;
    @FindBy(id = "label_193_6_2")
    private WebElement christmasWasteType;
    @FindBy(id = "input_193_4")
    private WebElement address;
    @FindBy(id = "input_193_9")
    private WebElement collectionDate;
    @FindBy(id = "label_193_63_0")
    private WebElement cornerBlockYes;
    @FindBy(id = "label_193_63_1")
    private WebElement cornerBlockNo;
    @FindBy(id = "input_193_67")
    private WebElement pickupInstructions;
    @FindBy(id = "input_193_82")
    private WebElement corenerInstructions;
    @FindBy(id = "input_193_111")
    private WebElement csoEmail;
    @FindBy(id = "input_193_85")
    private WebElement fName;
    @FindBy(id = "input_193_86")
    private WebElement lName;
    @FindBy(id = "input_193_35")
    private WebElement phoneType;
    @FindBy(id = "input_193_102")
    private WebElement phoneNo;
    @FindBy(id = "input_193_14")
    private WebElement email;
    @FindBy(id = "input_193_14_2")
    private WebElement confEmail;
    @FindBy(id = "choice_193_31_1")
    private WebElement smsNotification;
    @FindBy(id = "label_193_31_1")
    private WebElement smsNotificationLabel;
    @FindBy(id = "label_193_99_1")
    private WebElement privacyCheckLabel;
    @FindBy(id = "choice_193_99_1")
    private WebElement privacyCheck;
    @FindBy(id = "label_193_42_1")
    private WebElement urgentRouteLabel;
    @FindBy(id = "label_193_101_1")
    private WebElement wasteLabel;
    @FindBy(id = "gform_submit_button_193")
    private WebElement submitButton;

    @FindBy(id = "input_193_40")
    private WebElement reasonDropdown;

    @FindBy(id = "input_193_49")
    private WebElement otherReason;

    public WasteCollectionPage clickNext(String pageNo) {
        Logg.logger.info("PageNo:" + pageNo);
        super.clickNext(formNo, pageNo);
        return this;
    }

    public WasteCollectionPage clickPrevious(String pageNo) {
        Logg.logger.info("Page no:" + pageNo);
        super.clickPrevious(formNo, pageNo);
        return this;
    }

    public WasteCollectionPage setCollectionAddress(String address) {
        Logg.logger.info("Waste collection address:" + address);
        super.selectAddress(this.address, address);
        return this;
    }

    public WasteCollectionPage selectWasteType(String wasteType) {
        Logg.logger.info("Waste type:" + wasteType);
        if (wasteType.equalsIgnoreCase("Hard Waste"))
            waitAndClick(driver, hardWasteType);
        else if (wasteType.equalsIgnoreCase("Green waste"))
            waitAndClick(driver, greenWasteType);
        else if (wasteType.equalsIgnoreCase("Christmas Tree"))
            waitAndClick(driver, christmasWasteType);
        return this;
    }

    public WasteCollectionPage selectDate(int index) {
        Logg.logger.info("Index:" + index + 1);
        try {
            selectDropdown_Index(driver, waitUntilElementIsVisible(driver, collectionDate), index + 0);
        } catch (Exception e) {
            ExtentReporting.extent_Warn(driver, "Cannot select date because" + getErrorMsg(), "", "", getErrorMsg());
        }
        return this;
    }

    public WasteCollectionPage setCornerBlock(String choice) {
        Logg.logger.info("Corner Block:" + choice);
        if (choice.equalsIgnoreCase("yes"))
            waitAndClick(driver, cornerBlockYes);
        else if (choice.equalsIgnoreCase("no"))
            waitAndClick(driver, cornerBlockNo);
        return this;
    }

    public WasteCollectionPage setCornerBlockInst(String instructions) {
        Logg.logger.info("Corner block instructions:" + instructions);
        waitAndSendKeys(driver, corenerInstructions, instructions);
        return this;
    }

    public WasteCollectionPage setInstructions(String instructions) {
        Logg.logger.info("General instructions:" + instructions);
        waitAndSendKeys(driver, pickupInstructions, instructions);
        return this;
    }

    public WasteCollectionPage setFName(String name) {
        Logg.logger.info("First Name:" + name);
        waitAndSendKeys(driver, fName, name);
        return this;
    }

    public WasteCollectionPage setLName(String name) {
        Logg.logger.info("Last Name:" + name);
        waitAndSendKeys(driver, lName, name);
        return this;
    }

    public WasteCollectionPage setEmail(String email) {
        Logg.logger.info("Email:" + email);
        waitAndSendKeys(driver, this.email, email);
        return this;
    }

    public WasteCollectionPage setConfEmail(String email) {
        Logg.logger.info("Conf Email:" + email);
        waitAndSendKeys(driver, confEmail, email);
        return this;
    }

    public WasteCollectionPage setPhoneType(String phoneType) {
        Logg.logger.info(" phone type:" + phoneType);
        selectDropdown_VisibleText(driver, this.phoneType, phoneType);
        return this;
    }

    public WasteCollectionPage setPhoneNumber(String phoneNumber) {
        Logg.logger.info(" Phone number:" + phoneNumber);
        waitAndSendKeys(driver, phoneNo, phoneNumber);
        return this;
    }

    public WasteCollectionPage setSMSNotifications(String option) {
        Logg.logger.info("Set SMS Notifications to:" + option);
        if (option.equalsIgnoreCase("yes"))
            if (!smsNotification.isSelected())
                waitAndClick(driver, smsNotificationLabel);
        if (option.equalsIgnoreCase("no"))
            if (smsNotification.isSelected())
                waitAndClick(driver, smsNotificationLabel);
        return this;
    }

    public WasteCollectionPage setPersonalDetails(String fname, String lname, String phonetype, String phoneNumber, String sms) throws ParseException {
        Logg.logger.info(String.format("Personal details:  Fname:%s,  Lname:%s,  Email:%s, Conf Email:%s, Phone type:%s, Phone:%s, receive SMS notification:%s", fname, lname, email, confEmail, phonetype, phoneNumber, sms));
        this.setFName(fname)
                .setLName(lname)
                .setPhoneType(phonetype)
                .setPhoneNumber(phoneNumber);
        if (phonetype.equalsIgnoreCase("mobile"))
            setSMSNotifications(sms);
        return this;
    }

    public WasteCollectionPage setCsoEmail(String email) {
        Logg.logger.info("Email:" + email);
        waitAndSendKeys(driver, csoEmail, email);
        return this;
    }

    public WasteCollectionPage setPolicy(String option) {
        Logg.logger.info("Set read &accept privacy policy to:" + option);

        if (option.equalsIgnoreCase("yes")) {
            if (!privacyCheck.isSelected())
                waitAndClick(driver, privacyCheckLabel);
        }
        if (option.equalsIgnoreCase("no"))
            if (privacyCheck.isSelected())
                waitAndClick(driver, privacyCheckLabel);
        return this;
    }

    public WasteCollectionPage setWasteLabel(String option) {
        Logg.logger.info("Set understand what hard waste is collected:" + option);
        waitAndClick(driver, wasteLabel);
        return this;
    }

    public WasteCollectionPage setUrgentRoute() {
        Logg.logger.info("");
        waitAndClick(driver, urgentRouteLabel);
        return this;
    }

    public WasteCollectionPage submitForm() {
        Logg.logger.info("");
        waitAndClick(driver, finalSubmit);
//        super.submitForm(formNo);
        return this;
    }

    public WasteCollectionPage setReason(String reason, String otherReason) {
        Logg.logger.info("Extra hard waste collection reason:" + reason);
        selectDropdown_VisibleText(driver, reasonDropdown, reason);
        if (reason.equalsIgnoreCase("Other"))
            setOtherReason(otherReason);
        return this;
    }

    public WasteCollectionPage setOtherReason(String reason) {
        Logg.logger.info("Other reason:" + reason);
        waitAndSendKeys(driver, otherReason, reason);
        return this;
    }

    public WasteCollectionPage setGreenWasteLabel() throws Exception {

        String greenLabel = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[8]/div/div[1]")).getText();
        if (greenLabel.equalsIgnoreCase("Unfortunately, bundled green waste can only be booked during March, April, September and October for the months of April and October. If you need to dispose of your green waste, please take it to the Boroondara Recycling and Waste Centre.")) {
            extent_Pass(driver, "Case Name Verification Passed", greenLabel.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", greenLabel.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE GREEN WASTE LABEL LEVEL ");
        }
        return this;
    }

    public WasteCollectionPage setChristmasWasteLabel() throws Exception {

//        String greenLabel = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[16]/div[2]/div[1]")).getText();
        String greenLabel = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[8]/div/div[1]")).getText();
        if (greenLabel.equalsIgnoreCase("Unfortunately, Christmas tree collections can only be booked during December and January for the month of January. If you need to dispose of your Christmas tree, please take it to the  please take it to the Boroondara Recycling and Waste Centre.")) {
            extent_Pass(driver, "Case Name Verification Passed", greenLabel.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", greenLabel.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CHRISTMAS WASTE LABEL LEVEL ");
        }
        return this;
    }

    @Test(dataProvider = "ROP_DataProvider", description = "Verify that the review page values can be compared")
    public WasteCollectionPage getReviewPage(String expReviewPage) throws Exception {

        //Get the Hidden Values
        //GUID
        GUID = driver.findElement(By.id("input_193_53")).getAttribute("value");
        System.out.println("***************The GUID from FORM SUBMISSION is\n*************** " + GUID + "");

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_193_23\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
//        if (reviewPage.equalsIgnoreCase(expReviewPage)) {
//            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
//        } else {
//            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
//            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");
//
//        }


        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_193_115\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }

        return this;

    }

    @Test(dataProvider = "ROP_DataProvider", description = "Verify that the CRM values are verified")
    public WasteCollectionPage captureCRMIndividual(String expCaseType, String expSubType, String expCustomerName, String expLocation,
                                                    String expType,String expBusinessNo, String expMobileNo, String expAlternateNo,
                                                    String expFName, String expLName, String expEmail, String expCaseCount) throws Exception {

        Thread.sleep(15000);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.addArguments("window-size=1920,1080");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
        System.out.println(driver.manage().window().getSize());
        driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
        System.out.println(driver.manage().window().getSize());

        Thread.sleep(20000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);


        //Switch to the frame that contains the grid
        driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
        Thread.sleep(30000);


        Actions action = new Actions(driver);
        String identifierText = driver.findElement(By.xpath("//table[@id='gridBodyTable']/tbody/tr['" + GUID + "']/td[23]/nobr")).getText();
        System.out.println("The GUID text in CRM is " + identifierText + "");
        WebElement identifier = driver.findElement(By.xpath("//table[@id='gridBodyTable']/tbody/tr['" + GUID + "']/td[23]/nobr"));
        Logg.logger.info("Is the GUID correct:" + identifier);


        action.doubleClick(identifier).perform();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        if (caseID.isDisplayed()) {
            extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
            throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
        }
        caseID.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseName.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }
        Thread.sleep(5000);
        //Select the Sub-case Type
        String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
        System.out.println("The SubcaseType is " + subCaseName + "");
        if (subCaseName.equalsIgnoreCase(expSubType)) {
            extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
        }
        //Select the origin
        String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
        Logg.logger.info("Origin name:" + originName);
        if (originName.equalsIgnoreCase("Web")) {
            extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
        } else {
            extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
        }
        //Select the customer name
        String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
        Logg.logger.info("Customer name:" + customerName);

        if (customerName.equalsIgnoreCase(expCustomerName)) {
            extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
        } else {
            extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
        }

        //Verify the Location Field
        String locationField = driver.findElement(By.xpath("//*[@id=\"PropertyQuickView_PropertyQuickView_account_address1_composite\"]/div[1]/span")).getText();
        if (locationField.contains(expLocation)) {
            extent_Pass(driver, "Location Verification Passed", locationField.toString(), "", "");
        } else {
            extent_Warn(driver, "Location verification Failed", locationField.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ADDRESS LEVEL ");
        }




        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to the Case ESCALATION TAB
        WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
        Thread.sleep(10000);
        caseTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String firstResponse = driver.findElement(By.xpath("//*[@id=\"firstresponsesent\"]/div[1]/span")).getText();
        Logg.logger.info("First Response is:" + firstResponse);

        //Check if the Timer is not ticking and First response is Succeeded
        if (firstResponse.equalsIgnoreCase("No")) {
            extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
        }




        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to Notes
        WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
        Thread.sleep(10000);
        notesTabSelected.click();
//        Thread.sleep(20000);

        //Navigate to Activities
        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
        actTabSelected.click();
        Thread.sleep(10000);


        String emailTrigger = driver.findElement(By.xpath("(//img[@alt='Email'])[3]")).getAttribute("src");
        Thread.sleep(20000);
        Logg.logger.info("Email:" + emailTrigger);




//        //Select the customer name
//        Thread.sleep(20000);
//        WebElement custName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[1]/div/table/tbody/tr[9]/td[2]/div[1]/div[1]/span[1]"));
//        custName.click();
//        Thread.sleep(10000);



        //Select the customer name
        Thread.sleep(20000);
        WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
        custName.click();
        Thread.sleep(10000);

        //Customer Matching
        //Contact Type
        String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
        if (contactType.equalsIgnoreCase(expType)) {
            extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
            throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
        }
//        //Get the Business Phone Number
//        String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//        if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//            throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//        }
//        //Get the Mobile Number
//        String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//        if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//        }
//        //Get the Landline Number
//        String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//        if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//            throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//        }

        //Get the First Name
        String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
        if (firstName.equalsIgnoreCase(expFName)) {
            extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
            throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
        }

        //Get the Last Name OR Business Name
        String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
        if (lastName.equalsIgnoreCase(expLName)) {
            extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLName, lastName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLName, lastName);
            throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
        }
        //Get the Email
        String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
        if (emailName.equalsIgnoreCase(expEmail)) {
            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
        }


        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
        Thread.sleep(10000);


        //Navigate to the Customer Case Records Tab
        WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
        Thread.sleep(10000);
        customerCaseRecords.click();

        Thread.sleep(5000);

        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
        Logg.logger.info("Total Number of Cases:" + caseRecords);


        Thread.sleep(10000);
        navigateBack(driver);
        Thread.sleep(10000);

        driver.quit();

        return this;
    }

    @DataProvider

    public Object[][] Waste_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"Waste");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"Waste",30);

        return testObjArray;
    }
}
