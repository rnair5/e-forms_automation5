package pageObjects.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class ManualPage extends BaseFormsClass {

    public ManualPage(WebDriver driver) {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver, this);
    }



    //New Case Button
    @FindBy(xpath = "//*[@id=\"incident|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.incident.NewRecord\"]/span/a/img")
    private WebElement newCase;
    //Enter the SubType
    @FindBy(xpath = "//*[@id=\"header_process_ecl_casesubtype_ledit\"]")
    private WebElement subType;
    //SubType DropDown
    @FindBy(xpath= "//*[@id=\"item1\"]/a[2]/span/nobr/span")
    private WebElement subTypeDropDown;
    //Origin - Enter
    @FindBy(xpath = "input_241_16")
    private WebElement origin;



    @Test(dataProvider = "MAN_DataProvider", description = "Verify that the CRM values are verified")
    public ManualPage captureCRM(String expSubCase, String expOrigin, String expCustomerName, String expLocation, String instructions,
                                 String expCase, String expAddress, String expSlaTimer, String expFirstResponse, String expSlaStatus) throws Exception {

        Thread.sleep(15000);
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://RNair:Lavanya@445015!@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

        driver.manage().window().maximize();
        Thread.sleep(5000);

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        Thread.sleep(2000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);
        //Create a New case
        WebElement createNew = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/ul/li[1]/span/a/img"));
        createNew.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"navTabButtonUserInfoLinkId\"]/span[2]/img"));
        caseID.click();

        //Enter the Sub-Case Type
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Thread.sleep(10000);
        //Enter the Sub-Case Type
        WebElement enterSubType = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype_ledit\"]"));
        enterSubType.sendKeys(expSubCase);

        WebElement originSearch = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode\"]/div[1]/span"));
        originSearch.click();
        WebElement originEmail = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode_i\"]/option[4]"));
        originEmail.click();
        Thread.sleep(10000);

        //Customer Details
        WebElement customerEnter = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]"));
        customerEnter.click();
        WebElement customerEnter1 = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new_ledit\"]"));
        customerEnter1.sendKeys(expCustomerName);


        //Service Location Details
        WebElement addressEnter = driver.findElement(By.xpath("//*[@id=\"cob_relatedproperty\"]/div[1]"));
        addressEnter.click();
        WebElement addressEnter1 = driver.findElement(By.xpath("//*[@id=\"cob_relatedproperty_ledit\"]"));
        addressEnter1.sendKeys(expLocation);

        //Instructions
        WebElement instructionsClick = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]"));
        instructionsClick.click();
        WebElement instructionsEnter = driver.findElement(By.xpath("//*[@id=\"description_i\"]"));
        instructionsEnter.sendKeys(instructions);
        //Save
        driver.switchTo().defaultContent();
        WebElement save = driver.findElement(By.xpath("//*[@id=\"incident|NoRelationship|Form|Mscrm.Form.incident.Save\"]/span/a/span"));
        save.click();
        Thread.sleep(5000);
        save.click();
        //Wait
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        Thread.sleep(10000);
        // Get the Case Details
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID1 = driver.findElement(By.xpath("//*[@id=\"navTabButtonUserInfoLinkId\"]/span[2]/img"));
        caseID1.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseName.equalsIgnoreCase(expCase)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }
        Thread.sleep(5000);
        //Select the Sub-case Type
        String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
        System.out.println("The SubcaseType is " + subCaseName + "");
        if (subCaseName.equalsIgnoreCase(expSubCase)) {
            extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
        }

        //Select the customer name
        String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
        Logg.logger.info("Customer name:" + customerName);

        if (customerName.equalsIgnoreCase(expCustomerName)) {
            extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
        } else {
            extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
        }

        //Verify the Location Field
        String locationField = driver.findElement(By.xpath("//*[@id=\"PropertyQuickView_PropertyQuickView_account_address1_composite\"]/div[1]/span")).getText();
        if (locationField.contains(expAddress)) {
            extent_Pass(driver, "Location Verification Passed", locationField.toString(), "", "");
        } else {
            extent_Warn(driver, "Location verification Failed", locationField.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ADDRESS LEVEL ");
        }

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to the Case ESCALATION TAB
        WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
        Thread.sleep(10000);
        caseTabSelected.click();

        driver.switchTo().parentFrame();
        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to Notes
        WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
        Thread.sleep(10000);
        notesTabSelected.click();
//        Thread.sleep(20000);

        //Navigate to Activities
        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
        actTabSelected.click();
        Thread.sleep(10000);

        if(driver.findElements(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div")).size() < 1){
            extent_Pass(driver, "Email is not Triggered", caseID1.toString(), "", "");
        } else {
            extent_Warn(driver, "Email is triggered", caseID1.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE COMMS LEVEL ");
        }
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseIDRefresh.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);

        // FIRST RESPONSE
        Thread.sleep(20000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        //Navigate to the Case ESCALATION TAB
        WebElement caseTabSelected1 = driver.findElement(By.id("tab_casestatus"));
        Thread.sleep(10000);
        caseTabSelected1.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String firstResponse = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
        Logg.logger.info("First Response is:" + firstResponse);

        //Check if the Timer is not ticking and First response is Succeeded
        if (!firstResponse.equalsIgnoreCase("Succeeded") && (!firstResponse.equalsIgnoreCase("No") && (!firstResponse.equalsIgnoreCase("Yes")))) {
            extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
        }

        // First Response Sent
        String firstResponseSent = driver.findElement(By.xpath("//*[@id=\"firstresponsesent\"]/div[1]/span")).getText();
        Logg.logger.info("First Response is:" + firstResponseSent);

        //Check if the Timer is not ticking and First response is Succeeded
        if (firstResponseSent.equalsIgnoreCase("No")) {
            extent_Pass(driver, "FIRST RESPONSE SENT- Verification Passed", firstResponseSent.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE SENT- verification Failed", firstResponseSent.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE SENT FAILED ");
        }

        // First Response SLA
        String firstResponseSLA = driver.findElement(By.xpath("//*[@id=\"firstresponseslastatus\"]/div[1]/span")).getText();
        Logg.logger.info("First Response is:" + firstResponseSLA);

        //Check if the Timer is not ticking and First response is Succeeded
        if (firstResponseSLA.equalsIgnoreCase("In Progress")) {
            extent_Pass(driver, "FIRST RESPONSE SENT- Verification Passed", firstResponseSLA.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE SENT- verification Failed", firstResponseSLA.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE SENT FAILED ");
        }

        Thread.sleep(10000);
        driver.quit();
        return this;

    }

    @Test(dataProvider = "MAN_DataProvider", description = "Verify that the CRM values are verified")
    public ManualPage captureCRMAnimal(String expSubCase, String expOrigin, String expCustomerName, String expLocation, String instructions,
                                 String expCase, String expSlaTimer, String expFirstResponse, String expSlaStatus) throws Exception {

        Thread.sleep(15000);
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://RNair:Lavanya@445015!@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

        driver.manage().window().maximize();
        Thread.sleep(5000);

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        Thread.sleep(2000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);
        //Create a New case
        WebElement createNew = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/ul/li[1]/span/a/img"));
        createNew.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"navTabButtonUserInfoLinkId\"]/span[2]/img"));
        caseID.click();

        //Enter the Sub-Case Type
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        Thread.sleep(10000);
        //Enter the Sub-Case Type
        WebElement enterSubType = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype_ledit\"]"));
        enterSubType.sendKeys(expSubCase);
//        Thread.sleep(9000);

        //Click the search Button
//        WebElement subTypeSearch = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype_i\"]"));
//        subTypeSearch.click();
//        WebElement selectAnimal = driver.findElement(By.xpath("//*[@id=\"item1\"]/a[2]/span/nobr/span"));
//        selectAnimal.click();
        WebElement originSearch = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode\"]/div[1]/span"));
        originSearch.click();
        WebElement originEmail = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode_i\"]/option[4]"));
        originEmail.click();
        Thread.sleep(10000);
        //Customer Details
        WebElement customerEnter = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]"));
        customerEnter.click();
        WebElement customerEnter1 = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new_ledit\"]"));
        customerEnter1.sendKeys(expCustomerName);
//        WebElement customerSearch = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new_i\"]"));
//        customerSearch.click();
//        WebElement customerSelect = driver.findElement(By.xpath("//li[@id='item9']/a[2]"));
//        customerSelect.click();
        //Service Location Details
        WebElement addressEnter = driver.findElement(By.xpath("//*[@id=\"cob_relatedproperty\"]/div[1]"));
        addressEnter.click();
        WebElement addressEnter1 = driver.findElement(By.xpath("//*[@id=\"cob_relatedproperty_ledit\"]"));
        addressEnter1.sendKeys(expLocation);
//        WebElement addressSearch = driver.findElement(By.xpath("//*[@id=\"cob_relatedproperty_i\"]"));
//        addressSearch.click();
//        WebElement addressSelect = driver.findElement(By.xpath("//li[@id='item9']/a[2]"));
//        addressSelect.click();
//        Date Details - Start
        WebElement dateEnter = driver.findElement(By.xpath("//*[@id=\"new_dateoffirstactiontaken\"]/div[1]"));
        dateEnter.click();
        WebElement dateEnter1 = driver.findElement(By.xpath("//*[@id=\"DateInput\"]"));
        dateEnter1.sendKeys("15/11/2020");
        //Date Details - End
        WebElement dateEnterEnd = driver.findElement(By.xpath("//*[@id=\"cob_requestreceiptdatetime\"]/div[1]"));
        dateEnterEnd.click();
        WebElement dateEnterEnd1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div[1]/div[1]/div[2]/div[1]/div/table/tbody/tr[23]/td[2]/div/div[2]/table/tr/td[1]/input"));
        dateEnterEnd1.sendKeys("15/12/2020");
        //Instructions
        WebElement instructionsClick = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]"));
        instructionsClick.click();
        WebElement instructionsEnter = driver.findElement(By.xpath("//*[@id=\"description_i\"]"));
        instructionsEnter.sendKeys(instructions);
//        String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
//        Logg.logger.info("SubCase Type :" + subCaseName);
//        if (subCaseName.equalsIgnoreCase(expSubCase)) {
//            extent_Pass(driver, "ORIGIN Verification Passed", subCaseName.toString(), "", "");
//        } else {
//            extent_Warn(driver, "ORIGIN Verification Failed", subCaseName.toString(), "", "");
//            throw new Exception("TEST CASE FAILED AT THE Subcase LEVEL ");
//        }
        //Save
        driver.switchTo().defaultContent();
        WebElement save = driver.findElement(By.xpath("//*[@id=\"incident|NoRelationship|Form|Mscrm.Form.incident.Save\"]/span/a/span"));
        save.click();
        Thread.sleep(5000);
        save.click();
        //Next Stage
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
//        driver.switchTo().frame(0);
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        Thread.sleep(10000);
//        WebElement nextStage = driver.findElement(By.xpath("//*[@id=\"stageNavigateActionContainer\"]/div"));
//        nextStage.click();
//        nextStage.click();
//        WebElement nextStageCreate = driver.findElement(By.xpath("//*[@id=\"bpfNavigateCreate\"]/div[1]"));
//        nextStageCreate.click();
//        Thread.sleep(5000);
//        nextStageCreate.click();
//        //Registration Details
//
//        WebElement microChip = driver.findElement(By.xpath("//*[@id=\"header_process_cob_microchipno\"]/div[1]/span"));
//        microChip.click();
//        WebElement mcNo = driver.findElement(By.xpath("//*[@id=\"header_process_cob_microchipno_i\"]"));
//        mcNo.sendKeys("12345");
        Thread.sleep(10000);

        driver.quit();
        return this;

    }

        @DataProvider

        public Object[][] MAN_DataProvider () throws Exception {
            // Setting up the Test Data Excel file

            ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "MAN");

            // Fetching the Test Case row number from the Test Data Sheet

            Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "MAN", 13);

            return testObjArray;
        }


    }





