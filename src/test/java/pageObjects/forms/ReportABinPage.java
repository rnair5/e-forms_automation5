package pageObjects.forms;

import libraries.ExcelUtils;
import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class ReportABinPage extends BaseFormsClass {
    String formNo="227";
    static String GUID ="";
    String propertyType;
    int specifiedNo=0;
    int visitorNo=0;
    public ReportABinPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }


    //Submit Form
    @FindBy(id="submitbtn")
    private WebElement finalSubmit;

    String form_ID="#gform_page_"+formNo;
    @FindBy(id = "input_227_52")
    private WebElement address;

    //Reason for Contacting Us
    @FindBy(id = "label_227_11_0")
    private WebElement notEmptiedBin;
    @FindBy(id = "label_227_11_1")
    private WebElement lostBin;
    @FindBy(id = "label_227_11_2")
    private WebElement damagedBin;
    @FindBy(id = "label_227_11_3")
    private WebElement fogoBin;

    //Bin Type
    @FindBy(id = "label_227_19_1")
    private WebElement generalBin;
    @FindBy(id = "label_227_19_2")
    private WebElement recycleBin;
    @FindBy(id = "label_227_19_3")
    private WebElement greenBin;
    @FindBy(id = "label_227_84_2")
    private WebElement swapFogoBin;

    //Bin Type - Second Type
    @FindBy(id = "label_227_39_1")
    private WebElement generalBin1;
    @FindBy(id = "label_227_39_2")
    private WebElement recycleBin1;
    @FindBy(id = "label_227_39_3")
    private WebElement greenBin1;

    //Missed Bin ----> Sub-Options
    @FindBy(id = "label_227_59_0")
    private WebElement missedBin;
    @FindBy(id = "label_227_59_1")
    private WebElement lateBin;
    @FindBy(id = "label_227_59_2")
    private WebElement partialBin;
    @FindBy(id = "label_227_59_3")
    private WebElement rejectBin;

    //Date Picker
    @FindBy(xpath = " //*[@id=\"field_227_23\"]/div[2]/img")
    private WebElement datePickUp;

    //Where do your Bins usually get collected from?
    @FindBy(id = "label_227_36_0")
    private WebElement natureStrip;
    @FindBy(id = "label_227_36_1")
    private WebElement rearProperty;
    @FindBy(id = "label_227_36_2")
    private WebElement cornerBlock;

    //Damaged Bin- Sub-Options
    //Set-1
    @FindBy(id = "label_227_46_1")
    private WebElement bodyBin1;
    @FindBy(id = "label_227_46_2")
    private WebElement lidBin1;
    @FindBy(id = "label_227_46_3")
    private WebElement wheelBin1;

    //Set-2
    @FindBy(id = "label_227_47_1")
    private WebElement bodyBin2;
    @FindBy(id = "label_227_47_2")
    private WebElement lidBin2;
    @FindBy(id = "label_227_47_3")
    private WebElement wheelBin2;

    //Set-3
    @FindBy(id = "label_227_48_1")
    private WebElement bodyBin3;
    @FindBy(id = "label_227_48_2")
    private WebElement lidBin3;
    @FindBy(id = "label_227_48_3")
    private WebElement wheelBin3;

    //Description of Location
    @FindBy(id = "input_227_37")
    private WebElement locationDescription;

    //Contact Page
    @FindBy(id="input_227_74")
    private  WebElement fName;
    @FindBy(id="input_227_75")
    private  WebElement lName;
    @FindBy(id="input_227_70")
    private  WebElement phoneType;
    @FindBy(id="input_227_71")
    private  WebElement phoneNo;
    @FindBy(id="input_227_5")
    private  WebElement email;
    @FindBy(id="choice_227_72_1")
    private WebElement smsNotification;
    @FindBy(id="label_227_72_1")
    private WebElement smsNotificationLabel;

    @FindBy(id = "gform_submit_button_227")
    private WebElement submitButton;



//    public ReportABinPage clickNext(String pageNo)
//    {
//        Logg.logger.info("PageNo:"+pageNo);
//        super.clickNext(formNo,pageNo);
//        return this;
//    }

    public ReportABinPage clickNext(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickNext(formNo,pageNo);
        return this;
    }
    public ReportABinPage clickPrevious(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickPrevious(formNo,pageNo);
        return this;
    }
    public ReportABinPage setAddress(String address)
    {
        Logg.logger.info("address:"+address);
        selectFirstItemFromLookup(driver,this.address,address);
        return this;
    }
    public ReportABinPage setReasonType(String reasonType)
    {
        Logg.logger.info("Reason for Contacting:"+reasonType);
        if (reasonType.equalsIgnoreCase("My bin wasn't emptied"))
            waitAndClick(driver,notEmptiedBin);
        if(reasonType.equalsIgnoreCase("My bin is lost"))
            waitAndClick(driver,lostBin);
        if(reasonType.equalsIgnoreCase("My bin is damaged"))
            waitAndClick(driver,damagedBin);
        if(reasonType.equalsIgnoreCase("I need FOGO equipment"))
            waitAndClick(driver,fogoBin);
        return this;
    }
    public ReportABinPage setBinType(String type)
    {
        Logg.logger.info("Bin Type:"+type);
        if (type.equalsIgnoreCase("Household waste (green lid)"))
            waitAndClick(driver,generalBin);
        if(type.equalsIgnoreCase("Recycling (yellow lid)"))
            waitAndClick(driver,recycleBin);
        if (type.equalsIgnoreCase("Green/Food Organics and Garden Organics (orange or lime lid)"))
            waitAndClick(driver,greenBin);
        return this;
    }

    public ReportABinPage setBinType1(String type)
    {
        Logg.logger.info("Bin Type:"+type);
        if (type.equalsIgnoreCase("Household waste (green lid)"))
            waitAndClick(driver,generalBin1);
        if(type.equalsIgnoreCase("Recycling (yellow lid)"))
            waitAndClick(driver,recycleBin1);
        if (type.equalsIgnoreCase("Green/Food Organics and Garden Organics (orange or lime lid)"))
            waitAndClick(driver,greenBin1);
        if (type.equalsIgnoreCase("FOGO starter pack (includes kitchen caddy, caddy liners and infobook)"))
            waitAndClick(driver,swapFogoBin);
        return this;
    }

    public ReportABinPage setProblemType(String type)
    {
        Logg.logger.info("Why was the Bin Missed:"+type);
        if (type.equalsIgnoreCase("The truck missed my bin"))
            waitAndClick(driver,missedBin);
        if(type.equalsIgnoreCase("I put my bins out late"))
            waitAndClick(driver,lateBin);
        if (type.equalsIgnoreCase("My bin was only partially emptied"))
            waitAndClick(driver,partialBin);
        if (type.equalsIgnoreCase("My waste was rejected"))
            waitAndClick(driver,rejectBin);
        return this;
    }


    public ReportABinPage setDamagedPart(String type)
    {
        Logg.logger.info("Which part was Damaged:"+type);
        if (type.equalsIgnoreCase("Body of the bin"))
            waitAndClick(driver,bodyBin3);
        if(type.equalsIgnoreCase("Lid and/or pins"))
            waitAndClick(driver,lidBin3);
        if (type.equalsIgnoreCase("Wheels"))
            waitAndClick(driver,wheelBin3);
        return this;
    }

    public ReportABinPage selectStartDate(String startDate) {
        Logg.logger.info("Start Date:" + startDate);
        WebElement dateWidget = driver.findElement(By.xpath("//*[@id=\"field_227_17\"]/div[2]/img"));
        dateWidget.click();
        WebElement date = driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[3]/a"));
        date.click();
//        WebElement dateWidget = driver.findElement(By.id("input_227_24"));
//        dateWidget.click();
//        dateWidget.sendKeys(startDate);
        return this;
    }


    public ReportABinPage enterDate(String startDate) {
        Logg.logger.info("Start Date:" + startDate);
        WebElement dateWidget = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[3]/div[1]/ul/li[9]/div[2]/input"));
        waitAndSendKeys(driver, dateWidget, startDate);
//        WebElement dateWidget = driver.findElement(By.id("input_227_24"));
//        dateWidget.click();
//        dateWidget.sendKeys(startDate);
        return this;
    }

    public ReportABinPage setBinCollection(String type)
    {
        Logg.logger.info("Where do your bins usually get collected from?:"+type);
        if (type.equalsIgnoreCase("On the naturestrip in front of my house."))
            waitAndClick(driver,natureStrip);
        if(type.equalsIgnoreCase("At the back of my property."))
            waitAndClick(driver,rearProperty);
        if (type.equalsIgnoreCase("Around the corner (I live on a corner block)."))
            waitAndClick(driver,cornerBlock);
        return this;
    }

    public ReportABinPage setLocationInformation(String information) {
        Logg.logger.info("More Information:" + information);
        waitAndSendKeys(driver, locationDescription, information);
        return this;
    }


    public ReportABinPage setFName(String name)
    {
        Logg.logger.info("First Name:"+name);
        waitAndSendKeys(driver,fName,name);
        return this;
    }
    public ReportABinPage setLName(String name)
    {
        Logg.logger.info("Last Name:"+name);
        waitAndSendKeys(driver,lName,name);
        return this;
    }
    public ReportABinPage setEmail(String email)
    {
        Logg.logger.info("Email:"+email);
        waitAndSendKeys(driver,this.email,email);
        return this;
    }

    public ReportABinPage setPhoneType(String phoneType)
    {
        Logg.logger.info(" phone type:"+phoneType);
        selectDropdown_VisibleText(driver,this.phoneType,phoneType);
        return this;
    }
    public ReportABinPage setPhoneNumber(String phoneNumber)
    {
        Logg.logger.info(" Phone number:"+phoneNumber);
        waitAndSendKeys(driver,phoneNo,phoneNumber);
        return this;
    }
    public ReportABinPage setSMSNotifications(String option)
    {
        Logg.logger.info("Set SMS Notifications to:"+option);
        if(option.equalsIgnoreCase("yes"))
            if(!smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        if(option.equalsIgnoreCase("no"))
            if(smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        return this;
    }
    public ReportABinPage setPersonalDetails(String fname,String lname,  String phonetype, String phoneNumber,String sms) throws ParseException {
        Logg.logger.info(String.format(" details:  Fname:%s,  Lname:%s,  Email:%s, Phone type:%s, Phone:%s, receive SMS notification:%s", fname,lname,email,phonetype,phoneNumber,sms));
        this.setFName(fname)
                .setLName(lname)
                .setPhoneType(phonetype)
                .setPhoneNumber(phoneNumber);
        if(phonetype.equalsIgnoreCase("mobile"))
            setSMSNotifications(sms);
        return this;
    }

    public ReportABinPage submitForm() {
        Logg.logger.info("");
        waitAndClick(driver,finalSubmit);
//        super.submitForm(formNo);
        return this;
    }

    public ReportABinPage getReviewPage(String expReviewPage) throws Exception {

        //Get the Hidden Values
        //GUID
        GUID = driver.findElement(By.id("input_227_64")).getAttribute("value");
        System.out.println("The GUID is " + GUID + "");

        Thread.sleep(3000);


        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_227_29\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
//        if (reviewPage.equalsIgnoreCase(expReviewPage)) {
//            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
//        } else {
//            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
//            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");
//
//        }


        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_227_81\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }
        return this;

    }


    @Test(dataProvider = "RBP_DataProvider", description = "Verify that the CRM values are verified")
    public ReportABinPage captureCRM(String expCaseType, String expSubType, String expCustomerName, String expLocation,
                                    String instructions, String expType,String expBusinessNo, String expMobileNo, String expAlternateNo,
                                    String expFName, String expLName, String expEmail) throws Exception {

        Thread.sleep(15000);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.addArguments("window-size=1920,1080");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
        System.out.println(driver.manage().window().getSize());
        driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
        System.out.println(driver.manage().window().getSize());

        Thread.sleep(20000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);


        //Switch to the frame that contains the grid
        driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
        Thread.sleep(10000);


        Actions action = new Actions(driver);
        String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
        System.out.println("The GUID text in CRM is " + identifierText + "");
        WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
        Logg.logger.info("Is the GUID correct:" + identifier);



        action.doubleClick(identifier).perform();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        if (caseID.isDisplayed()) {
            extent_Pass(driver, "Case Details are displayed", caseID.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Details are displayed", caseID.toString(), "", "");
            throw new Exception("CASE DETAILS PAGE IS NOT DISPLAYED ");
        }
        caseID.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseName.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }
        Thread.sleep(5000);
        //Select the Sub-case Type
        String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
        System.out.println("The SubcaseType is " + subCaseName + "");
        if (subCaseName.equalsIgnoreCase(expSubType)) {
            extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
        }
        //Select the origin
        String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
        Logg.logger.info("Origin name:" + originName);
        if (originName.equalsIgnoreCase("Web")) {
            extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
        } else {
            extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
        }
        //Select the customer name
        String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
        Logg.logger.info("Customer name:" + customerName);

        if (customerName.equalsIgnoreCase(expCustomerName)) {
            extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
        } else {
            extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
        }

        //Verify the Location Field
        String locationField = driver.findElement(By.xpath("//*[@id=\"PropertyQuickView_PropertyQuickView_account_address1_composite\"]/div[1]/span")).getText();
        if (locationField.contains(expLocation)) {
            extent_Pass(driver, "IN Verification Passed", locationField.toString(), locationField, "");
        } else {
            extent_Warn(driver, "IN verification Failed", locationField.toString(), locationField, "");
            throw new Exception("TEST CASE FAILED AT THE Location LEVEL ");
        }

        //Verify the Instructions Field
        String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();

        if (instructionsField.equalsIgnoreCase(instructions)) {
            extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
        } else {
            extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
        }

        Thread.sleep(10000);



        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to the Case ESCALATION TAB
        WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
        Thread.sleep(10000);
        caseTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String firstResponse = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[6]/table/tbody/tr[3]/td[2]/div/div[1]/span")).getText();
        Logg.logger.info("First Response is:" + firstResponse);

        //Check if the Timer is not ticking and First response is Succeeded
        if (firstResponse.equalsIgnoreCase("No")) {
            extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
        }


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to Notes
        WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
        Thread.sleep(10000);
        notesTabSelected.click();
//        Thread.sleep(20000);

        //Navigate to Activities
        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
        actTabSelected.click();
        Thread.sleep(10000);


        String emailTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div")).getAttribute("objecttypecode");
        Thread.sleep(20000);
        Logg.logger.info("Email:" + emailTrigger);

        Thread.sleep(20000);


        //Check if SMS is triggered
        Thread.sleep(20000);
        String smsTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[3]/div[1]/div/div")).getAttribute("objecttypecode");
        Logg.logger.info("SMS:" + smsTrigger);


        //DOCUMENT DETAILS
        //Navigate to the Document Tabs
        Thread.sleep(20000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        WebElement documentTabSelected = driver.findElement(By.id("tab_waste"));
        Thread.sleep(10000);
        if (documentTabSelected.isDisplayed()) {
            extent_Pass(driver, "Waste Tab Verification Passed", documentTabSelected.toString(), "", "");
        } else {
            extent_Warn(driver, "Waste Tab Verification Failed", documentTabSelected.toString(), "", "");
            throw new Exception("Waste TAB IS NOT DISPLAYED ");
        }
        documentTabSelected.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Thread.sleep(10000);
        driver.switchTo().parentFrame();
        Thread.sleep(5000);


        //Get the Name of the First Document - First Document
        String oneDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[2]/td[2]/div[1]/div[1]/span[1]")).getText();
        if (oneDocName.equalsIgnoreCase("Missed Bin Request")) {
            extent_Pass(driver, "NAME FORMAT - DOC1- Verification Passed", oneDocName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - DOC1 - verification Failed", oneDocName.toString(), "", "");
            throw new Exception("DOC1 NAMING CONVENTION FAILED ");
        }
//
        //Get the Name of the Second Document - PDF
        String certName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[3]/td[1]/div/div/div[2]/div/div/div/div/div/div/div/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
        if (certName.equalsIgnoreCase("Garbage")) {
            extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", certName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", certName.toString(), "", "");
            throw new Exception("PDF NAMING CONVENTION FAILED ");
        }



        //Get the Name of the Fourth Document - Notification
        String notificationName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[3]/td[1]/div/div/div[2]/div/div/div/div/div/div/div/table/tbody/tr[5]/td[2]/div/div[1]/span[1]")).getText();
        if (notificationName.contains("Bin Not Listed")) {
            extent_Pass(driver, "NAME FORMAT - NOTIFICATION - Verification Passed", notificationName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - NOTIFICATION - verification Failed", notificationName.toString(), "", "");
            throw new Exception("NOTIFICATION NAMING CONVENTION FAILED ");
        }



//
//        //Get the Total Number of records
//        String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
//        if (totalDocuments.equalsIgnoreCase(expCount)) {
//            extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
//        } else {
//            extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
//            throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
//        }





        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }


        //Select the customer name
        Thread.sleep(20000);
        WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
        custName.click();
        Thread.sleep(10000);

        //Customer Matching
        //Contact Type
        String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
        if (contactType.equalsIgnoreCase(expType)) {
            extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
            throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
        }
//        //Get the Business Phone Number
//        String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//        if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//            throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//        }
        //Get the Mobile Number
//        String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//        if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//        }
//        //Get the Landline Number
//        String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//        if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//            throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//        }

//        //Get the First Name
//        String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
//        if (firstName.equalsIgnoreCase(expFName)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
//            throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
//        }

        //Get the Last Name OR Business Name
        String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
        if (lastName.equalsIgnoreCase(expLName)) {
            extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLName, lastName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLName, lastName);
            throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
        }
        //Get the Email
        String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
        if (emailName.equalsIgnoreCase(expEmail)) {
            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
        }


        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
        Thread.sleep(10000);


        //Navigate to the Customer Case Records Tab
        WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
        Thread.sleep(10000);
        customerCaseRecords.click();

        Thread.sleep(5000);

        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
        Logg.logger.info("Total Number of Cases:" + caseRecords);
//        if (caseRecords) {
//            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
//            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
//        }


        Thread.sleep(10000);
        navigateBack(driver);
        Thread.sleep(10000);

        driver.quit();

        return this;
    }




    @DataProvider

    public Object[][] RBP_DataProvider () throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "RBP");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "RBP", 29);

        return testObjArray;
    }


}
