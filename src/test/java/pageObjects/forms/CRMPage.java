package pageObjects.forms;

import libraries.Logg;
import libraries.Utilities;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import static libraries.Utilities.*;

public class CRMPage extends BaseFormsClass {
    String formNo = "203";

    public CRMPage(WebDriver driver) {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver, this);
    }

    String form_ID = "#gform_page_" + formNo;


//    @FindBy(className= "homeButtonImage")
//    private WebElement sitemapHomeTab;


//    @FindBy(css= "contentIFrame0")
//    private WebElement frameTab;


    @FindBy(className= "homeButtonImage")
    private WebElement sitemapHomeTab;

    @FindBy(id="dashboardSelectorContainer")
    private WebElement dashTab;


    @FindBy(id = "TabCS")
    private WebElement serviceAreaElement;

    @FindBy(xpath = "//a[@id='rightNavLink']")
    private WebElement serviceRightLink;


    @FindBy(xpath = "//a[@id='nav_cases']")
    private WebElement serviceCaseSubArea;




    public CRMPage clickHome() throws InterruptedException {
//        Logg.logger.info("Current Button:"+Home);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        waitAndClick(driver, sitemapHomeTab);
        waitAndClick(driver, dashTab);
        Thread.sleep(2000);

        return this;



    }
}