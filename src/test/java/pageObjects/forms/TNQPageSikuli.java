//package pageObjects.forms;
//
//import Tests.forms.PDFBoxReadFromFile;
//import Tests.forms.PDFReader;
//import com.asprise.ocr.Ocr;
//import libraries.ExcelUtils;
//import net.sourceforge.tess4j.Tesseract;
//import net.sourceforge.tess4j.TesseractException;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.PageFactory;
//import org.sikuli.basics.Debug;
////import org.sikuli.script.*;
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//import javax.imageio.ImageIO;
//import javax.swing.*;
//import java.awt.*;
//import java.awt.datatransfer.UnsupportedFlavorException;
//import java.awt.event.KeyEvent;
//import java.awt.image.BufferedImage;
//import java.awt.image.RescaleOp;
//import java.io.File;
//import java.io.IOException;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.awt.Graphics2D;
//
//import static Tests.forms.BaseClass.formsTestData;
//
//public class TNQPageSikuli extends BaseFormsClass {
//    String formNo="247";
//    static String GUID ="";
//    static String compReceiptNumber ="";
//
//    public TNQPageSikuli(WebDriver driver)
//    {
//        super(driver);
//
//        PageFactory.initElements(driver,this);
//    }
//
//    String form_ID="#gform_page_"+formNo;
//
//    //Submit Form
//    @FindBy(id="saveandcontinue")
//    private WebElement finalSubmit;
//
//    @FindBy(id="submitbtn")
//    private WebElement nonPaymentSubmit;
//
//    public TNQPageSikuli closeChrome(){
//        driver.quit();
//        return this;
//    }
//
//    public TNQPageSikuli processImg(BufferedImage ipimage,
//                       float scaleFactor,
//                       float offset)
//                    throws IOException, TesseractException
//            {
//                // Making an empty image buffer
//                // to store image later
//                // ipimage is an image buffer
//                // of input image
//                BufferedImage opimage
//                        = new BufferedImage(1050,
//                        1024,
//                        ipimage.getType());
//
//                // creating a 2D platform
//                // on the buffer image
//                // for drawing the new image
//                Graphics2D graphic
//                        = opimage.createGraphics();
//
//                // drawing new image starting from 0 0
//                // of size 1050 x 1024 (zoomed images)
//                // null is the ImageObserver class object
//                graphic.drawImage(ipimage, 0, 0,
//                        1050, 1024, null);
//                graphic.dispose();
//
//                // rescale OP object
//                // for gray scaling images
//                RescaleOp rescale
//                        = new RescaleOp(scaleFactor, offset, null);
//
//                // performing scaling
//                // and writing on a .png file
//                BufferedImage fopimage
//                        = rescale.filter(opimage, null);
//                ImageIO
//                        .write(fopimage,
//                                "jpg",
//                                new File("D:\\Tess4J\\Testing and learning\\output.png"));
//
//                // Instantiating the Tesseract class
//                // which is used to perform OCR
//                Tesseract it = new Tesseract();
//
//                it.setDatapath("D:\\Program Files\\Workspace\\Tess4J");
//
//                // doing OCR on the image
//                // and storing result in string str
//                String str = it.doOCR(fopimage);
//                System.out.println(str);
//                return this;
//            }
//
//    public void main(String[] args) throws Exception
//    {
//                File f
//                        = new File(
//                        "D:\\Tess4J\\Testing and learning\\Final Learning Results\\input.jpg");
//
//                BufferedImage ipimage = ImageIO.read(f);
//
//                // getting RGB content of the whole image file
//                double d
//                        = ipimage
//                        .getRGB(ipimage.getTileWidth() / 2,
//                                ipimage.getTileHeight() / 2);
//
//                // comparing the values
//                // and setting new scaling values
//                // that are later on used by RescaleOP
//                if (d >= -1.4211511E7 && d < -7254228) {
//                    processImg(ipimage, 3f, -10f);
//                }
//                else if (d >= -7254228 && d < -2171170) {
//                    processImg(ipimage, 1.455f, -47f);
//                }
//                else if (d >= -2171170 && d < -1907998) {
//                    processImg(ipimage, 1.35f, -10f);
//                }
//                else if (d >= -1907998 && d < -257) {
//                    processImg(ipimage, 1.19f, 0.5f);
//                }
//                else if (d >= -257 && d < -1) {
//                    processImg(ipimage, 1f, 0.5f);
//                }
//                else if (d >= -1 && d < 2) {
//                    processImg(ipimage, 1f, 0.35f);
//                }
//           return;
//        }
//
//
//    public TNQPageSikuli captureScreen() throws Exception {
//
//        try {
//            Robot robot = new Robot();
//            String format = "jpg";
//            String deskTopPath = "C:\\Users\\rnair\\ObjSS\\output";
//            String fileName = deskTopPath + "\\" + "FullScreenshot." + format;
//
//            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
//            BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
//            ImageIO.write(screenFullImage, format, new File(fileName));
//
//            System.out.println("A full screenshot saved!");
//        } catch (AWTException | IOException ex) {
//            System.err.println(ex);
//        }
//        return this;
//    }
//
//
//        public TNQPageSikuli getEmail() throws IOException {
//            Ocr.setUp(); // one time setup
//            Ocr ocr = new Ocr(); // create a new OCR engine
//            ocr.startEngine("eng", Ocr.SPEED_FASTEST); // English
//
//            String s = ocr.recognize(new File[]{new File("C:\\Users\\rnair\\ObjSS\\emailTNQ.png")},
//                    Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
//            System.out.println(s);
//            ocr.stopEngine();
//            return this;
//        }
//
//
//        public TNQPageSikuli getObjective() throws IOException, FindFailed, UnsupportedFlavorException, InterruptedException {
//
//            try {
//
//                Robot robot = new Robot();
//
//
////                Thread.sleep(7000);
////
//////            App.open("C:\\Program Files (x86)\\OBJECTIVE\\Navigator\\Objective.exe");
////            App.open("C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\Microsoft Office 2013\\Outlook 2013.lnk");
////
//////            Runtime.getRuntime().exec("C:\\Program Files (x86)\\OBJECTIVE\\Navigator\\Objective");
////                Thread.sleep(7000);
////
////                Screen s = new Screen();
////                Thread.sleep(7000);
////
////            s.type("C:\\Users\\rnair\\ObjSS\\searchmail","CAS-812461-R4Y4X9");
////        Thread.sleep(15000);
//////                String format = "jpg";
//////                String deskTopPath = "C:\\Users\\rnair\\ObjSS\\output";
//////                String fileName = deskTopPath + "\\" + "FullScreenshot." + format;
//////
//////                Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
//////                BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
//////                ImageIO.write(screenFullImage, format, new File(fileName));
//////
//////                System.out.println("A full screenshot saved!");
//////                PointerInfo inf = MouseInfo.getPointerInfo();
//////                Point p = inf.getLocation();
//////        s.click("C:\\Users\\rnair\\ObjSS\\5");
////////                Region reg = new Screen();
//////                Thread.sleep(8000);
//////                Pattern p = new Pattern("C:\\Users\\rnair\\ObjSS\\5");
//////
//////
//////                Match imageLoc = reg.find(p);
//////                     int x = imageLoc.getX();
//////                int y = imageLoc.getY();
//////
//////                System.out.println(x);
//////                System.out.println(y);
////
////            int x = 900;
////            int y = 300;
////            int a = 699;
////            int b = 295;
////            s.click(new Location(x, y));
////            Thread.sleep(7000);
////        s.doubleClick(new Location(a,b));
////        Thread.sleep(8000);
////                String format = "png";
////                String deskTopPath = "C:\\Users\\rnair\\ObjSS\\output";
////                String fileName = deskTopPath + "\\" + "FullScreenshot." + format;
////
////                Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
////                BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
////                ImageIO.write(screenFullImage, format, new File(fileName));
////
////                System.out.println("A full screenshot saved!");
//////                s.doubleClick("C:\\Users\\rnair\\ObjSS\\firstResult");
////
//////                ImageIO.write(s.capture(App.focusedWindow()).getImage(), "png", new File("1.png"));
//                Thread.sleep(10000);
////            robot.keyPress(KeyEvent.VK_SHIFT);
//                //This will press shift key on keyboard.
//                App.open("C:\\Program Files (x86)\\OBJECTIVE\\Navigator\\Objective.exe");
////            Runtime.getRuntime().exec("C:\\Program Files (x86)\\OBJECTIVE\\Navigator\\Objective");
//            Thread.sleep(7000);
////            robot.keyRelease(KeyEvent.VK_SHIFT);
//
//                Thread.sleep(20000);
//
//
//
//                Screen s = new Screen(0);
////             int i = s.getID();
////             System.out.println(i);
//
//
//
//                Thread.sleep(10000);
////            s.find("C:\\Users\\rnair\\ObjSS\\objscreen");
////            s.click("C:\\Users\\rnair\\ObjSS\\rnairerase");
//////            // Provide the image path of element on which you want to perform action
//////            Thread.sleep(7000);
////                int x1 = 599;
////                int y1 = 200;
////                int a1 = 699;
////                int b1 = 295;
////                s.click(new Location(x1, y1));
////                Thread.sleep(7000);
////            s.click("C:\\Users\\rnair\\ObjSS\\f1");
//////
//////            Thread.sleep(7000);
////            s.click("C:\\Users\\rnair\\ObjSS\\testUrl");
////            Thread.sleep(7000);
////            s.click("C:\\Users\\rnair\\ObjSS\\portdd");
////            Thread.sleep(7000);
////            s.click("C:\\Users\\rnair\\ObjSS\\8028");
////                Thread.sleep(10000);
////                robot.keyPress(KeyEvent.VK_CONTROL);
////                robot.keyPress(KeyEvent.VK_F);
////                robot.keyRelease(KeyEvent.VK_CONTROL);
////                robot.keyRelease(KeyEvent.VK_F);
//                s.click("C:\\Users\\rnair\\ObjSS\\search1");
////                s.click("C:\\Users\\rnair\\ObjSS\\testwindows");
//                Thread.sleep(10000);
////                s.click("C:\\Users\\rnair\\ObjSS\\findObjects");
////                Thread.sleep(7000);
//                s.click("C:\\Users\\rnair\\ObjSS\\switch");
//                s.click("C:\\Users\\rnair\\ObjSS\\switch");
//                Thread.sleep(7000);
////                String format = "png";
////                String deskTopPath = "C:\\Users\\rnair\\ObjSS\\output";
////                String fileName = deskTopPath + "\\" + "FullScreenshot." + format;
////
////                Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
////                BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
////                ImageIO.write(screenFullImage, format, new File(fileName));
////
////                System.out.println("A full screenshot saved!");
//                s.click("C:\\Users\\rnair\\ObjSS\\reset");
//                s.click("C:\\Users\\rnair\\ObjSS\\properties");
//                s.click("C:\\Users\\rnair\\ObjSS\\untick");
//                s.click("C:\\Users\\rnair\\ObjSS\\name");
//                s.click("C:\\Users\\rnair\\ObjSS\\objId");
//                s.click("C:\\Users\\rnair\\ObjSS\\begins");
//                s.click("C:\\Users\\rnair\\ObjSS\\contains");
//                Thread.sleep(5000);
//                s.find("C:\\Users\\rnair\\ObjSS\\entry");
//
//                s.type("C:\\Users\\rnair\\ObjSS\\entry", "A8332957");
//                s.click("C:\\Users\\rnair\\ObjSS\\results");
//                Thread.sleep(7000);
//                s.find("C:\\Users\\rnair\\ObjSS\\parent");
//                s.rightClick("C:\\Users\\rnair\\ObjSS\\img");
//                Thread.sleep(7000);
//                s.click("C:\\Users\\rnair\\ObjSS\\rightClick");
//                Thread.sleep(7000);
//                s.find("C:\\Users\\rnair\\ObjSS\\path");
//                Thread.sleep(9000);
//                App.close("C:\\Program Files (x86)\\OBJECTIVE\\Navigator\\Objective.exe");
//
////                s.click("C:\\Users\\rnair\\ObjSS\\reset");
//            } catch (InterruptedException | AWTException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            return this;
//        }
//
//
//        @Test(dataProvider = "NSP_DataProvider", description = "Verify that the CRM values are verified")
//
//        public TNQPageSikuli readPDF(String pdf) throws IOException {
//
//
//            PDFReader pdfManager = new PDFReader();
//
//            pdfManager.setFilePath("C:\\Users\\rnair\\Downloads\\" + pdf + ".pdf");
//
//            try {
//
//                String text = pdfManager.toText();
////            String[] linesFile = text.split("\n");// this array is initialized with a single element
////            String issueTypePDF = linesFile[4].replaceFirst("^\\s* ","");
//
//                System.out.println(text);
//
//            } catch (IOException ex) {
//                Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//            return this;
//        }
//
//
//
//
//    @DataProvider
//
//    public Object[][] TNQ_DataProvider () throws Exception {
//        // Setting up the Test Data Excel file
//
//        ExcelUtils.setExcelFile(System.getProperty("user.dir") + "/TestData//" + formsTestData, "TNQ");
//
//        // Fetching the Test Case row number from the Test Data Sheet
//
//        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir") + "/TestData//" + formsTestData, "TNQ", 52);
//
//        return testObjArray;
//    }
//
//}




