package pageObjects.forms;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static libraries.Utilities.*;

public class DisabledParkingPermitPage extends BaseFormsClass {
    String formNo="194";
    public DisabledParkingPermitPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    //Submit Form
    @FindBy(id="submitbtn")
    private WebElement finalSubmit;

    @FindBy(id = "input_194_3")
    private WebElement dppNumber;
    @FindBy(id = "button_194_eligibility")
    private WebElement dppEligibility;
    @FindBy(id = "input_194_6")
    private WebElement permitAddress;
    @FindBy(css="#ui-id-1 li")
    private WebElement firstPermitAddress;
    @FindBy(id = "label_194_7_0")
    private WebElement currentRadioLabelYes;
    @FindBy(id = "label_194_7_1")
    private WebElement currentRadioLabelNo;
    @FindBy(id = "choice_194_7_0")
    private WebElement currentRadioYes;
    @FindBy(id = "choice_194_7_1")
    private WebElement currentRadioNo;
    @FindBy(id = "input_194_30")
    private WebElement currentAddress;
    @FindBy(css="#ui-id-2 li")
    private WebElement firstCurrentAddress;
    @FindBy(id = "label_194_44_0")
    private WebElement postalLabelYes;
    @FindBy(id = "label_194_44_1")
    private WebElement postalLabelNo;
    @FindBy(id = "choice_194_44_0")
    private WebElement postalYes;
    @FindBy(id = "choice_194_44_1")
    private WebElement postalNo;
    @FindBy(id = "label_194_9_0")
    private WebElement customerPermitHolderLabel;
    @FindBy(id = "label_194_9_1")
    private WebElement customerAgentLabel;
    @FindBy(id = "choice_194_9_0")
    private WebElement customerPermitHolder;
    @FindBy(id = "choice_194_9_1")
    private WebElement customerAgent;
    @FindBy(id = "input_194_62")
    private WebElement agentFName;
    @FindBy(id = "input_194_63")
    private WebElement agentLName;
    @FindBy(id = "input_194_12")
    private WebElement agentRelationship;
    @FindBy(id = "input_194_64")
    private WebElement permitHolderFname;
    @FindBy(id = "input_194_65")
    private WebElement permitHolderLname;
    @FindBy(id = "input_194_68")
    private WebElement permitHolderDOB;
    @FindBy(xpath = "//select[@class='ui-datepicker-year']")
    private WebElement permitHolderYear;
    @FindBy(xpath = "//select[@class='ui-datepicker-month']")
    private WebElement permitHolderMonth;
    @FindBy(xpath = "//select[@class='ui-datepicker-month']")
    private WebElement permitHolderDate;
    @FindBy(id = "input_194_18")
    private WebElement permitHolderEmail;
    @FindBy(id = "input_194_18_2")
    private WebElement permitHolderConfEmail;
    @FindBy(id = "input_194_59")
    private WebElement permitHolderPhoneType;
    @FindBy(id = "input_194_66")
    private WebElement permitHolderPhoneNo;
    @FindBy(id = "label_194_60_1")
    private WebElement smsNotificationLabel;
    @FindBy(id = "choice_194_60_1")
    private WebElement smsNotification;
    @FindBy(id = "label_194_70_1")
    private WebElement privacyCheckLabel;
    @FindBy(id = "choice_194_70_1")
    private WebElement privacyCheck;
    @FindBy(id = "gform_submit_button_194")
    private WebElement submitButton;
    @FindBy(id="input_194_76")
    private WebElement csoEmail;


    @FindBy(id="input_194_48_1")
    private WebElement streetAddress;
    @FindBy(id="input_194_48_3")
    private WebElement suburb;
    @FindBy(id="input_194_48_4")
    private WebElement stateDropdown;
    @FindBy(id="input_194_48_5")
    private WebElement postCode;

    public DisabledParkingPermitPage clickNext(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickNext(formNo,pageNo);
        return this;
    }
    public DisabledParkingPermitPage clickPrevious(String pageNo)
    {
        Logg.logger.info("PageNo:"+pageNo);
        super.clickPrevious(formNo,pageNo);
        return this;
    }
    public DisabledParkingPermitPage setDppNumber(String dppNumber)
    {
        Logg.logger.info("DPP number:"+dppNumber);
        waitAndClick(driver,this.dppNumber);
        waitAndSendKeys(driver,this.dppNumber,dppNumber);
        return this;
    }
    public DisabledParkingPermitPage clickCheckEligibility()
    {
        Logg.logger.info("");
        waitAndClick(driver,dppEligibility);
        return this;
    }
    public DisabledParkingPermitPage setPermitAddress(String address)
    {
        Logg.logger.info("address:"+address);
        waitAndSendKeys(driver,permitAddress,address);
        waitAndClick(driver,firstPermitAddress);
        return this;
    }
    public DisabledParkingPermitPage setIsCurrentAddress(String option)
    {
        Logg.logger.info("Is Permit address is current address?"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,currentRadioLabelYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,currentRadioLabelNo);
        return this;
    }
    public DisabledParkingPermitPage setCurrentAddress(String currentAddress)
    {
        Logg.logger.info("Current address:"+currentAddress);
        if(currentRadioNo.isSelected()) {
            waitAndSendKeys(driver, this.currentAddress, currentAddress);
            waitAndClick(driver,firstCurrentAddress);
        }
        return this;
    }
    public DisabledParkingPermitPage setIsCustomerAgent(String option)
    {
        Logg.logger.info("Is the customer an agent?"+option);
        if(option.equalsIgnoreCase("yes"))
                waitAndClick(driver,customerAgentLabel);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,customerPermitHolderLabel);
        return this;

    }
    public DisabledParkingPermitPage setAgentDetails(String fname,String lname,String relation) {
        Logg.logger.info(String.format("Agent details: Agent Fname:%s, Agent Lname:%s, Agent relationship to Permit holder:%s", fname,lname,relation));
        if(customerAgent.isSelected())
        {
            setAgentFName(fname);
            setAgentLName(lname);
            setAgentRelationship(relation);
        }
        return this;
    }
    public DisabledParkingPermitPage setPermitHolderDetails(String fname,String lname,String dob, String phonetype, String phoneNumber, String sms) throws ParseException {
        Logg.logger.info(String.format("Permit Holder details: Permit Holder Fname:%s, Permit Holder Lname:%s, Permit holder dob:%s,Permit holder Phone type:%s,Permit holder Phone:%s,Permit holder receive SMS notification:%s", fname,lname,dob,phonetype,phoneNumber,sms));
        setPermitHolderFName(fname)
                .setPermitHolderLName(lname)
                .setPermitHolderDOB(dob)
                .setPermitHolderPhoneType(phonetype)
                .setPermitHolderPhoneNumber(phoneNumber);
        if(phonetype.equalsIgnoreCase("mobile"))
                setSMSNotifications(sms);
        return this;
    }
    public DisabledParkingPermitPage setCsoEmail(String email)
    {
        Logg.logger.info("Email:"+email);
        waitAndSendKeys(driver,csoEmail,email);
        return this;
    }
    public boolean isDppValid(String dpp)
    {
        Logg.logger.info("DPermit"+dpp);
        setDppNumber(dpp);
        clickCheckEligibility();
        if(getErrorMsg().equalsIgnoreCase("Valid permit number entered. Please click 'Next' to continue."))
            return true;
        else
            return false;
    }
    public DisabledParkingPermitPage setPermitHolderFName(String fname)
    {
        Logg.logger.info("Permit Holder First Name:"+fname);
        waitAndSendKeys(driver,permitHolderFname,fname);
        return this;
    }
    public DisabledParkingPermitPage setPermitHolderLName(String lname)
    {
        Logg.logger.info("Permit Holder last name:"+lname);
        waitAndSendKeys(driver,permitHolderLname,lname);
        return this;
    }
    public DisabledParkingPermitPage setPermitHolderDOB(String dob) throws ParseException {
        Logg.logger.info("Permit Holder DOB:"+dob);
        String date,month,year;
        String dob1[]=dob.split("-");
        date = dob1[0];
        month = dob1[1];
        year = dob1[2];

        selectDateFromPicker(driver,permitHolderDOB,date,month,year);
        //waitAndSendKeys(driver,permitHolderDOB,dob);
        return this;
    }


    public DisabledParkingPermitPage setPermitHolderEmail(String email)
    {
        Logg.logger.info("Permit Holder email:"+email);
        waitAndSendKeys(driver,permitHolderEmail,email);
        return this;
    }
    public DisabledParkingPermitPage setPermitHolderConfEmail(String email)
    {
        Logg.logger.info("Permit Holder Confirmation Email:"+email);
        waitAndSendKeys(driver,permitHolderConfEmail,email);
        return this;
    }
    public DisabledParkingPermitPage setPermitHolderPhoneType(String phoneType)
    {
        Logg.logger.info("Permit Holder phone type:"+phoneType);
        selectDropdown_VisibleText(driver,permitHolderPhoneType,phoneType);
        return this;
    }
    public DisabledParkingPermitPage setPermitHolderPhoneNumber(String phoneNumber)
    {
        Logg.logger.info("Permit Holder Phone number:"+phoneNumber);
        waitAndSendKeys(driver,permitHolderPhoneNo,phoneNumber);
        return this;
    }
    public DisabledParkingPermitPage setAgentFName(String fname)
    {
        Logg.logger.info("agent First Name:"+fname);
        waitAndSendKeys(driver,agentFName,fname);
        return this;
    }
    public DisabledParkingPermitPage setAgentLName(String lname)
    {
        Logg.logger.info("Agent last name:"+lname);
        waitAndSendKeys(driver,agentLName,lname);
        return this;
    }
    public DisabledParkingPermitPage setAgentRelationship(String relationship)
    {
        Logg.logger.info("Agent relationship to Permit Holder:"+relationship);
        waitAndSendKeys(driver,agentRelationship,relationship);
        return this;
    }
    public DisabledParkingPermitPage setSMSNotifications(String option)
    {
        Logg.logger.info("Set SMS Notifications to:"+option);
        if(option.equalsIgnoreCase("yes"))
            if(!smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        if(option.equalsIgnoreCase("no"))
            if(smsNotification.isSelected())
                waitAndClick(driver,smsNotificationLabel);
        return this;
    }
    public DisabledParkingPermitPage setPolicy(String option)
    {
        Logg.logger.info("Set read &accept privacy policy to:"+option);
        if(option.equalsIgnoreCase("yes")) {
            if(!privacyCheck.isSelected())
                waitAndClick(driver,privacyCheckLabel);
        }
        if(option.equalsIgnoreCase("no"))
            if(privacyCheck.isSelected())
                waitAndClick(driver,privacyCheckLabel);
        return this;
    }
    public DisabledParkingPermitPage setPostalAddress(String streetname, String suburb, String state, String postCode)
    {
        Logg.logger.info(String.format("Street Name:%s, Suburb:%s, State:%s, PostCode:%s",streetname,suburb,state,postCode));
        if(postalNo.isSelected()) {
            waitAndSendKeys(driver, this.streetAddress, streetname);
            waitAndSendKeys(driver, this.suburb, suburb);
            selectDropdown_VisibleText(driver, this.stateDropdown, state);
            waitAndSendKeys(driver, this.postCode, postCode);
        }
        return this;
    }
    public DisabledParkingPermitPage setIsPostalAddress(String option)
    {
        Logg.logger.info("Is the permit address the postal address:"+option);
        if(option.equalsIgnoreCase("yes"))
            waitAndClick(driver,postalLabelYes);
        if(option.equalsIgnoreCase("no"))
            waitAndClick(driver,postalLabelNo);
        return this;
    }
    public DisabledParkingPermitPage submitForm() {
        Logg.logger.info("");
        waitAndClick(driver,finalSubmit);
//        super.submitForm(formNo);
        return this;
    }
}
