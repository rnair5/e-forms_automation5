package pageObjects.forms;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class BaseFormsClass {
    protected WebDriver driver;
    @FindBy(css="div.noty_body")
    private WebElement errorMsg;
    public BaseFormsClass(WebDriver driver)
    {
        this.driver=driver;
        Logg.logger.info("");
    }
    public void clickNext(String formNo,String pageNo)
    {
        Logg.logger.info("FormNo:"+formNo+" PageNo:"+pageNo);
        String css="#gform_page_"+formNo+"_"+pageNo+" .gform_next_button";
        WebElement nextButton = driver.findElement(By.cssSelector(css));
        waitAndClick(driver,nextButton);
        waitForPageLoaded(driver);
    }
    public void clickPrevious(String formNo,String pageNo)
    {
        Logg.logger.info("Form_Number:"+formNo+" Page number:" +pageNo);
        String css="#gform_page_"+formNo+"_"+pageNo+" .gform_previous_button";
        WebElement nextButton = driver.findElement(By.cssSelector(css));
        waitAndClick(driver,nextButton);
    }
    public void selectAddress(WebElement addressBox, String address)
    {
        Logg.logger.info("Addressbox:"+ addressBox.toString()+"Address:"+address);
        selectFirstItemFromLookup(driver,addressBox,address);
    }
    public void submitForm(String formNo)
    {
        Logg.logger.info("Form No:"+formNo);
        String id="gform_submit_button_"+formNo;
        waitAndClick(driver,By.id(id));
    }
    public String getErrorMsg()
    {
        Logg.logger.info("");
        try {
            waitUntilElementIsVisible(driver, errorMsg);
            return errorMsg.getText();
        }catch (Exception e) {
            extent_Warn(driver,"Error message not found",errorMsg.toString(),"","");
            return null;
        }
    }
}
