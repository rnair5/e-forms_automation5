package pageObjects.forms;

import Tests.forms.PDFReader;
import libraries.ExcelUtils;
import libraries.Logg;
import libraries.Utilities;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static Tests.forms.BaseClass.formsTestData;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Warn;
import static libraries.Utilities.*;

public class OOHPage extends BaseFormsClass {
    String formNo="238";
    static String GUID ="";
    static String compReceiptNumber ="";
    public OOHPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    String form_ID="#gform_page_"+formNo;

    //Submit Form
    @FindBy(id="saveandcontinue")
    private WebElement finalSubmit;

    //Application Details
    @FindBy(id = "label_238_9_0")
    private WebElement hoursYes;
    @FindBy(id = "label_238_9_1")
    private WebElement hoursNo;

    //Planning Permit Number
    @FindBy(id = "input_238_10")
    private WebElement permitNumber;

    //Which condition of your planning permit will be impacted
    @FindBy(id = "input_238_11")
    private WebElement impact;

    //Address
    @FindBy(id = "input_238_13")
    private WebElement address;

    //Multiple Address Check
    @FindBy(id = "label_238_18_1")
    private WebElement multipleAddress;
    //Multiple Address Entry
    @FindBy(id="input_238_57")
    private WebElement multiAddressEntry;
    //Works Entry
    @FindBy(id="input_238_24")
    private WebElement worksEntry;
    // When do you want to work Out of Hours
    @FindBy(id="input_238_70")
    private WebElement oohEntry;
    //Do you want to work across all of these dates, or just a selection?
    @FindBy(id="input_238_26")
    private WebElement dateEntry;
    //Notification activities for large-scale works
    @FindBy(id="input_238_31")
    private WebElement largeScaleEntry;

   //Contact Details
   @FindBy(id = "label_238_34_1")
   private WebElement companyNo;
   //The Property is Owned by a company
   @FindBy(id = "label_238_34_0")
   private WebElement companyYes;
   @FindBy(id="input_238_37")
   private WebElement companyName;
    @FindBy(id="input_238_38")
    private WebElement companyABN;

    @FindBy(id="input_238_39_1")
    private WebElement compContactAddressLineOne;
    @FindBy(id="input_238_39_3")
    private WebElement compContactAddressLineThree;
    @FindBy(id="input_238_39_4")
    private WebElement compContactAddressLineFour;
    @FindBy(id="input_238_39_5")
    private WebElement compContactPostcode;


    @FindBy(id="input_238_41")
    private WebElement ownerFirstName;
    @FindBy(id="input_238_42")
    private WebElement ownerLastName;


    @FindBy(id = "label_238_45_0")
    private WebElement mobType;
    @FindBy(id = "label_238_45_1")
    private WebElement landType;
    @FindBy(id = "input_238_46")
    private WebElement mobNo;
    @FindBy(id = "input_238_48")
    private WebElement landNo;

    @FindBy(id="input_238_43")
    private WebElement email;
    @FindBy(id="label_238_47_1")
    private WebElement smsNotificationLabel;
    @FindBy(id = "choice_238_47_1")
    private WebElement smsNotification;

    //Site Contact
    @FindBy(id="input_238_50")
    private WebElement siteFirstName;
    @FindBy(id="input_238_51")
    private WebElement siteLastName;
    @FindBy(id = "input_238_52")
    private WebElement siteMobNo;
    
    //Declaration
    @FindBy(id="label_238_53_1")
    private WebElement declaration;

    //Next Page 1
    @FindBy(id="gform_next_button_239_151")
    private WebElement nextOne;

    //Next Page 1
    @FindBy(id="gform_next_button_239_152")
    private WebElement nextTwo;

    //Submit Button
    @FindBy(id = "gform_submit_button_228")
    private WebElement submitButton;

    //Payment Declined Message
    @FindBy(xpath = "//*[@id=\"field_237_8\"]/h2/div/ul/li/p[1]")
    private WebElement declinedMessage;

    //Payment Declined link
    @FindBy(xpath = "//*[@id=\"field_237_8\"]/h2/div/ul/li/p[2]/a")
    private WebElement declinedLink;


    public boolean isPaymentDeclined()
    {
        Logg.logger.info("");
        try {
            if (waitUntilElementIsVisible(driver,declinedMessage).isDisplayed())
                return true;

            else
                return false;
        }catch (Exception e)
        {
            return false;
        }
    }

    public OOHPage submitPaymentAgain() {

        waitAndClick(driver, declinedLink);

        return this;
    }

    public OOHPage clickNext(String pageNo) {
        Logg.logger.info("PageNo:" + pageNo);
        super.clickNext(formNo, pageNo);
        return this;
    }

    public OOHPage clickPrevious(String pageNo) {
        Logg.logger.info("Page no:" + pageNo);
        super.clickPrevious(formNo, pageNo);
        return this;
    }

    //Invalid Address Verification
    public OOHPage setInvalidAddress(String addresss) throws Exception {
        waitAndSendKeys(driver, address, addresss);
        WebElement randomClick = driver.findElement(By.xpath("//*[@id=\"gf_step_237_1\"]/span[2]"));
        randomClick.click();
        String invalidAddressLabel = driver.findElement(By.xpath("//*[@id=\"error-info_input_237_9\"]")).getText();
        if (invalidAddressLabel.contains("Please enter a valid Boroondara address")) {
            extent_Pass(driver, "Invalid Address- Verification Passed", invalidAddressLabel.toString(), "", "");
        } else {
            extent_Warn(driver, "Invalid Address - verification Failed", invalidAddressLabel.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
        }
        return this;
    }

    //Invalid Phone Number
    public OOHPage setInvalidPhone() throws Exception {
        String invalidPhoneLabel = driver.findElement(By.xpath("//*[@id=\"error\"]/ol/li/a")).getText();
        if (invalidPhoneLabel.contains("Mobile number - Please enter a valid Australian mobile phone number.")) {
            extent_Pass(driver, "Invalid Phone- Verification Passed", invalidPhoneLabel.toString(), "", "");
        } else {
            extent_Warn(driver, "Invalid Phone - verification Failed", invalidPhoneLabel.toString(), "", "");
            throw new Exception(" TEST FOR INVALID PHONE NUMBER CHECK FAILED ");
        }
        return this;
    }

    //Invalid ABN Number
    public OOHPage setInvalidABN() throws Exception {
        String invalidPhoneLabel = driver.findElement(By.xpath("//*[@id=\"error\"]/ol/li/a")).getText();
        if (invalidPhoneLabel.contains("Company ABN - Please check your ABN. A valid ABN is 11 digits.")) {
            extent_Pass(driver, "Invalid ABN- Verification Passed", invalidPhoneLabel.toString(), "", "");
        } else {
            extent_Warn(driver, "Invalid ABN - verification Failed", invalidPhoneLabel.toString(), "", "");
            throw new Exception(" TEST FOR INVALID ABN NUMBER CHECK FAILED ");
        }
        return this;
    }

    //Invalid Card Number
    public OOHPage setInvalidCard() throws Exception {
        String invalidCardLabel = driver.findElement(By.xpath("/html/body/form/fieldset/div/span")).getText();
        if (invalidCardLabel.contains("Invalid card number")) {
            extent_Pass(driver, "Invalid Card Number- Verification Passed", invalidCardLabel.toString(), "Invalid Card Number", invalidCardLabel);
        } else {
            extent_Warn(driver, "Invalid Card Number - verification Failed", invalidCardLabel.toString(), "Invalid Card Number", invalidCardLabel);
            throw new Exception(" TEST FOR INVALID CARD NUMBER CHECK FAILED ");
        }
        return this;
    }

    //Invalid Expiry Year
    public OOHPage setInvalidExpiryYear() throws Exception {
        String invalidExpiryYearLabel = driver.findElement(By.xpath("/html/body/form/fieldset/div/span")).getText();
        if (invalidExpiryYearLabel.contains("Invalid expiry year")) {
            extent_Pass(driver, "Invalid Expiry Year- Verification Passed", invalidExpiryYearLabel.toString(), "Invalid expiry year", invalidExpiryYearLabel);
        } else {
            extent_Warn(driver, "Invalid Expiry Year - verification Failed", invalidExpiryYearLabel.toString(), "Invalid expiry year", invalidExpiryYearLabel);
            throw new Exception(" TEST FOR INVALID Expiry Year FAILED ");
        }
        return this;
    }


    //Invalid Expiry Month
    public OOHPage setInvalidExpiryMonth() throws Exception {
        String invalidExpiryMonthLabel = driver.findElement(By.xpath("/html/body/form/fieldset/div/span")).getText();
        if (invalidExpiryMonthLabel.contains("Invalid expiry month")) {
            extent_Pass(driver, "Invalid Expiry Month- Verification Passed", invalidExpiryMonthLabel.toString(), "Invalid expiry month", invalidExpiryMonthLabel);
        } else {
            extent_Warn(driver, "Invalid Expiry Month - verification Failed", invalidExpiryMonthLabel.toString(), "Invalid expiry month", invalidExpiryMonthLabel);
            throw new Exception(" TEST FOR INVALID Expiry Month FAILED ");
        }
        return this;
    }


    //Invalid CVV
    public OOHPage setInvalidCVV() throws Exception {
        Thread.sleep(10000);
        String invalidCVVLabel = driver.findElement(By.xpath("//*[@id=\"popup_container\"]")).getText();
        WebElement invalidCVVPopUp  = driver.findElement(By.xpath("//*[@id=\"popup_container\"]"));
        invalidCVVPopUp.click();
        if (invalidCVVLabel.contains("Invalid expiry month")) {
            extent_Pass(driver, "Invalid CVV- Verification Passed", invalidCVVLabel.toString(), "Invalid expiry month", invalidCVVLabel);
        } else {
            extent_Warn(driver, "Invalid CVV - verification Failed", invalidCVVLabel.toString(), "Invalid expiry month", invalidCVVLabel);
            throw new Exception(" TEST FOR INVALID CVV FAILED ");
        }
        return this;
    }

    //Printer Friendly Version
    public OOHPage getPrinterVersionDetails() throws Exception {
        Thread.sleep(5000);
        WebElement printerLink  = driver.findElement(By.xpath("//*[@id=\"primary\"]"));
        printerLink.click();
        String printerDetails = driver.findElement(By.xpath("//*[@id=\"primary\"]")).getText();
        System.out.println("***************The Printer Friendly Page Content is\n*************** " + printerDetails + "");
        if (!printerDetails.isEmpty()) {
            extent_Pass(driver, "PRINTER FRIENDLY- Verification Passed", printerDetails.toString(), "", printerDetails);
        } else {
            extent_Warn(driver, "PRINTER FRIENDLY - verification Failed", printerDetails.toString(), "", printerDetails);
            throw new Exception(" TEST FOR PRINTER FRIENDLY FAILED ");
        }

        return this;
    }

    //FeedBack Form
    public OOHPage getFeedback() throws Exception {
        Thread.sleep(5000);

        WebElement feedBackOption  = driver.findElement(By.xpath("//*[@id=\"label_7_15_1\"]"));
        feedBackOption.click();
        WebElement feedBackSubmit = driver.findElement(By.xpath("//*[@id=\"submitbtn\"]"));
        feedBackSubmit.click();
        Thread.sleep(10000);
        String feedBackText = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/header/h1")).getText();
        if (feedBackText.equalsIgnoreCase("Thank you for your feedback")) {
            extent_Pass(driver, "FEEDBACK- Verification Passed", feedBackText.toString(), "Thank you for your feedback", feedBackText);
        } else {
            extent_Warn(driver, "FEEDBACK - verification Failed", feedBackText.toString(), "Thank you for your feedback", feedBackText);
            throw new Exception(" TEST FOR FEEDBACK FAILED ");
        }
        return this;
    }




    public OOHPage setAddress(String addresss) {
        Logg.logger.info("First Address:" + addresss);
        super.selectAddress(this.address, addresss);
        return this;
    }

    public OOHPage setWorkOutside(String size) {
        Logg.logger.info("Work Outside:" + size);
        if (size.equalsIgnoreCase("Yes"))
            waitAndClick(driver, hoursYes);
        if (size.equalsIgnoreCase("No"))
            waitAndClick(driver, hoursNo);
        return this;
    }


    public OOHPage setPermitNo(String information) {
        Logg.logger.info("Works Information:" + information);
        waitAndSendKeys(driver, permitNumber, information);
        return this;
    }


    public OOHPage setPermitInformation(String information) {
        Logg.logger.info("Works Information:" + information);
        waitAndSendKeys(driver, impact, information);
        return this;
    }

    public OOHPage checkMultipleAddress(String size) {
        Logg.logger.info("Ground Size:" + size);
        if (size.equalsIgnoreCase("My building site spans across multiple addresses"))
            waitAndClick(driver, multipleAddress);

        return this;
    }

    public OOHPage setMultipleAddress(String information) {
        Logg.logger.info("Works Information:" + information);
        waitAndSendKeys(driver, multiAddressEntry, information);
        return this;
    }

    public OOHPage setWorksEntry(String information) {
        Logg.logger.info("Works Information:" + information);
        waitAndSendKeys(driver, worksEntry, information);
        return this;
    }

    public OOHPage setOOHEntry(String information) {
        Logg.logger.info("Works Information:" + information);
        waitAndSendKeys(driver, oohEntry, information);
        return this;
    }

    public OOHPage setDateEntry(String information) {
        Logg.logger.info("Works Information:" + information);
        waitAndSendKeys(driver, dateEntry, information);
        return this;
    }

    public OOHPage setScaleEntry(String information) {
        Logg.logger.info("Works Information:" + information);
        waitAndSendKeys(driver, largeScaleEntry, information);
        return this;
    }

    public OOHPage uploadLetters() throws AWTException, InterruptedException {
        Thread.sleep(7000);
//        driver.findElement(By.cssSelector("input[type='file']")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[18]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[18]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[18]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[18]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[18]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        Thread.sleep(5000);
        return this;
    }

    public OOHPage uploadActivities() throws AWTException, InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[19]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestXLS.xlsx ");
        Thread.sleep(5000);
        return this;
    }

    public OOHPage uploadTraffic() throws AWTException, InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[21]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        Thread.sleep(5000);
        return this;
    }

    public OOHPage uploadSupporting() throws AWTException, InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[22]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[22]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[22]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[22]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div/form/div[2]/div[2]/div[1]/ul/li[22]/div[2]/div[1]/div[2]/input")).sendKeys("C:\\Users\\rnair\\e-forms_endtoend\\TestData\\TestPNG.PNG ");
        Thread.sleep(5000);
        return this;
    }


    public OOHPage setPersonType(String person) {
        Logg.logger.info("Who is Applying:" + person);
        if (person.equalsIgnoreCase("Yes"))
            waitAndClick(driver, companyYes);
        if (person.equalsIgnoreCase("No"))
            waitAndClick(driver, companyNo);
        return this;
    }

    public OOHPage setCompanyName(String name) {
        Logg.logger.info("Company Name:" + name);
        waitAndSendKeys(driver, companyName, name);
        return this;
    }

    public OOHPage setCompanyABN(String name) {
        Logg.logger.info("ABN Number:" + name);
        waitAndSendKeys(driver, companyABN, name);
        return this;
    }

    public OOHPage setFName(String name) {
        Logg.logger.info("First Name:" + name);
        waitAndSendKeys(driver, ownerFirstName, name);
        return this;
    }

    public OOHPage setLName(String name) {
        Logg.logger.info("Last Name:" + name);
        waitAndSendKeys(driver, ownerLastName, name);
        return this;
    }

    public OOHPage setEmail(String email) {
        Logg.logger.info("Email:" + email);
        waitAndSendKeys(driver, this.email, email);
        return this;
    }

    public OOHPage setPhoneType(String phoneType) {
        Logg.logger.info("Phone Type:" + phoneType);
        if (phoneType.equalsIgnoreCase("Mobile"))
            waitAndClick(driver, mobType);
        if (phoneType.equalsIgnoreCase("Landline"))
            waitAndClick(driver, landType);
        return this;
    }


    public OOHPage setPhoneNumber(String phoneNumber) {
        Logg.logger.info(" Phone number:" + phoneNumber);
        if (mobType.isSelected())
            waitAndSendKeys(driver, mobNo, phoneNumber);
        if (landType.isSelected())
            waitAndSendKeys(driver, landNo, phoneNumber);
        return this;
    }

    public OOHPage setMobPhoneNumber(String mobphoneNum) {
        Logg.logger.info(" Phone number:" + mobphoneNum);
        waitAndSendKeys(driver, mobNo, mobphoneNum);

        return this;
    }

    public OOHPage setlandPhoneNumber(String lanphoneNum) {
        Logg.logger.info(" Phone number:" + lanphoneNum);
        waitAndSendKeys(driver, landNo, lanphoneNum);
        return this;
    }

    public OOHPage setSMSNotifications(String option) {
        Logg.logger.info("Set SMS Notifications to:" + option);
        if (option.equalsIgnoreCase("yes"))
            if (!smsNotification.isSelected())
                waitAndClick(driver, smsNotificationLabel);
        if (option.equalsIgnoreCase("no"))
            if (smsNotification.isSelected())
                waitAndClick(driver, smsNotificationLabel);
        return this;
    }

    public OOHPage setDeclarations(String declarations) {
        Logg.logger.info("declarations:" + declarations);
        if (declarations.contains("By lodging this application, I declare that I am the person identified in the applicant details and that all information provided is, to the best of my knowledge, true and correct. I understand it is an offence to provide false information and penalties apply."))
            waitAndClick(driver, declaration);
        return this;
    }



    public OOHPage setCorpContactAddressOne(String addressOne) {
        Logg.logger.info(" addressOne:" + addressOne);
        waitAndSendKeys(driver,compContactAddressLineOne, addressOne);
        return this;
    }

    //
    public OOHPage setCorpContactAddressTwo(String addressTwo) {
        Logg.logger.info(" addressTwo:" + addressTwo);
        waitAndSendKeys(driver, compContactAddressLineThree, addressTwo);
        return this;
    }

    //
    public OOHPage setCorpContactAddressFour(String addressFour) {
        Logg.logger.info(" addressFour:" + addressFour);
        waitAndSendKeys(driver, compContactAddressLineFour, addressFour);
        return this;
    }

    //
    public OOHPage setCorpPostCode(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
        waitAndSendKeys(driver, compContactPostcode, setPostCode);
        return this;
    }

    //
    public OOHPage selectInterAddress(String category) {
        Logg.logger.info("category:" + category);
        WebElement addressDropdown= driver.findElement(By.id("input_238_39_6"));
        addressDropdown.click();
        if (category.equalsIgnoreCase("India"))
            selectDropdown_Index(driver, addressDropdown, 103);
        return this;
    }

    public OOHPage setSiteFName(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
        waitAndSendKeys(driver, siteFirstName, setPostCode);
        return this;
    }

    public OOHPage setSiteLName(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
        waitAndSendKeys(driver, siteLastName, setPostCode);
        return this;
    }

    public OOHPage setSiteNumber(String setPostCode) {
        Logg.logger.info(" setPostCode:" + setPostCode);
        waitAndSendKeys(driver, siteMobNo, setPostCode);
        return this;
    }


    public OOHPage setPersonalDetails(String fname, String lname, String phonetype) throws ParseException {
        Logg.logger.info(String.format(" details:  Fname:%s, Lname:%s, Phone type:%s", fname, lname, phonetype));
        this.setFName(fname)
                .setLName(lname)
                .setPhoneType(phonetype);
        return this;
    }

    public OOHPage submitForm() {
        Logg.logger.info("");
        waitAndClick(driver, finalSubmit);
        return this;
    }

    @Test(dataProvider = "OOH_DataProvider", description = "Verify that the review page values can be compared")
    public OOHPage getIntroPage(String expIntro) throws Exception {

        //Capture the Intro page Content
        String introContent = driver.findElement(By.xpath("/html/body/div[1]/div/div")).getText();
        System.out.println("***************The Introduction Page Content is\n*************** " + introContent + "");

        if (introContent.equalsIgnoreCase(expIntro)) {
            extent_Pass(driver, "******YESSSS!!!!*******PASSED INTRO PAGE COMPARISON", introContent.toString(), "", "");
        } else {
            extent_Warn(driver, "******OH NO!!!!*******FAILED INTRO PAGE COMPARISON", introContent.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE INTRO PAGE VERIFICATION LEVEL ");

        }

        return this;

    }





    @Test(dataProvider = "OOH_DataProvider", description = "Verify that the review page values can be compared")
    public OOHPage getReviewPage(String expReviewPage, String price) throws Exception {

        //Get the Hidden Values
        //GUID
        GUID = driver.findElement(By.id("input_238_4")).getAttribute("value");
        System.out.println("***************The GUID from FORM SUBMISSION is\n*************** " + GUID + "");
        String productCode = driver.findElement(By.id("input_238_60")).getAttribute("data-product-id");
        System.out.println("***************The Product Code is*************** \n" + productCode + "");
        Thread.sleep(3000);

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_238_55\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
        if (reviewPage.equalsIgnoreCase(expReviewPage)) {
            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
        } else {
            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");

        }
        String lastPrice = reviewPage.substring(reviewPage.lastIndexOf("\n"));
        String actualPrice = lastPrice.replace("\n","");
        System.out.println("***************The PRICE IS************* " + actualPrice + "");

        if (actualPrice.equalsIgnoreCase(price.trim())) {
            extent_Pass(driver, "The Price verification is correct", price.toString(), "", "");
        } else {
            extent_Warn(driver, "The Price verification Failed", price.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE PRICE VERIFICATION LEVEL ");

        }

        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_238_56\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }

        return this;

    }

    @Test(dataProvider = "ROP_DataProvider", description = "Verify that the review page values can be compared")
    public OOHPage getReviewPageBack(String expReviewBack, String expPriceBack) throws Exception {

        //Get the Hidden Values
        //GUID
        GUID = driver.findElement(By.id("input_238_4")).getAttribute("value");
        System.out.println("***************The GUID from FORM SUBMISSION is\n*************** " + GUID + "");
        String productCode = driver.findElement(By.id("input_238_60")).getAttribute("data-product-id");
        System.out.println("***************The Product Code is*************** \n" + productCode + "");
        Thread.sleep(3000);

        // Verify the Review page
        String reviewPage = driver.findElement(By.xpath("//*[@id=\"field_238_55\"]/table/tbody/tr/td")).getText();
        System.out.println("***************The Review Page is\n*************** " + reviewPage + "");
        if (reviewPage.equalsIgnoreCase(expReviewBack)) {
            extent_Pass(driver, "******YESSSS!!!!*******PASSED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
        } else {
            extent_Warn(driver, "******OH NO!!!!*******FAILED REVIEW PAGE COMPARISON", reviewPage.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE REVIEW PAGE VERIFICATION LEVEL ");

        }
        String lastPrice = reviewPage.substring(reviewPage.lastIndexOf("\n"));
        String actualPrice = lastPrice.replace("\n","");
        System.out.println("***************The PRICE IS************* " + actualPrice + "");

        if (actualPrice.equalsIgnoreCase(expPriceBack.trim())) {
            extent_Pass(driver, "The Price verification is correct", expPriceBack.toString(), "", "");
        } else {
            extent_Warn(driver, "The Price verification Failed", expPriceBack.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE PRICE VERIFICATION LEVEL ");

        }

        //Check the Privacy Policy
        String privacyPolicy = driver.findElement(By.xpath("//*[@id=\"field_238_56\"]")).getText();
        if (privacyPolicy.contains("Privacy policy")) {
            extent_Pass(driver, "The Privacy Policy exists", privacyPolicy.toString(), "", "");
        } else {
            extent_Warn(driver, "The Privacy Policy does not exist", privacyPolicy.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE POLICY VERIFICATION LEVEL ");

        }

        return this;

    }
    public OOHPage getPaymentDetails() throws InterruptedException, ParseException, AWTException {


        //Get the Issue type from the review page
        Thread.sleep(15000);
        String paymentDetails = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]")).getText();
        System.out.println("The payment details are " + paymentDetails + "");
        //String Receipt Number
        String expReceiptNumber = driver.findElement(By.xpath("/html/body/div[1]/div/div/main/article/div/div[2]/div/section/div/div[1]/p[1]")).getText();
        System.out.println("The Receipt Number " + expReceiptNumber + "");
        compReceiptNumber = expReceiptNumber.substring(16);
        System.out.println("The Trimmed RN is " + compReceiptNumber + "");

        return this;
    }

    public OOHPage browserBack() throws InterruptedException, ParseException, AWTException {


        //Get the Issue type from the review page
        driver.navigate().back();
        Thread.sleep(5000);
        driver.navigate().back();
        Thread.sleep(5000);
        driver.navigate().back();
        Thread.sleep(10000);
        driver.navigate().back();
        Thread.sleep(10000);
        finalSubmit.click();
        Thread.sleep(10000);
//        Alert alertDialog = driver.switchTo().alert();
//        // Get the alert text
//        String alertText = alertDialog.getText();
//        System.out.println("The alert text message is " + alertText + "");
//        // Click the OK button on the alert.
//        Thread.sleep(15000);
//        if (alertText.contains("Your session has expired. Please restart the form."))
//            alertDialog.accept();
        Thread.sleep(5000);

        return this;
    }

    public OOHPage sessionEnd() throws InterruptedException, ParseException, AWTException {

        Thread.sleep(5000);
        // Switch the driver context to the alert
        Alert alertDialog = driver.switchTo().alert();
        // Get the alert text
        String alertText = alertDialog.getText();
        System.out.println("The alert text message is " + alertText + "");
        // Click the OK button on the alert.
        Thread.sleep(15000);
        if (alertText.contains("Your session has expired. Please restart the form."))
            alertDialog.accept();
        Thread.sleep(5000);

        return this;
    }


    @Test(dataProvider = "OOH_DataProvider", description = "Verify that the CRM values are verified")
    public OOHPage captureCRM(String expCaseType, String expSubType, String expCustomerName, String expLocation,
                                          String expDoc1, String expDoc2, String expDoc3, String expDoc4,
                                          String expCount, String expType,
                                          String expBusinessNo, String expMobileNo, String expAlternateNo, String expFName,
                                          String expLName, String expEmail, String expCaseCount) throws Exception {


        Thread.sleep(15000);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.addArguments("window-size=1920,1080");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
        System.out.println(driver.manage().window().getSize());
        driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
        System.out.println(driver.manage().window().getSize());
        Thread.sleep(10000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);


        //Switch to the frame that contains the grid
        driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
        Thread.sleep(10000);


        Actions action = new Actions(driver);
        String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
        System.out.println("The GUID text in CRM is " + identifierText + "");
        WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
        Logg.logger.info("Is the GUID correct:" + identifier);

//
//        Actions action = new Actions(driver);
//        String identifierText = driver.findElement(By.xpath("//table[@id='gridBodyTable']/tbody/tr['" + GUID + "']/td[23]/nobr")).getText();
//        System.out.println("The GUID text in CRM is " + identifierText + "");
//        WebElement identifier = driver.findElement(By.xpath("//table[@id='gridBodyTable']/tbody/tr[contains(text(), '" + GUID + "']/td[23]/nobr"));
//        Logg.logger.info("Is the GUID correct:" + identifier);



        action.doubleClick(identifier).perform();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(5000);
//        driver.switchTo().frame(driver.findElement(By.id("InlineDialog_Iframe")));
//        Thread.sleep(5000);
//        WebElement emailPopup1 = driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/button"));
//        emailPopup1.click();
//        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseID.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseName.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }
        Thread.sleep(5000);
        //Select the Sub-case Type
        String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
        System.out.println("The SubcaseType is " + subCaseName + "");
        if (subCaseName.equalsIgnoreCase(expSubType)) {
            extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
        }
        //Select the origin
        String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
        Logg.logger.info("Origin name:" + originName);
        if (originName.equalsIgnoreCase("Web")) {
            extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
        } else {
            extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
        }
        //Select the customer name
        String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
        Logg.logger.info("Customer name:" + customerName);

        if (customerName.equalsIgnoreCase(expCustomerName)) {
            extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
        } else {
            extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
        }

        //Verify the Location Field
        String locationField = driver.findElement(By.xpath("//*[@id=\"PropertyQuickView_PropertyQuickView_account_address1_composite\"]/div[1]/span")).getText();
        if (locationField.contains(expLocation)) {
            extent_Pass(driver, "Location Verification Passed", locationField.toString(), "", "");
        } else {
            extent_Warn(driver, "Location verification Failed", locationField.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ADDRESS LEVEL ");
        }
//
//        //Verify the Instructions Field
//        String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();
//
//        if (instructionsField.equalsIgnoreCase(Instructions)) {
//            extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
//        } else {
//            extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), Instructions, "");
//            throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
//        }
//
        Thread.sleep(10000);



        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to the Case ESCALATION TAB
        WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
        Thread.sleep(10000);
        caseTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String firstResponse = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
        Logg.logger.info("First Response is:" + firstResponse);

        //Check if the Timer is not ticking and First response is Succeeded
        if (firstResponse.equalsIgnoreCase("Succeeded")) {
            extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
        }


        Thread.sleep(5000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);


        //Navigate to the PAYMENTS TAB
        WebElement payTabSelected = driver.findElement(By.id("tab_paymentrecords"));
        Thread.sleep(10000);
        payTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String receiptNumber = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[3]/div")).getText();
//        String compReceiptNumber = expReceiptNumber.substring(16);
//        System.out.println("The Trimmed RN is " + compReceiptNumber + "");
        if (receiptNumber.contains(compReceiptNumber)) {
            extent_Pass(driver, "Payment receipt- Verification Passed", receiptNumber.toString(), compReceiptNumber, receiptNumber);
        } else {
            extent_Warn(driver, "Payment receipt - verification Failed", receiptNumber.toString(), compReceiptNumber, receiptNumber);
            throw new Exception(" VERIFICATION OF RECEIPT FAILED ");
        }

        String amount = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[5]/div")).getText();
        if (amount.equalsIgnoreCase("$188.00")) {
            extent_Pass(driver, "Amount- Verification Passed", amount.toString(), "", "");
        } else {
            extent_Warn(driver, "Amount - verification Failed", amount.toString(), "", "");
            throw new Exception(" VERIFICATION OF AMOUNT FAILED ");
        }

        String paidStatus = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[6]/nobr")).getText();
        if (paidStatus.equalsIgnoreCase("Paid")) {
            extent_Pass(driver, "Payment Status- Verification Passed", paidStatus.toString(), "", "");
        } else {
            extent_Warn(driver, "Payment Status - verification Failed", paidStatus.toString(), "", "");
            throw new Exception(" VERIFICATION OF Payment Status FAILED ");
        }





        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to Notes
        WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
        Thread.sleep(10000);
        notesTabSelected.click();
//        Thread.sleep(20000);

        //Navigate to Activities
        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
        actTabSelected.click();
        Thread.sleep(10000);


        String emailTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div")).getAttribute("objecttypecode");
        Thread.sleep(20000);
        Logg.logger.info("Email:" + emailTrigger);

        Thread.sleep(20000);


        //Check if SMS is triggered
        Thread.sleep(20000);
        String smsTrigger = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[2]/div[2]/div/div[4]/div/div/div[2]/div[3]/div[1]/div/div")).getAttribute("objecttypecode");
        Logg.logger.info("SMS:" + smsTrigger);


//        //Select the customer name
//        Thread.sleep(20000);
//        WebElement custName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[1]/div/table/tbody/tr[9]/td[2]/div[1]/div[1]/span[1]"));
//        custName.click();
//        Thread.sleep(10000);

        //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseIDRefresh.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);

        //DOCUMENT DETAILS
        //Navigate to the Document Tabs
        Thread.sleep(20000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
        Thread.sleep(10000);
        if (documentTabSelected.isDisplayed()) {
            extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
        } else {
            extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
            throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
        }
        documentTabSelected.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Thread.sleep(10000);
        driver.switchTo().parentFrame();
        Thread.sleep(5000);


        //Get the Name of the First Document - PDF Document
        String pdfDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (pdfDocName.equalsIgnoreCase(expDoc1)) {
            extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", pdfDocName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", pdfDocName.toString(), "", "");
            throw new Exception("PDF NAMING CONVENTION FAILED ");
        }
//

//        String ossReceiptName = expReceipt + " + "+ compReceiptNumber;
//        Logg.logger.info("Receipt Format:" + ossReceiptName);
        //Get the Name of the Second Document - Certificate
        String certName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
        if (certName.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - CERTIFICATE - Verification Passed", certName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - CERTIFICATE - verification Failed", certName.toString(), "", "");
            throw new Exception("CERTIFICATE NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Third Document - Dial before you Dig
        String dialName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
        if (dialName.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - DIAL - Verification Passed", dialName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - DIAL - verification Failed", dialName.toString(), "", "");
            throw new Exception("DIAL NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Fourth Document - Notification
        String notificationName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
        if (notificationName.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - NOTIFICATION - Verification Passed", notificationName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - NOTIFICATION - verification Failed", notificationName.toString(), "", "");
            throw new Exception("NOTIFICATION NAMING CONVENTION FAILED ");
        }


        //Click the Next page to view the Documents

        WebElement docPage2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
        docPage2.click();
        Thread.sleep(10000);

//        driver.switchTo().parentFrame();



        //Get the Document details from the Second Page
        //Get the Name of the First Document - Receipt
//        String ossReceiptName = expReceipt + " + "+ compReceiptNumber;
//        Logg.logger.info("Receipt Format:" + ossReceiptName);

        String receiptDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (receiptDocName.contains(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - RECEIPT - Verification Passed", receiptDocName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - RECEIPT - verification Failed", receiptDocName.toString(), "", "");
            throw new Exception("RECEIPT NAMING CONVENTION FAILED ");
        }
        //Get the Name of the Second Document - Site
        String siteName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
        if (siteName.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - SITE - Verification Passed", siteName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SITE - verification Failed", siteName.toString(), "", "");
            throw new Exception("SITE NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Third Document - Supporting Document 1
        String supportDoc1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
        if (supportDoc1.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 1 - Verification Passed", supportDoc1.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 1 - verification Failed", supportDoc1.toString(), "", "");
            throw new Exception("SUPPORTING DOCUMENT 1 NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Fourth Document - Supporting Document 2
        String supportDoc2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
        if (supportDoc2.equalsIgnoreCase(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 2 - Verification Passed", supportDoc2.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 2 - verification Failed", supportDoc2.toString(), "", "");
            throw new Exception("SUPPORTING DOCUMENT 2 NAMING CONVENTION FAILED ");
        }

        //Click the Next page to view the Documents
        WebElement docPage3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
        docPage3.click();
        Thread.sleep(10000);

        //Get the Document details from the Third Page
        //Get the Name of the First Document - Supporting Document 3
        String supportDoc3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (supportDoc3.contains(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORT 3 - Verification Passed", supportDoc3.toString(), expDoc3, "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORT 3 - verification Failed", supportDoc3.toString(), expDoc3, "");
            throw new Exception("SUPPORT 3 NAMING CONVENTION FAILED ");
        }
        //Get the Name of the Second Document - Supporting Document 4
        String supportDoc4 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
        if (supportDoc4.equalsIgnoreCase(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORT 4 - Verification Passed", supportDoc4.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORT 4 - verification Failed", supportDoc4.toString(), "", "");
            throw new Exception("SUPPORT 4 NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Third Document - Supporting Document 5
        String supportDoc5 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
        if (supportDoc5.equalsIgnoreCase(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 5 - Verification Passed", supportDoc5.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 5 - verification Failed", supportDoc5.toString(), "", "");
            throw new Exception("SUPPORTING DOCUMENT 5 NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Fourth Document - Traffic
        String trafficDoc = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
        if (trafficDoc.equalsIgnoreCase(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - TRAFFIC - Verification Passed", trafficDoc.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - TRAFFIC - verification Failed", trafficDoc.toString(), "", "");
            throw new Exception("TRAFFIC NAMING CONVENTION FAILED ");
        }

        //Click the Next page to view the Documents
        WebElement docPage4 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
        docPage4.click();
        Thread.sleep(10000);

        //Get the Name of the Third Document - Supporting Document 5
        String supportDoc6 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]/nobr/a")).getText();
        if (supportDoc6.equalsIgnoreCase(expDoc4)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 5 - Verification Passed", supportDoc6.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 5 - verification Failed", supportDoc6.toString(), "", "");
            throw new Exception("SUPPORTING DOCUMENT 5 NAMING CONVENTION FAILED ");
        }

//        //Get the Name of the Third Document - Supporting Document 5
//        String supportDoc7 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]/nobr/a")).getText();
//        if (supportDoc7.equalsIgnoreCase(expDoc4)) {
//            extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 5 - Verification Passed", supportDoc6.toString(), "", "");
//        } else {
//            extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 5 - verification Failed", supportDoc6.toString(), "", "");
//            throw new Exception("SUPPORTING DOCUMENT 5 NAMING CONVENTION FAILED ");
//        }


        //Get the Total Number of records
        String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
        if (totalDocuments.equalsIgnoreCase(expCount)) {
            extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
        } else {
            extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
            throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
        }



        //Click the First Page to view the Documents

        WebElement docPageFirst = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[1]/img"));
        docPageFirst.click();
        Thread.sleep(10000);

        //Click to download PDF Document
        String parenthandle = driver.getWindowHandle();
        WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[3]/nobr/a"));
        pdfDocOne.click();
        Thread.sleep(10000);
        driver.switchTo().window(parenthandle);
        driver.navigate().refresh();
        Thread.sleep(2000);
        Thread.sleep(10000);
        driver.switchTo().defaultContent();
        WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseIDone.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }


        //Select the customer name
        Thread.sleep(20000);
        WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
        custName.click();
        Thread.sleep(10000);

        //Customer Matching
        //Contact Type
        String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
        if (contactType.equalsIgnoreCase(expType)) {
            extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
            throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
        }
////        //Get the Business Phone Number
////        String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
////        if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
////            extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
////        } else {
////            extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
////            throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
////        }
////        //Get the Mobile Number
////        String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
////        if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
////            extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
////        } else {
////            extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
////            throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
////        }
////        //Get the Landline Number
////        String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
////        if (landPhone.equalsIgnoreCase(expAlternateNo)) {
////            extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
////        } else {
////            extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
////            throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
////        }
////
//        //Get the First Name
//        String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
//        if (firstName.equalsIgnoreCase(expFName)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
//            throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
//        }
//
//        //Get the Last Name OR Business Name
//        String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
//        if (lastName.equalsIgnoreCase(expLName)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLName, lastName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLName, lastName);
//            throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
//        }
        //Get the Email
        String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
        if (emailName.equalsIgnoreCase("Reshma.Nair@boroondara.vic.gov.au")) {
            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
        }
//
//
        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
        Thread.sleep(10000);


        //Navigate to the Customer Case Records Tab
        WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
        Thread.sleep(10000);
        customerCaseRecords.click();

        Thread.sleep(5000);

        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
        Logg.logger.info("Total Number of Cases:" + caseRecords);
//        if (caseRecords) {
//            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
//            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
//        }


        Thread.sleep(10000);
        navigateBack(driver);
        Thread.sleep(10000);
//
        driver.quit();


//        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/chromedriver.exe");
//        WebDriver driver1 = new ChromeDriver();
//        driver1.navigate().to("http://localhost:63342/e-forms_endtoend/test-output/ExtentReportResults.html");
//
//        driver1.manage().window().maximize();
//        driver1.navigate().refresh();
//        Thread.sleep(5000);
//        WebElement caseStatusDetails = driver1.findElement(By.xpath("//*[@id=\"test-collection\"]/li[2]/div[1]/span[1]"));
//        caseStatusDetails.click();
//
        return this;

    }


    @Test(dataProvider = "OOHP_DataProvider", description = "Verify that the CRM values are verified")
    public OOHPage captureCRMIndividual(String expCaseType, String expSubType, String expCustomerName, String expLocation,
                                        String expDoc1, String expDoc2, String expDoc3, String expDoc4,
                                        String expCount, String expType,
                                        String expBusinessNo, String expMobileNo, String expAlternateNo, String expFName,
                                        String expLName, String expEmail, String expCaseCount) throws Exception {


        Thread.sleep(15000);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.addArguments("window-size=1920,1080");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\rnair\\e-forms_endtoend\\src\\test\\resources\\chromedriver.exe");
//        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");
        System.out.println(driver.manage().window().getSize());
        driver = new ChromeDriver(options);
//        driver.manage().window().setSize(new org.openqa.selenium.Dimension(2100, 1200));
        driver.navigate().to("https://TestAutomation:s4G@3%@tv@eT@mel04crm.boroondara.vic.gov.au/CoBSIT/main.aspx");

//        driver.manage().window().setSize(new Dimension(1920, 1080));
////
        System.out.println(driver.manage().window().getSize());

        Thread.sleep(20000);
        WebElement sitemapHomeTab = driver.findElement(By.className("homeButtonImage"));
        sitemapHomeTab.click();
        Thread.sleep(4000);
        WebElement serviceAreaElement = driver.findElement(By.id("TabCS"));
        serviceAreaElement.click();
        Thread.sleep(10000);
        WebElement serviceRightLink = driver.findElement(By.xpath("//a[@id='nav_cases']"));
        serviceRightLink.click();
        Thread.sleep(10000);


        //Switch to the frame that contains the grid
        driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
        Thread.sleep(10000);


        Actions action = new Actions(driver);
        String identifierText = driver.findElement(By.xpath("//*[text()='" + GUID + "']")).getText();
        System.out.println("The GUID text in CRM is " + identifierText + "");
        WebElement identifier = driver.findElement(By.xpath("//*[text()='" + GUID + "']"));
        Logg.logger.info("Is the GUID correct:" + identifier);



        action.doubleClick(identifier).perform();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseID = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseID.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseName.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }
        Thread.sleep(5000);
        //Select the Sub-case Type
        String subCaseName = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casesubtype1\"]/div[1]/span[1]")).getText();
        System.out.println("The SubcaseType is " + subCaseName + "");
        if (subCaseName.equalsIgnoreCase(expSubType)) {
            extent_Pass(driver, "Case Name Verification Passed", subCaseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", subCaseName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CASE SUBTYPE NAME LEVEL ");
        }
        //Select the origin
        String originName = driver.findElement(By.xpath("//*[@id=\"header_process_caseorigincode1\"]/div[1]/span")).getText();
        Logg.logger.info("Origin name:" + originName);
        if (originName.equalsIgnoreCase("Web")) {
            extent_Pass(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
        } else {
            extent_Warn(driver, "ORIGIN Verification Passed", originName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ORIGIN LEVEL ");
        }
        //Select the customer name
        String customerName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]")).getText();
        Logg.logger.info("Customer name:" + customerName);

        if (customerName.equalsIgnoreCase(expCustomerName)) {
            extent_Pass(driver, "Customer name Verification Passed", customerName.toString(), "", "");
        } else {
            extent_Warn(driver, "Customer Name verification Failed", customerName.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE CUSTOMER NAME LEVEL ");
        }

        //Verify the Location Field
        String locationField = driver.findElement(By.xpath("//*[@id=\"PropertyQuickView_PropertyQuickView_account_address1_composite\"]/div[1]/span")).getText();
        if (locationField.contains(expLocation)) {
            extent_Pass(driver, "Location Verification Passed", locationField.toString(), "", "");
        } else {
            extent_Warn(driver, "Location verification Failed", locationField.toString(), "", "");
            throw new Exception("TEST CASE FAILED AT THE ADDRESS LEVEL ");
        }

//        //Verify the Instructions Field
//        String instructionsField = driver.findElement(By.xpath("//*[@id=\"description\"]/div[1]/span")).getText();
//
//        if (instructionsField.equalsIgnoreCase(Instructions)) {
//            extent_Pass(driver, "Instruction Verification Passed", instructionsField.toString(), "", "");
//        } else {
//            extent_Warn(driver, "Instruction verification Failed", instructionsField.toString(), "", "");
//            throw new Exception("TEST CASE FAILED AT THE INSTRUCTIONS LEVEL ");
//        }

        Thread.sleep(5000);



        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to the Case ESCALATION TAB
        WebElement caseTabSelected = driver.findElement(By.id("tab_casestatus"));
        Thread.sleep(10000);
        caseTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String firstResponse = driver.findElement(By.xpath("//*[@id=\"firstresponseslaquickform_firstresponseslaquickform_slakpiinstance_First_Response_In\"]/div/span[2]")).getText();
        Logg.logger.info("First Response is:" + firstResponse);

        //Check if the Timer is not ticking and First response is Succeeded
        if (firstResponse.equalsIgnoreCase("Succeeded")) {
            extent_Pass(driver, "FIRST RESPONSE- Verification Passed", firstResponse.toString(), "", "");
        } else {
            extent_Warn(driver, "FIRST RESPONSE - verification Failed", firstResponse.toString(), "", "");
            throw new Exception(" VERIFICATION OF FIRST RESPONSE FAILED ");
        }


        Thread.sleep(5000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);


        //Navigate to the PAYMENTS TAB
        WebElement payTabSelected = driver.findElement(By.id("tab_paymentrecords"));
        Thread.sleep(10000);
        payTabSelected.click();

        driver.switchTo().parentFrame();
        //Get the first response value
        String receiptNumber = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[3]/div")).getText();
//        String compReceiptNumber = expReceiptNumber.substring(16);
//        System.out.println("The Trimmed RN is " + compReceiptNumber + "");
        if (receiptNumber.contains(compReceiptNumber)) {
            extent_Pass(driver, "Payment receipt- Verification Passed", receiptNumber.toString(), compReceiptNumber, receiptNumber);
        } else {
            extent_Warn(driver, "Payment receipt - verification Failed", receiptNumber.toString(), compReceiptNumber, receiptNumber);
            throw new Exception(" VERIFICATION OF RECEIPT FAILED ");
        }

        String amount = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[5]/div")).getText();
        if (amount.equalsIgnoreCase("$188.00")) {
            extent_Pass(driver, "Amount- Verification Passed", amount.toString(), "", "");
        } else {
            extent_Warn(driver, "Amount - verification Failed", amount.toString(), "", "");
            throw new Exception(" VERIFICATION OF AMOUNT FAILED ");
        }

        String paidStatus = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[11]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr/td[6]/nobr")).getText();
        if (paidStatus.equalsIgnoreCase("Paid")) {
            extent_Pass(driver, "Payment Status- Verification Passed", paidStatus.toString(), "", "");
        } else {
            extent_Warn(driver, "Payment Status - verification Failed", paidStatus.toString(), "", "");
            throw new Exception(" VERIFICATION OF Payment Status FAILED ");
        }



        Thread.sleep(5000);



        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        Thread.sleep(10000);

        //Navigate to Notes
        WebElement notesTabSelected = driver.findElement(By.id("tab_notes"));
        Thread.sleep(10000);
        notesTabSelected.click();
//        Thread.sleep(20000);

        //Navigate to Activities
        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        WebElement actTabSelected = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[8]/table/tbody/tr[2]/td/div/span/div[1]/a[2]"));
//        Thread.sleep(10000);
        actTabSelected.click();
        Thread.sleep(10000);


        String emailTrigger = driver.findElement(By.xpath("(//img[@alt='Email'])[3]")).getAttribute("src");
        Thread.sleep(20000);
        Logg.logger.info("Email:" + emailTrigger);




//        //Select the customer name
//        Thread.sleep(20000);
//        WebElement custName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[1]/div/table/tbody/tr[9]/td[2]/div[1]/div[1]/span[1]"));
//        custName.click();
//        Thread.sleep(10000);

        //WAIT FOR DOCUMENTS TO UPLOAD - REFRESH
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.switchTo().defaultContent();
        WebElement caseIDRefresh = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseIDRefresh.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Thread.sleep(10000);

        //DOCUMENT DETAILS
        //Navigate to the Document Tabs
        Thread.sleep(20000);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabsbybusinessrule")));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        WebElement documentTabSelected = driver.findElement(By.id("tab_DocumentLinks"));
        Thread.sleep(10000);
        if (documentTabSelected.isDisplayed()) {
            extent_Pass(driver, "Document Tab Verification Passed", documentTabSelected.toString(), "", "");
        } else {
            extent_Warn(driver, "Document Tab Verification Failed", documentTabSelected.toString(), "", "");
            throw new Exception("DOCUMENT TAB IS NOT DISPLAYED ");
        }
        documentTabSelected.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        Thread.sleep(10000);
        driver.switchTo().parentFrame();
        Thread.sleep(5000);


        //Get the Name of the First Document - PDF Document
        String pdfDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (pdfDocName.equalsIgnoreCase(expDoc1)) {
            extent_Pass(driver, "NAME FORMAT - PDF - Verification Passed", pdfDocName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - PDF - verification Failed", pdfDocName.toString(), "", "");
            throw new Exception("PDF NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Second Document - Certificate
        String certName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
        if (certName.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - CERTIFICATE - Verification Passed", certName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - CERTIFICATE - verification Failed", certName.toString(), "", "");
            throw new Exception("CERTIFICATE NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Third Document - Dial before you Dig
        String dialName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
        if (dialName.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - DIAL - Verification Passed", dialName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - DIAL - verification Failed", dialName.toString(), "", "");
            throw new Exception("DIAL NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Fourth Document - Notification
        String notificationName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
        if (notificationName.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - NOTIFICATION - Verification Passed", notificationName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - NOTIFICATION - verification Failed", notificationName.toString(), "", "");
            throw new Exception("NOTIFICATION NAMING CONVENTION FAILED ");
        }


        //Click the Next page to view the Documents

        WebElement docPage2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
        docPage2.click();
        Thread.sleep(10000);

//        driver.switchTo().parentFrame();



        //Get the Document details from the Second Page
        //Get the Name of the First Document - Receipt
//        String ossReceiptName = expReceipt + " + "+ compReceiptNumber;
//        Logg.logger.info("Receipt Format:" + ossReceiptName);

//        String receiptDocName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
//        if (receiptDocName.contains(ossReceiptName)) {
//            extent_Pass(driver, "NAME FORMAT - RECEIPT - Verification Passed", receiptDocName.toString(), "", "");
//        } else {
//            extent_Warn(driver, "NAME FORMAT - RECEIPT - verification Failed", receiptDocName.toString(), "", "");
//            throw new Exception("RECEIPT NAMING CONVENTION FAILED ");
//        }
        //Get the Name of the Second Document - Site
        String siteName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (siteName.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - SITE - Verification Passed", siteName.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SITE - verification Failed", siteName.toString(), "", "");
            throw new Exception("SITE NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Third Document - Supporting Document 1
        String supportDoc1 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
        if (supportDoc1.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 1 - Verification Passed", supportDoc1.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 1 - verification Failed", supportDoc1.toString(), "", "");
            throw new Exception("SUPPORTING DOCUMENT 1 NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Fourth Document - Supporting Document 2
        String supportDoc2 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
        if (supportDoc2.equalsIgnoreCase(expDoc2)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 2 - Verification Passed", supportDoc2.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 2 - verification Failed", supportDoc2.toString(), "", "");
            throw new Exception("SUPPORTING DOCUMENT 2 NAMING CONVENTION FAILED ");
        }

        //Click the Next page to view the Documents
        WebElement docPage3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
        docPage3.click();
        Thread.sleep(10000);

        //Get the Document details from the Third Page
        //Get the Name of the First Document - Supporting Document 3
        String supportDoc3 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (supportDoc3.contains(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORT 3 - Verification Passed", supportDoc3.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORT 3 - verification Failed", supportDoc3.toString(), "", "");
            throw new Exception("SUPPORT 3 NAMING CONVENTION FAILED ");
        }
        //Get the Name of the Second Document - Supporting Document 4
        String supportDoc4 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
        if (supportDoc4.equalsIgnoreCase(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORT 4 - Verification Passed", supportDoc4.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORT 4 - verification Failed", supportDoc4.toString(), "", "");
            throw new Exception("SUPPORT 4 NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Third Document - Supporting Document 5
        String supportDoc5 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[3]/td[2]")).getText();
        if (supportDoc5.equalsIgnoreCase(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORTING DOCUMENT 5 - Verification Passed", supportDoc5.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORTING DOCUMENT 5 - verification Failed", supportDoc5.toString(), "", "");
            throw new Exception("SUPPORTING DOCUMENT 5 NAMING CONVENTION FAILED ");
        }

        //Get the Name of the Fourth Document - Traffic
        String trafficDoc = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[4]/td[2]")).getText();
        if (trafficDoc.equalsIgnoreCase(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - TRAFFIC - Verification Passed", trafficDoc.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - TRAFFIC - verification Failed", trafficDoc.toString(), "", "");
            throw new Exception("TRAFFIC NAMING CONVENTION FAILED ");
        }


        //Click the Next page to view the Documents
        WebElement docPage4 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[3]/img"));
        docPage4.click();
        Thread.sleep(10000);

        //Get the Document details from the Third Page
        //Get the Name of the First Document - Supporting Document 3
        String supportDoc10 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[2]")).getText();
        if (supportDoc10.contains(expDoc3)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORT 3 - Verification Passed", supportDoc10.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORT 3 - verification Failed", supportDoc10.toString(), "", "");
            throw new Exception("SUPPORT 3 NAMING CONVENTION FAILED ");
        }
        //Get the Name of the Second Document - Supporting Document 4
        String supportDoc11 = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[2]/td[2]")).getText();
        if (supportDoc11.equalsIgnoreCase(expDoc4)) {
            extent_Pass(driver, "NAME FORMAT - SUPPORT 4 - Verification Passed", supportDoc11.toString(), "", "");
        } else {
            extent_Warn(driver, "NAME FORMAT - SUPPORT 4 - verification Failed", supportDoc11.toString(), "", "");
            throw new Exception("SUPPORT 4 NAMING CONVENTION FAILED ");
        }




        //Get the Total Number of records
        String totalDocuments = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[1]/span[3]/span")).getText();
        if (totalDocuments.equalsIgnoreCase(expCount)) {
            extent_Pass(driver, "TOTAL NUMBER OF DOCUMENTS- Verification Passed", totalDocuments.toString(), "", "");
        } else {
            extent_Warn(driver, "TOTAL NUMBER OF DOCUMENTS - verification Failed", totalDocuments.toString(), "", "");
            throw new Exception(" VERIFICATION OF TOTAL NUMBER OF DOCUMENTS FAILED ");
        }


        //Click the First Page to view the Documents

        WebElement docPageFirst = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[3]/div/table/tbody/tr/td[2]/a[1]/img"));
        docPageFirst.click();
        Thread.sleep(10000);

        //Click to download PDF Document
        String parenthandle = driver.getWindowHandle();
        WebElement pdfDocOne = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div[1]/div/div/div/div[1]/div[2]/div[2]/div[9]/table/tbody/tr[2]/td/div/span/div/div[2]/div/div/div[1]/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[3]/nobr/a"));
        pdfDocOne.click();
        Thread.sleep(10000);
        driver.switchTo().window(parenthandle);
        driver.navigate().refresh();
        Thread.sleep(10000);
        driver.switchTo().defaultContent();
        WebElement caseIDone = driver.findElement(By.xpath("//*[@id=\"TabNode_tab0Tab-main\"]/a/span/span"));
        caseIDone.click();

        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(0);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        //CASE AND CUSTOMER DETAILS
        //Select the CaseType name
        String caseNameRefresh = driver.findElement(By.xpath("//*[@id=\"header_process_ecl_casetype1\"]/div[1]/span[1]")).getText();
        if (caseNameRefresh.equalsIgnoreCase(expCaseType)) {
            extent_Pass(driver, "Case Name Verification Passed", caseName.toString(), "", "");
        } else {
            extent_Warn(driver, "Case Name verification Failed", caseName.toString(), "", "");
            System.out.println("The Actual Case Name in CRM is " + caseName + "");
            System.out.println("The expected Case Name is " + expCaseType + "");
            throw new Exception("TEST CASE FAILED AT THE CASE NAME LEVEL ");

        }



        //Select the customer name
        Thread.sleep(20000);
        WebElement custName = driver.findElement(By.xpath("//*[@id=\"ecl_complainant_new\"]/div[1]/span[1]"));
        custName.click();
        Thread.sleep(10000);

        //Customer Matching
        //Contact Type
        String contactType = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[4]/td[2]/div/div[1]/span")).getText();
        if (contactType.equalsIgnoreCase(expType)) {
            extent_Pass(driver, "CUSTOMER MATCHING - CONTACT TYPE- Verification Passed", contactType.toString(), expType, contactType);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - CONTACT TYPE- verification Failed", contactType.toString(), expType, contactType);
            throw new Exception(" VERIFICATION OF CONTACT TYPE FAILED ");
        }
//        //Get the Business Phone Number
//        String businessPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[8]/td[2]/div/div[1]/span/a")).getText();
//        if (businessPhone.equalsIgnoreCase(expBusinessNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- Verification Passed", businessPhone.toString(), expBusinessNo, businessPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - BUSINESS NUMBER- verification Failed", businessPhone.toString(), expBusinessNo, businessPhone);
//            throw new Exception(" VERIFICATION OF BUSINESS NUMBER FAILED ");
//        }
//        //Get the Mobile Number
//        String mobilePhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[9]/td[2]/div/div[1]/span/a")).getText();
//        if (mobilePhone.equalsIgnoreCase(expMobileNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - MOBILE NUMBER- Verification Passed", mobilePhone.toString(), expMobileNo, mobilePhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - MOBILE NUMBER- verification Failed", mobilePhone.toString(), expMobileNo, mobilePhone);
//            throw new Exception(" VERIFICATION OF MOBILE NUMBER FAILED ");
//        }
//        //Get the Landline Number
//        String landPhone = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[10]/td[2]/div/div[1]/span")).getText();
//        if (landPhone.equalsIgnoreCase(expAlternateNo)) {
//            extent_Pass(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- Verification Passed", landPhone.toString(), expAlternateNo, landPhone);
//        } else {
//            extent_Warn(driver, "CUSTOMER MATCHING - ALTERNATE NUMBER- verification Failed", landPhone.toString(), expAlternateNo, landPhone);
//            throw new Exception(" VERIFICATION OF ALTERNATE NUMBER FAILED ");
//        }

        //Get the First Name
        String firstName = driver.findElement(By.xpath(" /html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[5]/td[2]/div/div[1]/span")).getText();
        if (firstName.equalsIgnoreCase(expFName)) {
            extent_Pass(driver, "CUSTOMER MATCHING - FIRST NAME- Verification Passed", firstName.toString(), expFName, firstName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - FIRST NAME- verification Failed", firstName.toString(), expFName, firstName);
            throw new Exception(" VERIFICATION OF FIRST NAME FAILED ");
        }

        //Get the Last Name OR Business Name
        String lastName = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[7]/td[2]/div/div[1]")).getText();
        if (lastName.equalsIgnoreCase(expLName)) {
            extent_Pass(driver, "CUSTOMER MATCHING - LAST NAME- Verification Passed", lastName.toString(), expLName, lastName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - LAST NAME- verification Failed", lastName.toString(), expLName, lastName);
            throw new Exception(" VERIFICATION OF LAST NAME FAILED ");
        }
        //Get the Email
        String emailName = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[11]/td[2]/div/div[1]")).getText();
        if (emailName.equalsIgnoreCase(expEmail)) {
            extent_Pass(driver, "CUSTOMER MATCHING - EMAIL- Verification Passed", emailName.toString(), expEmail, emailName);
        } else {
            extent_Warn(driver, "CUSTOMER MATCHING - EMAIL- verification Failed", emailName.toString(), expEmail, emailName);
            throw new Exception(" VERIFICATION OF EMAIL FAILED ");
        }


        Thread.sleep(5000);


        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.switchTo().frame(driver.findElement(By.id("WebResource_formtabs")));
        Thread.sleep(10000);


        //Navigate to the Customer Case Records Tab
        WebElement customerCaseRecords = driver.findElement(By.id("CUSTOMER_DETAILS_TAB"));
        Thread.sleep(10000);
        customerCaseRecords.click();

        Thread.sleep(5000);

        driver.switchTo().parentFrame();
        Thread.sleep(10000);
        String caseRecords = driver.findElement(By.xpath("//*[@id=\"tab_contactcasessgrid_ItemsTotal\"]")).getText();
        Logg.logger.info("Total Number of Cases:" + caseRecords);


        Thread.sleep(10000);
        navigateBack(driver);
        Thread.sleep(10000);

        driver.quit();

        return this;
    }

    @Test(dataProvider = "OOH_DataProvider", description = "Verify that the CRM values are verified")

    public OOHPage readPDF(String pdf, String pdfText) throws Exception {
        Logg.logger.info("");

        PDFReader pdfManager = new PDFReader();

        pdfManager.setFilePath("C:\\Users\\rnair\\Downloads\\" +pdf+ ".pdf");
        String text = pdfManager.toText();

        if (text.contains(pdfText)) {
            extent_Pass(driver, "PDF VERIFICATION - Verification Passed", text.toString(), "", "");
        } else {
            extent_Warn(driver, "PDF VERIFICATION -verification Failed", text.toString(), "", "");
            throw new Exception(" PDF VERIFICATION - FAILED ");
        }
        return this;
    }

    @DataProvider

    public Object[][] OOH_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"OOH");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"OOH",55);

        return testObjArray;
    }
    @DataProvider
    public Object[][] OOHP_DataProvider() throws Exception {
        // Setting up the Test Data Excel file

        ExcelUtils.setExcelFile(System.getProperty("user.dir")+"/TestData//"+formsTestData,"OOH");

        // Fetching the Test Case row number from the Test Data Sheet

        Object testObjArray[][] = ExcelUtils.getTableArray(System.getProperty("user.dir")+"/TestData//"+formsTestData,"OOH",55);

        return testObjArray;
    }

}
