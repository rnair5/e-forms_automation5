package pageObjects.frontend;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;
import pageObjects.frontend.components.JobListingItem;

public class CareersListingPage extends BasePageClass {
    public CareersListingPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
        jobListingItem = new JobListingItem(driver);
    }

    public JobListingItem jobListingItem;
}
