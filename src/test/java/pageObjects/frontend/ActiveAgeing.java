package pageObjects.frontend;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;
import pageObjects.frontend.components.ActivityCategoryTile;
import pageObjects.frontend.components.FeatureFactBanner;
import pageObjects.frontend.components.ServiceTiles;
import pageObjects.frontend.components.TopicTiles;

import java.util.List;

import static libraries.ExtentReporting.extent_Failure;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.Utilities.*;

public class ActiveAgeing extends BasePageClass {
    public ActiveAgeing(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
        featureFactBanner = new FeatureFactBanner(driver);
        serviceTiles = new ServiceTiles(driver);
        topicTiles = new TopicTiles(driver);
        activityCategoryTile = new ActivityCategoryTile(driver);
    }
    @FindBy(css=".active-ageing-landing-hero")
    private WebElement activeAgeingLandingHero;

    @FindBy(id="edit-search-term")
    private WebElement searchTextBox;

    @FindBy(css="div.active-ageing-landing-hero__tags>a")
    private List<WebElement> categoryTags;

    public FeatureFactBanner featureFactBanner;

    public ServiceTiles serviceTiles;
    public TopicTiles topicTiles;
    public ActivityCategoryTile activityCategoryTile;

    public boolean isActiveAgeingHeroDisplayed()
    {
        Logg.logger.info("");
        if(waitUntilElementIsVisible(driver,activeAgeingLandingHero) == null)
            return false;
        return true;
    }
    public void enterSearchText(String text)
    {
        Logg.logger.info("text:"+text);
        waitAndClick(driver,searchTextBox)
                .sendKeys(text);
    }
    public boolean isCategoryTagsDisplayed()
    {
        Logg.logger.info("");
        enterSearchText("");
        if (waitUntilElementIsVisible(driver,categoryTags).size() <=0)
            return false;
        return true;
    }

    public void selectCategory(String category)
    {
        Logg.logger.info("Category:"+category);
        boolean found=false;
        if(waitUntilElementIsVisible(driver,categoryTags).size()>0) {
            for (int i = 0; i < categoryTags.size(); i++) {
                if (categoryTags.get(i).getAttribute("data-title").equalsIgnoreCase(category)) {
                    found = true;
                    categoryTags.get(i).click();
                    extent_Pass(driver, "Found the category:", "", category, category);
                    break;
                }
            }
            if (found == false)
                extent_Failure(driver, "Could not find the category requested:", "", category, "");
        }
        else
            extent_Failure(driver,"The category tags are not displayed in the active ageing landing page search box","","","");
    }
    public String selectCategory(int index)
    {
        Logg.logger.info("Index:"+index);
        try {
            if (waitUntilElementIsVisible(driver, categoryTags).size() > 0) {
                String categorySelected = categoryTags.get(index).getText().trim();
                categoryTags.get(index).click();
                extent_Pass(driver, "Category tags are displayed and the given category is clicked", Integer.toString(index), "",categorySelected);
                return categorySelected;
            }
            else {
                extent_Failure(driver,"Category tags are not displayed for the search",categoryTags.toString(),"","");
            }
        }catch(Exception e)
        {
            extent_Failure(driver,"The requested category is not found", Integer.toString(index),"","");
        }
        return null;
    }
}
