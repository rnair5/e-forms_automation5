package pageObjects.frontend;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;
import static libraries.Utilities.*;

public class MegaMenu extends BasePageClass {
    public  MegaMenu(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    public void navigateToMainMenu(String menuItem)
    {
        Logg.logger.info("");
        waitAndClick(driver, By.linkText(menuItem));
    }
    public void navigateToSubMenu(String mainMenu,String subMenu)
    {
        Logg.logger.info("");
        hover_On_Element(driver,By.linkText(mainMenu));
        waitAndClick(driver,By.linkText(subMenu));
        waitForPageLoaded(driver);
    }
}
