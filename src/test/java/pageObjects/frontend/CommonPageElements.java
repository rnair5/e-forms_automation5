package pageObjects.frontend;

import libraries.Logg;

import static libraries.ExtentReporting.extent_Failure;
import static libraries.Utilities.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

public class CommonPageElements extends BasePageClass {
    public CommonPageElements(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(css="footer.global-footer")
    private WebElement footer;

    @FindBy(css="form#contact-message-feedback-form")
    private WebElement feedbackForm;

    @FindBy(css="nav.pager")
    private WebElement pageNavigation;

    @FindBy(css="nav.pager .pager__item--next a")
    private WebElement nextNavigation;

    @FindBy(css ="#readspeaker_button a.rsbtn_play")
    private WebElement readSpeakerButtonPlay;
    @FindBy(css ="#readspeaker_button a[title='Pause']")
    private WebElement readSpeakerButtonPause;

    public boolean isReadSpeakerPresent()
    {
        Logg.logger.info("");
        try{
            waitUntilElementIsVisible(driver,readSpeakerButtonPlay);
            return true;
        } catch (Exception e)
        {
            return false;
        }
    }

    public void clickReadSpeaker()
    {
        Logg.logger.info("");
        waitAndClick(driver,readSpeakerButtonPlay);
        waitUntilElementIsVisible(driver,readSpeakerButtonPause);
    }

    public boolean isFooterPresent()
    {
        Logg.logger.info("");
        try{
            waitUntilElementIsVisible(driver,footer);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean isFeedbackFormPresent()
    {
        Logg.logger.info("");
        try{
            waitUntilElementIsVisible(driver,feedbackForm);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    public boolean isPageNavigationPresent()
    {
        Logg.logger.info("");
        try{
            waitUntilElementIsVisible(driver,pageNavigation);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    public void clickNextNavigation()
    {
        try {
        Logg.logger.info("");
        if(isPageNavigationPresent()) {
            waitAndClick(driver, nextNavigation);
            waitForPageLoaded(driver);
        }
        else
            extent_Failure(driver,"Page navigation is not present in the page","","","");
        }
        catch(Exception e)
        {
            extent_Failure(driver,"Unable to find/click the next button on page navigation","","","");
        }
    }
}
