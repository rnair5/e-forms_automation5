package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.List;

import static libraries.ExtentReporting.extent_Failure;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Pass_NoScreenshot;
import static libraries.Utilities.waitAndClick;
import static libraries.Utilities.waitForPageLoaded;
import static libraries.Utilities.waitUntilElementIsVisible;

public class ActivityCategoryTile extends BasePageClass {
    public ActivityCategoryTile(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "paragraph--type--para-activity-category-tile")
    private WebElement categoryTileSection;

    @FindBy(css = ".paragraph--type--para-activity-category-tile .field--name-field-para-section-heading")
    private WebElement categoryTileTitle;

    @FindBy(css = ".paragraph--type--para-activity-category-tile .arrow-link a")
    private WebElement categoryTileLink;

    @FindBy(className = "activity-category-tile-container")
    private List<WebElement> activityCategoryTiles;

    public String getActivityCategoryTile_Title() {
        Logg.logger.info("");
        try {
            String result= waitUntilElementIsVisible(driver,categoryTileTitle)
                    .getText();
            extent_Pass(driver,"The Activity Category tile has a title","","",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"The Activity Category title is not present","","","");
            return null;
        }
    }

    public String getActivityCategoryTile_LinkUrl() {
        Logg.logger.info("");
        try {
            String result= waitUntilElementIsVisible(driver,categoryTileLink)
                    .getAttribute("href");
            extent_Pass_NoScreenshot(driver,"The Activity Category tile has a view all URL","","",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"The Activity Category tile doesn't have a view all URL","","","");
            return null;
        }
    }

    public String getActivityCategoryTile_LinkText() {
        Logg.logger.info("");
        try {
            String result= waitUntilElementIsVisible(driver,categoryTileLink)
                    .getText();
            extent_Pass_NoScreenshot(driver,"The Activity Category tile has a view all URL linktext","","",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"The Activity Category tile doesn't have a view all URL lintext","","","");
            return null;
        }
    }
    public void clickActivityCategoryTile_LinkUrl() {
        Logg.logger.info("");
        waitAndClick(driver,categoryTileLink);
        waitForPageLoaded(driver);
    }
    public int getActivityCategories() {
        Logg.logger.info("");
        try{
            int result= waitUntilElementIsVisible(driver,activityCategoryTiles).size();
            extent_Pass(driver,"Activity Category Tiles are present on the page","","",Integer.toString(result));
            return result;
        }
        catch(Exception e) {
            extent_Failure(driver,"Activity Category Tiles are not present on the page","","","");
            return 0;
        }
    }
    public boolean isActivityCategoryTileSectionDisplayed()
    {
        Logg.logger.info("");
        try{
            waitUntilElementIsVisible(driver,categoryTileSection);
            extent_Pass(driver,"Activity Category Tile section is present on the page","","",categoryTileSection.toString());
            return true;
        } catch(Exception e) {
            return false;
        }
    }
    public String getActivityCategory_Title(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,activityCategoryTiles).
                    get(index).
                    findElement(By.cssSelector(".content h3")).
                    getText();
            extent_Pass(driver,"Activity Category at index has a title","Index:"+Integer.toString(index),"",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"Activity Category  heading is not present at index", "Index:"+index,"","");
            return null;
        }
    }
    public String getActivityCategory_Description(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,activityCategoryTiles).
                    get(index).
                    findElement(By.cssSelector(".content .field--name-description p")).
                    getText();
            extent_Pass_NoScreenshot(driver,"Activity Category at index has a description","Index:"+Integer.toString(index),"",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"Activity Category description is not present at index", "Index:"+index,"","");
            return null;
        }
    }
    public String getActivityCategory_Link(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,activityCategoryTiles).
                    get(index).
                    findElement(By.tagName("a")).
                    getAttribute("href");
            extent_Pass_NoScreenshot(driver,"Activity Category at index has a link","Index:"+Integer.toString(index),"",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"Activity Category link is not present at index", "Index:"+index,"","");
            return null;
        }
    }
    public void clickActivityCategory_Link(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            waitUntilElementIsVisible(driver,activityCategoryTiles).
                    get(index).
                    findElement(By.tagName("a")).
                    click();
            waitForPageLoaded(driver);
            extent_Pass_NoScreenshot(driver,"Navigated to Activity Category at index","Index:"+Integer.toString(index),"","");
        } catch(Exception e)
        {
            extent_Failure(driver,"Navigation to Activity Category link failed", "Index:"+index,"","");
        }
    }

    public String getActivityCategory_ImageTitle(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,activityCategoryTiles).
                    get(index).
                    findElement(By.tagName("img")).
                    getAttribute("title");
            extent_Pass_NoScreenshot(driver,"Activity Category at index has an image","Index:"+Integer.toString(index),"",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"Activity Category image is not present at index", "Index:"+index,"","");
            return null;
        }
    }

}
