package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import static libraries.ExtentReporting.extent_Failure;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Pass_NoScreenshot;
import static libraries.Utilities.waitAndClick;
import static libraries.Utilities.waitForPageLoaded;
import static libraries.Utilities.waitUntilElementIsVisible;

public class FeatureFactBanner extends BasePageClass {
    public FeatureFactBanner(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    @FindBy(css="div.featured-fact-banner")
    private WebElement featureFactBanner;

    @FindBy(css="div.featured-fact-banner h2.field--name-field-para-title")
    private WebElement featureFactBannerTitle;

    @FindBy(css="div.featured-fact-banner .field--name-field-para-subtitle")
    private WebElement featureFactBannerSubtitle;

    @FindBy(css="div.featured-fact-banner .field--name-field-para-text-long")
    private WebElement featureFactBannerDesc;

    @FindBy(css="div.featured-fact-banner div.arrow-link a")
    private WebElement featureFactBannerLink;

    public boolean isFeatureFactBannerDisplayed()
    {
        Logg.logger.info("");
        try {
            waitUntilElementIsVisible(driver,featureFactBanner);
            return true;
        } catch (Exception e)
        {
            return false;
        }
    }
    public String getFeatureFactBanner_Title()
    {
        Logg.logger.info("");
        try {
            String result=waitUntilElementIsVisible(driver,featureFactBannerTitle).
                    getText();
            extent_Pass(driver,"Feature fact banner title is present","","",result);
            return result;
        } catch (Exception e)
        {
            extent_Failure(driver,"Feature fact banner title is not present","","","");
            return null;
        }
    }
    public String getFeatureFactBanner_SubTitle()
    {
        Logg.logger.info("");
        try {
            String result=waitUntilElementIsVisible(driver,featureFactBannerSubtitle).
                    getText();
            extent_Pass_NoScreenshot(driver,"Feature fact banner subtitle is present","","",result);
            return result;
        } catch (Exception e)
        {
            extent_Failure(driver,"Feature fact banner subtitle is not present","","","");
            return null;
        }
    }
    public String getFeatureFactBanner_Description()
    {
        Logg.logger.info("");
        try {
            String result=waitUntilElementIsVisible(driver,featureFactBannerDesc).
                    getText();
            extent_Pass_NoScreenshot(driver,"Feature fact banner Description is present","","",result);
            return result;
        } catch (Exception e)
        {
            extent_Failure(driver,"Feature fact banner Description is not present","","","");
            return null;
        }
    }
    public String getFeatureFactBanner_Link()
    {
        Logg.logger.info("");
        try {
            String result=waitUntilElementIsVisible(driver,featureFactBannerLink).
                    getAttribute("href");
            extent_Pass_NoScreenshot(driver,"Feature fact banner Link is present","","",result);
            return result;
        } catch (Exception e)
        {
            extent_Failure(driver,"Feature fact banner Link is not present","","","");
            return null;
        }
    }
    public void clickOnFeatureFactBanner_Link()
    {
        Logg.logger.info("");
        try {
            waitAndClick(driver,featureFactBannerLink);
            waitForPageLoaded(driver);
            extent_Pass(driver,"Navigated to the feature fact banner link","","","");
        } catch (Exception e)
        {
            extent_Failure(driver,"Could not navigate to the feature fact banner link","","","");
        }
    }
}
