package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.List;

import static libraries.Utilities.waitAndClick;
import static libraries.Utilities.waitForPageLoaded;
import static libraries.Utilities.waitUntilElementIsVisible;

public class IconGrid extends BasePageClass {
    public IconGrid(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(css=".paragraph--type--para-icon-grid")
    private WebElement iconGrid;

    @FindBy(css=".paragraph--type--para-icon-grid .content-tile-listing-title h2")
    private WebElement iconGridTitle;

    @FindBy(css=".paragraph--type--para-icon-grid .content-tile-listing-title a")
    private WebElement iconGridLink;

    @FindBy(css=".field--name-field-para-icons a")
    private List<WebElement> icons;

    public boolean isIconGridPresent()
    {
        Logg.logger.info("");
        try {
            waitUntilElementIsVisible(driver,iconGrid);
            return true;
        } catch(Exception e)
        {
            return false;
        }
    }

    public String getIconGridTitle()
    {
        Logg.logger.info("");
        try{
        return waitUntilElementIsVisible(driver,iconGridTitle).getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getIconGridLinkText()
    {
        Logg.logger.info("");
        try{
        return waitUntilElementIsVisible(driver,iconGridLink).getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getIconGridLinkUrl()
    {
        Logg.logger.info("");
        try {
        return waitUntilElementIsVisible(driver,iconGridLink).getAttribute("href");
        } catch(Exception e) {
            return null;
        }
    }
    public void clickIconGridLinkUrl()
    {
        Logg.logger.info("");
        waitAndClick(driver,iconGridLink);
        waitForPageLoaded(driver);
    }
    public String getIconTitle(int index)
    {
        Logg.logger.info("index:"+index);
        try{
        return waitUntilElementIsVisible(driver,icons)
                .get(index)
                .findElement(By.tagName("h3"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public int getIcons()
    {
        Logg.logger.info("");
        try {
        return waitUntilElementIsVisible(driver,icons)
                .size();
        } catch(Exception e) {
            return 0;
        }
    }
    public boolean isIconImagePresent(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            waitUntilElementIsVisible(driver,icons)
                    .get(index)
                    .findElement(By.tagName("svg"));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public String getIconDescription(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver, icons)
                    .get(index)
                    .findElement(By.className("field--name-field-para-text-long"))
                    .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getIconLink(int index)
    {
        Logg.logger.info("index:"+index);
        try {
        return waitUntilElementIsVisible(driver,icons)
                .get(index)
                .getAttribute("href");
    } catch(Exception e) {
        return null;
    }
    }
    public void clickIconLink(int index)
    {
        Logg.logger.info("index:"+index);
        waitAndClick(driver,icons.get(index));
        waitForPageLoaded(driver);
    }
}
