package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import static libraries.Utilities.waitAndSendKeys;

public class ParagraphSingleLink extends BasePageClass {
    public ParagraphSingleLink(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }

    @FindBy(css="[data-drupal-selector='edit-field-content-components-0-subform-field-para-link-0-uri']")
    private WebElement linkUrl;
    @FindBy(css="[data-drupal-selector='edit-field-content-components-0-subform-field-para-link-0-title']")
    private WebElement linkText;

    public ParagraphSingleLink setUrl(String url)
    {
        Logg.logger.info("url:"+url);
        waitAndSendKeys(driver,linkUrl,url);
        return this;
    }
    public ParagraphSingleLink setLinkText(String linkText)
    {
        Logg.logger.info("linkText:"+linkText);
        waitAndSendKeys(driver,this.linkText,linkText);
        return this;
    }
}
