package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.List;

import static libraries.Utilities.selectFirstPublishedMenuItem;
import static libraries.Utilities.selectMenuItem;
import static libraries.Utilities.waitAndSendKeys;

public class ContentTileReferenced extends BasePageClass {
    public ContentTileReferenced(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(css="input[data-drupal-selector='edit-field-content-components-0-subform-field-para-title-0-value']")
    private WebElement backendTitle;
    @FindBy(css="input[data-drupal-selector='edit-field-content-components-0-subform-field-para-link-0-uri']")
    private WebElement backendUrl;
    @FindBy(css="input[data-drupal-selector='edit-field-content-components-0-subform-field-para-link-0-title']")
    private WebElement backendLinkText;
    @FindBy(xpath="//input[contains(@data-drupal-selector,'edit-field-content-components-0-subform-field-para-tiles-')]")
    private List<WebElement> backendTiles;

    public ContentTileReferenced setTitle(String title)
    {
        Logg.logger.info("Title"+title);
        waitAndSendKeys(driver,backendTitle,title);
        return this;
    }
    public ContentTileReferenced setUrl(String url)
    {
        Logg.logger.info("Url:"+url);
        waitAndSendKeys(driver,backendUrl,url);
        selectFirstPublishedMenuItem(driver,url);
        return this;
    }
    public ContentTileReferenced setLinkText(String text)
    {
        Logg.logger.info("Linktext:"+text);
        waitAndSendKeys(driver,backendLinkText,text);
        return this;
    }
    public ContentTileReferenced setTile(int index,String text)
    {
        Logg.logger.info(String.format("Tile:%s Tile text:%s",index,text));
        waitAndSendKeys(driver,backendTiles.get(index),text);
        selectMenuItem(driver,text,index);
        return this;
    }
}
