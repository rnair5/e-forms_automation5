package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.List;

import static libraries.Utilities.waitAndClick;
import static libraries.Utilities.waitForPageLoaded;
import static libraries.Utilities.waitUntilElementIsVisible;

public class LatestJobTile extends BasePageClass {
    public LatestJobTile(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(css=".paragraph--type--para-latest-jobs")
    private WebElement latestJobTile;

    @FindBy(css=".paragraph--type--para-latest-jobs .content-tile-listing-title h2")
    private WebElement latestJobTileTitle;

    @FindBy(css=".paragraph--type--para-latest-jobs .content-tile-listing-title a")
    private WebElement latestJobTileLink;

    @FindBy(css=".view-jobs .view-header")
    private WebElement latestJobTileDisplayNumber;

    @FindBy(css=".view-jobs .view-content a")
    private List<WebElement> latestJobs;

    public boolean isLatestJobTilePresent()
    {
        Logg.logger.info("");
        try {
            waitUntilElementIsVisible(driver,latestJobTile);
            return true;
        } catch(Exception e)
        {
            return false;
        }
    }

    public String getLatestJobTileTitle()
    {
        Logg.logger.info("");
        try{
        return waitUntilElementIsVisible(driver,latestJobTileTitle).getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getLatestJobTileLinkText()
    {
        Logg.logger.info("");
        try {
        return waitUntilElementIsVisible(driver,latestJobTileLink).getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getLatestJobTileLinkUrl()
    {
        Logg.logger.info("");
        try{
        return waitUntilElementIsVisible(driver,latestJobTileLink).getAttribute("href");
        } catch(Exception e) {
            return null;
        }
    }
    public void clickLatestJobTileLinkUrl()
    {
        Logg.logger.info("");
        waitAndClick(driver,latestJobTileLink);
        waitForPageLoaded(driver);
    }
    public String getJobTitle(int index)
    {
        Logg.logger.info("index:"+index);
        try {
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .findElement(By.cssSelector("h3 span"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getLatestJobTileDisplayNumber()
    {
        Logg.logger.info("");
        try{
        return waitUntilElementIsVisible(driver,latestJobTileDisplayNumber)
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public int getLatestJobs()
    {
        Logg.logger.info("");
        try{
        return waitUntilElementIsVisible(driver,latestJobs)
                .size();
        } catch(Exception e) {
            return 0;
        }
    }

    public String getJobLink(int index)
    {
        Logg.logger.info("index:"+index);
        try{
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .getAttribute("href");
        } catch(Exception e) {
            return null;
        }
    }
    public void clickJobLink(int index)
    {
        Logg.logger.info("index:"+index);
        waitAndClick(driver,latestJobs.get(index));
        waitForPageLoaded(driver);
    }
    public String getJobType(int index)
    {
        Logg.logger.info("index:"+index);
        try {
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .findElement(By.cssSelector(".job-type .field--name-field-job-type"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getJobTypeLabel(int index)
    {
        Logg.logger.info("index:"+index);
        try{
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .findElement(By.cssSelector(".job-type>div"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getJobDuration(int index)
    {
        Logg.logger.info("index:"+index);
        try{
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .findElement(By.cssSelector(".job-type .field--name-field-job-duration"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getJobSalary(int index)
    {
        Logg.logger.info("index:"+index);
        try{
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .findElement(By.cssSelector(".field--name-field-job-salary .field__item"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getJobSalaryLabel(int index)
    {
        Logg.logger.info("index:"+index);
        try {
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .findElement(By.cssSelector(".field--name-field-job-salary .field__label"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getJobApplicationCloseDate(int index)
    {
        Logg.logger.info("index:"+index);
        try {
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .findElement(By.cssSelector(".field--name-field-date-end .field__item"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getJobApplicationCloseDateLabel(int index)
    {
        Logg.logger.info("index:"+index);
        try{
        return waitUntilElementIsVisible(driver,latestJobs)
                .get(index)
                .findElement(By.cssSelector(".field--name-field-date-end .field__label"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
    public String getViewJobApplicationLinkText(int index)
    {
        Logg.logger.info("index:"+index);
        try {
        return waitUntilElementIsVisible(driver, latestJobs)
                .get(index)
                .findElement(By.cssSelector(".field--name-field-link-reference span"))
                .getText();
        } catch(Exception e) {
            return null;
        }
    }
}
