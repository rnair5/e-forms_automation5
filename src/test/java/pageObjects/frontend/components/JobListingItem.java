package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.List;

import static libraries.Utilities.waitUntilElementIsVisible;

public class JobListingItem extends BasePageClass {
    public JobListingItem(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(css=".paragraph--type--para-views .view-jobs")
    private WebElement jobListing;

    @FindBy(css=".view-jobs article")
    private List<WebElement> jobs;

    @FindBy(css = ".view-jobs .view-empty")
    private WebElement noJobsResult;

    public boolean isJobListingDisplayed()
    {
        Logg.logger.info("");
        try{
            return waitUntilElementIsVisible(driver,jobListing).isDisplayed();
        } catch (Exception e)
        {
            return false;
        }
    }
    public int getJobs()
    {
        Logg.logger.info("");
        try {
            return waitUntilElementIsVisible(driver, jobs).size();
        } catch(Exception e) {
            return 0;
        }
    }
    public String getJobTitle(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector("h3 span"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobType(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".field--name-field-job-type .field__item"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobTypeLabel(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".field--name-field-job-type .field__label"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobDuration(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".field--name-field-job-duration .field__item"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobDurationLabel(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".field--name-field-job-duration .field__label"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobSalary(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".field--name-field-job-salary .field__item"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobSalaryLabel(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".field--name-field-job-salary .field__label"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobApplicationCloseDate(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".group-footer .field--name-field-date-end .field__item"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobApplicationCloseDateLabel(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".group-footer .field--name-field-date-end .field__label"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }

    public String getJobLinkUrl(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".group-footer .field--name-field-link-reference a"))
                    .getAttribute("href");
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobLinkText(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".group-footer .field--name-field-link-reference a"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }

    public void clickJobLinkUrl(int index)
    {
        Logg.logger.info("index:" + index);
        waitUntilElementIsVisible(driver, jobs)
                .get(index)
                .findElement(By.cssSelector(".group-footer .field--name-field-link-reference a"))
                .click();
    }
    public String getJobTeaser(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".field--name-field-short-desc"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getJobDescription(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            return waitUntilElementIsVisible(driver,jobs)
                    .get(index)
                    .findElement(By.cssSelector(".field--type-text-with-summary"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getNoResultText()
    {
        Logg.logger.info("");
        try{
            return waitUntilElementIsVisible(driver,noJobsResult).getText();
        } catch(Exception e)
        {
            return null;
        }
    }
}
