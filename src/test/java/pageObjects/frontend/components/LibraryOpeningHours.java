package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.ArrayList;
import java.util.List;

import static libraries.Utilities.waitAndClick;
import static libraries.Utilities.waitForPageLoaded;
import static libraries.Utilities.waitUntilElementIsVisible;

public class LibraryOpeningHours extends BasePageClass {
    public LibraryOpeningHours(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(className = "library-opening-hours")
    private WebElement libraryOpeningHoursWidget;

    @FindBy(css=".library-opening-hours-header h2")
    private WebElement heading;

    @FindBy(css=".library-opening-hours-header a")
    private  WebElement openingHoursLink;

    @FindBy(css=".library-opening-hours-table tr")
    private List<WebElement> libraries;

    public String getHeading()
    {
        Logg.logger.info("");
        try{
            return waitUntilElementIsVisible(driver,heading).getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public boolean isLibraryOpeningHoursWidgetDisplayed()
    {
        Logg.logger.info("");
        try{
            waitUntilElementIsVisible(driver,libraryOpeningHoursWidget);
            return true;
        } catch(Exception e) {
            return false;
        }
    }
    public void clickSeeAllOpeningHoursLink()
    {
        Logg.logger.info("");
        waitAndClick(driver,openingHoursLink);
        waitForPageLoaded(driver);
    }
    public String getLibraryName(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            return waitUntilElementIsVisible(driver,libraries)
                    .get(index)
                    .findElement(By.className("library-name"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getLibraryOpenHour(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            return waitUntilElementIsVisible(driver,libraries)
                    .get(index)
                    .findElement(By.className("library-open-hour"))
                    .getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public String getLibraryOpenHour(String libraryName)
    {
        Logg.logger.info("Library Name:"+libraryName);
        try{
            for(int i=0;i<libraries.size();i++)
            {
                if(libraryName.equalsIgnoreCase(getLibraryName(i))) {
                    return getLibraryOpenHour(i);
                }
            }
            return null;
        } catch(Exception e)
        {
            return null;
        }
    }
    public int getLibraries()
    {
        Logg.logger.info("");
        try {
            return waitUntilElementIsVisible(driver,libraries).size();
        } catch(Exception e)
        {
            return 0;
        }
    }
    public List<String> getLibraryNames()
    {
        Logg.logger.info("");
        List<String> libraryNames = new ArrayList<String>();
        for(int i=0;i<libraries.size();i++)
        {
            libraryNames.add(getLibraryName(i));
        }
        return  libraryNames;
    }
    public List<String> getLibraryOpenHours()
    {
        Logg.logger.info("");
        List<String> libraryHours = new ArrayList<String>();
        for(int i=0;i<libraries.size();i++)
        {
            libraryHours.add(getLibraryOpenHour(i));
        }
        return  libraryHours;
    }
}
