package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.List;

import static libraries.ExtentReporting.extent_Failure;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Pass_NoScreenshot;
import static libraries.Utilities.waitAndClick;
import static libraries.Utilities.waitForPageLoaded;
import static libraries.Utilities.waitUntilElementIsVisible;

public class ServiceTiles extends BasePageClass {
    public ServiceTiles(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(className = "paragraph--type--para-services-tile")
    private WebElement serviceTileSection;
    @FindBy(css = ".paragraph--type--para-services-tile div.field--name-field-para-section-heading")
    private WebElement serviceTileSectionHeading;
    @FindBy(css = ".paragraph--type--para-services-tile .arrow-link a")
    private WebElement serviceTileSectionLink;

    @FindBy(className = "services-tile-container")
    private List<WebElement> serviceTiles;

    public int getServices()
    {
        Logg.logger.info("");
        try{
            int result= waitUntilElementIsVisible(driver,serviceTiles).size();
            extent_Pass(driver,"Service Tile is present on the page","","",Integer.toString(result));
            return result;
        }
        catch(Exception e) {
            extent_Failure(driver,"Service tile is not present on the page","","","");
            return 0;
        }
    }
    public String getService_Title(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,serviceTiles).
                    get(index).
                    findElement(By.cssSelector(".content h2")).
                    getText();
            extent_Pass(driver,"Service tile at index has a title","Index:"+Integer.toString(index),"",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"Service title is not present at index", "Index:"+index,"","");
            return null;
        }
    }
    public String getService_Description(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,serviceTiles).
                    get(index).
                    findElement(By.cssSelector(".content .field--name-field-para-text-long")).
                    getText();
            extent_Pass_NoScreenshot(driver,"Service at index has a description","Index:"+Integer.toString(index),"",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"Service description is not present at index", "Index:"+index,"","");
            return null;
        }
    }
    public String getService_Link(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,serviceTiles).
                    get(index).
                    findElement(By.tagName("a")).
                    getAttribute("href");
            extent_Pass_NoScreenshot(driver,"Service at index has a link","Index:"+Integer.toString(index),"",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"Service link is not present at index", "Index:"+index,"","");
            return null;
        }
    }

    public String getService_ImageTitle(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,serviceTiles).
                    get(index).
                    findElement(By.tagName("img")).
                    getAttribute("title");
            extent_Pass_NoScreenshot(driver,"Service at index has an image","Index:"+Integer.toString(index),"",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"Service at index does not have an image", "Index:"+index,"","");
            return null;
        }
    }
    public boolean isServiceTileSectionDisplayed()
    {
        Logg.logger.info("");
        try{
            waitUntilElementIsVisible(driver,serviceTileSection);
            extent_Pass(driver,"Service tile is present on the page","","",serviceTileSection.toString());
            return true;
        } catch(Exception e) {
            extent_Failure(driver,"Service tile is not present on the page","","","");
            return false;
        }
    }
    public String getServiceTile_Heading()
    {
        Logg.logger.info("");
        try {
            String result=waitUntilElementIsVisible(driver,serviceTileSectionHeading)
                    .getText();
            extent_Pass(driver,"Service Tile has a heading","","",result);
            return result;
        } catch (Exception e)
        {
            extent_Failure(driver,"Service Tile does not have a heading","","","");
            return null;
        }
    }
    public String getServiceTile_LinkText()
    {
        Logg.logger.info("");
        try {
            String result=waitUntilElementIsVisible(driver,serviceTileSectionLink)
                    .getText();
            extent_Pass_NoScreenshot(driver,"Service Tile has a Link text","","",result);
            return result;
        } catch (Exception e)
        {
            extent_Failure(driver,"Service Tile does not have a Link text","","","");
            return null;
        }
    }
    public String getServiceTile_LinkUrl()
    {
        Logg.logger.info("");
        try {
            String result=waitUntilElementIsVisible(driver,serviceTileSectionLink)
                    .getAttribute("href");
            extent_Pass_NoScreenshot(driver,"Service Tile has a Link url","","",result);
            return result;
        } catch (Exception e)
        {
            extent_Failure(driver,"Service Tile does not have a Link url","","","");
            return null;
        }
    }
    public void clickService_Link(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            waitUntilElementIsVisible(driver,serviceTiles).
                    get(index).
                    findElement(By.tagName("a")).
                    click();
            waitForPageLoaded(driver);
            extent_Pass_NoScreenshot(driver,"Service at index has a link","Index:"+Integer.toString(index),"","");
        } catch(Exception e)
        {
            extent_Failure(driver,"Service link is not present at index", "Index:"+index,"","");
        }
    }
    public void clickServiceTile_LinkUrl()
    {
        Logg.logger.info("");
        try {
            waitAndClick(driver,serviceTileSectionLink);
            waitForPageLoaded(driver);
            extent_Pass_NoScreenshot(driver,"Service Tile has a Link url","","","");
        } catch (Exception e)
        {
            extent_Failure(driver,"Service Tile does not have a Link url","","","");
        }
    }
}
