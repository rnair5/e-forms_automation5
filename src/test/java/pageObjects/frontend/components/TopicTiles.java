package pageObjects.frontend.components;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.List;

import static libraries.ExtentReporting.*;
import static libraries.Utilities.*;

public class TopicTiles extends BasePageClass {
    public TopicTiles(WebDriver driver) {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(className = "topic-listing-container")
    private WebElement topicTileSection;

    @FindBy(css = ".topic-listing-header h2")
    private WebElement topicTitle;

    @FindBy(css=".topic-listing-header a")
    private  WebElement allTopicLink;

    @FindBy(css = ".topic-listing a")
    private List<WebElement> topics;

    public String getTopicTitle() {
        Logg.logger.info("");
        try {
             String result= waitUntilElementIsVisible(driver,topicTitle)
                    .getText();
             extent_Pass(driver,"The topic tile has a title","","",result);
             return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"The topic title is not present","","","");
            return null;
        }
    }

    public String getTopicLinkUrl() {
        Logg.logger.info("");
        try {
            String result= waitUntilElementIsVisible(driver,allTopicLink)
                    .getAttribute("href");
            extent_Pass_NoScreenshot(driver,"The topic tile has a view all URL","","",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"The topic tile doesn't have a view all URL","","","");
            return null;
        }
    }

    public String getTopicLinkText() {
        Logg.logger.info("");
        try {
            String result= waitUntilElementIsVisible(driver,allTopicLink)
                    .getText();
            extent_Pass_NoScreenshot(driver,"The topic tile has a view all URL linktext","","",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"The topic tile doesn't have a view all URL lintext","","","");
            return null;
        }
    }
    public void clickTopicLinkUrl() {
        Logg.logger.info("");
        waitAndClick(driver,allTopicLink);
        waitForPageLoaded(driver);
    }
    public int getTopics() {
        Logg.logger.info("");
        try{
            int result= waitUntilElementIsVisible(driver,topics).size();
            extent_Pass_NoScreenshot(driver,"Topic Tiles are present on the page","","",Integer.toString(result));
            return result;
        }
        catch(Exception e) {
            extent_Failure(driver,"Topic tiles are not present on the page","","","");
            return 0;
        }
    }
    public boolean isTopicTileSectionDisplayed()
    {
        Logg.logger.info("");
        try{
            waitUntilElementIsVisible(driver,topicTileSection);
            extent_Pass(driver,"Topic tile section is present on the page","","",topicTileSection.toString());
            return true;
        } catch(Exception e) {
            return false;
        }
    }
    public String getTopicText(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            String result= waitUntilElementIsVisible(driver,topics)
                    .get(index)
                    .getText();
            extent_Pass(driver,"The topic tile has a view all URL linktext","","",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"The topic tile doesn't have a view all URL lintext","","","");
            return null;
        }
    }
    public String getTopicLink(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            String result= waitUntilElementIsVisible(driver,topics)
                    .get(index)
                    .getAttribute("href");
            extent_Pass_NoScreenshot(driver,"The topic tile has a view all URL linktext","","",result);
            return result;
        } catch(Exception e)
        {
            extent_Failure(driver,"The topic tile doesn't have a view all URL lintext","","","");
            return null;
        }
    }
    public void clickTopicLink(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            waitUntilElementIsVisible(driver,topics)
                    .get(index)
                    .click();
            waitForPageLoaded(driver);
            extent_Pass(driver,"Navigated to the topic link","","","");
        } catch(Exception e)
        {
            extent_Failure(driver,"Unable to navigate to the topic link","","","");
        }
    }
}
