package pageObjects.frontend;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;
import static libraries.Utilities.*;

public class PageClass extends BasePageClass {
    public PageClass(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    @FindBy(css = ".content-pre-content-container h1")
    private WebElement title;
    @FindBy(xpath="//*[contains(@class,'field-rich-text')]/p")
    private WebElement richtextbox;
    @FindBy(className = "field--name-field-listing-description")
    private WebElement listingDescription;
    public String getTitle()
    {
        Logg.logger.info("");
        waitUntilElementIsVisible(driver,title);
        return title.getText();
    }
    public String getRichText()
    {
        Logg.logger.info("");
        waitUntilElementIsVisible(driver,richtextbox);
        return richtextbox.getText();
    }
    public String getListingDescription()
    {
        Logg.logger.info("");
        return waitUntilElementIsVisible(driver,listingDescription).getText();
    }
}
