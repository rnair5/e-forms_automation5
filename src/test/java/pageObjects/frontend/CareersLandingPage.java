package pageObjects.frontend;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;
import pageObjects.frontend.components.IconGrid;
import pageObjects.frontend.components.LatestJobTile;

import static libraries.Utilities.waitAndClick;

public class CareersLandingPage extends BasePageClass {
    public CareersLandingPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver, this);
        iconGrid = new IconGrid(driver);
        latestJobTile = new LatestJobTile(driver);
    }
    public IconGrid iconGrid;
    public LatestJobTile latestJobTile;
    @FindBy(css=".child-nav-wrapper a[href='/about-council/jobs-and-careers/view-current-job-vacancies']")
    private WebElement currentOpportunitiesLink;

    public void clickCurrentOpportunities()
    {
        Logg.logger.info("");
        waitAndClick(driver,currentOpportunitiesLink);
    }
}
