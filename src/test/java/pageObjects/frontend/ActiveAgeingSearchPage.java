package pageObjects.frontend;

import libraries.Logg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import java.util.ArrayList;
import java.util.List;

import static libraries.ExtentReporting.extent_Failure;
import static libraries.ExtentReporting.extent_Pass;
import static libraries.ExtentReporting.extent_Pass_NoScreenshot;
import static libraries.Utilities.*;

public class ActiveAgeingSearchPage extends BasePageClass {
    public ActiveAgeingSearchPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    @FindBy(css="p.search-input-summary span")
    private List<WebElement> searchQuery;

    @FindBy(id="edit-key")
    private WebElement searchKeyword;

    @FindBy(css="div.result-listing div.views-row article")
    private List<WebElement> searchResults;

    @FindBy(id="edit-submit-activity-listing")
    private WebElement searchButton;

    @FindBy(css="fieldset#edit-category--wrapper button")
    private WebElement categoryBox;

    @FindBy(css="fieldset#edit-field-suburb--wrapper button")
    private WebElement locationBox;

    @FindBy(css="fieldset#edit-days--wrapper button")
    private WebElement daysBox;

    @FindBy(id="edit-options-free")
    private WebElement freeCheckbox;

    @FindBy(id="edit-options-accessible")
    private WebElement accessibleCheckbox;

    @FindBy(id="edit-options-transport")
    private WebElement transportCheckbox;

    @FindBy(css="#edit-category label")
    private List<WebElement> categoryLabels;

    @FindBy(css="#edit-category input")
    private List<WebElement> categoryCheckboxes;

    @FindBy(css=".page-wrap #edit-field-suburb label")
    private List<WebElement> locationLabels;

    @FindBy(css=".page-wrap #edit-field-suburb input")
    private List<WebElement> locationCheckboxes;

    @FindBy(css=".page-wrap #edit-days label")
    private List<WebElement> dayLabels;

    @FindBy(css=".page-wrap #edit-days input")
    private List<WebElement> dayCheckboxes;

    @FindBy(css="a.csa-reset")
    private WebElement resetButton;

    @FindBy(css="p.result-number")
    private WebElement searchResultNumber;

    @FindBy(css="summary .details-title")
    private WebElement moreRefinements;

    @FindBy(css=".view-footer strong")
    private WebElement lessResultsText;

    @FindBy(css=".view-empty h3")
    private WebElement noResultText;

    @FindBy(css="article .heading .field--name-title")
    private List<WebElement> activityTitles;

    public ActiveAgeingSearchPage enterSearchKeyword(String keyword)
    {
        Logg.logger.info("KeyWord:"+keyword);
        waitAndSendKeys(driver,searchKeyword,keyword);
        return this;
    }
    public ActiveAgeingSearchPage selectCategory(String category)
    {
        Logg.logger.info("Category:"+category);
        boolean found = false;
        waitAndClick(driver,categoryBox);
        for(int i=0;i<waitUntilElementIsVisible(driver,categoryLabels).size();i++)
        {
            if(categoryLabels.get(i).getText().equalsIgnoreCase(category)) {
                found = true;
                waitAndClick(driver, categoryCheckboxes.get(i));
            }
        }
        if(found == false)
            extent_Failure(driver,"Unable to find the given category in the dropdown:",category,"","");
        return this;
    }
    public ActiveAgeingSearchPage selectLocation(String location)
    {
        Logg.logger.info("Location:"+location);
        waitAndClick(driver,locationBox);
        boolean found = false;
        int size=waitUntilElementIsVisible(driver,locationLabels).size();
        for(int i=0;i<size;i++)
        {
            if(locationLabels.get(i).getText().equalsIgnoreCase(location)) {
                found = true;
                waitAndClick(driver, locationCheckboxes.get(i));
            }
        }
        if(found == false)
            extent_Failure(driver,"Unable to find the given suburb in the dropdown:",location,"","");
        return this;
    }
    public List<String> getLocations()
    {
        Logg.logger.info("");
        waitAndClick(driver,locationBox);
        List<String> locations = new ArrayList<>();
        int size=locationLabels.size();
        for(int i=0;i<size;i++)
        {
            locations.add(locationLabels.get(i).getText());
        }
        waitAndClick(driver,locationBox);
        return locations;
    }
    public ActiveAgeingSearchPage selectDays(String day)
    {
        Logg.logger.info("Day:"+day);
        boolean found = false;
        waitAndClick(driver,daysBox);
        for(int i=0;i<waitUntilElementIsVisible(driver,dayLabels).size();i++)
        {
            if(dayLabels.get(i).getText().equalsIgnoreCase(day)) {
                found = true;
                waitAndClick(driver, dayCheckboxes.get(i));
            }
        }
        if(found == false)
            extent_Failure(driver,"Unable to find the given day in the dropdown:",day,"","");
        return this;

    }
    public ActiveAgeingSearchPage selectFree()
    {
        Logg.logger.info("");
        clickMoreRefinements();
        if (!waitUntilElementIsVisible(driver,freeCheckbox).isSelected())
            waitAndClick(driver,freeCheckbox);
        return this;
    }
    public ActiveAgeingSearchPage selectAccessible()
    {
        Logg.logger.info("");
        clickMoreRefinements();
        if(!waitUntilElementIsVisible(driver,accessibleCheckbox).
                isSelected())
            waitAndClick(driver,accessibleCheckbox);
        return this;
    }
    public ActiveAgeingSearchPage selectTransportAvailable()
    {
        Logg.logger.info("");
        clickMoreRefinements();
        if(!waitUntilElementIsVisible(driver,transportCheckbox).isSelected())
            waitAndClick(driver,transportCheckbox);
        return this;
    }
    public ActiveAgeingSearchPage clickApply()
    {
        Logg.logger.info("");
        waitAndClick(driver,searchButton);
        return this;
    }
    public ActiveAgeingSearchPage clickReset()
    {
        Logg.logger.info("");
        waitAndClick(driver,resetButton);
        return this;
    }
    public boolean isSearchQueryContain(String search)
    {
        Logg.logger.info("Search:"+search);
        for(int i=0;i<waitUntilElementIsVisible(driver,searchQuery).size();i++)
        {
            if(searchQuery.get(i).getText().equalsIgnoreCase("\""+search+"\""))
                return true;
        }
        return false;
    }
    public String getSearchQuery()
    {
        Logg.logger.info("");
        String searchQueryResult="";
        try {
            for (int i = 0; i < waitUntilElementIsVisible(driver, searchQuery).size(); i++)
                searchQueryResult = searchQueryResult + searchQuery.get(i).getText();
            extent_Pass(driver,"Search query is present","","",searchQueryResult);
            return searchQueryResult;
        } catch (Exception e)
        {
            extent_Failure(driver,"Search query not present","","","");
            return null;
        }
    }
    public int getSearchResultsOnPage()
    {
        Logg.logger.info("");
        try {
            return searchResults.size();
        }
        catch(Exception e)
        {
            extent_Failure(driver,"Cannot find the search results on the page","","","");
            return 0;
        }
    }
    public int getTotalSearchResults()
    {
        Logg.logger.info("");
        String[] results= waitUntilElementIsVisible(driver,searchResultNumber).
                getText().
                split(" ");
        if(results.length == 7)
            return Integer.parseInt(results[5]);
        else
            return 0;
    }
    public String getDisplayingText()
    {
        Logg.logger.info("");
        try{
            return waitUntilElementIsVisible(driver,searchResultNumber).getText();
        } catch(Exception e)
        {
            return null;
        }
    }
    public boolean isSearchResultDisplayed()
    {
        Logg.logger.info("");
        if(getSearchResultsOnPage()>0)
            return true;
        else
            return false;
    }
    public String getSearchResultTitle(int index)
    {
        Logg.logger.info("index:" + index);
        try {
            String result = waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElement(By.cssSelector(".heading .field--name-title")).
                    getText();
            scrollToElement(driver,searchResults.get(index));
            extent_Pass_NoScreenshot(driver, "Found the heading on the search result at index:", Integer.toString(index), "", result);
            return result;
        } catch (Exception e) {
            extent_Failure(driver, "Cannot find the heading on search result at index:", Integer.toString(index), "", "");
            return null;
        }
    }
    public boolean isSearchResultImagePresent(int index)
    {
        Logg.logger.info("index:"+index);
        try {

            waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElement(By.className("taxonomy-term"));
            extent_Pass_NoScreenshot(driver,"Found the category image on the search result at index:",Integer.toString(index),"","");
            return true;
        } catch (Exception e) {
            extent_Failure(driver, "Cannot find the category image on search result at index:", Integer.toString(index), "", "");
            return false;
        }
    }

    public String getSearchResultDescription(int index)
    {
        Logg.logger.info("index:"+index);
        try{
            String result = waitUntilElementIsVisible(driver,searchResults).
                get(index).
                findElement(By.cssSelector(".field--name-field-activity-description , .field--name-field-short-desc")).
                getText();
            extent_Pass_NoScreenshot(driver,"Found the description on the search result at index:",Integer.toString(index),"",result);
            return result;
        }
        catch (Exception e)
        {
            extent_Failure(driver,"Cannot find the description on search result at index:",Integer.toString(index),"","");
            return null;
        }
    }

    public String getSearchResultCtaLink(int index) {
        Logg.logger.info("index:" + index);
        try {
            String result = waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElement(By.cssSelector("a.cta")).
                    getAttribute("href");
            extent_Pass_NoScreenshot(driver, "Found the CTA link on the search result at index:", Integer.toString(index), "", result);
            return result;
        } catch (Exception e) {
            extent_Failure(driver, "Cannot find the CTA link on search result at index:", Integer.toString(index), "", "");
            return null;
        }
    }
    public String getGoogleMapsLink(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            String result =  waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElement(By.cssSelector(".address +a")).
                    getAttribute("href");
            extent_Pass_NoScreenshot(driver,"Found the google maps link on the search result at index:",Integer.toString(index),"",result);
            return result;
        }
        catch (Exception e)
        {
            extent_Failure(driver,"Cannot find the google maps link on search result at index:",Integer.toString(index),"","");
            return null;
        }
    }
    public String getAddress(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            List<WebElement> addressElements = waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElements(By.cssSelector(".address span"));
            String address = "";
            for (int i = 0; i < addressElements.size(); i++) {
                address = address + " " + addressElements.get(i).getText();
            }
            address = (address.equalsIgnoreCase("")) ? null : address;
            extent_Pass_NoScreenshot(driver, "Address is present in the search result at index:", Integer.toString(index), "address:", address);
            return address;
        }
        catch(Exception e)
        {
            extent_Failure(driver,"Address element is not found in the search result at index:",Integer.toString(index),"","");
            return null;
        }
    }
    public boolean isSearchResultAccessible(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElement(By.cssSelector(".field--name-field-activity-options.tags a.accessible"));
            extent_Pass_NoScreenshot(driver,"The search result at index is accessible",Integer.toString(index),"","");
            return true;
        }
        catch(Exception e) {
            extent_Pass_NoScreenshot(driver,"The search result at index is not accessible",Integer.toString(index),"","");
            return false;
        }
    }
    public boolean isSearchResultTransportAvailable(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElement(By.cssSelector(".field--name-field-activity-options.tags a.transport-available"));
            extent_Pass_NoScreenshot(driver,"The search result at index has transportation available",Integer.toString(index),"","");
            return true;
        }
        catch(Exception e) {
            extent_Pass_NoScreenshot(driver,"The search result at index doesn't have transportation facility",Integer.toString(index),"","");
            return false;
        }
    }
    public String getContactNumber(int index)
    {
        Logg.logger.info("index:"+index);
        try {
            String result = waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElement(By.cssSelector(".footer .contact a")).
                    getText();
            extent_Pass_NoScreenshot(driver,"The search result at index has contact number",Integer.toString(index),"",result);
            return result;
        }
        catch(Exception e) {
            extent_Failure(driver,"The search result at index doesn't have contact number",Integer.toString(index),"","");
            return null;
        }
    }
    public List<String> getDays(int index)
    {
        try {
            List<WebElement> dayElements = waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElements(By.cssSelector(".footer .field--name-field-activity-days a"));
            List<String> days = new ArrayList<String>();
            for (int i = 0; i < dayElements.size(); i++) {
                days.add(dayElements.get(i).getText());
            }
            extent_Pass_NoScreenshot(driver, "Days are present in the search result at index:", Integer.toString(index), "address:", days.toString());
            return days;
        }
        catch(Exception e)
        {
            extent_Failure(driver,"Days are not present is not found in the search result at index:",Integer.toString(index),"","");
            return null;
        }
    }
    public void clickSearchResultLink(int index) {
        Logg.logger.info("index:" + index);
        try {
            waitUntilElementIsVisible(driver, searchResults).
                    get(index).
                    findElement(By.cssSelector("a.cta")).
                    click();
            waitForPageLoaded(driver);
            extent_Pass_NoScreenshot(driver, "Clicked the CTA link on the search result at index and moved to page", Integer.toString(index), "", getCurrentUrl(driver));
        } catch (Exception e) {
            extent_Failure(driver, "Cannot find the CTA link on search result at index:", Integer.toString(index), "", "");
        }
    }
    public ActiveAgeingSearchPage clickMoreRefinements()
    {
        Logg.logger.info("");
        if(waitUntilElementIsVisible(driver,moreRefinements).
                findElement(By.tagName("span")).
                getText().equalsIgnoreCase("Show"))
            waitAndClick(driver,moreRefinements);
        return this;
    }
    public String getNoResultText()
    {
        Logg.logger.info("");
        return waitUntilElementIsVisible(driver,noResultText)
                .getText();
    }
    public String getLessResultText()
    {
        Logg.logger.info("");
        return waitUntilElementIsVisible(driver,lessResultsText)
                .getText();
    }
    public boolean isTitleSorted()
    {
        Logg.logger.info("");
        String last = activityTitles.get(0).getText();
        for (int i = 1; i < activityTitles.size(); i++) {
            String current = activityTitles.get(i).getText();
            if(last.compareToIgnoreCase(current) > 0)
                return false;
            last=current;
        }
        return true;
    }
}
