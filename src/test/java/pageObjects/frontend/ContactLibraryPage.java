package pageObjects.frontend;

import libraries.Logg;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pageObjects.BasePageClass;

import static libraries.Utilities.waitUntilElementIsVisible;

public class ContactLibraryPage extends BasePageClass {
    public ContactLibraryPage(WebDriver driver)
    {
        super(driver);
        Logg.logger.info("");
        PageFactory.initElements(driver,this);
    }
    public String getOpenHour(String library, String day)
    {
        Logg.logger.info(String.format("Library:%s , Day:%s",library,day));
        String openingHoursTableXpath="//h3[contains(text(),'"+library+"')]//following-sibling::table//td[text()='"+day+"']//following-sibling::td";
        return waitUntilElementIsVisible(driver,openingHoursTableXpath).getText();
    }

}
