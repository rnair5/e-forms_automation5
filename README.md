This is a framework for the Boroondara eForms Regression. 

Prerequisites:
1.	Download and install Java jdk  from https://www.oracle.com/technetwork/java/index.html if you don’t have it
2.	Download Git from https://git-scm.com/downloads and install with default settings
3.	Download Maven from https://maven.apache.org/download.cgi and unzip it in any folder (preferably within C:\Program Files). Installation instruction also here
4.	Make sure JAVA_HOME variable is created and updated to the jdk path. Add the maven bin folder path to the PATH variable.


