@echo off

set SELENIUM_TESTS_HOME="C:\workspace\Auto_Forms"
set BACKUP_FOLDER="C:\Backup\"

echo Killing Chrome Processes if exist...
tasklist /fi "imagename eq chrome.exe" |find ":" > nul
if errorlevel 1 taskkill /F /IM chrome.exe /T

tasklist /fi "imagename eq chromedriver.exe" |find ":" > nul
if errorlevel 1 taskkill /F /IM chromedriver.exe /T
mv %SELENIUM_TESTS_HOME% %SELENIUM_TESTS_HOME%
echo Starting Chrome Tests...

cd %SELENIUM_TESTS_HOME%
mvn clean compile test



